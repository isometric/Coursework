options(stringsAsFactors = FALSE)
set.seed(5472)

election_data <- read.csv("~/Desktop/STAT 241/CandidatesVotes.csv")

votes_for_elected <- election_data$Number.of.votes.for.candidate.elected
votes_per_riding <- election_data$Total.number.of.votes

vote_share_for_elected <- votes_for_elected/votes_per_riding


# hist(votes_for_elected, plot=TRUE, horizontal = TRUE)

# boxplot(vote_share_for_elected, plot=TRUE, horizontal = TRUE)
# hist(vote_share_for_elected)


election_data["Percentage.of.votes.first.place"] <- vote_share_for_elected
parties <- unique(election_data$Political.affiliation)
riding_share_by_party <- vector(length=length(parties))
for (i in 1:length(parties)) {
  riding_share_by_party[i] = subset(election_data, Political.affiliation == parties[i], Percentage.of.votes.first.place)
}

# boxplot(riding_share_by_party)

# boxplot(election_data$Percentage.of.votes.between.first.and.second.place~election_data$Political.affiliation)

outlier <- 0
rand_samp <- array()
rand_samp_mean <- array()
rand_samp_sd <- array()
rand_samp_left <- array()
rand_samp_right <- array()
for (i in 1:1000) {
  rand_samp <- sample(vote_share_for_elected, 100)
  rand_samp_mean[i] <- mean(rand_samp)
  rand_samp_sd <- sd(rand_samp)
  rand_samp_err <- qnorm(0.95)*rand_samp_sd/sqrt(100)

  rand_samp_left <- rand_samp_mean[i] - rand_samp_err
  rand_samp_right <- rand_samp_mean[i] + rand_samp_err

  if (rand_samp_mean[i] <= 0.4684789 || rand_samp_mean[i] >= 0.5000669){
    outlier <- outlier+1
  }
}


hist(rand_samp_mean, plot=TRUE)