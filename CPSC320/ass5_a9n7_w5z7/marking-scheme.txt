Total before late penalty: [21.5/22]


  Question 1: [1/2]
  ----------

      The answer  log n (without  a base) is worth  1/2. For full  marks they
      need the base (1/p) for the log.



  Question 2: [1.5/2]
  ----------

      One mark for answering No. One mark for stating the probability.



  Question 3: [3/3]
  ----------

      Deduct 1 mark  if the answer includes pointers that  may be considered,
      but not followed (for instance the pointer from "45" to the end node at
      level 2). Deduct 2 marks for a more serious mistake.




  Question 4: [15/15]
  ----------

      SkipListNode [2/2]
          One mark for adding a "count" array variable to the SkipListNode class.
          One mark for having appropriate accessors/mutators for it.

      Insertions and Deletions [8/8]
          2 marks for computing and saving nodes' ranks during a search operation.
          3 marks for updating the "count" array correctly during insertions.
          3 marks for updating the "count" array correctly during deletions.

      Implementation of Select(i) [3/3]
          1.5 marks for mimicking Search()
          1.5 marks for using ranks instead of values to determine where the item is.

      Testing and Documentation[2/2]



  Question 5: [1/1]
  ----------


