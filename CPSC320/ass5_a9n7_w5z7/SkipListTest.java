import java.util.Random;

/*
 **
 ** Main program: perform operations on a skip list, and count comp
 **
 */
public class SkipListTest
{
    private final static int NumberOfOperations = 10;
    private final static String usage = "***Usage: java SkipListTest <noElements>";

    /*
     ** Main program.
     */
    public static void main(String[] args)
    {
        Random rng = new Random();

        /*
         ** Check the arguments.
         */
        if (args.length != 1)
        {
            System.out.println(usage);
            return;
        }

        int noElements;

        try
        {
            noElements = Integer.parseInt(args[0]);
        }
        catch (java.lang.NumberFormatException e)
        {
            System.out.println(usage);
            return;
        }

        if (noElements <= 0)
        {
            System.out.println(usage);
            return;
        }

        /*
         ** Declarations.
         */
        boolean[]  inOut = new boolean[noElements * 2];
        SkipList<Integer> skiplist = new SkipList<Integer>();
        SkipList<Integer> skiplist2 = new SkipList<Integer>();
        SkipList<Integer> skiplist3 = new SkipList<Integer>();

        /*
         ** Create an array with the proper size and initialize it and the skip list.
         */
        for (int i = 0; i < noElements; i++)
        {
            inOut[2*i] = false;
            inOut[2*i+1] = true;

            skiplist.insertKey(new  Integer(2*i+1));
            skiplist2.insertKey(new  Integer(2*i+1));
            skiplist3.insertKey(new  Integer(2*i+1));
        }

        /*
         ** Now do a large number of random operations.
         */
        for (int i = 0; i < NumberOfOperations; i++)
        {

            /*
             ** Pick a random element.
             */
            int idx = Math.abs(rng.nextInt()) % (2 * noElements);
            Integer myInt = new Integer(idx);
            
            /**
             * Random element 2
             */
            int idf = Math.abs(rng.nextInt()) % (2 * noElements);
            Integer myInt2 = new Integer(idf);
            
            /**
             * Random element 3
             */
            int idg = Math.abs(rng.nextInt()) % (2 * noElements);
            Integer myInt3 = new Integer(idg);

            /*
             ** Insert it or delete if as appropriate.
             */
            if (inOut[idx] || inOut[idf] || inOut[idg])
            {
                skiplist.deleteKey(myInt);
                skiplist.deleteKey(myInt2);
                skiplist.deleteKey(myInt3);
                skiplist2.deleteKey(myInt);
                skiplist2.deleteKey(myInt2);
                skiplist2.deleteKey(myInt3);
            }
            else
            {
                skiplist.insertKey(myInt);
                skiplist.insertKey(myInt2);
                skiplist.insertKey(myInt3);
                skiplist2.insertKey(myInt);
                skiplist3.insertKey(myInt2);
                skiplist2.insertKey(myInt3);
                skiplist3.insertKey(myInt);
                skiplist2.insertKey(myInt2);
                skiplist3.insertKey(myInt3);
                
            }

        }

        /*
         ** Print the final skip list.
         */
        System.out.println(skiplist);
        System.out.println(skiplist2);
        System.out.println(skiplist3);
        System.out.println(skiplist.select(2));
        System.out.println(skiplist2.select(2));
        System.out.println(skiplist3.select(2));
        System.out.println(skiplist.forwardPtrDist(1));
        System.out.println(skiplist2.forwardPtrDist(1));
        System.out.println(skiplist3.forwardPtrDist(1));
    }
}
