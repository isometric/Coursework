import java.util.Random;

/*
 **
 ** Skip list: a skip-list node with methods to
 **     (1) generate a  random level,
 **     (2) search for  a value in the list,
 **     (3) insert a value in the list, and
 **     (4) delete a value from the list.
 **
 */

public class SkipList<EType extends Comparable<EType>> extends SkipListNode<EType>
{
	/*
	 ** Constants.
	 */
	private static final int MAX_LEVEL = 32;

	/*
	 ** Instance variables.
	 */
	private int maxLevel_;
	private int noElements_;
	private final Random rng_;
	private final SkipListNode<EType>[] searchTrail_;
	private int[] searchTrailDistance_;

	/*
	 ** Constructor.
	 */
	@SuppressWarnings("unchecked")
	public SkipList()
	{
		super(MAX_LEVEL);

		maxLevel_ = 1;
		noElements_ = 0;
		rng_ = new Random();
		searchTrail_ = SkipListNode.createArray(MAX_LEVEL, this);
		searchTrailDistance_ = new int[MAX_LEVEL];

		for (int i = 1; i <= MAX_LEVEL; i++)
		{
			setForwardPtr(i, null, 0);
		}
	}

	/**
	 * @return a string representation of the skip list.
	 */
	@Override
	public String toString()
	{
		StringBuffer sbuffer = new StringBuffer();

		SkipListNode<EType> node = this.forwardPtr(1);

		drawArrow(sbuffer, 1, maxLevel_, maxLevel_, '|', '|');
		drawArrow(sbuffer, 1, maxLevel_, maxLevel_, '|', '|');

		int nextLevels = (node == null) ? maxLevel_ : node.levels();
		drawArrow(sbuffer, 1, maxLevel_, nextLevels, '|', 'V');

		while (node != null)
		{
			sbuffer.append(node.toString(maxLevel_));
			node = node.forwardPtr(1);

			nextLevels = (node == null) ? maxLevel_ : node.levels();
			drawArrow(sbuffer, 1, maxLevel_, nextLevels, '|', 'V');
		}

		return sbuffer.toString();
	}

	/**
	 * Generate a random level according to the desired probability distribution.
	 *
	 * @return the random level.
	 */
	private int randomLevel()
	{
		int level = 1;

		while (rng_.nextFloat() < 0.5 && level < MAX_LEVEL) level++;
		return level;
	}

	/**
	 * Search for a key in the skip list, keeping track of the nodes where we went "down"
	 * at each level instead of moving forward. These nodes contain the pointers that may
	 * need to be updated when insertions and deletions are performed.
	 *
	 * @param key the key to search for
	 */
	private void buildSearchTrail(EType key) {
		SkipListNode<EType> current = this;
		SkipListNode<EType> next;
		int elapsedDistance = 0;
		
		for (int level = maxLevel_; level > 0; level--) {
			while (true) {
				next = current.forwardPtr(level);
				if (next != null && key.compareTo(next.getData()) > 0) {
					current = next;
					elapsedDistance += current.forwardPtrDist(level);
				} else {
					break;
				}
			}

			// searchTrail is always saturated, repeats occur when dropping more than 1 level
			searchTrail_[level-1] = current;
			searchTrailDistance_[level-1] = elapsedDistance;
		}
	}

	/**
	 * Determine whether or not the key was found by the searchTrail() method.
	 *
	 * @param key the key we were searching for
	 * @return the node that contains this key, or null if the key is not in the skip list.
	 */
	private SkipListNode<EType> found(EType key)
	{
		buildSearchTrail(key);
		SkipListNode<EType> candidateNode = searchTrail_[0].forwardPtr(1);
		return (candidateNode != null && key.equals(candidateNode.getData())) ? candidateNode : null;
	}

	/**
	 *  Search for a key in a skip list.
	 *
	 * @param key the key we were searching for
	 * @return the key we were searching for, or null if it is not in the skip list.
	 */
	public EType searchKey(EType key)
	{
		SkipListNode<EType> candidateNode = found(key);
		return (candidateNode != null) ? candidateNode.getData() : null;
	}

	public EType select(int index) {
		if (index >= noElements_) return null;
		
		SkipListNode<EType> current = this;
		int currentIndex = 0;
		for (int level = maxLevel_; level > 0; level--) {
			while (current.forwardPtrDist(level) != 0 && currentIndex + current.forwardPtrDist(level) <= index ) {
			current = current.forwardPtr(level);	
			}
		}
		return current.getData();
	}
	
	
	/**
	 * Delete a key from the skip list, if it is present.
	 *
	 * @param key the key to delete from the skip list.
	 */
	public void deleteKey(EType key)
	{
		SkipListNode<EType> nodeToDelete = found(key);

		if (nodeToDelete != null)
		{
			for (int level = maxLevel_; level >= 1; level--)
			{	
				if (level> nodeToDelete.levels()){
					//  case above newNode
					if (searchTrail_[level-1].forwardPtrDist(level) != 0) {
						// ptr valid = overshot -> minus 1 from dist
						searchTrail_[level-1].setForwardPtr(level, 
								searchTrail_[level-1].forwardPtr(level), 
								searchTrail_[level-1].forwardPtrDist(level) - 1);
					} // ptr invalid = still invalid -> do nothing 
				} else {
					// case not above newNode
					if (nodeToDelete.forwardPtrDist(level) != 0) {
						// ptr valid = normal case -> bypass 
						int distBypass = searchTrail_[level-1].forwardPtrDist(level) + nodeToDelete.forwardPtrDist(level) - 1;
						searchTrail_[level-1].setForwardPtr(level, nodeToDelete.forwardPtr(level), distBypass);
					} else {
						// ptr invalid = degenerate case -> make new pointer invalid also
						searchTrail_[level-1].setForwardPtr(level, null, 0);
						if (searchTrail_[level-1] == this)
						{
							maxLevel_--;
						}
					}
				}
			}
			noElements_--;
		}
	}

	/**
	 * Insert a new key in the skip list, if it is not already there.
	 *
	 * @param key the key to insert in the skip list.
	 */
	public void insertKey(EType key)
	{
		if (found(key) == null)
		{

			/*
			 ** Make a new list element.
			 */
			noElements_++;
			SkipListNode<EType> newNode = new SkipListNode<EType>(randomLevel(), key);

			/*
			 ** Fixup the lists at all levels.
			 */
			
			// for when newNode level > current maxLevel
			for (int level = newNode.levels(); level > maxLevel_; level--)
			{
				setForwardPtr(level, newNode, searchTrailDistance_[0] + 1);
				newNode.setForwardPtr(level, null, 0);
			}

			// for all other levels
			for (int level = maxLevel_; level >= 1; level--)
			{	
				if (level> newNode.levels()){
					//  case above newNode
					if (searchTrail_[level-1].forwardPtrDist(level) != 0) {
						// ptr valid = overshot -> add 1 to dist
						searchTrail_[level-1].setForwardPtr(level, 
								searchTrail_[level-1].forwardPtr(level), 
								searchTrail_[level-1].forwardPtrDist(level) + 1);
					} // ptr invalid = still invalid -> do nothing 
				} else {
					// case not above newNode
					if (searchTrail_[level-1].forwardPtrDist(level) != 0) {
						// ptr valid = normal case -> treat like staircase
						int distTrailToNew = 1 + searchTrailDistance_[0] - searchTrailDistance_[level-1];
						int distNewToNext = 1 + searchTrail_[level-1].forwardPtrDist(level) - distTrailToNew;
						newNode.setForwardPtr(level, searchTrail_[level-1].forwardPtr(level), distNewToNext);
						searchTrail_[level-1].setForwardPtr(level, newNode, distTrailToNew);
					} else {
						// ptr invalid = make it valid -> points to new Node
						int distTrailToNew = searchTrailDistance_[level-1]-searchTrailDistance_[0];
						searchTrail_[level-1].setForwardPtr(level, 
								newNode, 
								distTrailToNew);
						newNode.setForwardPtr(level, null, 0);
					}
				}
			}
			maxLevel_ = Math.max(maxLevel_, newNode.levels());
		}
	}
}
