#lang plai

;;;; TODO: PROBLEM #1 ;;;;
;;
;; Complete the function.  This should be a simple one!

;; compute-greater-power : positive positive -> positive
;; Computes the greater of x^y and y^x (i.e., x raised to
;; the power of y and y raised to the power of x).
;;
;; Hint: look up the identifier "expt" in the helpdesk.

(define (compute-greater-power x y)
  (let ([xpwry (expt x y)]
        [ypwrx (expt y x)])
    (if (> xpwry ypwrx) xpwry ypwrx)))
; Trivial casec
(test (compute-greater-power 1 1) 1)

; Cases where order matters.
(test (compute-greater-power 1 2) 2)
(test (compute-greater-power 2 1) 2)

; Cases where order and operation both matter.
(test (compute-greater-power 2 3) 9)
(test (compute-greater-power 3 2) 9)

; An extra case just to feel confident.
; (Note that in this case the smaller of x and y
; raised to the larger "wins".)
(test (compute-greater-power 4 5) 1024)



;;;; TODO: PROBLEM #2 ;;;;
;;
;; Complete the function.
;; You'll certainly want to look up string functions to make this work.
;; You could also convert the strings to lists and work with them in that form!

;; match-length : string string -> natural

;; Compute the number of letters exactly matching at the start of the two strings
;; (before there's a difference).
;;
;; E.g., (match-length "Tsawwassen" "Tsilhqot'in") evaluates to 2.

(define (match-length string1 string2)
   (second (foldl (lambda (char1 char2 acc)
                    (if (and (first acc) (equal? char1 char2))
                        (list true  (+ (second acc) 1))
                        (list false (second acc))))
             (list true 0)
             (take (string->list string1) (min (string-length string1) (string-length string2)))
             (take (string->list string2) (min (string-length string1) (string-length string2))))))


; Trivial caseta
(test (match-length "" "") 0)
(test (match-length "" "arbutus") 0)
(test (match-length "arbutus" "") 0)

; Simple one-character match/non-match.
(test (match-length "a" "a") 1)
(test (match-length "a" "b") 0)

; Matching prefixes.
(test (match-length "Tsawwassen" "Tsilhqot'in") 2)

; One is a prefix of the other.
(test (match-length "false" "false creek") 5)
(test (match-length "false creek" "false") 5)

; Exactly matching longer strings.
(test (match-length "Wittgenstein" "Wittgenstein") 12)

; Case matters
(test (match-length "a" "A") 0)



;;;; TODO: PROBLEM #3 ;;;;
;;
;; Complete the function.  You'll want to remind yourself of
;; some list functions for this one!


;; interleave : (listof any?) (listof any?) -> (listof any?)
;; Interleave the elements of list1 and list2.
;;
;; So, give back a list with the first element of list1, then the
;; first of list2, then the second of list1, then the second of list2,
;; and so on.
;;
;; If one list ends before the other, include all leftover elements of
;; the other list in a row.
(define (interleave list1 list2)
    (cond [(empty? list1) list2]
          [(empty? list2) list1]
          [else (append (list (first list1))
                        (list (first list2))
                        (interleave (rest list1) (rest list2)))]))

(test (interleave empty empty) empty)
(test (interleave '(1 2 3) empty) '(1 2 3))
(test (interleave empty '(1 2 3)) '(1 2 3))
(test (interleave '(1) '(2)) '(1 2))
(test (interleave '(2) '(1)) '(2 1))
(test (interleave '(1 3) '(2)) '(1 2 3))
(test (interleave '(1 3 5 7) '(2 4 6 8 9)) '(1 2 3 4 5 6 7 8 9))
(test (interleave '(1 3 5 7 8 9) '(2 4 6)) '(1 2 3 4 5 6 7 8 9))

; Not just numbers!
(test (interleave '(z y x w) '("a" 1 "and a" 2 "and a" 3))
      '(z "a" y 1 x "and a" w 2 "and a" 3))


;;;; TODO: PROBLEM #4 ;;;;
;;
;; Complete the function.  More list practice!

;; contains-sequence : (list-of symbol?) (list-of symbol?) -> boolean?
;; Determine whether data contains the elements from sequence in order (but not necessarily right next to each other).
(define (contains-sequence data sequence)
  (cond [(empty? sequence) true]
        [(empty? data) false]
        [(equal? (first data) (first sequence)) (contains-sequence (rest data) (rest sequence))]
        [else (contains-sequence (rest data) sequence)]))

;; True tests, trivial though complex
;; plus repeated symbols.
(test (contains-sequence empty empty) true)
(test (contains-sequence '(a) empty) true)
(test (contains-sequence '(a b) '(a)) true)
(test (contains-sequence '(a b) '(b)) true)
(test (contains-sequence '(a b c) '(a c)) true)
(test (contains-sequence '(a b c) '(a b)) true)
(test (contains-sequence '(a b c) '(a b c)) true)
(test (contains-sequence '(a b a) '(a a)) true)

;; False test, trivial though complex.
(test (contains-sequence empty '(a)) false)
(test (contains-sequence '(a) '(b)) false)
(test (contains-sequence '(a b) '(b a)) false)
(test (contains-sequence '(a b) '(a a)) false)


;;;; TODO: PROBLEM #5 ;;;;
;;
;; Write thorough tests for the truth-or-lie? function declared below.
;; Do NOT use check-expect; use plai's test construct instead.

(define-type Bool-expr
  [simple-expr (value boolean?)]
  [conj-expr (lhs Bool-expr?) (rhs Bool-expr?)]   ; conjunction (AND)
  [disj-expr (lhs Bool-expr?) (rhs Bool-expr?)])  ; disjunction (OR)

;; truth-or-lie? : Bool-expr -> boolean?
;; Determines whether the boolean expression represented is true or false.
;;
;; Note: type-case is REALLY handy for data defined with define-type.
;; However, you can also access fields with functions like conj-expr-lhs
;; which, given a conj-expr, returns its lhs field.

(define (truth-or-lie? exp)
  (type-case Bool-expr exp
    [simple-expr (v) v]
    [conj-expr (lhs rhs) (and (truth-or-lie? lhs)
                              (truth-or-lie? rhs))]
    [disj-expr (lhs rhs) (or (truth-or-lie? lhs)
                             (truth-or-lie? rhs))]))

;; Write your tests here:

(test (truth-or-lie? (simple-expr true)) true)
(test (truth-or-lie? (simple-expr false)) false)


(test (truth-or-lie? (conj-expr (simple-expr false) (simple-expr false))) false)
(test (truth-or-lie? (conj-expr (simple-expr false) (simple-expr true ))) false)
(test (truth-or-lie? (conj-expr (simple-expr true ) (simple-expr false))) false)
(test (truth-or-lie? (conj-expr (simple-expr true ) (simple-expr true ))) true )

(test (truth-or-lie? (disj-expr (simple-expr false) (simple-expr false))) false)
(test (truth-or-lie? (disj-expr (simple-expr false) (simple-expr true ))) true )
(test (truth-or-lie? (disj-expr (simple-expr true ) (simple-expr false))) true )
(test (truth-or-lie? (disj-expr (simple-expr true ) (simple-expr true ))) true )


(test (truth-or-lie? (disj-expr (simple-expr false) (conj-expr (simple-expr false) (simple-expr false)))) false)
(test (truth-or-lie? (disj-expr (simple-expr false) (conj-expr (simple-expr true ) (simple-expr true )))) true )
(test (truth-or-lie? (disj-expr (simple-expr true ) (conj-expr (simple-expr false) (simple-expr false)))) true )
(test (truth-or-lie? (disj-expr (simple-expr true ) (conj-expr (simple-expr true ) (simple-expr false)))) true )

(test (truth-or-lie? (conj-expr (simple-expr false) (disj-expr (simple-expr false) (simple-expr false)))) false)
(test (truth-or-lie? (conj-expr (simple-expr false) (disj-expr (simple-expr true ) (simple-expr true )))) false)
(test (truth-or-lie? (conj-expr (simple-expr true ) (disj-expr (simple-expr false) (simple-expr false)))) false)
(test (truth-or-lie? (conj-expr (simple-expr true ) (disj-expr (simple-expr true ) (simple-expr true )))) true )


(test (truth-or-lie? (conj-expr (simple-expr false) (disj-expr (conj-expr (simple-expr true) (simple-expr true))  (simple-expr true) ))) false)
(test (truth-or-lie? (disj-expr (simple-expr false) (disj-expr (conj-expr (simple-expr true) (simple-expr true))  (simple-expr true) ))) true )
(test (truth-or-lie? (disj-expr (simple-expr false) (disj-expr (disj-expr (simple-expr true) (simple-expr true))  (simple-expr true) ))) true )
(test (truth-or-lie? (disj-expr (simple-expr true)  (disj-expr (disj-expr (simple-expr true) (simple-expr true))  (simple-expr true) ))) true )


;;;; TODO: PROBLEM #6 ;;;;
;;
;; Time for define-type.  This is a big one.
;; find-species works well with a HtDP-like "template" approach.
;; is-extinct? should be easy using find-species.
;; common-ancestor is fairly tricky, but let the test cases guide you!
;;
;; Complete find-species, is-extinct?, and common-ancestor.


;; The "tree of life" is the biological tree of species charting
;; the evolution of one species from another.
(define-type Tree-of-life
  [empty-tree]
  [species (name string?) (extinct? boolean?) (child1 Tree-of-life?) (child2 Tree-of-life?)])

(define human-species-ToL (species "human" false (empty-tree) (empty-tree)))
(define troll-species-ToL (species "troll" true (empty-tree) (empty-tree)))
(define three-species-ToL (species "missing-link" true human-species-ToL troll-species-ToL))

;; find-species : string Tree-of-life -> (or false Tree-of-life)
;; Produces the Tree-of-life node representing the named species if it exists.
;; Otherwise, produces false.
;;
;; Precondition: the species with the given name appears AT MOST once.
(define (find-species target tree)
  (type-case Tree-of-life tree
    [empty-tree () false]
    [species (name extinct? lchild rchild) 
             (if (equal? name target) tree
                 (or (find-species target lchild) (find-species target rchild)))]))

(test (find-species "elsa" (empty-tree)) false)
(test (find-species "elsa" human-species-ToL) false)
(test (find-species "elsa" three-species-ToL) false)
(test (find-species "human" human-species-ToL) human-species-ToL)
(test (find-species "human" three-species-ToL) human-species-ToL)
(test (find-species "troll" three-species-ToL) troll-species-ToL)



;; is-extinct? : string Tree-of-life -> boolean
;; Determines whether the given species is recorded as extinct in the given tree.
;;
;; Precondition: the species with the given name appears exactly once in the tree.
;; Hint: use find-species!
(define (is-extinct? name tree)
   (type-case Tree-of-life (find-species name tree)
    [empty-tree () false]
    [species (name extinct? lchild rchild) extinct?]))

(test (is-extinct? "troll" three-species-ToL) true)
(test (is-extinct? "human" three-species-ToL) false)

;; common-ancestor : string string Tree-of-life -> (or false Tree-of-life)
;; Returns the node of the closest common ancestor OR false if one or both species does not exist in the tree.
;; DOES NOT NEED TO BE EFFICIENT.
;;
;; Precondition: each named species appears AT MOST once (i.e., zero or one time).
(define (common-ancestor name1 name2 tree)
  (type-case Tree-of-life tree
    [empty-tree () false]
    [species (name extinct? lchild rchild) 
             (if (and (find-species name1 tree) (find-species name2 tree))
                 (if (or (common-ancestor name1 name2 lchild) (common-ancestor name1 name2 rchild))
                     (or (common-ancestor name1 name2 lchild) (common-ancestor name1 name2 rchild))
                     tree)
                 false)]))


; Neither appears
(test (common-ancestor "elsa" "anna" (empty-tree)) false)
(test (common-ancestor "elsa" "anna" human-species-ToL) false)
(test (common-ancestor "elsa" "anna" three-species-ToL) false)

; Only one appears (both orders)
(test (common-ancestor "elsa" "human" human-species-ToL) false)
(test (common-ancestor "human" "elsa" human-species-ToL) false)
(test (common-ancestor "elsa" "human" three-species-ToL) false)
(test (common-ancestor "human" "elsa" three-species-ToL) false)

; One is THIS node, other in subtree.
(test (common-ancestor "missing-link" "human" three-species-ToL) three-species-ToL)
(test (common-ancestor "human" "missing-link" three-species-ToL) three-species-ToL)
(test (common-ancestor "missing-link" "troll" three-species-ToL) three-species-ToL)
(test (common-ancestor "troll" "missing-link" three-species-ToL) three-species-ToL)

; Both appear in different subtrees.
(test (common-ancestor "troll" "human" three-species-ToL) three-species-ToL)
(test (common-ancestor "human" "troll" three-species-ToL) three-species-ToL)


; Both appear in the same subtree.
(define subtree-appearances-right (species "goo" false
                                           (species "foo" true (empty-tree) (empty-tree))
                                           three-species-ToL))
(define subtree-appearances-left (species "goo" false
                                          three-species-ToL
                                          (species "foo" true (empty-tree) (empty-tree))))

(test (common-ancestor "troll" "human" subtree-appearances-right) three-species-ToL)
(test (common-ancestor "human" "troll" subtree-appearances-right) three-species-ToL)
(test (common-ancestor "troll" "human" subtree-appearances-left) three-species-ToL)
(test (common-ancestor "human" "troll" subtree-appearances-left) three-species-ToL)
(test (common-ancestor "missing-link" "human" subtree-appearances-right) three-species-ToL)
(test (common-ancestor "human" "missing-link" subtree-appearances-right) three-species-ToL)
(test (common-ancestor "missing-link" "human" subtree-appearances-left) three-species-ToL)
(test (common-ancestor "human" "missing-link" subtree-appearances-left) three-species-ToL)

; Both are ONE node.
(test (common-ancestor "troll" "troll" troll-species-ToL) troll-species-ToL)
(test (common-ancestor "troll" "troll" subtree-appearances-left) troll-species-ToL)


;;;; TODO: PROBLEM #7 ;;;;
;;
;; Here's a small EBNF specification for a language.  Finish the
;; abstract syntax (a define-type named OE for "order expression") for
;; the language.
;;
;; <expr> ::= (group <expr> <expr>)
;;          | (sequentially <expr> <expr>)
;;          | (together <expr> <expr>)
;;          | (join <expr> <expr>)
;;          | (arrive <expr>)
;;          | (give <expr> name <id> in <expr>)
;;          | <string>
;;          | <id>
;;
;; Notes: Use "name" as the name of the (give <expr> name <id> in
;; <expr>) variant.  Assume that a <string> is just a Racket string,
;; and an <id> is just a Racket symbol.  We suggest you call the
;; <string> variant string-literal and the <id> variant id-ref.

(define-type OE
  [group (expr1 OE?) (expr2 OE?)]
  ;; add more cases here
  )