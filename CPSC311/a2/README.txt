README.txt file to be submitted with CPSC 311 assignments.

Please fill out each TODO item in the header but change nothing else,
particularly nothing before the colon ":" on each line!

=================== HEADER =======================

Student #1, Name: TODO
Student #1, ugrad.cs.ubc.ca login: TODO
Student #1, ID: TODO

Student #2, Name: TODO (or write "NONE")
Student #2, ugrad.cs.ubc.ca login: TODO (or write "NONE")
Student #2, ID: TODO (or write "NONE")

Project: TODO (fill in assignment/project name, e.g., A2: Evaluation)

Acknowledgment that you understand and have followed the course's
collaboration policy (READ IT at
http://www.ugrad.cs.ubc.ca/~cs311/current/_syllabus.php#Collaboration_Policy):

Signed: TODO (put your names here again as a signature)

=================== BODY =======================

Plese fill in each of the following:

Approximate hours on the project: TODO (when you get there)

Acknowledgment of assistance (per the collab policy!): TODO

For teams, rough breakdown of work: TODO (no more than a paragraph needed!)




What's your favorite programming language keyword (reserved word, that
is)?  TODO fill one in here.




TODO: Document any additional issues about your submission you feel
might be important.


