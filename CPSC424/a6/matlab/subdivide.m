%
% CPSC 424 Assignment 6 
%
% subdivide
%
% This function subdivides the given control polygon k times according to 
% the requested subdivision scheme.
%
% If desired, each point is moved to the limit curve using eigen-analysis.
%

function [x, y] = subdivide(x, y, k, scheme, move_to_limit)

% x and y are the x- and y- coordinates of the control points.
% k is the number of subdivision iterations to perform.
% scheme specifies the scheme to use:
%   if scheme == 1, use the 1/8 1/8 3/4 (Cubic) subdivision scheme
%   if scheme == 2, use the 1/5 1/5 3/5 subdivision scheme
% move_to_limit is a boolean flag indicating whether all points should be
% moved onto the limit curve after subdivision is performed.

% YOUR IMPLEMENTATION GOES HERE

newx = [];
newy = [];
centW = 0;
sideW = 0;
if move_to_limit == 0;

else
    if scheme == 1;
        centW = 3/4;
        sideW = 1/8;
    else 
        centW = 3/5;
        sideW = 1/5;
    end  
    for asdlflasdf = 1:k;
        prev = cat(2, length(x):length(x), 1:length(x)-1);
        curr = 1:length(x);
        next = cat(2, 2:length(x), 1:1);

        for i = 1:length(x);
            newx(end+1) = (x(prev(i)) + x(curr(i)))/2;
            newy(end+1) = (y(prev(i)) + y(curr(i)))/2;

            newx(end+1) = x(curr(i))*centW + sideW * (x(prev(i)) + x(next(i)));
            newy(end+1) = y(curr(i))*centW + sideW * (y(prev(i)) + y(next(i)));

        end
        x = newx;
        y = newy;
    end
end

