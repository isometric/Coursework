%
% CPSC 424 Assignment 6
%
% Script that tests the subdivide function.
%
% Your task for this question is to implement subdivide().
%

x1 = [0 1 3 3];
y1 = [0 2 3 1];

% No eigen-analysis
figure(1);
% Cubic
subplot(4,2,1);
plot(x1,y1,'.k');
patch(x1,y1,'g');
title('Cubic');
[x,y] = subdivide(x1, y1, 1, 1, 0);
subplot(4,2,3);
plot(x,y,'.k');
patch(x,y,'g');
title('k = 1');
[x,y] = subdivide(x1, y1, 5, 1, 0);
subplot(4,2,5);
plot(x,y,'.k');
patch(x,y,'g');
title('k = 5');
[x,y] = subdivide(x1, y1, 10, 1, 0);
subplot(4,2,7);
plot(x,y,'.k');
patch(x,y,'g');
title('k = 10');
% 1/5 1/5 3/5
subplot(4,2,2);
plot(x1,y1,'.k');
patch(x1,y1,'g');
title('1/5 1/5 3/5');
[x,y] = subdivide(x1, y1, 1, 2, 0);
subplot(4,2,4);
plot(x,y,'.k');
patch(x,y,'g');
[x,y] = subdivide(x1, y1, 5, 2, 0);
subplot(4,2,6);
plot(x,y,'.k');
patch(x,y,'g');
[x,y] = subdivide(x1, y1, 10, 2, 0);
subplot(4,2,8);
plot(x,y,'.k');
patch(x,y,'g');

% With eigen-analysis
figure(2);
% Cubic
subplot(4,2,1);
plot(x1,y1,'.k');
patch(x1,y1,'g');
title('Cubic');
[x,y] = subdivide(x1, y1, 0, 1, 1);
subplot(4,2,3);
plot(x,y,'.k');
patch(x,y,'g');
title('k = 0');
[x,y] = subdivide(x1, y1, 1, 1, 1);
subplot(4,2,5);
plot(x,y,'.k');
patch(x,y,'g');
title('k = 1');
[x,y] = subdivide(x1, y1, 5, 1, 1);
subplot(4,2,7);
plot(x,y,'.k');
patch(x,y,'g');
title('k = 5');
% 1/5 1/5 3/5
subplot(4,2,2);
plot(x1,y1,'.k');
patch(x1,y1,'g');
title('1/5 1/5 3/5');
[x,y] = subdivide(x1, y1, 0, 2, 1);
subplot(4,2,4);
plot(x,y,'.k');
patch(x,y,'g');
[x,y] = subdivide(x1, y1, 1, 2, 1);
subplot(4,2,6);
plot(x,y,'.k');
patch(x,y,'g');
[x,y] = subdivide(x1, y1, 5, 2, 1);
subplot(4,2,8);
plot(x,y,'.k');
patch(x,y,'g');



x2 = [0 1 3 2 4 3 2];
y2 = [0 3 2 4 3 0 1];

% No eigen-analysis
figure(3);
% Cubic
subplot(4,2,1);
plot(x2,y2,'.k');
patch(x2,y2,'g');
title('Cubic');
[x,y] = subdivide(x2, y2, 1, 1, 0);
subplot(4,2,3);
plot(x,y,'.k');
patch(x,y,'g');
title('k = 1');
[x,y] = subdivide(x2, y2, 5, 1, 0);
subplot(4,2,5);
plot(x,y,'.k');
patch(x,y,'g');
title('k = 5');
[x,y] = subdivide(x2, y2, 10, 1, 0);
subplot(4,2,7);
plot(x,y,'.k');
patch(x,y,'g');
title('k = 10');
% 1/5 1/5 3/5
subplot(4,2,2);
plot(x2,y2,'.k');
patch(x2,y2,'g');
title('1/5 1/5 3/5');
[x,y] = subdivide(x2, y2, 1, 2, 0);
subplot(4,2,4);
plot(x,y,'.k');
patch(x,y,'g');
[x,y] = subdivide(x2, y2, 5, 2, 0);
subplot(4,2,6);
plot(x,y,'.k');
patch(x,y,'g');
[x,y] = subdivide(x2, y2, 10, 2, 0);
subplot(4,2,8);
plot(x,y,'.k');
patch(x,y,'g');

% With eigen-analysis
figure(4);
% Cubic
subplot(4,2,1);
plot(x2,y2,'.k');
patch(x2,y2,'g');
title('Cubic');
[x,y] = subdivide(x2, y2, 0, 1, 1);
subplot(4,2,3);
plot(x,y,'.k');
patch(x,y,'g');
title('k = 0');
[x,y] = subdivide(x2, y2, 1, 1, 1);
subplot(4,2,5);
plot(x,y,'.k');
patch(x,y,'g');
title('k = 1');
[x,y] = subdivide(x2, y2, 5, 1, 1);
subplot(4,2,7);
plot(x,y,'.k');
patch(x,y,'g');
title('k = 5');
% 1/5 1/5 3/5
subplot(4,2,2);
plot(x2,y2,'.k');
patch(x2,y2,'g');
title('1/5 1/5 3/5');
[x,y] = subdivide(x2, y2, 0, 2, 1);
subplot(4,2,4);
plot(x,y,'.k');
patch(x,y,'g');
[x,y] = subdivide(x2, y2, 1, 2, 1);
subplot(4,2,6);
plot(x,y,'.k');
patch(x,y,'g');
[x,y] = subdivide(x2, y2, 5, 2, 1);
subplot(4,2,8);
plot(x,y,'.k');
patch(x,y,'g');

