#include "subdivision.h"
#include <cstdlib>
#include "trimesh.h"
#include <stdio.h>
#include <vector>



Subdivision::Subdivision()
{
    // Default Constructor

    // YOUR CODE HERE
}


Subdivision::~Subdivision()
{
    // Destructor

    // YOUR CODE HERE
}


void Subdivision::initialize(TriMesh* controlMesh)
{
    // Initializes this subdivision object with a mesh to use as
    // the control mesh (ie: subdivision level 0).

    subdivision_meshes.push_back(*controlMesh);
}


TriMesh* Subdivision::subdivide(int level_requested)
{
    // Subdivides the control mesh to the given subdivision level.
    // Returns a pointer to the subdivided mesh.

    // HINT: Create a new subdivision mesh for each subdivision level and
    // store it in memory for later.
    // If the calling code asks for a level that has already been computed,
    // just return the pre-computed mesh!


    TriMesh next_level;
    if (max_level_computed < level_requested) {
        while (max_level_computed < level_requested) {
            next_level = subdivision_meshes[max_level_computed];

            std::vector<TriMesh::HalfEdge*> half_edges =  next_level.getEdges();

            for (std::vector<TriMesh::HalfEdge*>::iterator half_edge = half_edges.begin(); half_edge != half_edges.end(); ++half_edge)
            {
                if ((*half_edge)->modified) continue;

                Vector new_vertex_pos = ((*half_edge)->origin()->pos() + (*half_edge)->next()->origin()->pos())/2;
                TriMesh::Vertex * new_vertex = next_level.addVertex(new_vertex_pos);
                (*new_vertex)->edge() =


                (*half_edge)->twin()

                (*half_edge)->twin()->modified = true;
                (*half_edge)->modified = true;
            }

            // std::vector<TriMesh::Face*> faces =  next_level.getFaces();
            // for (std::vector<TriMesh::Face*>::iterator face = faces.begin(); face != faces.end(); ++face)
            // {
            //     // add 3 faces

            // }

            // std::vector<TriMesh::Face*> faces =  next_level.getFaces();
            // for (std::vector<TriMesh::Face*>::iterator face = faces.begin(); face != faces.end(); ++face)
            // {
            //     // compute positions
            // }



            half_edges =  next_level.getEdges();
            for (std::vector<TriMesh::HalfEdge*>::iterator half_edge = half_edges.begin(); half_edge != half_edges.end(); ++half_edge)
            {
                half_edge->dirty = false;
            }



            subdivision_meshes.push_back(next_level);
            next_level.clear();
            max_level_computed++;
        }
    }

    return &subdivision_meshes[level_requested];

    // YOUR CODE HERE
}



//
// Private Functions
//


// YOUR CODE HERE

