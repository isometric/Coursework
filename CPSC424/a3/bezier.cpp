#include "bezier.h"

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
// #include <GL/glut.h>

#include <cmath>
#include <cassert>
#include <stdio.h>
#include <iostream>

Vector2D BezierCurve::evaluate(float t) const
{
    assert(t >= 0.0f && t <= 1.0f);
    assert(controlPoints.size() > 1);

    int n = controlPoints.size()-1;
    Vector2D point = Vector2D(0.f, 0.f);

    for (int i = 0; i <= n; i++) {
        point += binomialCoefficient(n, i) * pow((1-t), (n-i)) * pow(t, i) * controlPoints[i];
    }
    // Evaluate the Bezier curve at the given t parameter.
    // You may find the following functions useful:
    //  - BezierCurve::binomialCoefficient(m,i) computes "m choose i",
    //      aka: (m over i)
    //  - std::pow(t,i) computes t raised to the power i

    //@@@@@
    // YOUR CODE HERE
    //@@@@@

    return point; // REPLACE THIS
}


void BezierCurve::subdivide(BezierCurve& curve1, BezierCurve& curve2) const
{
    // Subdivide this Bezier curve into two curves.
    // Return the two smaller curves in curve1 and curve2.

    //@@@@@
    // YOUR CODE HERE
    //@@@@@

    std::vector<Vector2D> curves[2];
    curves[0].push_back(controlPoints.front());
    curves[1].push_back(controlPoints.back());

    std::vector<Vector2D> midpoints[2];
    midpoints[0] = controlPoints;

    for (int i = 0; i < (static_cast<int>(controlPoints.size()) - 1); i++) {
        midpoints[(i+1)%2].clear();
        for (int j = 0; j < (int(midpoints[i%2].size())-1); j++) {
            Vector2D midpoint = 0.5 * midpoints[i%2][j] + 0.5 * midpoints[i%2][j+1];

            midpoints[(i+1)%2].push_back(midpoint);
            if (j == 0)
                curves[0].push_back(midpoint);
            if (j == (int(midpoints[i%2].size()) - 2))
                curves[1].push_back(midpoint);
        }
    }

    curve1 = BezierCurve(curves[0]);
    curve2 = BezierCurve(curves[1]);
}


void BezierCurve::draw() const
{
    // Draw this Bezier curve.
    // Do this by evaluating the curve at some finite number of t-values,
    // and drawing line segments between those points.
    // You may use the BezierCurve::drawLine() function to do the actual
    // drawing of line segments.


    float stepsize = 1.f/(16.f*controlPoints.size());
    Vector2D last = controlPoints[0];
    for (float t = stepsize; t <= 1.f + stepsize; t += stepsize)
    {
        Vector2D next = evaluate(std::min(1.f, t));
        drawLine(last, next);
        last = next;
    }
    //@@@@@
    // YOUR CODE HERE
    //@@@@@

}


void BezierCurve::drawControlPolygon() const
{
    for (size_t i = 1; i < controlPoints.size(); ++i)
    {
        drawLine(controlPoints[i-1], controlPoints[i]);
    }
}


unsigned long BezierCurve::binomialCoefficient(int n, int k)
{
    // Compute nCk ("n choose k")
    // WARNING: Vulnerable to overflow when n is very large!

    assert(k >= 0);
    assert(n >= k);

    unsigned long result = 1;
    for (int i = 1; i <= k; ++i)
    {
        result *= n-(k-i);
        result /= i;
    }
    return result;
}


void BezierCurve::drawLine(const Vector2D& p1, const Vector2D& p2)
{
    glBegin(GL_LINES);
    glVertex2f(p1[0], p1[1]);
    glVertex2f(p2[0], p2[1]);
    glEnd();
}


void CurveManager::drawCurves() const
{
    if (points == NULL || points->size() < 2)
    {
        return;
    }

    if (curveMode == BASIC_MODE)
    {
        // Basic Mode
        //
        // Create a Bezier curve from the entire set of points,
        // and then simply draw it to the screen.

        BezierCurve curve(*points);
        curve.draw();

    }
    else if (curveMode == SUBDIVISION_MODE)
    {
        // Subdivision mode
        //
        // Create a Bezier curve from the entire set of points,
        // then subdivide it the number of times indicated by the
        // subdivisionLevel variable.
        // The control polygons of the subdivided curves will converge
        // to the actual bezier curve, so we only need to draw their
        // control polygons.



        BezierCurve curve(*points);

        std::vector<BezierCurve> curves[2];
        curves[0].push_back(curve);

        //std::cout << subdivisionLevel << std::endl;
        int i;
        for (i = 0; i < subdivisionLevel; i++) {
            curves[(i+1)%2].clear();

            std::vector<BezierCurve>::iterator it = curves[i%2].begin();
            for (; it != curves[i%2].end(); ++it) {
                BezierCurve left;
                BezierCurve right;
                (*it).subdivide(left, right);
                curves[(i+1)%2].push_back(left);
                curves[(i+1)%2].push_back(right);
            }
        }

        std::cout << "subdivisionLevel " << subdivisionLevel << std::endl;
        std::vector<BezierCurve>::iterator it = curves[subdivisionLevel%2].begin();
        for (; it != curves[subdivisionLevel%2].end(); ++it) {
            it->drawControlPolygon();
        }

        // curve.drawControlPolygon();
        // BezierCurve curve1;
        // BezierCurve curve2;

        // curve.subdivide(curve1, curve2);

        // curve1.draw();
        // curve2.draw();


        //@@@@@
        // YOUR CODE HERE
        //@@@@@

    }
    else if (curveMode == PIECEWISE_MODE)
    {
        // Piecewise mode
        //
        // Create multiple Bezier curves out of the set of poitns,
        // each of degree equal to the piecewiseDegree variable.
        // (The last curve may have degree less than piecewiseDegree.)


        if (continuityMode == C0_MODE)
        {
            // C0 continuity
            //
            // Each piecewise curve should be C0 continuous with adjacent
            // curves, meaning they should share an endpoint.

            //@@@@@
            // YOUR CODE HERE
            //@@@@@

            std::vector<BezierCurve> curves;
            std::vector<Vector2D> ctrl_points = *points;
            std::vector<std::vector<Vector2D> > data_for_curves;

            std::vector<Vector2D> v;
            data_for_curves.push_back(v);

            int points_left = ctrl_points.size();
            while(points_left) {
                if (points_left <= piecewiseDegree || int(data_for_curves.back().size()) < (piecewiseDegree)) {

                    data_for_curves.back().push_back(ctrl_points.back());
                //} else if (int(data_for_curves.back().size()) < (piecewiseDegree + 1)) {

                } else {
                    data_for_curves.back().push_back(ctrl_points.back());
                    std::vector<Vector2D> v;
                    v.push_back(ctrl_points.back());
                    data_for_curves.push_back(v);
                }
                ctrl_points.pop_back();
                points_left = int(ctrl_points.size());
            }

            std::vector<std::vector<Vector2D> >::iterator it = data_for_curves.begin();
            for (; it != data_for_curves.end(); ++ it) {
                curves.push_back(BezierCurve(*it));
            }

            std::vector<BezierCurve>::iterator curve = curves.begin();
            for (; curve != curves.end(); ++curve) {
                curve->draw();
            }

        }
        else if (continuityMode == C1_MODE)
        {
            // C1 continuity
            //
            // Each piecewise curve should be C1 continuous with adjacent
            // curves.  This means that not only must they share an endpoint,
            // they must also have the same tangent at that endpoint.
            // You will likely need to add additional control points to your
            // Bezier curves in order to enforce the C1 property.
            // These additional control points do not need to show up onscreen.

            //@@@@@
            // YOUR CODE HERE
            //@@@@@
            std::vector<BezierCurve> curves;
            std::vector<Vector2D> ctrl_points = *points;
            std::vector<std::vector<Vector2D> > data_for_curves;

            std::vector<Vector2D> v;
            data_for_curves.push_back(v);

            int points_left = ctrl_points.size();
            while(points_left) {
                if (points_left <= piecewiseDegree || int(data_for_curves.back().size()) < (piecewiseDegree)) {

                    data_for_curves.back().push_back(ctrl_points.back());
                    ctrl_points.pop_back();

                } else {

                    Vector2D prev = data_for_curves.back().back();
                    Vector2D curr = ctrl_points.back();
                    ctrl_points.pop_back();
                    Vector2D next = ctrl_points.back();

                    Vector2D dist = (prev - next) / 4;

                    data_for_curves.back().push_back(curr + dist);
                    data_for_curves.back().push_back(curr);

                    std::vector<Vector2D> v;
                    v.push_back(curr);
                    v.push_back(curr - dist);

                    data_for_curves.push_back(v);
                }
                points_left = int(ctrl_points.size());
            }

            std::vector<std::vector<Vector2D> >::iterator it = data_for_curves.begin();
            for (; it != data_for_curves.end(); ++ it) {
                curves.push_back(BezierCurve(*it));
            }

            std::vector<BezierCurve>::iterator curve = curves.begin();
            for (; curve != curves.end(); ++curve) {
                curve->draw();
                curve->drawControlPolygon();
            }

        }
    }
}


