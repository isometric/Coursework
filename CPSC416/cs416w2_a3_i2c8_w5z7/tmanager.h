#ifndef TMANAGER_H
#define TMANAGER_H 1

#include "common.h"



int main(int argc, char const *argv[]);



void handle_worker_msg(msgType * msg);
void handle_timeouts();
void check_voters();




void broadcast(int index, msgType * message);

/* extern hacks */
extern unsigned int     cmd_port;
extern int              cmd_port_fd;
extern struct sockaddr_in cmd_inaddr;

extern ObjectData           objData;
extern LogHeader            logHeader;

extern msgType              recieved_msg;

/* state flags */
int crashing;


#endif /* TWORKER_H */
