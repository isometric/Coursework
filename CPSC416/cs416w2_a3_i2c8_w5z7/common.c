#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <ctype.h>
#include <errno.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <poll.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include "common.h"
#include <stdio.h>


void recover_and_initialize(int isManager) {
    int err = 0;
    char file_name[128];
    struct stat     fstatus;


    /* init file descriptors */
    snprintf(file_name, sizeof(file_name), "WorkerData_%u.data", cmd_port);
    dataObjectFD = open(file_name, O_RDWR | O_CREAT | O_SYNC, S_IRUSR | S_IWUSR );
    if (dataObjectFD < 0 ) {
        char msg[256];
        snprintf(msg, sizeof(msg), "Opening %s failed", file_name);
        perror(msg);
        err++;
    }

    snprintf(file_name, sizeof(file_name),"ShiViz_%u.dat", cmd_port);
    vectorLogFD = open(file_name, O_WRONLY | O_CREAT | O_APPEND | O_SYNC, S_IRUSR | S_IWUSR );
    if (vectorLogFD < 0 ) {
        char msg[256];
        snprintf(msg, sizeof(msg), "Opening %s failed", file_name);
        perror(msg);
        err++;
    }

    snprintf(file_name, sizeof(file_name), "WorkerLog_%u.log", cmd_port);
    logfileFD = open(file_name, O_RDWR | O_CREAT | O_SYNC, S_IRUSR | S_IWUSR );
    if (logfileFD < 0 ) {
        char msg[256];
        snprintf(msg, sizeof(msg), "Opening %s failed", file_name);
        perror(msg);
        err++;
    }



    /* init mmap'ed structures */
    // TODO: check for mmap errs ptr returns 0xffffffff errval 22
    if(fstat(dataObjectFD, &fstatus) < 0 ) {
        perror("Filestat failed");
        err ++;
    }
    if (fstatus.st_size < sizeof(ObjectData)) {
            ObjectData  template;
            memset(&template, 0, sizeof(template));

            if (write(dataObjectFD, &template, sizeof(template)) != sizeof(template)) {
               printf("Object Data writing error\n");
               err++;
            }
    }
    mmap(&objData, sizeof(objData), PROT_WRITE|PROT_READ, MAP_SHARED, dataObjectFD, 0);


    if(fstat(logfileFD, &fstatus) < 0 ) {
        perror("Filestat failed");
        err ++;
    }
    if (fstatus.st_size < sizeof(LogHeader)) {
            LogHeader  template;
            memset(&template, 0, sizeof(template));
            template.vectorClockMem[0].nodeId = cmd_port;
            template.vectorClockMem[0].time = 1;

            for (int i = 0; i < MAX_NODES; i++) template.active_transactions[i].status = -2;

            if  (write(logfileFD, &template, sizeof(template)) != sizeof(template)) {
                printf("Object Data writing error\n");
                err++;
            }
    }
    mmap(&logHeader, sizeof(LogHeader), PROT_WRITE|PROT_READ, MAP_SHARED, logfileFD, 0);


    lseek(logfileFD, 0, SEEK_END);



    /* open/initialize ports */
    cmd_port_fd = open_socket(cmd_port, &cmd_inaddr);

    if (logHeader.port) {
        mgr_port_fd = open_socket(mgr_port, &mgr_inaddr)
;        mgr_port = logHeader.port;
        memcpy(mgr_hostName, logHeader.hostName, sizeof(mgr_hostName));
    } else {
        mgr_port_fd = open_socket(0, &mgr_inaddr);
        mgr_port = ntohs(mgr_inaddr.sin_port);
        logHeader.port = mgr_port;
        msync(&logHeader, sizeof(LogHeader), MS_SYNC);
    }

    /* recovery process */
    // use isManager here if needed
    for (int i = 0; i < MAX_NODES; i++ ) {
        if (logHeader.active_transactions[i].status <= -2)
            continue;
        else if (logHeader.active_transactions[i].status <= 0)
            abort_transaction(i);
        else if (logHeader.active_transactions[i].status >= 2)
            commit_transaction(i);
            // do nothing if status == 1
    }


    if (err) { // wtf is with that printf?!
        printf("%d initialization error%sencountered, program exiting.\n", err, err>1? "s were ": " was ");
        exit(1);
    }

    printf("Starting up transaction worker on %u\n", cmd_port);
    printf("Port number:                      %u\n", cmd_port);
    printf("Log file name:                    %s\n", file_name);



    /* shiviz logging */
    char start_log[128];
    sprintf(start_log, "Starting up Node %u", cmd_port);
    logHeader.vectorClockMem[0].nodeId = cmd_port;
    vectorClock_increment_local();
    
    shivizLog_append(vectorLogFD, start_log, logHeader.vectorClockMem);
}




void abort_transaction(int index) {
    TransactionData * curr = &logHeader.active_transactions[index];

    memcpy(&objData, &curr->previous_values, sizeof(objData));
    msync(&objData, sizeof(ObjectData), MS_SYNC);

    curr->status = -1;
    gettimeofday(&curr->modified, NULL);

    if (write(logfileFD, curr, sizeof(TransactionData)) != sizeof(TransactionData)) {
        printf("Object Data writing error\n");
        exit(1);
    }

    memset(&curr->modified, 0, sizeof(curr->modified));
    memset(curr, 0, sizeof(TransactionData));
    curr->status = -2;

    msync(&logHeader, sizeof(logHeader), MS_SYNC);
}

void commit_transaction(int index) {
    TransactionData * curr = &logHeader.active_transactions[index];

    curr->status = -2;
    gettimeofday(&curr->modified, NULL);

    if (write(logfileFD, curr, sizeof(TransactionData)) != sizeof(TransactionData)) {
        printf("Object Data writing error\n");
        exit(1);
    }

    memset(curr, 0, sizeof(TransactionData));
    msync(&logHeader, sizeof(logHeader), MS_SYNC);
}


int begin_transaction(uint32_t tid) {
        for (int i = 0; i < MAX_NODES; i++)
            if (logHeader.active_transactions[i].tid == tid)
                    return -1;

        int optimalIndex = -1;
        unsigned long optimalIndexScore = 18446744073709551615UL; // max UL
        for (int i = 0; i < MAX_NODES; i++) {
            if (logHeader.active_transactions[i].status <= -2) {
                if (logHeader.active_transactions[i].modified.tv_sec < optimalIndexScore) {
                    optimalIndexScore = logHeader.active_transactions[i].modified.tv_sec;
                    optimalIndex = i; // pick oldest
                }
            }
        }
        if (optimalIndex == -1) {
            puts("no slot available?!");
            exit(1);
        }

        TransactionData * curr = &logHeader.active_transactions[optimalIndex];

        curr->tid = tid;
        gettimeofday(&curr->created, NULL);
        memcpy(&curr->previous_values, &objData, sizeof(objData));

        msync(&logHeader, sizeof(logHeader), MS_SYNC);

        return optimalIndex;
}

int get_active_trasaction_index(uint32_t tid) {
    for (int i = 0; i < MAX_NODES; i++) {
        if (logHeader.active_transactions[i].tid == tid && logHeader.active_transactions[i].status >= 0) {
            return i;
        }
    }
    return -1;
}



void send_message(int sock_fd, char* dest_addr, int dest_port, void * payload, int payload_size) {

    struct sockaddr_in client_addr;

    client_addr.sin_family = AF_INET;
    client_addr.sin_addr.s_addr = inet_addr(dest_addr);
    client_addr.sin_port = htons(dest_port);

    if (dest_port == mgr_port && strcmp(dest_addr, mgr_hostName) == 0) {
        if (send_delay && send_delay != -1000)
            sleep(abs(send_delay));
        if (send_delay < 0) {
            exit(0);
        }
    }

    vectorClock_increment_local(logfileFD);

    if (sendto(sock_fd, payload, payload_size, 0, (struct sockaddr *)&client_addr, sizeof(client_addr)) < 0) {
        perror("Send");
        exit(1);
    }
}



int open_socket(unsigned int port, struct sockaddr_in * curr_addr) {
    int sock_fd;

    if ((sock_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
        perror("Socket");
        exit(1);
    }

    curr_addr->sin_family = AF_INET;
    curr_addr->sin_port = htons(port);
    curr_addr->sin_addr.s_addr = INADDR_ANY;
    bzero(&(curr_addr->sin_zero),8);


    if (bind(sock_fd, (struct sockaddr *)curr_addr, sizeof(struct sockaddr_in)) == -1)
    {
        perror("Bind");
        exit(1);
    }
    return sock_fd;
}

int recieve_message(void * container, int container_size, int sock_fd, struct sockaddr_in serv_addr) {

    struct pollfd fds[1];
    fds[0].fd = sock_fd;
    fds[0].events = POLLIN;
    socklen_t serv_addr_len = sizeof(serv_addr);


    if (poll(fds, 1, 0)) {
        printf("POLL_VAL\n");

        int recieved = recvfrom(sock_fd, container, container_size, 0, (struct sockaddr *)&serv_addr, &serv_addr_len);

        if (recieved > 1) {
            return 1;
        } else {
            printf("recieved: %d\n", recieved);
            exit(1);
        }
    } else {
        return 0;
    }
}

void hton_msgType(msgType * data) {
    data->msgID   = htonl(data->msgID);
    data->tid     = htonl(data->tid);
    data->port    = htonl(data->port);
    data->newValue= htonl(data->newValue);
    data->delay   = htonl(data->delay);

}

void ntoh_msgType(msgType * data) {
    data->msgID   = ntohl(data->msgID);
    data->tid     = ntohl(data->tid);
    data->port    = ntohl(data->port);
    data->newValue= ntohl(data->newValue);
    data->delay   = ntohl(data->delay);

}

void vectorClock_increment_local() {
    logHeader.vectorClockMem[0].time ++;
    msync(&logHeader, sizeof(logHeader), MS_SYNC);
}

void vectorClock_syncronize(struct clock * dest_clocks, struct clock * remote_clocks) {
    for (int i = 0; i < MAX_NODES; i++ ) {
        if  (remote_clocks[i].nodeId) {
            for (int j = 0; j < MAX_NODES; j++ ) {
                if (dest_clocks[j].nodeId) {
                    if (dest_clocks[j].nodeId == remote_clocks[i].nodeId &&
                        dest_clocks[j].time < remote_clocks[i].time) {
                        dest_clocks[j].time = remote_clocks[i].time;
                    }
                } else {
                    dest_clocks[j].nodeId = remote_clocks[i].nodeId;
                    dest_clocks[j].time = remote_clocks[i].time;
                    break;
                }
            }
        }
    }
    msync(&logHeader, sizeof(logHeader), MS_SYNC);
}


unsigned int get_timeof_node(unsigned int nodeID) {
    for (int i = 0; i < MAX_NODES; i++)
        if (logHeader.vectorClockMem[i].nodeId == nodeID)
            return logHeader.vectorClockMem[i].time;

    return 0;
}

void shivizLog_append(int vectorLogFD, char * message, struct clock * clocks) {
    dprintf(vectorLogFD, "%s\n{", message);
    for (int i = 0; i < MAX_NODES; i++){
        if(clocks[i].nodeId > 0)
            dprintf(vectorLogFD, "\"N%u\" : %i ", clocks[i].nodeId, clocks[i].time);
    }
    dprintf(vectorLogFD, "}\n");
}

void set_send_shiviz_log_message(msgType message, unsigned int to_port){
    // TODO
    char log_msg[256];

    sprintf(log_msg, "Sending %d to N%u\n", message.msgID, to_port);

    shivizLog_append(vectorLogFD, log_msg, logHeader.vectorClockMem);
}

void set_receive_shiviz_log_message(msgType message, unsigned int to_port){
    // TODO
    char log_msg[256];

    sprintf(log_msg, "Received %d fromm N%u\n", message.msgID, to_port);

    shivizLog_append(vectorLogFD, log_msg, logHeader.vectorClockMem);
}

/************************ BEGIN DEAD CODE *************************/

    // msync(objData, sizeof(ObjectData), MS_SYNC);

    // retVal = munmap(objData, sizeof(ObjectData));
    // if (retVal < 0) {
    //  perror("Unmap failed");
    // }
    // return 0;
