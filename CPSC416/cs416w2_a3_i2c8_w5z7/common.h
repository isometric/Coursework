#ifndef MSG_H
#define MSG_H 1
#include <sys/time.h>
#include <stdio.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdint.h>

#define MAX_NODES 10

#define IDLEN     64
#define HOSTLEN IDLEN



/* A clock value consists of the node number (i.e. a node's port
   number) and the clock value.
*/

struct clock {
  unsigned int nodeId;
  unsigned int time;
};

enum cmdMsgKind {
    BEGINTX = 1000,
	JOINTX,
	NEW_A,
	NEW_B,
	NEW_IDSTR,
	DELAY_RESPONSE,
	CRASH,
	COMMIT,
	COMMIT_CRASH,
	ABORT,
    ABORT_CRASH,
	VOTE_ABORT,

    MGR_BEGIN,
    MGR_JOIN,
    MGR_COMMIT_REQ,
    MGR_VOTE_COMMIT,
    MGR_VOTE_ABORT,
};
typedef enum cmdMsgKind cmdMsgKind;


typedef     /* got the port number create a logfile name */
struct {
  char IDstring[IDLEN];   // This must be a null terminated ASCII string
  struct timeval lastUpdateTime;
  int A;
  int B;
  struct clock vectorClock[MAX_NODES];
} ObjectData;



typedef
struct {
    uint32_t        tid;
    int             status; //  -2 free, -1  abort, 0 normal, 1 voted, 2 commmitted
    struct timeval  created;
    struct timeval  modified;
    ObjectData      previous_values;
    struct clock    voter_status[MAX_NODES]; // 0 = norma, -1 abort, 1 commit

} TransactionData;

typedef
struct {
    struct clock    vectorClockMem[MAX_NODES];
    unsigned int    num_structs;
    unsigned int    port;
    char            hostName[HOSTLEN];
    TransactionData active_transactions[MAX_NODES];    // -1 signals empty

    struct timeval begin_req_time;
    struct timeval join_req_time;

} LogHeader;


typedef struct  {
    uint32_t    msgID;
    uint32_t    tid;        // Transaction ID
    uint32_t    port;
    int32_t     newValue;   // New value for A or B
    int32_t     delay;
    union {  // string data
        char    newID[IDLEN];
        char    hostName[HOSTLEN];
    } strData;
    struct clock clocks[MAX_NODES];
} msgType;



/* descriptors */
int                 vectorLogFD;
int                 dataObjectFD;
int                 logfileFD;

unsigned int        cmd_port;
int                 cmd_port_fd;
struct sockaddr_in  cmd_inaddr;

unsigned int        mgr_port;
int                 mgr_port_fd;
char                mgr_hostName[HOSTLEN];
struct sockaddr_in  mgr_inaddr;


/* mapped memmory */
ObjectData          objData;
LogHeader           logHeader;

/* temp */
msgType             recieved_msg;

/* flags */
int send_delay;

/* FUNcutions the joy */
void recover_and_initialize(int isManager);

int begin_transaction(uint32_t tid);
void abort_transaction(int index);
void commit_transaction(int index);

int get_active_trasaction_index(uint32_t tid);

void vectorClock_increment_local();
void vectorClock_syncronize(struct clock * dest_clock, struct clock * remote_clocks);
unsigned int find_time_in_vectorclock(struct clock * vectorclock, unsigned int nodeID);



int open_socket(unsigned int port, struct sockaddr_in * curr_addr);
void send_message(int sock_fd, char* dest_addr, int dest_port, void * payload, int payload_size);
int recieve_message(void * container, int container_size, int sock_fd, struct sockaddr_in serv_addr);

void hton_msgType(msgType * data);
void ntoh_msgType(msgType * data);



void shivizLog_append(int vectorLogFD, char * message, struct clock * clocks);
void set_send_shiviz_log_message(msgType message, unsigned int to_port);
void set_receive_shiviz_log_message(msgType message, unsigned int to_port);


#endif
