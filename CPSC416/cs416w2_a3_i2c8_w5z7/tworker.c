#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <ctype.h>
#include <errno.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <poll.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include "common.c"
#include "tworker.h"





int main(int argc, char const *argv[]) {

    if (argc != 2) {
        printf("usage: %s  portNum\n", argv[0]);
        exit(1);
    }

    cmd_port = strtoul(argv[1], NULL, 10);
    if (cmd_port == 0 ) {
        printf("Port conversion error\n");
        exit(1);
    }

    recover_and_initialize(0);

    puts("MAIN LOOP");

    while (1) {
        if (cmd_port_fd && recieve_message(&recieved_msg, sizeof(recieved_msg), cmd_port_fd, cmd_inaddr)) {
            handle_cmd_msg(&recieved_msg);
        }

        if (mgr_port_fd  && recieve_message(&recieved_msg, sizeof(recieved_msg), mgr_port_fd, mgr_inaddr)) {
            handle_mgr_msg(&recieved_msg);
        }

        // do crash and timeout bullshit
        handle_timeouts();

        usleep(100 * 1000); // 100 ms
    }
}

void handle_cmd_msg(msgType* msg) {
    ntoh_msgType(msg);
    msgType outMsg = {0};

    printf("Received message %u\n", msg->msgID);
    printf("Received message %u\n", msg->tid);

    switch (msg->msgID) {

        case  BEGINTX:
            outMsg.msgID = MGR_BEGIN;
            outMsg.port = cmd_port;
            outMsg.tid = msg->tid;

            memcpy(mgr_hostName, msg->strData.hostName, sizeof(mgr_hostName));
            memcpy(logHeader.hostName, mgr_hostName, sizeof(mgr_hostName));
            msync(&logHeader, sizeof(LogHeader), MS_SYNC);

            if (logHeader.active_transactions[0].status != -2){
                puts("only 1 transaction at a time supported");
                printf("current trasaction status %d\n", logHeader.active_transactions[0].status);
                break;
            }

            if (begin_transaction(msg->tid) < 0) {
                puts("transaction already in progress");
            }
            break;

        case  JOINTX:
            outMsg.msgID = MGR_BEGIN;
            outMsg.port = cmd_port;
            outMsg.tid = msg->tid;

            memcpy(mgr_hostName, msg->strData.hostName, sizeof(mgr_hostName));
            memcpy(logHeader.hostName, mgr_hostName, sizeof(mgr_hostName));
            msync(&logHeader, sizeof(LogHeader), MS_SYNC);

            if (logHeader.active_transactions[0].status != -2){
                puts("only 1 transaction at a time supported");
                printf("current trasaction status %d\n", logHeader.active_transactions[0].status);
                break;
            }

            if (begin_transaction(msg->tid) < 0) {
                puts("transaction already in progress");
            }
            break;

        case NEW_A:
            gettimeofday(&objData.lastUpdateTime, NULL);
            objData.A = msg->newValue;

            memcpy(&objData.vectorClock, &logHeader.vectorClockMem, sizeof(logHeader.vectorClockMem));
            msync(&objData, sizeof(ObjectData), MS_SYNC);
            break;

        case NEW_B:
            gettimeofday(&objData.lastUpdateTime, NULL);
            objData.B = msg->newValue;

            memcpy(&objData.vectorClock, &logHeader.vectorClockMem, sizeof(logHeader.vectorClockMem));
            msync(&objData, sizeof(ObjectData), MS_SYNC);
            break;

        case NEW_IDSTR:
            gettimeofday(&objData.lastUpdateTime, NULL);

            strcpy(objData.IDstring, msg->strData.newID);

            memcpy(&objData.vectorClock, &logHeader.vectorClockMem, sizeof(logHeader.vectorClockMem));
            msync(&objData, sizeof(ObjectData), MS_SYNC);
            break;

        case DELAY_RESPONSE:
            send_delay = msg->delay;
            break;

        case CRASH:
            printf("CRASH\n");
            vectorClock_increment_local();
            char crash_log[128];
            sprintf(crash_log, "Node %u crashed", cmd_port);
            shivizLog_append(vectorLogFD, crash_log, logHeader.vectorClockMem);

            exit(0);    // do we even increment clock or log?
            break;

        case COMMIT_CRASH:
            outMsg.newValue = 1;
        case COMMIT:
            outMsg.msgID = MGR_COMMIT_REQ;
            outMsg.tid = msg->tid;
            aborting_current = 0;
            break;

        case ABORT_CRASH:
            outMsg.newValue = 1;
        case ABORT:
            outMsg.msgID = MGR_COMMIT_REQ;
            outMsg.tid = msg->tid;

            int index = get_active_trasaction_index(msg->tid);
            if (index >= 0) abort_transaction(index);
            break;

        case VOTE_ABORT:
            aborting_current = 1;
            break;

        default:
            puts("handle_cmd_msg switch fell through");
            break;
    }

    if (outMsg.tid) {
        outMsg.port = mgr_port;
        memcpy(&outMsg.strData.hostName, msg->strData.hostName, sizeof(msg->strData.hostName));
        hton_msgType(&outMsg);
        if (mgr_port_fd && mgr_port) {
            send_message(mgr_port_fd, mgr_hostName, mgr_port, &outMsg, sizeof(msgType));
            set_send_shiviz_log_message(outMsg, mgr_port);
            switch (outMsg.tid) {
                case  BEGINTX:
                    gettimeofday(&begin_req_time, NULL);
                    break;
                case  JOINTX:
                    gettimeofday(&join_req_time, NULL);
                    break;
                case COMMIT_CRASH:
                case COMMIT:
                case ABORT_CRASH:
                case ABORT:
                    gettimeofday(&commit_req_time, NULL);
                    commit_req_30s = 0;
            }
        } else {
            puts("attempting to respond to invalid port");
        }
    }
}


void handle_mgr_msg(msgType * msg) {
    printf("HERE handle_mgr_msg\n");
    ntoh_msgType(msg);

    msgType outMsg = {0};

    vectorClock_syncronize(logHeader.vectorClockMem, msg->clocks);
    vectorClock_increment_local();

    set_receive_shiviz_log_message(outMsg, mgr_port);
    TransactionData * curr = &logHeader.active_transactions[0];

    switch (msg->msgID) {
        case MGR_BEGIN:
            memset(&begin_req_time, 0, sizeof(begin_req_time));
            if (msg->newValue) {
                // log success
            } else {
                // log fail
            }
            break;

        case MGR_JOIN:
            memset(&join_req_time, 0, sizeof(join_req_time));
            if (msg->newValue) {
                // log success
            } else {
                // log fail
            }
            break;

        case MGR_COMMIT_REQ:
            if (curr->status >= 0 && curr->tid == msg->tid) {
                memset(&commit_req_time, 0, sizeof(commit_req_time));
                if (aborting_current) {
                    curr->status = -1;
                    outMsg.msgID = MGR_VOTE_ABORT;
                    abort_transaction(0);
                } else {
                    curr->status = 1;
                    outMsg.msgID = MGR_VOTE_COMMIT;

                }
                msync(&logHeader, sizeof(LogHeader), MS_SYNC);
            } else {
                outMsg.msgID = MGR_VOTE_ABORT;
            }
            if (send_delay == -1000) exit(1);
            break;

        case MGR_VOTE_COMMIT:
            if (curr->status >= 0 && curr->tid == msg->tid)
                commit_transaction(0);
                memset(&curr->modified, 0, sizeof(curr->modified));
                msync(&logHeader, sizeof(logHeader), MS_SYNC);
            break;

        case MGR_VOTE_ABORT:
            if (curr->status >= 0 && curr->tid == msg->tid)
                abort_transaction(0);
            break;

        default:
            puts("handle_mgr_msg switch fell through");
            break;
    }

    if (outMsg.tid) {
        hton_msgType(&outMsg);
        if (mgr_port_fd && mgr_port && strlen(mgr_hostName) > 0) {
            memcpy(outMsg.clocks, logHeader.vectorClockMem, sizeof(logHeader.vectorClockMem));
            send_message(mgr_port_fd, mgr_hostName, mgr_port, &outMsg, sizeof(msgType));
            set_send_shiviz_log_message(outMsg, mgr_port);
        } else {
            puts("attempting to respond to invalid port");
        }
    }
}

void handle_timeouts() {
    if (logHeader.active_transactions[0].status < 0) return;
    TransactionData * curr = &logHeader.active_transactions[0];

    struct timeval current_time;
    gettimeofday(&current_time, NULL);

    if (begin_req_time.tv_sec && current_time.tv_sec - begin_req_time.tv_sec > 10) {
        msgType outMsg = {0};

        outMsg.msgID = MGR_BEGIN;
        outMsg.port = cmd_port;
        outMsg.tid = curr->tid;

        gettimeofday(&begin_req_time, NULL);
        hton_msgType(&outMsg);

        vectorClock_increment_local();
        memcpy(outMsg.clocks, logHeader.vectorClockMem, sizeof(logHeader.vectorClockMem));
        send_message(mgr_port_fd, mgr_hostName, mgr_port, &outMsg, sizeof(msgType));
        set_send_shiviz_log_message(outMsg, mgr_port);

    }
    if (join_req_time.tv_sec && current_time.tv_sec - join_req_time.tv_sec > 10) {
        msgType outMsg = {0};

        outMsg.msgID = MGR_JOIN;
        outMsg.port = cmd_port;
        outMsg.tid = curr->tid;

        gettimeofday(&join_req_time, NULL);
        hton_msgType(&outMsg);

        vectorClock_increment_local();
        memcpy(outMsg.clocks, logHeader.vectorClockMem, sizeof(logHeader.vectorClockMem));
        send_message(mgr_port_fd, mgr_hostName, mgr_port, &outMsg, sizeof(msgType));
        set_send_shiviz_log_message(outMsg, mgr_port);
    }

    if (begin_req_time.tv_sec && current_time.tv_sec - commit_req_time.tv_sec > 30)
            commit_req_30s = 1;

    if (begin_req_time.tv_sec && current_time.tv_sec - commit_req_time.tv_sec > 10) {
        msgType outMsg = {0};

        outMsg.msgID = MGR_COMMIT_REQ;
        outMsg.tid = curr->tid;

        gettimeofday(&commit_req_time, NULL);
        hton_msgType(&outMsg);

        vectorClock_increment_local();
        memcpy(outMsg.clocks, logHeader.vectorClockMem, sizeof(logHeader.vectorClockMem));
        send_message(mgr_port_fd, mgr_hostName, mgr_port, &outMsg, sizeof(msgType));
        set_send_shiviz_log_message(outMsg, mgr_port);
    }

}
