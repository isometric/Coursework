
#ifndef TWORKER_H
#define TWORKER_H 1

#include "common.h"



int main(int argc, char const *argv[]);




void handle_cmd_msg(msgType * msg);
void handle_mgr_msg(msgType * msg);
void handle_timeouts();



/* extern hacks */
extern unsigned int     cmd_port;
extern int              cmd_port_fd;
extern struct sockaddr_in cmd_inaddr;

extern unsigned int     mgr_port;
extern int              mgr_port_fd;
extern struct sockaddr_in mgr_inaddr;
extern char             mgr_hostName[HOSTLEN];


extern ObjectData           objData;
extern LogHeader            logHeader;

extern msgType              recieved_msg;
extern int                  send_delay;


/* state flags */
struct timeval begin_req_time;
struct timeval join_req_time;
struct timeval commit_req_time;

int commit_req_30s;
int aborting_current;


#endif /* TWORKER_H */
