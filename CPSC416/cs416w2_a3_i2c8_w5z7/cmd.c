#define _GNU_SOURCE

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <errno.h>
#include <strings.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "common.h"


int main(int argc, char const * argv[]) {


    struct sockaddr_in cmd_inaddr;
    int sock_fd = open_socket(0, &cmd_inaddr);
    size_t cmd_buff_size = 256;
    char * cmd_buff = malloc(cmd_buff_size + 1);
    ssize_t bytes_read;

    while (1) {
        // printf("> ");
        bytes_read = getline(&cmd_buff, &cmd_buff_size, stdin);

        msgType msg;
        memset(&msg, 0, sizeof(msg));

        char * command = strtok(cmd_buff, " ");
        char * dest_addr = strtok(NULL, " ");
        int dest_port = atol(strtok(NULL, " \n"));

        if (strcmp(command, "begin") == 0) {
            msg.msgID = BEGINTX;

            strncpy(msg.strData.hostName, strtok(NULL, " "), HOSTLEN);
            msg.port = atol(strtok(NULL, " "));
            msg.tid = atol(strtok(NULL, " "));

        } else if (strcmp(command, "join") == 0) {
            msg.msgID = JOINTX;

            strncpy(msg.strData.hostName, strtok(NULL, " "), HOSTLEN);
            msg.port = atol(strtok(NULL, " "));
            msg.tid = atol(strtok(NULL, " "));

        } else if (strcmp(command, "newa") == 0) {
            msg.msgID = NEW_A;

            msg.newValue = atoi(strtok(NULL, " "));

        } else if (strcmp(command, "newb") == 0) {
            msg.msgID = NEW_B;

            msg.newValue = atoi(strtok(NULL, " "));

        } else if (strcmp(command, "newid") == 0) {
            msg.msgID = NEW_IDSTR;

            strncpy(msg.strData.newID, strtok(NULL, " "), IDLEN);

        } else if (strcmp(command, "crash") == 0) {
            msg.msgID = CRASH;

        } else if (strcmp(command, "delay") == 0) {
            msg.msgID = DELAY_RESPONSE;

            msg.newValue = atoi(strtok(NULL, " "));

        } else if (strcmp(command, "commit") == 0) {
            msg.msgID = COMMIT;

        } else if (strcmp(command, "commitcrash") == 0) {
            msg.msgID = COMMIT_CRASH;

        } else if (strcmp(command, "abort") == 0) {
            msg.msgID = ABORT;

        } else if (strcmp(command, "abortcrash") == 0) {
            msg.msgID = ABORT_CRASH;

        } else if (strcmp(command, "voteabort") == 0) {
            msg.msgID = VOTE_ABORT;

        } else if (strcmp(command, "exit") == 0) {
            break;
        } else {
            puts("invalid command");
            continue;
        }


        hton_msgType(&msg);
        send_message(sock_fd, dest_addr, dest_port, &msg, sizeof(msg));
    }
    _exit(23);
}
