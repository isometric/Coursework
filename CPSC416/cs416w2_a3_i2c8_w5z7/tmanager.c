#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <ctype.h>
#include <errno.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <poll.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include "common.c"
#include <stdio.h>
#include "tmanager.h"

int main(int argc, char const *argv[]) {
    // This is some sample code feel free to delete it



    if (argc != 2) {
        printf("usage: %s  portNum\n", argv[0]);
        exit(1);
    }

    cmd_port = strtoul(argv[1], NULL, 10);
    if (cmd_port == 0 ) {
        printf("Port conversion error\n");
        exit(1);
    }

    recover_and_initialize(1);

    puts("MAIN LOOP");

    while (1) {
        if (cmd_port_fd && recieve_message(&recieved_msg, sizeof(recieved_msg), cmd_port_fd, cmd_inaddr)) {
            handle_worker_msg(&recieved_msg);
        }

        check_voters();

        usleep(100 * 1000); // 100 ms
    }
}


void handle_worker_msg(msgType * msg) {
    ntoh_msgType(msg);
    msgType outMsg = {0};
    outMsg.tid = msg->tid;

    printf("Received message %u\n", msg->msgID);
    printf("Received message %u\n", msg->tid);

    switch (msg->msgID) {
        int index;

        case MGR_BEGIN:
            outMsg.tid = MGR_BEGIN;
            if (get_active_trasaction_index(msg->tid) < 0) {
                outMsg.newValue = 1;

                index = begin_transaction(msg->tid);
                logHeader.active_transactions[index].voter_status[0].nodeId = msg->port;
                msync(&logHeader, sizeof(LogHeader), MS_SYNC);

            } else {
                outMsg.newValue = 0;
                //failed, must call join to be added
            }
            break;

        case MGR_JOIN:
            outMsg.tid = MGR_BEGIN;
            index = get_active_trasaction_index(msg->tid);
            if (index > 0) {
                outMsg.newValue = 1;
                for (int i = 0; i < MAX_NODES; i++) {
                    if (logHeader.active_transactions[index].voter_status[0].nodeId == 0)
                        logHeader.active_transactions[index].voter_status[0].nodeId = msg->port;
                }
            } else {
                outMsg.newValue = 0;
                //failed, must call join to be added
            }
            break;
        case MGR_COMMIT_REQ:
            if (msg->newValue) crashing = 1;
            index = get_active_trasaction_index(msg->tid);
            if (index >= 0) {
               logHeader.active_transactions[index].status = 1;
               // broadcast commit request for everyone
            } else {
                outMsg.msgID = MGR_VOTE_ABORT;
            }
            break;
        case MGR_VOTE_COMMIT:
            index = -1;
            for (int i = 0; i < MAX_NODES; i++) {
                if (logHeader.active_transactions[i].tid == msg->tid) {
                    index = i;
                    break;
                }
            }
            if (index >= 0) {
                for (int i = 0; i < MAX_NODES; i++) {
                    if (logHeader.active_transactions[index].voter_status[0].nodeId == msg->port) {
                        logHeader.active_transactions[index].voter_status[0].time = 1;
                        break;
                    }
                }
            } else {
                outMsg.msgID = MGR_VOTE_ABORT;
            }
        case MGR_VOTE_ABORT:
            index = get_active_trasaction_index(msg->tid);
            if (index >= 0) {
                abort_transaction(index);
                //TODO: broadcast abort message
            }
            break;

        default:
            puts("handle_worker_msg switch fell through");
            break;

    }
    if (outMsg.tid) {
        outMsg.port = mgr_port;
        hton_msgType(&outMsg);
        send_message(cmd_port_fd, msg->strData.hostName, cmd_port, &outMsg, sizeof(msgType));
    }
}

void broadcast(int index, msgType * message) {
    for (int i = 0; i < MAX_NODES; i++) {
        if(logHeader.active_transactions[index].voter_status[0].nodeId) {
            //send_message(cmd_port_fd, msg->strData.hostName, cmd_port, &outMsg, sizeof(msgType));
        }
    }
}

void check_voters() {
    for (int i = 0; i < MAX_NODES; i++) {
        if (logHeader.active_transactions[i].status <= -2 || // aborted
            logHeader.active_transactions[i].status <= 0 ||  // nothing to do
            logHeader.active_transactions[i].status <= 2) {  // committed
            continue;
        } else if (logHeader.active_transactions[i].status <= -1) {
            abort_transaction(i);
            //TODO; broadcast abort ransaction
        } else if (logHeader.active_transactions[i].status >= 1) {

            int committing = 1;
            for (int j = 0; j < MAX_NODES; j++) {
                if (logHeader.active_transactions[i].voter_status[j].time == -1) {
                    abort_transaction(i);
                    //TODO: brodcast abort transaction
                } else if (logHeader.active_transactions[i].voter_status[j].nodeId &&
                           logHeader.active_transactions[i].voter_status[j].nodeId != 1) {
                    committing = 0;
                }
            }
            if (committing) {
                commit_transaction(i);
                //TODO: brodcast commit transaction
            }
        }
    }
}
