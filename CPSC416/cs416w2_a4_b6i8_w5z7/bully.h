#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

#include "fgmpi.h"

#define ELECTION_ID   20
#define AYA_ID        21
#define IAA_ID        22
#define COORD_ID      23
#define ANSWER_ID     24
#define IAM_DEAD_JIM  998
#define TERMINATE     999


typedef struct
{
   int verbosity_level;          // treat as booean 0 = minimal 1 = verbose
   int timeout_period;          // universal timeout period when awaiting event
   int avg_aya_period;          // are you alive period, random centered at average
   int avg_rtl_preiod;          // return to live period, random centered at average
   int send_failure_chance;     // percentage probability of send_failure
} Perams;

int bully( int argc, char** argv );
int intParse(char** argv, int i, int* err);
inline void debug_printf(int verbosity_level, int priority, const char *format, ...);


/* Verbosity levels*/
#define MINIMAL 0
#define VERBOSE 1


/* From the file mpi.h this is the definition for MPI_status, with my comments on the
         fields

         typedef struct MPI_Status {
         int count;          // Seems to be the count, in bytes of the data received
         int cancelled;      // Indication of whether of not the operation was cancelled.
                             // this probably only applies if you are doing non-blocking
                             // operations
         int MPI_SOURCE;     // ID of the source
         int MPI_TAG;        // The actual message tag
         int MPI_ERROR;      // The error code if there was one
         } MPI_Status;
*/