#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdlib.h>
#include <time.h>
#include "mpi.h"
#include "fgmpi.h"
#include "bully.h"

FG_ProcessPtr_t binding_func(int argc __attribute__ ((unused)),
                             char** argv __attribute__ ((unused)),
                             int rank __attribute__ ((unused))){
    return (&bully);
}

FG_MapPtr_t map_lookup(int argc __attribute__ ((unused)),
                       char** argv __attribute__ ((unused)),
                       char* str __attribute__ ((unused))){
    return (&binding_func);
}

int main( int argc, char *argv[] )
{
    FGmpiexec(&argc, &argv, &map_lookup);
    return (0);
}


int bully( int argc, char** argv )
{
    int rank, size, coSize, sRank;

    Perams arg;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPIX_Get_collocated_size(&coSize);
    MPIX_Get_collocated_startrank(&sRank);


    if (rank == 0) {
        int err = 0;
        if (argc >= 6) {
            arg.verbosity_level     = intParse(argv, 1, &err);
            arg.timeout_period      = intParse(argv, 2, &err);
            arg.avg_aya_period      = intParse(argv, 3, &err);
            arg.send_failure_chance = intParse(argv, 4, &err);
            arg.avg_rtl_preiod      = intParse(argv, 5, &err);
        } else err ++;

        if (err) {
            printf("Errors encountered: %i\nUsage: mpirun -nfg X -n Y %s Z\n where X*Y must be >= Z\n", err, argv[0]);
            MPI_Abort(MPI_COMM_WORLD, err);
        }
    }
    srand(time(NULL) + clock() + rank);
    MPI_Bcast(&arg, 5, MPI_INT, 0, MPI_COMM_WORLD);


    /* Begin Bully Algorithim */
    int flag;
    int data;
    MPI_Status status;
    MPI_Request request;
    int msgCount = 0;
    int leaderRank = size-1; // can set to anyting theoretically


    while (1) {
        double curr_time = MPI_Wtime();
        MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);
        if (flag) {
            puts("foodfs dfsdf sdfd saf");
            MPI_Recv(&data, 1, MPI_INT, MPI_ANY_SOURCE, IAA_ID, MPI_COMM_WORLD, &status);
            debug_printf(arg.verbosity_level, 0, "Message from %d size %d tag %d contents %d\n",
                         status.MPI_SOURCE,
                         status.count,
                         status.MPI_TAG,
                         data);

        }


        if (rank == leaderRank) {




        } else {
            data = rank;
            MPI_Isend(&data, 1, MPI_INT, 0, IAA_ID, MPI_COMM_WORLD, &request);
            debug_printf(arg.verbosity_level, 0, "sending %d\n", data);
            sleep(2);



        }
        MPI_Wait(&request, MPI_STATUS_IGNORE);
    }
}


int intParse(char** argv, int i, int* err) {
    char *endPtr;
    int val = strtol(argv[i], &endPtr, 10);
    if (*endPtr == '\0') printf("argv[%d] = %d\n", i, val);
    else *err +=1;
    return val;
}

inline void debug_printf(int verbosity_level, int priority, const char *format, ...) {
    if (priority >= verbosity_level) return;
    va_list args;
    va_start (args, format);
    vprintf (format, args);
    va_end (args);
}
