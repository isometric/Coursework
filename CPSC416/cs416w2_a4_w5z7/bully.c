#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "mpi.h"
#include "fgmpi.h"
#include "bully.h"

FG_ProcessPtr_t binding_func(int argc __attribute__ ((unused)),
                             char** argv __attribute__ ((unused)),
                             int rank __attribute__ ((unused))){
    return (&bully);
}

FG_MapPtr_t map_lookup(int argc __attribute__ ((unused)),
                       char** argv __attribute__ ((unused)),
                       char* str __attribute__ ((unused))){
    return (&binding_func);
}

int main( int argc, char *argv[] )
{
    FGmpiexec(&argc, &argv, &map_lookup);
    return (0);
}


int bully( int argc, char** argv )
{
    int rank, size, coSize, sRank;

    Perams arg;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPIX_Get_collocated_size(&coSize);
    MPIX_Get_collocated_startrank(&sRank);


    if (rank == 0) {
        int err = 0;
        if (argc >= 6) {
            puts("verbosity_level");
            arg.verbosity_level     = intParse(argv, 1, &err);
            puts("timeout_period");
            arg.timeout_period      = intParse(argv, 2, &err);
            puts("avg_aya_period");
            arg.avg_aya_period      = intParse(argv, 3, &err);
            puts("send_failure_chance");
            arg.send_failure_chance = intParse(argv, 4, &err);
            puts("send_failure_chance");
            arg.avg_rtl_preiod      = intParse(argv, 5, &err);
        } else err ++;

        if (err) {
            printf("Errors encountered: %i\nUsage: mpirun -nfg X -n Y %s Z\n where X*Y must be >= Z\n", err, argv[0]);
            MPI_Abort(MPI_COMM_WORLD, err);
        }
        printf("Simulation size %d threads at %d threads per process \n\n", size, coSize);
    }
    srand(MPI_Wtime() * rank);
    MPI_Bcast(&arg, 5, MPI_INT, 0, MPI_COMM_WORLD);

    arg.raw_timeout_period = arg.timeout_period;
    arg.timeout_period = (float) arg.timeout_period * (2.f - ((float) rank)/(float) size) * (2.f - ((float) rank)/(float) size);
    arg.avg_aya_period = (float) arg.avg_aya_period * (2.f - ((float) rank)/(float) size) * (2.f - ((float) rank)/(float) size);

    /* Begin Bully Algorithim */

    if (rank == 0) {
        int time = 0;
        double last_update_time = MPI_Wtime();
        // MPI_Request requests[size-1];
        while (1) {
            if(MPI_Wtime() - last_update_time > arg.timeout_period/2) {
                if (time == -1) {
                    Payload msg = {time, time};
                    MPI_Send(&msg, 2, MPI_INT, 1, ELECTION_ID, MPI_COMM_WORLD);
                    puts("ELECT");
                    MPI_Request request;
                    MPI_Irecv(&msg, 2, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &request);
                    MPI_Wait(&request, MPI_STATUS_IGNORE);
                }
                time++;
                Payload msg = {time, time};
                for (int i = 1; i < size; ++i)
                    MPI_Send(&msg, 2, MPI_INT, i, CLOCK_ID, MPI_COMM_WORLD);
                    // MPI_Isend(&msg, 2, MPI_INT, i, CLOCK_ID, MPI_COMM_WORLD, &requests[i-1]);
                // MPI_Waitall(size-1, requests, MPI_STATUSES_IGNORE);
                last_update_time = MPI_Wtime();
            } else usleep(20000);
        }
    } else {
        NodeState state = {0};
        state.current_leader = size-1;
        MPI_Request requests[size];
        int requests_size = 0;
        // MPI_Request * recv_start = &requests[0];
        // MPI_Request * send_start = &requests[1];
        int completed_requests;
        int completed_request_indexes[size];

        while (1) {
            Payload msg_in = {0};
            MPI_Status msg_in_status;
            MPI_Request request;

            int was_dead = MPI_Wtime() < state.rtl_time;
            if (was_dead) debug_printf(arg.verbosity_level, MINIMAL, "DEAD coordinator %d died at %d\n", rank, state.time);
            do {
                MPI_Irecv(&msg_in, 2, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &request);
                MPI_Wait(&request, &msg_in_status);
                state.time = MAX(state.time, msg_in.time);

            } while (MPI_Wtime() < state.rtl_time);
            if (was_dead) {
                debug_printf(arg.verbosity_level, MINIMAL, "ALIVE node %d returned to life from time %d\n", rank, state.time);
                call_election(requests, &requests_size, &arg, &state, rank, size);
            }
            if (requests_size > 0) {
                MPI_Waitsome(requests_size, requests, &completed_requests, completed_request_indexes, MPI_STATUSES_IGNORE);
            }
                // MPI_Waitall(requests_size, requests, MPI_STATUSES_IGNORE);
            switch (msg_in_status.MPI_TAG) {
                case ELECTION_ID:
                    if (msg_in_status.MPI_SOURCE > rank) {
                        debug_printf(arg.verbosity_level, DEBUG, "ERROR ---------------------------------------------------\n\
                                                                  NODE %d ELECTED BY SUPERIOR NODE %d\n", rank, msg_in_status.MPI_SOURCE);
                    } else if (msg_in.data > state.current_election_id) {
                        debug_printf(arg.verbosity_level, VERBOSE, "node %d joined election %d from %d\n", rank, msg_in.data, state.current_election_id);
                        state.current_election_id = msg_in.data;
                        answer_elector(requests, &requests_size, &arg, &state, rank, msg_in_status.MPI_SOURCE);
                        elect_superiors(requests, &requests_size, &arg, &state, rank, size);
                    } else {
                        answer_elector(requests, &requests_size, &arg, &state, rank, msg_in_status.MPI_SOURCE);
                    }
                    break;
                case ANSWER_ID:
                    if (msg_in_status.MPI_SOURCE < rank) {
                        debug_printf(arg.verbosity_level, DEBUG, "ERROR ---------------------------------------------------\n\
                                                                  NODE %d ANSERED BY INFERIOR NODE %d\n", rank, msg_in_status.MPI_SOURCE);
                    } else if (msg_in.data >= state.current_election_id) {
                        if (state.current_election_timeout)
                            debug_printf(arg.verbosity_level, DEBUG, "node %d accepted answer from %d\n", rank, msg_in_status.MPI_SOURCE);
                        state.current_election_timeout = 0;
                    } else {
                        debug_printf(arg.verbosity_level, DEBUG, "node %d ignored answer from %d\n", rank, msg_in_status.MPI_SOURCE);
                    }
                    break;
                case COOORD_ID:
                    if (msg_in_status.MPI_SOURCE < rank) {
                        debug_printf(arg.verbosity_level, FIREHOSE, "node %d rejected coord from %d\n", rank, msg_in_status.MPI_SOURCE);
                        state.current_leader = msg_in_status.MPI_SOURCE;
                        // state.current_leader = size-1;
                        debug_printf(arg.verbosity_level, MINIMAL, "BADCOORD from %d detected at time %d, by node %d\n", state.current_leader, state.time,  rank);
                        call_election(requests, &requests_size, &arg, &state, rank, size);
                    } else {
                        debug_printf(arg.verbosity_level, FIREHOSE, "node %d accepted coord from %d\n", rank, msg_in_status.MPI_SOURCE);
                        state.current_leader = msg_in_status.MPI_SOURCE;
                        state.current_election_timeout = 0;
                        state.current_aya_timeout =  0;
                        state.next_aya_time= 0;
                    }
                    break;
                case AYA_ID:
                    if (rank != state.current_leader || msg_in_status.MPI_SOURCE > rank) {
                        debug_printf(arg.verbosity_level, DEBUG, "ERROR ---------------------------------------------------\n\
                                                                  NODE %d UNEXPECTED AYA FROM NODE %d\n", rank, msg_in_status.MPI_SOURCE);
                        debug_printf(arg.verbosity_level, DEBUG, "node %d current_leader = %d\n", state.current_leader);
                    } else if ((rand()/53693) % 100 > arg.send_failure_chance) {
                        answer_probe(requests, &requests_size, &arg, &state, rank, msg_in_status.MPI_SOURCE, msg_in.data);
                    } else {
                        state.rtl_time = MPI_Wtime() + ((rand()/53693) % (2 * arg.avg_rtl_preiod));
                    }
                    break;
                case IAA_ID:
                    if (msg_in_status.MPI_SOURCE != state.current_leader) {
                        debug_printf(arg.verbosity_level, DEBUG, "ERROR ---------------------------------------------------\n\
                                                                   NODE %d UNEXPECTED IAA FROM NODE %d\n", rank, msg_in_status.MPI_SOURCE);
                        debug_printf(arg.verbosity_level, DEBUG, "node %d current_leader = %d\n", state.current_leader);
                    } else if (msg_in.data == state.current_aya_id) {
                        state.current_aya_timeout = 0;
                        state.next_aya_time = 0;
                    } else {
                        debug_printf(arg.verbosity_level, DEBUG, "node %d ignored alive from %d\n", rank, msg_in_status.MPI_SOURCE);
                    }
                    break;
                case CLOCK_ID: // not sure this actuallly does anything other than to smash blocks
                    break;
                default:
                    debug_printf(arg.verbosity_level, DEBUG, "ERR: recieved unexpected type %d data %d time %d\n", msg_in_status.MPI_TAG, msg_in.data, msg_in.time);

            }
            //if (requests_size > 0) MPI_Waitsome(requests_size, requests, &completed_requests, completed_request_indexes, MPI_STATUSES_IGNORE);
            /* HANDLE TIMEOUTS */
            double current_time = MPI_Wtime();
            if (state.current_election_timeout) {
                if (current_time > state.current_election_timeout) {
                    declare_coord(requests, &requests_size, &arg, &state, rank, size);
                }
            }
            //if (requests_size > 0) MPI_Waitsome(requests_size, requests, &completed_requests, completed_request_indexes, MPI_STATUSES_IGNORE);

            if (rank != state.current_leader && state.current_election_timeout == 0) {
                if (state.current_aya_timeout == 0) {
                    if (state.next_aya_time == 0) {
                        state.next_aya_time = current_time + ((rand()/53693) % (2 * arg.avg_aya_period));
                        debug_printf(arg.verbosity_level, SANITY, "node %d set next aya to %lf\n", rank, state.next_aya_time);
                    } else if (current_time > state.next_aya_time) {
                        probe_leader(requests, &requests_size, &arg, &state, rank);
                    }
                } else if (current_time > state.current_aya_timeout) {
                    debug_printf(arg.verbosity_level, MINIMAL, "LEADERDEAD of %d detected at time %d, by node %d\n", state.current_leader, state.time,  rank);
                    call_election(requests, &requests_size, &arg, &state, rank, size);
                }
            }
            debug_printf(arg.verbosity_level, SANITY, "%d----stillalive----END\n", rank);
            // usleep(100);
        }
    }
}

/* Event Handlers */

void call_election(MPI_Request * requests, int * requests_size ,Perams * arg, NodeState * state, int rank,int size) {
    state->current_election_id = ((int) MPI_Wtime() / (4 * 2 * arg->raw_timeout_period)) + (size - state->current_leader); //1 + MAX(state->time, state->current_election_id);
    state->current_aya_timeout = 0;
    state->next_aya_time = 0;
    debug_printf(arg->verbosity_level, MINIMAL, "ELECTION %d started at time %d, by node %d\n", state->current_election_id, state->time,  rank);
    elect_superiors(requests, requests_size, arg, state, rank, size);
}


void elect_superiors(MPI_Request * requests, int * requests_size ,Perams * arg, NodeState * state, int rank, int size) {
    //debug_printf(arg->verbosity_level, DEBUG, "node %d ELECT SUPERIORS rank %d size %d\n", rank, rank, size);
    state->current_aya_timeout = 0;
    state->next_aya_time= 0;
    debug_printf(arg->verbosity_level, DEBUG, "node %d elected nodes %d through %d\n", rank, rank+1, size-1);
    state->current_election_timeout = MPI_Wtime() + arg->timeout_period; //* (2.f - ((float) rank)/(float) size);
    if (rank == size -1) {
        declare_coord(requests, requests_size , arg, state, rank, size);
        return;
    }
    Payload msg_out = {++state->time, state->current_election_id};
    //MPI_Request requests[size-(rank+1)];
    //int completed_requests[size-(rank+1)];
    //int out;
    for (int i = size-1; i > rank; i--) {
        debug_printf(arg->verbosity_level, SANITY, "node %d coord i %d index %d\n", rank, i, size-(rank+1));
        MPI_Isend(&msg_out, 2, MPI_INT, i, ELECTION_ID, MPI_COMM_WORLD, &requests[i-(rank+1)]);
        // MPI_Send(&msg_out, 2, MPI_INT, i, ELECTION_ID, MPI_COMM_WORLD);
        debug_printf(arg->verbosity_level, FIREHOSE, "node %d electing %d\n", rank, i);
    }
    *requests_size = (size-(rank+1));
    //MPI_Waitsome(size-(rank+1), requests, &out, completed_requests, MPI_STATUSES_IGNORE);
    // MPI_Waitall(size-(rank+1), requests, MPI_STATUSES_IGNORE);
}

void answer_elector(MPI_Request * requests, int * requests_size ,Perams * arg, NodeState * state, int rank, int target) {
    //MPI_Request request;
    Payload msg_out = {++state->time, state->current_election_id};
    MPI_Isend(&msg_out, 2, MPI_INT, target, ANSWER_ID, MPI_COMM_WORLD, requests);
    //MPI_Wait(&request, MPI_STATUS_IGNORE);
    debug_printf(arg->verbosity_level, FIREHOSE, "node %d answered %d\n", rank, target);
    *requests_size = 1;
}
void probe_leader(MPI_Request * requests, int * requests_size ,Perams * arg, NodeState * state, int rank) {
    //MPI_Request request;
    Payload msg_out = {++state->time, state->current_aya_id = (rand()/53693)};
    MPI_Isend(&msg_out, 2, MPI_INT, state->current_leader, AYA_ID, MPI_COMM_WORLD, requests);
    //MPI_Wait(&request, MPI_STATUS_IGNORE);
    debug_printf(arg->verbosity_level, VERBOSE, "node %d seinding aya to %d\n", rank, state->current_leader);
    state->current_aya_timeout = MPI_Wtime() + arg->timeout_period;
    *requests_size = 1;
}

void answer_probe(MPI_Request * requests, int * requests_size ,Perams * arg, NodeState * state, int rank, int target, int data) {
    // MPI_Request request;
    Payload msg_out = {++state->time, data};
    MPI_Isend(&msg_out, 2, MPI_INT, target, IAA_ID, MPI_COMM_WORLD, requests);
    //MPI_Wait(&request, MPI_STATUS_IGNORE);
    debug_printf(arg->verbosity_level, VERBOSE, "node %d alive msg to node %d\n", rank, target);
    *requests_size = 1;
}
void declare_coord(MPI_Request * requests, int * requests_size ,Perams * arg, NodeState * state, int rank, int size) {
    debug_printf(arg->verbosity_level, MINIMAL, "LEADER declared at time %d, by node %d ---------------------------\n", state->time,  rank);
    Payload msg = {++state->time, state->current_election_id};
    //MPI_Request requests[size-2];
    // int completed_requests[size-2];
    // int out;
    for (int i = 1; i < size-1; i++) {
        int index = (i >= rank)? i+1: i;
        debug_printf(arg->verbosity_level, SANITY, "node %d coord i %d index %d\n", rank, i, index);
        MPI_Isend(&msg, 2, MPI_INT, index, COOORD_ID, MPI_COMM_WORLD, &requests[i-1]);
        // MPI_Send(&msg, 2, MPI_INT, index, COOORD_ID, MPI_COMM_WORLD);
        debug_printf(arg->verbosity_level, DEBUG, "node %d declaring coord to %d\n", rank, index);
    }
    *requests_size = (size-2);
    // MPI_Waitall(size-2, requests, MPI_STATUSES_IGNORE);
    //MPI_Waitsome(size-2, requests, &out, completed_requests, MPI_STATUSES_IGNORE);
    state->current_leader = rank;
    state->current_election_timeout = 0;
    state->next_aya_time = 0;
    state->current_aya_timeout = 0;
}


/* Parser Helper */

int intParse(char** argv, int i, int* err) {
    char *endPtr;
    int val = strtol(argv[i], &endPtr, 10);
    if (*endPtr == '\0') printf("argv[%d] = %d\n", i, val);
    else *err +=1;
    return val;
}

inline void debug_printf(int verbosity_level, int priority, const char *format, ...) {
    if (priority > verbosity_level) return;
    //if (verbosity_level >= 4) for (int i = 0; i < priority; i++) printf("\t \t \t ");
    va_list args;
    va_start (args, format);
    vprintf (format, args);
    va_end (args);
}
