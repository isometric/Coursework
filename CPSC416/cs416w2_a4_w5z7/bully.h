#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

#include "fgmpi.h"

#define ELECTION_ID   20
#define ANSWER_ID     21
#define COOORD_ID     22

#define CLOCK_ID      30
#define AYA_ID        31
#define IAA_ID        32

#define TERMINATE     999


#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))


typedef struct {
    int verbosity_level;          // treat as booean 0 = minimal 1 = verbose
    int timeout_period;          // universal timeout period when awaiting event
    int avg_aya_period;          // are you alive period, random centered at average
    int avg_rtl_preiod;          // return to live period, random centered at average
    int send_failure_chance;     // percentage probability of send_failure
    int raw_timeout_period;
} Perams;

typedef struct {
    int time;
    int current_leader;
    double rtl_time;
    double next_aya_time;
    int current_aya_id;
    double current_aya_timeout;
    double current_election_timeout;
    int current_election_id;
} NodeState;

typedef struct {
    int time;
    int data;
} Payload;



int bully( int argc, char** argv );
int intParse(char** argv, int i, int* err);
inline void debug_printf(int verbosity_level, int priority, const char *format, ...);

void call_election(MPI_Request * requests, int * requests_size ,Perams * arg, NodeState * state, int rank, int size);
void elect_superiors(MPI_Request * requests, int * requests_size ,Perams * arg, NodeState * state, int rank, int size);
void answer_elector(MPI_Request * requests, int * requests_size ,Perams * arg, NodeState * state, int rank, int target);
void probe_leader(MPI_Request * requests, int * requests_size ,Perams * arg, NodeState * state, int rank);
void answer_probe(MPI_Request * requests, int * requests_size ,Perams * arg, NodeState * state, int rank, int target, int data);
void declare_coord(MPI_Request * requests, int * requests_size ,Perams * arg, NodeState * state, int rank, int size);

/* Verbosity levels*/
#define MINIMAL     0
#define DEBUG       1
#define VERBOSE     2
#define FIREHOSE    3
#define SANITY      4


/* From the file mpi.h this is the definition for MPI_status, with my comments on the
                 fields

                 typedef struct MPI_Status {
                 int count;          // Seems to be the count, in bytes of the data received
                 int cancelled;      // Indication of whether of not the operation was cancelled.
                                                         // this probably only applies if you are doing non-blocking
                                                         // operations
                 int MPI_SOURCE;     // ID of the source
                 int MPI_TAG;        // The actual message tag
                 int MPI_ERROR;      // The error code if there was one
                 } MPI_Status;
*/
