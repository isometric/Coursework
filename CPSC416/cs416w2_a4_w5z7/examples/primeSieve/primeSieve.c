/**********************************************************************
*   Sieve of Eratosthenes Prime Finder
*   Alan Wagner 2011
*   (1) sequential mapper and (2) random mapper
*   These mappers can be selected on command-line, without
*   re-compiling the code.
*   Three functions:
*      generator():  generates all odd numbers to pass into the sieve
*      sieveElement(): keeps first number (a prime), filters the rest
*      lastElement():  terminates the sieve
*   USAGE:
*     mpiexec -nfg 4 -n 4 ./primeSieve  -- generates nfg*n primes (16)
*
*    FG-MPI uses a packed assignment, for example the previous use creates
*    4 processes assigned as [0..3][4..7][8..11][12..15].  The use above has
*    0 as the generator so that process i generates the ith+1 prime.
*    This mapping is not the best as nfg is large since later processes
*    do not become active until after a large number of primes are found.
*
*     mpiexec -nfg X -n Y -genv FGMAP random ./primeSieve [seed] [cuts]
*
*     is a version that cuts the ring (as in a deck of cards)
*     to produce random length sequences of chain to more evenly
*     distribute the processes. Enough cuts results in completely random
*     assignment.  These parameters allow one to experiment with the load-balancing.
*
*    Other examples:
*    mpiexec -nfg 100 -n 4 -genv SCHEDULER block -genv FGMAP random ./primeSieve [seed] [cuts]
*
*     uses the receive-side block scheduler (round-robin scheduler (rr) is
*     the default scheduler if none is specified).
*
*   TERMINATION: When the last process receives the last prime it sends a STOP
*    message to the generator that then passes it down the chain terminating the
*    sieve processes.  All stop and the last process prints the time.
******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <mpi.h>

#define MAXINT 1000000
#define FALSE 0
#define TRUE !FALSE
#define DATA_TAG 111
#define STOP_TAG 999

/* forward declarations */
int lastElement(int argc, char **argv);
int sieveElement(int argc, char **argv);
int generator(int argc, char **argv);
int who_are_my_neighbors(int rank, int size, int *prevproc_ptr, int *nextproc_ptr);

/******* FG-MPI Boilerplate begin *********/
#include "fgmpi.h"
/* forward declarations */
FG_ProcessPtr_t sequential_mapper(int argc, char** argv, int rank);
FG_ProcessPtr_t random_mapper(int argc, char** argv, int rank);

FG_MapPtr_t map_lookup(int argc, char** argv, char* str)
{
    /* Two mapping functions */
    if ( str && !strcmp(str, "random")) {
        return (&random_mapper);
    } else if ( str && !strcmp(str, "seq")) {
        return (&sequential_mapper);
    }

    /* return default mapper if FGMAP environment variable is not specified */
    return (&sequential_mapper);
}

int main( int argc, char *argv[] )
{
    FGmpiexec(&argc, &argv, &map_lookup);

    return (0);
}
/******* FG-MPI Boilerplate end *********/


int sieveElement(int argc, char **argv)
{
    int nextproc, prevproc;
    int rank, size;
    MPI_Status status;
    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    who_are_my_neighbors(rank, size, &prevproc, &nextproc);

    uint32_t num,myprime=0;

    int notdone = TRUE;
    while ( notdone )
    	{
            MPI_Recv(&num,1,MPI_INT,prevproc,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
            if ( status.MPI_TAG == DATA_TAG )
		{
                    if ( myprime == 0 )
			{
                            myprime=num;
                            printf("%u, ",myprime);
	  		}
                    else if ( num % myprime )
			{
                            /* Not divisable by this prime */
                            MPI_Send(&num,1,MPI_INT,nextproc,DATA_TAG,MPI_COMM_WORLD);
	  		}
                    else { ; }
		} else if ( status.MPI_TAG == STOP_TAG )
		{
                    notdone = FALSE;
                    /* Send the terminate_TAG*/
                    num=1;
                    MPI_Send(&num,1,MPI_INT,nextproc,STOP_TAG,MPI_COMM_WORLD);
		} else
		{
                    fprintf(stderr, "ERROR ERROR bad TAG \n");
                    MPI_Abort(MPI_COMM_WORLD,rank);
		}
	}
    MPI_Finalize();
    return 0;
}


int generator(int argc, char **argv)
{
    int nextproc, prevproc;
    int rank, size;
    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Request request;
    MPI_Status  status;
    who_are_my_neighbors(rank, size, &prevproc, &nextproc);

    uint32_t myprime=2;
    uint32_t num=myprime+1;
    printf("%u, ",myprime);
    /* Set up a MPI_Irecv to stop the sieve */
    MPI_Irecv(&num,1,MPI_INT,prevproc,STOP_TAG,MPI_COMM_WORLD,&request);
    while ( num <= MAXINT )
	{
            int result=FALSE;
            MPI_Send(&num,1,MPI_INT,nextproc,DATA_TAG,MPI_COMM_WORLD);
            num+=2;
            MPI_Test(&request,&result,&status);
            if ( result == TRUE) break;
    	}
    num=0;
    MPI_Send(&num,1,MPI_INT,nextproc,STOP_TAG,MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
}


int lastElement(int argc, char **argv)
{
    int nextproc, prevproc;
    int rank, size;
    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Status status;
    uint32_t num,myprime=0;
    who_are_my_neighbors(rank, size, &prevproc, &nextproc);

    int notdone = TRUE;
    while ( notdone )
    	{
            MPI_Recv(&num,1,MPI_INT,prevproc,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
            if ( status.MPI_TAG == DATA_TAG )
		{
                    if ( myprime == 0 )
			{
                            myprime=num;
                            printf("%u \n",myprime);
                            /* Send STOP_TAG to generator */
                            num=1;
                            MPI_Send(&num,1,MPI_INT,nextproc,STOP_TAG,MPI_COMM_WORLD);
	  		}
		} else
		{
                    /* Received a STOP_TAG */
                    notdone = FALSE;
		}
	}
    MPI_Finalize();
    return 0;
}

FG_ProcessPtr_t sequential_mapper(int argc, char** argv, int rank)
{
    int worldsize;
    MPI_Comm_size(MPI_COMM_WORLD, &worldsize);

    if ( (rank == MAP_INIT_ACTION) || (rank == MAP_FINALIZE_ACTION) )
        return (NULL);
    if ( 0 == rank ) return (&generator);
    if ( worldsize-1 == rank ) return(&lastElement);
    return (&sieveElement);
}

/* A temporary shared array that holds the random
   permutation of the processes created by random mapper.
   This array is only read by the co-located processes
   once to discover their previous and next neighbors
   and is de-allocated after that */
int *proc = NULL;

FG_ProcessPtr_t random_mapper(int argc, char** argv, int rank)
{
    int first=FALSE;
    int last=FALSE;
    int nfg, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPIX_Get_collocated_size(&nfg);

    int cuts= (size/nfg)-1;
    int seed=0;

    if ( rank == MAP_INIT_ACTION ) {
        if ( argc == 2 ) {
            seed = atoi(argv[1]);
        } else if ( argc ==  3 ) {
            seed = atoi(argv[1]);
            cuts = atoi(argv[2]);
        } else {
            printf("USAGE:  primeSieve [seed] [cuts]\n");
            exit(-1);
        }
        /* do the cuts and swaps */
        srand(seed);
        int i;
        proc = malloc(sizeof(int)*size);
        int *procswap = calloc(sizeof(int),size);
        for ( i=0; i<size; i++) proc[i] = i;
        for ( i=0; seed && i<cuts; i++){
            int ik1 = rand() % size;
            int ik2 = rand() % size;
            int k1 = (ik1 < ik2 ? ik1 : ik2);
            int k2 = (ik1 >= ik2 ? ik1 : ik2);
            /* swap */
            if ( (k2-k1) != 0 )
            {
                if ((i % 2) == 0 )
                {
                    memcpy(procswap,&(proc[k1]), sizeof(int)*(k2-k1));
                    memcpy(&(procswap[k2-k1]),proc, sizeof(int)*k1);
                    memcpy(&(procswap[k2]),&(proc[k2]), sizeof(int)*(size-k2));
                }
                else
                {
                    memcpy(procswap,proc, sizeof(int)*k1);
                    memcpy(&(procswap[size-(k2-k1)]),&(proc[k1]), sizeof(int)*(k2-k1));
                    memcpy(&(procswap[k1]),&(proc[k2]), sizeof(int)*(size-k2));
                }
                int *tmp=proc; proc = procswap; procswap=tmp;
            }
        }
        free(procswap);
        return (NULL);
    }
    if ( rank == MAP_FINALIZE_ACTION )
        return (NULL);

    assert (proc != NULL);

    if  ( proc[size-1] == rank ) { last=TRUE; }
    if  ( proc[0] == rank ) { first=TRUE; }

    if ( first )
        return (&generator);
    else if ( last )
        return (&lastElement);
    else
        return (&sieveElement);

}

int who_are_my_neighbors(int rank, int size, int *prevproc_ptr, int *nextproc_ptr)
{
    int prevproc = -1, nextproc = -1;
    char *mapstr = getenv("FGMAP");

    if ( mapstr && !strcmp(mapstr, "random")){
        static int times_called = 0;
        int i;
        times_called++;
        assert(proc);
        for ( i=1; i<size-1; i++) if ( proc[i] == rank) { prevproc = proc[i-1]; nextproc = proc[i+1]; }
        if  ( proc[size-1] == rank ) { prevproc = proc[size-2]; nextproc = proc[0];}
        if  ( proc[0] == rank ) { prevproc = proc[size-1]; nextproc = proc[1];}
        if (times_called == size){
            free(proc);
        }
    }
    else {
        prevproc = (0==rank) ? size-1 : rank-1;
        nextproc = (size-1==rank) ? 0 : rank+1;
    }

    *prevproc_ptr = prevproc;
    *nextproc_ptr = nextproc;
    return (0);
}
