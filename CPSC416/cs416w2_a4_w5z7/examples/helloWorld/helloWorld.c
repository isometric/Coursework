#include <stdio.h>
#include "mpi.h"

/******* FG-MPI Boilerplate begin *********/
#include "fgmpi.h"
int helloworld( int argc, char** argv ); /*forward declaration*/
FG_ProcessPtr_t binding_func(int argc, char** argv, int rank){
    return (&helloworld);
}
FG_MapPtr_t map_lookup(int argc, char** argv, char* str){ 
    return (&binding_func);
}

int main( int argc, char *argv[] )
{
    FGmpiexec(&argc, &argv, &map_lookup);    
    return (0);
}
/******* FG-MPI Boilerplate end *********/

int helloworld( int argc, char** argv )
{
    int rank, size;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    printf("Hello! I am rank %d of %d\n", rank, size);
    MPI_Finalize();
    return(0);
}
