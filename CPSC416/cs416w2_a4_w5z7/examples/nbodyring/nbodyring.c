/*
  nbodyring.c
  =======
  (c) Cody Brown 2010
  Email: cody@cs.ubc.ca

This was done for Assignment 1 from UBC's CPSC 521 class.

It is a basic N-Body simulation. It mimics bodies in space.  The simulation
takes two arguments, NumOfPart and TimeStep.
	NumOfPart: Int number of particles to simulate, greater than 0.
	TimeStep: Int number of time steps to perform, greater than 0.

	% nbodyring NumOfPart TimeStep

The simulation assumes all the masses are constant and the same, and are currently
set to around 10^16.  Furthermore, delta_t is set to 0.1 units.  So every time
step is 1 delta_t.  grid_size is also preset to 800, and if MPE is enabled, it
will make a window grid_size*grid_size and populate the particles randomly in
this window.

The simulation can run in two modes, Normal (MPE) and NO_MPE.  This was done, since
the cluster I have access to does not have MPE installed on it. Furthermore, each node
can't display X11 graphics (or set to).  During compile time, if you define:
	mpicc -o nbodyring nbodyring.c -DNO_MPE -lm
with the -DNO_MPE flag, the simulation will not produce an X-Window and will just print the
locations of each point at each timestep.  This is useful for performing many timesteps
and particles on a large cluster.

Running the normal MPE version, will also show the positions and trail of each particle in 
an X-Window.  Each particle will have a random colour assigned to it.

To run the program please use the commands similar to the following:
	mpiexec -np 1 `pwd`/nbodyring 6 100
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "mpi.h"

#ifndef NO_MPE
#include "mpe.h"
#include "mpe_graphics.h"
#endif


/******* FG-MPI Boilerplate begin *********/
#include "fgmpi.h"
int FG_Process( int argc, char** argv ); /* forward declaration */
FG_ProcessPtr_t fgprocess_mapper(int argc, char** argv, int rank){
    return (&FG_Process);
}
FG_MapPtr_t map_lookup(int argc, char** argv, char* str){ 
    return (&fgprocess_mapper);
}

int main( int argc, char *argv[] )
{
    FGmpiexec(&argc, &argv, &map_lookup);
    
    return (0);
}
/******* FG-MPI Boilerplate end *********/


#define PART_DNC -999999999	//"DoNotCompute" flag for particles
#define tag_code 3141		//MPI tag flag
#define grid_size 800		//grid_size*grid_size window for my particles
#define dt 0.1			//delta_t for computing forces

//calculate the fx & fy force of all other particles on the given particle (part) on your processer.
void force_calc(int part,float* part_arr,int np,float* fx,float* fy);

PROCESS FG_Process(int argc, char** argv)
{
  int rank,size,rc,i,j,right,left;
  int m,k,ppn,rem;
  float fx,fy;
  float mass=4.15*pow(10,16);  //mass of object

  MPI_Status recv_status;
#ifndef NO_MPE
  MPE_XGraph w;
#endif

  if(argc != 3) {
    printf("Please supply NumOfPart and TimeStep arguments.\nFor example: ring NumOfPart TimeStep");
    return 1;
  }

  //grab number of particles from command line
  m = atoi(argv[1]);
  //grab number of timesteps from command line
  k = atoi(argv[2]);

  if(m < 1 || k < 1) {
    printf("The NumOfPart or TimeStep both need to be greater than 0.\n");
    return 1;
  }

  //initilize the MPI environment
  rc=MPI_Init(&argc,&argv);
  rc=MPI_Comm_size(MPI_COMM_WORLD,&size);
  rc=MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  //if we have more processors than particles, best not to continue.
  if(m < size) {
    if (rank == 0)
        printf("ERROR: The number of particles must be at least as many as processes available\n");
    MPI_Finalize();
    return 0;
  }


  //compute rank (number) of processors to the left and right
  right=rank+1;
  if(right == size) right=0;
  left=rank-1;
  if(left == -1) left=size-1;

  /*calculate the smallest particals per node we need to cover all the particles
    if it doesn't go in evenly make sure we know the remainder for particles not
    to compute, and put PART_DNC in there so we know this is "DoNotCompute"*/
  ppn=ceil((float)m/size);
  rem=ppn*size-m;

  float part_arr[size*ppn*2],part_send[ppn*2],part_temp[ppn*2],v[ppn*2];

  //initialize random seed
  srand(time(NULL)+rank);

  //populate our particles in the front of the array, or put PART_DNC if we have uneven particles for this processor
  for(j=0;j<ppn*2;j++) {
	  /*this will distributed the uneven "non-particles" evenly throughout the nodes
	    so that each processor is doing the max computation it can*/
	  if(rank<rem && j<2) part_arr[j]=PART_DNC;
	  else part_arr[j]=rand()%grid_size;

	  //set initial velocities all to zero
	  v[j]=0;
  }

  //open the window
#ifndef NO_MPE
  rc=MPE_Open_graphics(&w,MPI_COMM_WORLD,NULL,-1,-1,grid_size,grid_size,0);
#endif

  //repeat over timesteps
  while(k > 0) {
	  //initially populate our part_send array with our own particles.
	  for(j=0;j<ppn*2;j++) part_send[j]=part_arr[j];

	  //ring communication, repeat size-1 times
	  for(i=1;i<size;i++) {

	    //break communication if send message is bigger than buffer
	    if(rank%2 == 1)
	      {
		  //odd ranks will send / recv

		  //send to the right
		  rc=MPI_Send(&part_send,ppn*2,MPI_FLOAT,right,tag_code,MPI_COMM_WORLD);
		  //receive from the left
		  rc=MPI_Recv(&part_send,ppn*2,MPI_FLOAT,left,tag_code,MPI_COMM_WORLD,&recv_status);
	      }
	    else {
	          //even ranks will recv / send

		  //receive from the left
		  rc=MPI_Recv(&part_temp,ppn*2,MPI_FLOAT,left,tag_code,MPI_COMM_WORLD,&recv_status);
		  //send to the right
		  rc=MPI_Send(&part_send,ppn*2,MPI_FLOAT,right,tag_code,MPI_COMM_WORLD);

		  for(j=0;j<ppn*2;j++) part_send[j]=part_temp[j];
              }

		  //store recv particals in our array
		  for(j=0;j<ppn*2;j++) part_arr[(i*ppn*2)+j]=part_send[j];
	  }

	  //calculate my particles (ppn) new positions in my array (first ppn*2 elements of part_arr)
	  for(i=0;i<ppn;i++) {
		  //outputs forces fx and fy.
		  force_calc(i,part_arr,size*ppn,&fx,&fy);

		  //calculate the new velocities and new positons per Wilkinson - Pg126.
		  v[i*2]=v[i*2]+fx*dt/mass;
		  v[i*2+1]=v[i*2+1]+fy*dt/mass;

		  part_arr[i*2]=part_arr[i*2]+v[i*2]*dt;
		  part_arr[i*2+1]=part_arr[i*2+1]+v[i*2+1]*dt;
	  }

#ifndef NO_MPE
	  //print to MPE if have it, do not print DNC particles, make each particle cycle through colour.
	  for(i=0;i<ppn;i++) if(PART_DNC != part_arr[i*2]) MPE_Fill_circle(w,part_arr[i*2],part_arr[i*2+1],3,(rank*ppn+i)%15+1);
	  rc=MPE_Update(w);

          MPIX_Usleep(50000); //sleep 100ms time


	  //Delete old particle, by making it white.
	  if(k > 1) for(i=0;i<ppn;i++) if(PART_DNC != part_arr[i*2]) MPE_Fill_circle(w,part_arr[i*2],part_arr[i*2+1],3,MPE_WHITE);
#endif

	  if(rank == 0) printf("\nR-timestep %d: ",k);
	  if(rank == 0) for(j=0;j<size*ppn;j++) {
		  if(PART_DNC == part_arr[j*2]) printf("NaP ");
		  else printf("(%.2f,%.2f) ",part_arr[j*2],part_arr[j*2+1]);
	  }

	  k--;
  }

#ifndef NO_MPE
  //wait for mouse click before closing the X11 window
  int mouseX,mouseY,button;

  if(rank == 0) {
    do MPE_Get_mouse_press(w,&mouseX,&mouseY,&button);
    while(mouseX==0);
  }

  rc=MPE_Close_graphics(&w);
#endif

  MPI_Finalize();
  return 0;
}


void force_calc(int part,float* part_arr,int np,float* fx,float* fy) {
	int i;
	float xdiff,ydiff,r2,F,fx_t=0,fy_t=0;
	float mass=4.15*pow(10,15);  //mass of object
	float G=6.67*pow(10,-11);  //gravitational constant

	//if the current particle is set to "DoNotCompute" just return
	if(PART_DNC == part_arr[part*2]) {
		*fx=0.0;
		*fy=0.0;
		return;
	}

	for(i=0;i<np;i++) {
		//if the comparing particle is set to "DoNotCompute" then just ignore
		if(PART_DNC != part_arr[i*2]) {
			xdiff=part_arr[i*2]-part_arr[part*2];
			ydiff=part_arr[i*2+1]-part_arr[part*2+1];

			//calculate the force, if r0 is 0, then we have no force.
			r2=pow(xdiff,2)+pow(ydiff,2);
			if(r2!=0) F=G*mass*mass/r2;
			else F=0;

			//Apply damping so when they get too close, do not blow up.
			if(F>2*pow(10,18)) F=2*pow(10,18);

			//output the components of the force, if the x or y difference is 0, we have no force, so ignore.
			if(xdiff!=0 || r2!=0) fx_t += F*xdiff/sqrt(r2);
			if(ydiff!=0 || r2!=0) fy_t += F*ydiff/sqrt(r2);
		}
	}

	*fx=fx_t;
	*fy=fy_t;

	return;
}


