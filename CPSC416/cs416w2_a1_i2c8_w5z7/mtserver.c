/*
** Multithreaded server CPSC 416 - Assignment 1
*/
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/sysinfo.h>
#include "Thread.c"
#include <assert.h>
#include <pthread.h>


pthread_rwlock_t CURRENT_CLIENTS_LOCK ;
int CURRENT_CLIENTS = 0;

void incCurrentClients()
{
    pthread_rwlock_rdlock (&CURRENT_CLIENTS_LOCK);
    CURRENT_CLIENTS ++;
    pthread_rwlock_unlock(&CURRENT_CLIENTS_LOCK);
}

void decCuurrentClients()
{
    pthread_rwlock_rdlock (&CURRENT_CLIENTS_LOCK);
    CURRENT_CLIENTS--;
    pthread_rwlock_unlock(&CURRENT_CLIENTS_LOCK);
}


void* dispatcher(void* socket_data)
{
    int socket =  *(int*) socket_data;
    free(socket_data);

    int buffer_len = 0xfff;
    char buffer[buffer_len];
    bzero(buffer, buffer_len);


    int data_head = 0;
    int data_len = 0;

    printf("Dispatcher on socket: %d\n", socket);

    int error_count;

    int sum_val = 0;
    int sum_len = 0;
    while (1)
    {
        int data_tail = (data_head + data_len) % buffer_len;
                                                                    assert(data_len < buffer_len);
                                                                    assert(data_head != data_tail || data_len == 0);
        char* data_end = buffer + data_tail;  // first free byte
        int buff_avail = (data_tail < data_head)?
            data_head - data_tail :     // distance to head
            buffer_len - data_tail;     // distance to end

        //printf("BEFORE READ\n\tdata_head\t%d\tdata_len\t%d\n\tdata_tail\t%d\tbuff_avail\t%d\n", data_head, data_len, data_tail, buff_avail);
                                                                    assert(buff_avail);
        int bytes_read = recv(socket, data_end, buff_avail, 0);
        if (bytes_read <= 0) return NULL;   // means socket closes

        data_len += bytes_read;

        //data_tail = (data_head + bytes_read) % buffer_len;
        printf("AFTER READ\tdata_read\t%d\tdata_head\t%d\tdata_len\t%d\told data_tail\t%d\tbuff_avail\t%d\n", bytes_read, data_head, data_len, data_tail, buff_avail);
        printf("BUFFER:\t%s\t%c\n\n", buffer + data_head, buffer[0]);

        int data_head_prev;
        do
        {
            data_head_prev = data_head;
            while(data_len)
            {
                char curr_char = buffer[data_head];
                if (curr_char - '0' < 0 || curr_char - '0' > 9) break;

                sum_len++;
                sum_val += curr_char - '0';

                data_len--;
                data_head = (data_head + 1) % buffer_len;
                printf("added %c to sum_acc\n", curr_char);
            }
            //printf("sum_len %d sum_val %d\n", sum_len, sum_val);

            if (data_len && sum_len)
            {
                char first_char = buffer[data_head];
                printf("branching on %c in numbers exit\n", first_char);
                if (first_char == ' ')
                {
                    uint32_t response = htonl((uint32_t) sum_val);
                    send(socket, &response, sizeof(uint32_t), 0);
                    sum_val = 0;
                    sum_len = 0;

                    data_len--;
                    data_head = (data_head + 1) % buffer_len;
                    puts("processed list sequence termination");
                    error_count = 0;
                    continue;
                } else
                {
                    while(error_count < 3 && sum_len > 0)
                    {
                        sum_len --;
                        uint32_t response = htonl((uint32_t) -1);
                        send(socket, &response, sizeof(uint32_t), 0);
                        error_count ++;
                        puts("returned -1");
                    }
                    sum_len = 0;
                    sum_val = 0;
                }
            }

            if (data_len)
            {
                char first_char = buffer[data_head];
                printf("first char is %c alpha mode\n", first_char);
                int i;
                char* command;
                int error_found;


                command = "uptime";
                error_found = 0;

                //printf("%d\n", strlen(command));
                for (i = 0; i <  strlen(command) && i < data_len; i++) // hard coded logic
                {
                    char curr_char = buffer[(data_head + i) % buffer_len];
                    printf("%d\n", i);
                    if (command[i] != curr_char)
                    {
                        error_found = 1;
                        break;
                    } else if (i == strlen(command) - 1)
                    {
                        struct sysinfo info;
                        sysinfo(&info);
                        uint32_t response = htonl((uint32_t) info.uptime);
                        send(socket, &response, sizeof(uint32_t), 0);
                        printf("UPTIME %ld\n", info.uptime);

                        data_len -= strlen(command);
                        data_head = (data_head + strlen(command)) % buffer_len;

                        error_found = 0;
                        break;
                    }
                }
                //printf("ERROR FOUND: %d\n", error_found);
                if (error_found == 0) continue;

                command = "load";
                error_found=0;
                for (i = 0; i <  strlen(command) && i < data_len; i++) // hard coded logic
                {
                    char curr_char = buffer[(data_head + i) % buffer_len];
                    if (command[i] != curr_char)
                    {
                        error_found = 1;
                        break;
                    }else if (i == strlen(command) - 1)
                    {
                        uint32_t response = htonl((uint32_t) CURRENT_CLIENTS);
                        send(socket, &response, sizeof(uint32_t), 0);
                        printf("CURRENT_CLIENTS %d\n", CURRENT_CLIENTS);

                        data_len -= strlen(command);
                        data_head = (data_head + strlen(command)) % buffer_len;

                        error_found = 0;
                        break;
                    }
                }
                // printf("ERROR FOUND: %d\n", error_found);
                if (error_found == 0) continue;

                command = "exit";
                error_found = 0;
                for (i = 0; i <  strlen(command) && i < data_len; i++) // hard coded logic
                {
                    char curr_char = buffer[(data_head + i) % buffer_len];
                    if (command[i] != curr_char)
                    {
                        error_found = 1;
                        break;
                    }else if (i == strlen(command) - 1)
                    {
                        uint32_t response = 0;
                        send(socket, &response, sizeof(uint32_t), 0);
                        decCuurrentClients();
                        close(socket);

                        printf("EXITED CURRENT_CLIENTS %d\n", CURRENT_CLIENTS);
                        return NULL;
                    }
                }

                printf("ERROR FOUND: %d\n", error_found);
                if (error_found)
                {
                    uint32_t response = htonl((uint32_t) -1);
                    send(socket, &response, sizeof(uint32_t), 0);
                    error_count++;

                    puts("returned -1");

                    data_len --;
                    data_head = (data_head + 1) % buffer_len;
                }
            }

            if (error_count >= 3)
            {
                close(socket);
                printf("EXITED CURRENT_CLIENTS %d\n", CURRENT_CLIENTS);
                return NULL;
            }
        } while (data_len && data_head_prev != data_head);
    }
}

void spawn_worker(int socket)
{
    int *socket_data = malloc(sizeof(int));
    *socket_data = socket;

    struct Thread *worker = createThread(dispatcher, socket_data);
    runThread(worker, NULL);
}

int main(int argc, char *argv[])
{
    /* error checking arguements */
    if (argc != 3)
    {
        fprintf(stderr, "%s\n", "Usage: mtserver must accept 2 arguements");
        return 1;
    }

    int MAX_CLIENTS = atoi(argv[1]);
    int PORT = atoi(argv[2]);

    if (MAX_CLIENTS < 1)
    {
        fprintf(stderr, "%s\n", "Usage: please enter a valid MAX_CLIENT");
        return 1;
    }

    int listenfd = 0;
    struct sockaddr_in serv_addr, client_addr;
    socklen_t client_len;

    //printf("AF_INET %d\n", AF_INET);
    // create MAIN THREAD SOCKET
    if((listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        fprintf(stderr, "%s\n", "Error : Could not create socket");
        return 1;
    }

    //printf("listenfd %d\n", listenfd);
    // create serv_addr to bind with lsten_fd socket
    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(PORT);

    if (bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
    {
        fprintf(stderr, "%s\n", "Error: Could not bind to serv_addr");
    }


    listen(listenfd, MAX_CLIENTS);

    while(1)
    {
        int clientfd = 0;
        // initialize sendbuffer
        char sendBuff[1024];
        memset(&sendBuff, '0', sizeof(sendBuff));

        client_len = sizeof(client_addr);

        // will stall on accept for listenfd to receive a client
        if ((clientfd = accept(listenfd, (struct sockaddr *)&client_addr, &client_len)) < 0)
        {
            printf("%d\n", clientfd);
            fprintf(stderr, "%s\n", "Error: Could not connect to client");

        } else if (CURRENT_CLIENTS < MAX_CLIENTS)
        {

            printf("clientfd %d\n", clientfd);
            incCurrentClients();
            spawn_worker(clientfd);
        }

     }

    return 0;
}