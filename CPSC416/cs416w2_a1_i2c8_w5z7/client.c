
/*
* Sample client connecting to a socket server
* Borrowed from: http://www.thegeekstuff.com/2011/12/c-socket-programming/
*/

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h> 

int main(int argc, char *argv[])
{
    int sockfd = 0, n = 0, read_size;
    char recvBuf[1024];
    int received = 0;

    struct sockaddr_in serv_addr; 

    if(argc < 2)
    {
        printf("\n Usage: %s <ip of server> \n",argv[0]);
        return 1;
    } 

    memset(recvBuf, '0',sizeof(recvBuf));
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Error : Could not create socket \n");
        return 1;
    } 

    memset(&serv_addr, '0', sizeof(serv_addr)); 

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(atoi(argv[2])); 

    if(inet_pton(AF_INET, argv[1], &serv_addr.sin_addr)<=0)
    {
        printf("\n inet_pton error occured\n");
        return 1;
    } 

    if( connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
     printf("\n Error : Connect Failed \n");
     return 1;
    } 


    // Send Requests to Server

    char *requests[5] = {"uptime", "load", "1234 ", "somethingelse", "exit"};

    for (int i = 0; i < 5; i++) {
        char *request = requests[i];
        int len = strlen(request);
        printf("Request being sent: %s  with len = %d \n", request, len);

        send(sockfd, request, len, 0);
        sleep(1);
    }

/*
    char *request = "uptime";
    int len = strlen(request); 
    send(sockfd, request, len, 0);
*/


    while( (read_size = recv(sockfd , &received, sizeof(received), 0)) > 0 ) {
       // recvBuf[read_size] = '\0'; // append at the end to indicate end of request msg
	printf("Received: %d\n", ntohl(received));
    }

	printf("Server socket is closed now \n");
    return 0;
 }
