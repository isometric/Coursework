
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "msg.h"

#define READY    0
#define ELECTING 1

FILE *slog; 
pthread_rwlock_t NODE_STATE_LOCK;
int NODE_STATE = 0;
int PORT;
struct coordinator COORDINATOR;
char* group_nodes[MAX_NODES];
int node_count;

struct coordinator
{
  char* coord_ip;
  int coord_port;
};

void usage(char * cmd) 
{
  printf("usage: %s  portNum groupFileList logFile timeoutValue averageAYATime failureProbability \n",cmd);
}

void removeChar(char *str, char garbage) {

    char *src, *dst;
    for (src = dst = str; *src != '\0'; src++) {
        *dst = *src;
        if (*dst != garbage) dst++;
    }
    *dst = '\0';
}

void split_node_info(char* nodes_d[], char* node_info)
{
  char* delim = " ";
  char* token = strtok(node_info, delim);

  int i;
  for (i = 0; i < 2; i++)
  {
    nodes_d[i] = strdup(token);
    token = strtok(0, delim);
  }
}

void send_message(int sockfd, char* node_ip, int node_port, struct msg message)
{
   struct sockaddr_in addr;
   socklen_t addr_len;

   addr.sin_family = AF_INET; 
   addr.sin_addr.s_addr = inet_addr(node_ip); 
   addr.sin_port = htons(node_port);

   addr_len = sizeof(addr);   

   int n;
   if( (n = sendto(sockfd, &message, sizeof(struct msg), 0, (struct sockaddr *)&addr, addr_len)) < 0)
   {
     fprintf(stderr, "Node: %s %d failed sending message: %d\n", node_ip, node_port, message.msgID);
   }
}

void call_election(int sockfd)
{
  int j;
  for (j= 0; j < node_count; j++)
  {
    char* nodes_d[2];
    split_node_info(nodes_d, group_nodes[j]); 
    char* node_ip =  nodes_d[0];
    char* node_port = nodes_d[1];

    struct msg m;
    m.msgID = 10; 
    send_message(sockfd, node_ip, atoi(node_port), m);
  }
}

int read_nodes_from_file(char* group_nodes[], char* filename)
{
  int count = 0;

  FILE *file = NULL;
  file = fopen(filename, "r");
  char line[256];
  
  if (file != NULL)
  {
    while (fgets(line, sizeof(line), file))
    {
      printf("line: %s", line);
      char* node = strdup(line);
      if ( (node[0] != '\0') && (node[0] != ' ') && (node[0] != '\n'))
      {
        removeChar(node, '\n');
        group_nodes[count] = node;
        count++;
      }
      else
      {
        break;
      }
    }
    fclose(file);
  }
  else
  {
    fprintf(stderr, "%s\n", "Error: Invalid file");
    exit(0);
  }

  return count;
}

void shivizLog(char* log_entry)
{
  // OK - always write to same log file 
  if (slog == NULL)
  {
    printf("Error opening log\n");
    exit(1);
  }

  fprintf(slog, "%s", log_entry);
}

void fill_vector_clock(struct clock vectorclock[])
{
  int i;
    for (i = 0; i < MAX_NODES; ++i)
    {
 
            vectorclock[i].nodeId = (unsigned int) i;
            vectorclock[i].time = (unsigned int) 10+i;
        
    }
}

int main(int argc, char ** argv) {

  // This is some sample code feel free to delete it
  
  int  port;
  char *         groupListFileName;
  char *         logFileName;
  unsigned long  timeoutValue;
  unsigned long  AYATime;
  unsigned long  myClock = 1;
  unsigned long  sendFailureProbability;

  char * end;
  int err = 0;

  port = strtoul(argv[1], &end, 10);
  if (argv[1] == end) {
    printf("Port conversion error\n");
    err++;
  }

  printf("Port number:              %d\n", port);

  printf("Starting up Node %d\n", port);
  PORT = port;
  slog = fopen("ShiVizLog.dat", "a");

  char start_log[80];
  sprintf(start_log, "Starting up Node %d\n", port);
  shivizLog(start_log);

  if (err) {
    printf("%d conversion error%sencountered, program exiting.\n",
     err, err>1? "s were ": " was ");
    return -1;
  }

  /***************************** UDP Socket setup ***************************/

  int listenfd = 0;
  struct sockaddr_in myaddr;
  socklen_t myaddr_len;


  // create NODE SOCKET
  if((listenfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
  {
      fprintf(stderr, "%s\n", "Error : Could not create socket");
      return 1;
  }

  memset((char *)&myaddr, 0, sizeof(myaddr));

  myaddr.sin_family = AF_INET; 
  myaddr.sin_addr.s_addr = INADDR_ANY; 
  myaddr.sin_port = htons(port);

  myaddr_len = sizeof(myaddr);

  if (bind(listenfd, (struct sockaddr*)&myaddr, sizeof(myaddr)) < 0)
  {
      fprintf(stderr, "%s\n", "Error: Could not bind to myaddr");
  }

  myaddr.sin_family = AF_INET; 
  myaddr.sin_addr.s_addr = INADDR_ANY; 
  myaddr.sin_port = htons(port);

  while(1)
  {
    struct msg m;
    m.msgID = ELECT;
    m.electionID = (unsigned int) 10;
    fill_vector_clock(m.vectorClock);

    send_message(listenfd, "127.0.0.1", 1902, m);
    // send_message(listenfd, "127.0.0.1", 1885, m);
    /*
    int n;
    if( (n = sendto(listenfd, &m, sizeof(struct msg), 0, (struct sockaddr *)&myaddr, myaddr_len)) < 0)
    {
        fprintf(stderr, "Node: %d failed sending message: %d\n", PORT, m.msgID);
    }*/
  }

}