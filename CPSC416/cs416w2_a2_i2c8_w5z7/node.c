
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <arpa/inet.h>
#include <poll.h>
#include <time.h>
#include <assert.h>
#include <math.h>
#include "msg.h"

struct coordinator
{
    char* coord_ip;
    int coord_port;
};

int PORT;
struct clock my_clock;
struct coordinator COORDINATOR;
char* group_nodes[MAX_NODES];
int node_count;
int WAITING_AYA = 0;
struct clock  myVectorClock[MAX_NODES];
unsigned int election_id[9];
int election_candidate[9] = {-1,-1,-1,-1,-1,-1,-1,-1,-1};
int election_time[9];
unsigned long time_last_coord_iaa;
unsigned long time_last_coord_elect;
int PASS_FIRST;
char* LOG_FILE;

void usage(char * cmd)
{
    printf("usage: %s  portNum groupFileList logFile timeoutValue averageAYATime failureProbability \n",cmd);
}

char* message_type(msgType m)
{
  char *p;
  switch (m) {
      case ELECT:
        p = "ELECT";
        break;
      case ANSWER:
        p = "ANSWER";
        break;
      case COORD:
        p = "COORD";
        break;
      case AYA:
        p = "AYA";
        break;
      case IAA:
        p = "IAA";
        break;

  }

  return p;
}

void remove_char(char *str, char garbage) {

        char *src, *dst;
        for (src = dst = str; *src != '\0'; src++) {
                *dst = *src;
                if (*dst != garbage) dst++;
        }
        *dst = '\0';
}

void shivizLog(char* log_entry)
{
    // OK - always write to same log file
    FILE* slog = fopen(LOG_FILE, "a");
    if (slog == NULL)
    {
        printf("Error opening log\n");
        exit(1);
    }

    fprintf(slog, "%s", log_entry);
    fclose(slog);
}

void split_node_info(char* nodes_d[], char* node_info)
{
    char* delim = " ";
    char* token = strtok(node_info, delim);

    int i;
    for (i = 0; i < 2; i++)
    {
        nodes_d[i] = strdup(token);
        token = strtok(0, delim);
    }
}

char* append(char* s1, char* s2)
{
    char *result = malloc(strlen(s1)+strlen(s2)+1);//+1 for the zero-terminator
    //in real code you would check for errors in malloc here
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

char* log_vector_time(char* string, struct clock vectorclock[])
{
    int i;
    char* s = string;
    for (i = 0; i < MAX_NODES; i++)
    {
        if(vectorclock[i].nodeId > 0)
        {
            char node[64];
            sprintf(node, "\"N%u\" : %u, ", vectorclock[i].nodeId, vectorclock[i].time);
            s = append(s, node);
        }
        else
        {
            continue;
        }
    }

    s[strlen(s)-2] = '\0';
    s = append(s, "}\n");

    return s;

}

unsigned int find_time_in_vectorclock(struct clock vectorclock[], unsigned int nodeID)
{
    int i;
    for (i = 0; i < MAX_NODES; i++)
    {
        if (vectorclock[i].nodeId == nodeID)
        {
            return vectorclock[i].time;
        }
    }

    return (unsigned int) 0;
}

int add_to_local_vector_clock(unsigned int nodeID, unsigned int utime)
{

    int i;
    for (i = 0; i < MAX_NODES; i++)
    {
        if (myVectorClock[i].nodeId == 0)
        {
            myVectorClock[i].nodeId = nodeID;
            myVectorClock[i].time = utime;
            printf("Added node:%u\tTime:%u\n", nodeID, utime);
            return i;
        }
    }

    return -1;

}

int update_local_vetor_clock(unsigned int nodeID, unsigned int utime)
{
    int i;
    for (i = 0; i < MAX_NODES; i++)
    {
        if (myVectorClock[i].nodeId == nodeID)
        {
            myVectorClock[i].time = utime;
            printf("Updated node:%u\tTime:%u\n", nodeID, utime);
            return i;
        }
    }

    return -1;
}

void assign_msg_vectorclock(struct clock vectorclock[])
{
    int i;
    for (i = 0; i < MAX_NODES; ++i)
    {
        if (myVectorClock[i].nodeId != 0)
        {
            vectorclock[i].nodeId = myVectorClock[i].nodeId;
            vectorclock[i].time = myVectorClock[i].time;
        }
        else
        {
            vectorclock[i].nodeId = 0;
            vectorclock[i].time = 0;
        }
    }
}

void fill_vector_clock(struct clock vectorclock[])
{
  int i;
    for (i = 0; i < MAX_NODES; ++i)
    {
        vectorclock[i].nodeId = (unsigned int) 0;
        vectorclock[i].time = (unsigned int) 0;
    }
}


void send_message(int sockfd, char* node_ip, int node_port, struct msg message)
{
     struct sockaddr_in addr;
     socklen_t addr_len;

     addr.sin_family = AF_INET;
     addr.sin_addr.s_addr = inet_addr(node_ip);
     addr.sin_port = htons(node_port);

     addr_len = sizeof(addr);

     // incerement vector own clock to send
     my_clock.time += (unsigned int) 1;
     update_local_vetor_clock(PORT,my_clock.time);
     assign_msg_vectorclock(message.vectorClock);

     if (sendto(sockfd, &message, sizeof(struct msg), 0, (struct sockaddr *)&addr, addr_len) < 0)
     {
         fprintf(stderr, "Node: %s %d failed sending message: %u\n", node_ip, node_port, message.msgID);
     }


     char log_msg[256];
     char temp[25];
     sprintf(temp, "N%d {", PORT);
     char* slog_v_time = log_vector_time(temp, message.vectorClock);
     sprintf(log_msg, "Sending %s to N%d\n", message_type(message.msgID), node_port);
     shivizLog(log_msg);
     shivizLog(slog_v_time);

     printf("Node: %s %d sent message: %u\n", node_ip, node_port, message.msgID);

}

int get_election_index(unsigned int electionId)
{
    int i;
    //printf("election id %u\n", electionId);
    for (i = 0; i < 9; ++i) {
      //  printf("%u\n", election_id[i]);
        if(election_id[i] == electionId) return i;
    }

    char log_msg[64];
    sprintf(log_msg, "Election ID %u not found\n", electionId);
    shivizLog(log_msg);

    return -1;
}

int get_election_free_index()
{
    int i;
    for (i = 0; i < 9; ++i)
        if(election_candidate[i] < 0) return i;
    return -1;
}

long get_time_milis() {
    struct timespec spec;
    clock_gettime(CLOCK_REALTIME, &spec);

    return round(spec.tv_sec * 1000 + spec.tv_nsec / 1.0e6);
}

void elect_superior_nodes(int sockfd, unsigned int election_id)
{
    time_last_coord_elect = get_time_milis();
    int j;
    for (j= 0; j < node_count; j++)
    {
        puts("elect_loop");
        char* nodes_d[2];
        split_node_info(nodes_d, group_nodes[j]);
        char* node_ip =  nodes_d[0];
        int node_port = atoi(nodes_d[1]);

        if (node_port <= PORT || node_port <= 0)
        {
            continue;
        }

        struct msg m;
        m.msgID = ELECT;
        m.electionID = election_id;

        send_message(sockfd, node_ip, node_port, m);
    }
}

void broadcast_coord(int sockfd, unsigned int electionId)
{
    struct msg m;
    m.msgID = COORD;
    m.electionID = electionId;

    int j;
    for (j = 0; j < node_count; j++) {
        char* nodes_d[2];
        split_node_info(nodes_d, group_nodes[j]);
        char* node_ip =  nodes_d[0];
        int node_port = atoi(nodes_d[1]);

        if (node_port == PORT || node_port <= 0) continue;

        send_message(sockfd, node_ip, node_port, m);
    }

}

int read_nodes_from_file(char* group_nodes[], char* filename)
{
    int count = 0;

    FILE *file = NULL;
    file = fopen(filename, "r");

    if (file != NULL)
    {
        char line[256];
        while (fgets(line, sizeof(line), file))
        {
            printf("line: %s", line);
            char* node = strdup(line);
            if ( (node[0] != '\0') && (node[0] != ' ') && (node[0] != '\n'))
            {
                remove_char(node, '\n');
                group_nodes[count] = node;
                count++;
            }
            else
            {
                break;
            }
        }
        fclose(file);
    }
    else
    {
        fprintf(stderr, "%s\n", "Error: Invalid file");
        exit(0);
    }

    return count;
}


// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    else {
        return &(((struct sockaddr_in6*)sa)->sin6_addr);
    }
}


char *trimwhitespace(char *str)
{
  char *end;

  // Trim leading space
  while(isspace(*str)) str++;

  if(*str == 0)  // All spaces?
    return str;

  // Trim trailing space
  end = str + strlen(str);
  while(end > str && isspace(*end)) end--;

  // Write new null terminator
  *(end+1) = 0;

  return str;
}

int main(int argc, char ** argv) {

    // This is some sample code feel free to delete it

    int  port;
    char *         groupListFileName;
    char *         logFileName;
    unsigned long  timeoutValue;
    unsigned long  AYATime;
    unsigned long  sendFailureProbability;

    if (argc != 7)
    {
        usage(argv[0]);
        return -1;
    }

    char * end;
    int err = 0;

    port = strtoul(argv[1], &end, 10);
    if (argv[1] == end) {
        printf("Port conversion error\n");
        err++;
    }

    groupListFileName = argv[2];
    logFileName       = argv[3];
    timeoutValue      = strtoul(argv[4], &end, 10);

    if (argv[4] == end) {
        printf("Timeout value conversion error\n");
        err++;
    }

    AYATime  = strtoul(argv[5], &end, 10);
    if (argv[5] == end) {
        printf("AYATime conversion error\n");
        err++;
    }

    sendFailureProbability  = strtoul(argv[6], &end, 10);
    if (argv[5] == end) {
        printf("sendFailureProbability conversion error\n");
        err++;
    }

    printf("Port number:              %d\n", port);
    printf("Group list file name:     %s\n", groupListFileName);
    printf("Log file name:            %s\n", logFileName);
    printf("Timeout value:            %lu\n", timeoutValue);
    printf("AYATime:                  %lu\n", AYATime);
    printf("Send failure probability: %lu\n", sendFailureProbability);

    printf("Starting up Node %d\n", port);
    PORT = port;
    LOG_FILE = logFileName;
    // TODO


    if (err) {
        printf("%d conversion error%sencountered, program exiting.\n",
         err, err>1? "s were ": " was ");
        return -1;
    }

    // Init own vector clock
    my_clock.nodeId = PORT;
    my_clock.time = (unsigned int) 1;

    fill_vector_clock(myVectorClock);

    char start_log[128];
    char start_vector_log[128];
    sprintf(start_log, "Starting up Node %d\n", PORT);
    sprintf(start_vector_log, "N%d {N%d : %u}\n", PORT, PORT, my_clock.time);
    char* trimmed_slog = trimwhitespace(start_log);
    shivizLog(trimmed_slog);
    shivizLog(start_vector_log);

    /* add self to locl vector clock*/
    add_to_local_vector_clock(PORT, (unsigned int) 1);

    /***************************** UDP Socket setup ***************************/

    int listenfd = 0;
    struct sockaddr_in myaddr;
    socklen_t myaddr_len;

    printf("groupListFileName: %s\n", groupListFileName);
    node_count = read_nodes_from_file(group_nodes, groupListFileName);

    int k;
    for (k = 0; k < node_count; k++)
    {
        printf("group_nodes[i]: %s\n", group_nodes[k]);
    }

    // create NODE SOCKET
    if((listenfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
    {
            fprintf(stderr, "%s\n", "Error : Could not create socket");
            return 1;
    }

    memset((char *)&myaddr, 0, sizeof(myaddr));

    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr = INADDR_ANY;
    myaddr.sin_port = htons(port);

    myaddr_len = sizeof(myaddr);

    if (bind(listenfd, (struct sockaddr*)&myaddr, sizeof(myaddr)) < 0)
    {
            fprintf(stderr, "%s\n", "Error: Could not bind to myaddr");
    }

    /*Initialize COORDINATOR*/
    // TODO @James CHECK
    COORDINATOR.coord_port = -1;
    COORDINATOR.coord_ip = malloc(INET6_ADDRSTRLEN);
    memset(COORDINATOR.coord_ip,0,sizeof(&COORDINATOR.coord_ip));

    char recv_ip_global[INET6_ADDRSTRLEN];
    int recv_port_global = 0;

    struct pollfd fds[1];
    fds[0].fd = listenfd;
    fds[0].events = POLLIN;

    /****************** INIT LOOP for COORD or ELECT *******************/
    time_last_coord_iaa = get_time_milis();
    while (1) {
        int pollRet = poll(fds, 1, 0);

        long current_time = get_time_milis();
        long elapsed_time = current_time - time_last_coord_iaa;
        if (elapsed_time/1000 >= timeoutValue) {
            puts("start election");
            elect_superior_nodes(listenfd, (unsigned int) (rand()+current_time)%(3*PORT));

            int election_index = get_election_free_index();;
            election_id[election_index] = (rand()+current_time)%(3*PORT);
            printf("Starting election %u, index %d \n", election_id[election_index], election_index);
            assert(get_election_index(election_id[election_index] == election_index));
            election_time[election_index] = get_time_milis();
            election_candidate[election_index] = 1;

            time_last_coord_iaa = get_time_milis();

            //  wait to find out who is COORDINATOR before breaking to next loop
            break;
        } else if (pollRet > 0) {

            struct msg recv_message;
            if( recvfrom(listenfd, &recv_message, sizeof(struct msg), 0, (struct sockaddr *)&myaddr, &myaddr_len) < 0)
            {
                fprintf(stderr, "%s\n", "Error receiving message");
            }
            char recv_ip[INET6_ADDRSTRLEN];
            inet_ntop(myaddr.sin_family, get_in_addr((struct sockaddr *)&myaddr),
                                recv_ip, sizeof recv_ip);
            int recv_port = ntohs( (myaddr.sin_port) );

            printf("Received: %d from %s\t%d\n", recv_message.msgID, recv_ip, recv_port);

            strcpy(recv_ip_global, recv_ip);
            recv_port_global = recv_port;

            /*check local vector clock with one received from message*/
            unsigned int msg_node_time = find_time_in_vectorclock(recv_message.vectorClock, recv_port);
            unsigned int self_node_time = find_time_in_vectorclock(recv_message.vectorClock, PORT);
            puts("1");
            /*update recv_node time on local vector clock*/
            if(update_local_vetor_clock((unsigned int)recv_port, msg_node_time) <  0)
            {
                // have not added node to vector clock yet
                add_to_local_vector_clock((unsigned int) recv_port, msg_node_time);
            }

            /*Log receiving message*/
            int l;
            for (l = 0; l < MAX_NODES; ++l)
            {
                printf("M VectorClock: %u\t%u\n", recv_message.vectorClock[l].nodeId, recv_message.vectorClock[l].time);
            }

            char log_msg[256];
            char temp[32];
            sprintf(log_msg, "Receive %s from N%d\n", message_type(recv_message.msgID), recv_port);
            char* trimmed_log_msg = trimwhitespace(log_msg);
            sprintf(temp, "N%d {", recv_port);
            char* trimmed_temp = trimwhitespace(temp);
            char* slog_v_time = log_vector_time(trimmed_temp, recv_message.vectorClock);
            shivizLog(trimmed_log_msg);
            shivizLog(slog_v_time);
            puts("2");
            if (self_node_time <= my_clock.time)
            {
                update_local_vetor_clock(recv_port, msg_node_time);
            }else
            {
                 // log if wrong
                 char log_msg[256];
                 char temp[25];
                 sprintf(log_msg, "Received incorrect vector clock from N%d\n", recv_port);
                 sprintf(temp, "N%d {", PORT);
                 my_clock.time += (unsigned int) 1;
                 update_local_vetor_clock(PORT, my_clock.time);
                 char* slog_v_time = log_vector_time(temp, myVectorClock);
                 shivizLog(log_msg);
                 shivizLog(slog_v_time);
            }

            if (recv_message.msgID == ELECT) {
                puts("recv elect");
                if (recv_port < PORT) {
                    struct msg m;
                    m.msgID = ANSWER;
                    m.electionID = recv_message.electionID;

                    int i;
                    for (i = 0; i < MAX_NODES; ++i)
                    {
                        printf("M Vector CLOCK: %u\t%u\n", m.vectorClock[i].nodeId, m.vectorClock[i].time);
                    }

                    send_message(listenfd, recv_ip, recv_port, m);
                }
                elect_superior_nodes(listenfd, recv_message.electionID);
                if (get_election_index(recv_message.electionID) < 0) {
                    int election_index = get_election_free_index();
                    election_id[election_index] = recv_message.electionID;
                    election_time[election_index] = get_time_milis();
                    election_candidate[election_index] = 1;
                }
            } else if (recv_message.msgID == COORD) {
                if (recv_port < PORT) {
                    elect_superior_nodes(listenfd, recv_message.electionID);

                    int election_index = get_election_free_index();
                    election_id[election_index] = (rand()+current_time)%(3*PORT);
                    election_time[election_index] = get_time_milis();
                    election_candidate[election_index] = 1;
                    break;
                }
                // TODO @James CHECK
                COORDINATOR.coord_ip = recv_ip_global;
                COORDINATOR.coord_port = recv_port_global;
                break;
            }
        } else {
            usleep(200 * 1000);
        }
    }
    puts("exiting init loop");

/*******************************BEGIN MAIN LOOP***********************************************/
    while (1) {
        long current_time = get_time_milis();
        //printf("elapsed_time %lu\n", current_time - time_last_coord_iaa);

        if ((current_time - time_last_coord_iaa)/1000 >= AYATime) {
            // TODO: make first round with invalid coord do something else
            //printf("COORDINATOR.coord_ip:%s\t, COORDINATOR.coord_port%d\n", COORDINATOR.coord_ip, COORDINATOR.coord_port);
            if (COORDINATOR.coord_port >= 0) {
                struct msg aya;
                aya.msgID = AYA;
                aya.electionID = PORT;

                send_message(listenfd, COORDINATOR.coord_ip, COORDINATOR.coord_port, aya);
                puts("pls make it here");
            }
        }

        int i;
        for (i = 0; i < 9; i ++) {
            if ((current_time - election_time[i])/1000 > timeoutValue) {
                if (election_candidate[i]  == PORT) {
                    printf("election candid i: %u\n", election_candidate[i]);
                    assert(election_candidate[i] == PORT);
                    puts("assume coord");
                    // TODO @James CHECK
                    COORDINATOR.coord_port = election_candidate[i];
                    broadcast_coord(listenfd, election_id[i]);
                    election_candidate[i] = -1;
                }

            }
        }

        int pollRet = poll(fds, 1, 0);
        if (pollRet) printf("pollRet %d\n", pollRet);
        if (pollRet == 0) {
          usleep(200 * 1000);
        } else if (pollRet > 0) {

            struct msg recv_message;
            if( recvfrom(listenfd, &recv_message, sizeof(struct msg), 0, (struct sockaddr *)&myaddr, &myaddr_len) < 0)
            {
                fprintf(stderr, "%s\n", "Error receiving message");
            }
            char recv_ip[INET6_ADDRSTRLEN];
            inet_ntop(myaddr.sin_family, get_in_addr((struct sockaddr *)&myaddr),
                                recv_ip, sizeof recv_ip);
            int recv_port = ntohs( (myaddr.sin_port) );

            printf("Received: %d from %s\t%d\n", recv_message.msgID, recv_ip, recv_port);

            strcpy(recv_ip_global, recv_ip);
            recv_port_global = recv_port;

            /*check local vector clock with one received from message*/
            unsigned int msg_node_time = find_time_in_vectorclock(recv_message.vectorClock, recv_port);
            unsigned int self_node_time = find_time_in_vectorclock(recv_message.vectorClock, PORT);

            /*Log receiving message*/
            char log_msg[256];
            char temp[32];
            sprintf(log_msg, "Receive %s to N%d\n", message_type(recv_message.msgID), recv_port);
            //printf("Receive %s from N%d\n", message_type(recv_message.msgID), recv_port);

            char* trimmed_log_msg = trimwhitespace(log_msg);
            sprintf(temp, "N%d {", PORT);
            char* trimmed_temp = trimwhitespace(temp);
            char* slog_v_time = log_vector_time(trimmed_temp, recv_message.vectorClock);
            shivizLog(trimmed_log_msg);
            shivizLog(slog_v_time);

            if (self_node_time <= my_clock.time)
            {
                update_local_vetor_clock(recv_port, msg_node_time);
            }else
            {
                 // log if wrong
                 char log_msg[256];
                 char temp[25];
                 sprintf(log_msg, "Received incorrect vector clock from N%d\n", recv_port);
                 sprintf(temp, "N%d {", PORT);

                 my_clock.time += (unsigned int) 1;
                 update_local_vetor_clock(PORT, my_clock.time);
                 char* slog_v_time = log_vector_time(temp, myVectorClock);

                 shivizLog(log_msg);
                 shivizLog(slog_v_time);
            }

            struct msg to_send;
            puts("processing message");
            switch (recv_message.msgID) {
                case ELECT:
                    // start timer for election
                    // if times out before election Declare ourself leader send a COORD message
                    puts("ELECT");
                    if (get_election_index(recv_message.electionID) < 0) {
                        puts("adding self as candidate");
                        int election_index = get_election_free_index();;
                        election_id[election_index] = (rand()+current_time)%(3*PORT);
                        election_time[election_index] = get_time_milis();
                        election_candidate[election_index] = 1;
                    }
                    if (PORT > recv_port) {
                        puts("send asnwer");
                        struct msg m;
                        m.msgID = ANSWER;
                        m.electionID = recv_message.electionID;
                        send_message(listenfd, recv_ip, recv_port, m);
                    }
                    puts("electing superiors");
                    elect_superior_nodes(listenfd, recv_message.electionID);
                    break;
                case ANSWER:
                    puts("ANSER");
                    if (get_election_index(recv_message.electionID) < 0) {
                        // TODO log, this shouldn't happened
                        printf("not a candidate in election %u", recv_message.electionID);
                    } else {
                        puts("removing self as candidate");
                        election_candidate[get_election_index(recv_message.electionID)] = -1;
                    }
                    break;
                case COORD:
                    // if we get a COORD message that has declares a coordinate < than our port # call election
                    puts("recv coord---------------");
                    if (recv_port < PORT) {
                        puts("userp coord");
                        elect_superior_nodes(listenfd, recv_message.electionID);

                        break;
                    }

                    if (get_election_index(recv_message.electionID) >= 0) {
                        // TODO log error, still valid candidate for this node
                        // incerement vector own clock to err

                         my_clock.time += (unsigned int) 1;
                         update_local_vetor_clock(PORT,my_clock.time);
                         char* slog_v_time = log_vector_time(temp, recv_message.vectorClock);

                         char log_msg[256];
                         char temp[25];
                         sprintf(temp, "N%d {", PORT);
                         sprintf(log_msg, "Error: Receive %s from N%d when still a valid candidate\n", message_type(COORD), recv_port);
                         shivizLog(log_msg);
                         shivizLog(slog_v_time);
                    }
                    // TODO @James CHECK
                    strcpy(COORDINATOR.coord_ip, recv_ip_global);
                    COORDINATOR.coord_port = recv_port_global;
                    break;
                case AYA:
                    // This node is the COORD
                    // Reply back with IAA to node that sent AYA message
                    if (COORDINATOR.coord_port == PORT) { // if we are the coordinate send IAA message
                        /* code */
                        to_send.msgID = IAA;
                        to_send.electionID = PORT;

                        send_message(listenfd, recv_ip, recv_port, to_send);
                    } else {
                        // TODO: log

                        // incerement vector own clock to err
                         my_clock.time += (unsigned int) 1;
                         update_local_vetor_clock(PORT,my_clock.time);
                         char* slog_v_time = log_vector_time(temp, recv_message.vectorClock);

                         char log_msg[256];
                         char temp[25];
                         sprintf(temp, "N%d {", PORT);
                         sprintf(log_msg, "Error: Receive %s from N%d when not COORDINATOR\n", message_type(AYA), recv_port);
                         shivizLog(log_msg);
                         shivizLog(slog_v_time);
                    }

                    break;
                case IAA:
                    // reset clock
                    if(COORDINATOR.coord_port != recv_port) { // Error receive IAA not from the coordinator
                        elect_superior_nodes(listenfd, recv_message.electionID);
                        int election_index = get_election_free_index();;
                        election_id[election_index] = (rand()+current_time)%(3*PORT);
                        election_time[election_index] = get_time_milis();
                        election_candidate[election_index] = 1;
                    } else {
                        time_last_coord_iaa = get_time_milis();
                    }

                    break;
            }
            puts("finished processing message");

        } else {
            fprintf(stderr, "UDP Poll error");
        }
    }
}
