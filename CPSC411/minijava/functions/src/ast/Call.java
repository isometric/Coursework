package ast;

import visitor.Visitor;

public class Call extends Expression {
	
	public final String name;
	public final NodeList<Expression> args;
	
	public Call(String name, NodeList<Expression> args) {
		super();
		this.name = name;
		this.args = args;
	}
	

	@Override
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}

}
