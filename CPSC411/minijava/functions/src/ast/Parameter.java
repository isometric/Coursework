package ast;

import visitor.Visitor;

public class Parameter extends AST {
	
	public final Type paramType;
	public final String name;
	
	public Parameter(Type paramType, String name) {
		this.paramType = paramType;
		this.name = name;
	}
	

	@Override
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}

}
