package typechecker;

import ast.NodeList;
import ast.Parameter;
import ast.Type;
import util.ImpTable;

public class FunctionSymbol {
	
	public final Type retType;
	public String stringRep;
	public final NodeList<Parameter> params;
	public final ImpTable<Type> localVariables;
	
	public FunctionSymbol(Type retType, String name, NodeList<Parameter> params) {
		this.retType = retType;
		this.params = params;
		localVariables = new ImpTable<Type>();
		setRep(name);
	}

	private void setRep(String name) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(retType.toString());
		buffer.append(" ");
		buffer.append(name);
		buffer.append("(");
		
		if(params.size() > 0){
			buffer.append(params.elementAt(0).paramType.toString());
			buffer.append(" ");
			buffer.append(params.elementAt(0).name);
		}
		
		for(int i = 1; i < params.size(); i++){
			buffer.append(" ,");
			buffer.append(params.elementAt(i).paramType.toString());
			buffer.append(" ");
			buffer.append(params.elementAt(i).name);
		}
		
		buffer.append(")");
		this.stringRep = buffer.toString();
	}
	
	@Override
	public String toString() {
		return stringRep;
	}

}
