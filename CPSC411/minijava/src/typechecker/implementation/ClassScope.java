package typechecker.implementation;

import ast.Type;
import util.ImpTable;
import util.ImpTable.DuplicateException;

public class ClassScope extends Scope {
	
	public ImpTable<Type> fieldTable;
	public ImpTable<MethodScope> methodScopes;
	public ClassScope parent;
	
	
	public ClassScope(ClassScope parent) {
		this.parent = parent;
		this.fieldTable = new ImpTable<Type>();
		this.methodScopes = new ImpTable<MethodScope>();
	}
	
	public Type lookupField(String name) {
		Type t;
		t = fieldTable.lookup(name);
		if (t != null)
			return t;
		if (parent != null)
			t = parent.lookupField(name);
		return t;
	}
	
	
	public MethodScope lookupMethod(String name) {
		MethodScope t;
		t = methodScopes.lookup(name);
		if (t != null)
			return t;
		if (parent != null)
			t = parent.lookupMethod(name);
		return t;
	}
	
	
	public void defField(String name, Type value) throws DuplicateException{
		fieldTable.put(name, value);
	}
	
	public void defMethod(String name, MethodScope value) throws DuplicateException{
		methodScopes.put(name, value);
	}
	
	
	
	
	

}
