package typechecker.implementation;

import ast.FunctionType;
import ast.Type;
import util.ImpTable;
import util.ImpTable.DuplicateException;

public class MethodScope extends Scope {
	
	public FunctionType functionType;
	public ClassScope thisScope;
	
	
	public MethodScope(ClassScope thisScope, FunctionType functionType) {
		this.thisScope = thisScope;
		this.functionType = functionType;
	}
	
	public Type lookupField(String name) {
		Type t;
		t = functionType.locals.lookup(name);
		if (t != null)
			return t;
		if (thisScope != null)
			t = thisScope.lookupField(name);
		return t;
	}
	
	public void defField(String name, Type value) throws DuplicateException{
		functionType.locals.put(name, value);
	}
	
	
	

}
