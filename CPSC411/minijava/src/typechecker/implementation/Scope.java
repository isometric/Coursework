package typechecker.implementation;

import ast.Type;
import util.ImpTable.DuplicateException;

public abstract class Scope {

	public abstract void defField(String name, Type value) throws DuplicateException;
	public abstract Type lookupField(String name);

}
