package typechecker.implementation;

import java.util.HashMap;

import ast.AST;
import ast.And;
import ast.ArrayAssign;
import ast.ArrayLength;
import ast.ArrayLookup;
import ast.Assign;
import ast.Block;
import ast.BooleanLiteral;
import ast.BooleanType;
import ast.Call;
import ast.ClassDecl;
import ast.Conditional;
import ast.FunctionDecl;
import ast.FunctionType;
import ast.IdentifierExp;
import ast.If;
import ast.IntArrayType;
import ast.IntegerLiteral;
import ast.IntegerType;
import ast.LessThan;
import ast.MainClass;
import ast.MethodDecl;
import ast.Minus;
import ast.NewArray;
import ast.NewObject;
import ast.NodeList;
import ast.Not;
import ast.ObjectType;
import ast.Plus;
import ast.Print;
import ast.Program;
import ast.This;
import ast.Times;
import ast.Type;
import ast.UnknownType;
import ast.VarDecl;
import ast.While;
import typechecker.ErrorReport;
import util.ImpTable;
import util.ImpTable.DuplicateException;
import visitor.DefaultVisitor;

/**
 * This visitor implements Phase 1 of the TypeChecker. It constructs the symboltable.
 * 
 * @author norm
 */
public class BuildSymbolTableVisitor extends DefaultVisitor<ImpTable<ClassScope>> {
	

	private final ImpTable<ClassScope> classes = new ImpTable<ClassScope>();
	private HashMap<String, ClassScope> wishList = new HashMap<String, ClassScope>();
	private final ErrorReport errors;
	private ClassScope thisClass = null;
	private MethodScope thisMethod = null;

	
	public BuildSymbolTableVisitor(ErrorReport errors) {
		this.errors = errors;
	}

	/////////////////// Phase 1 ///////////////////////////////////////////////////////
	// In our implementation, Phase 1 builds up a symbol table containing all the
	// global identifiers defined in a Functions program, as well as symbol tables
	// for each of the function declarations encountered. 
	//
	// We also check for duplicate identifier definitions in each symbol table 

	@Override
	public ImpTable<ClassScope> visit(Program n) {
		n.mainClass.accept(this);
		n.classes.accept(this); // process all the "normal" classes.
		return classes;
	}
	
	@Override
	public <T extends AST> ImpTable<ClassScope> visit(NodeList<T> ns) {
		for (int i = 0; i < ns.size(); i++)
			ns.elementAt(i).accept(this);
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(Assign n) {
		n.value.accept(this);
		return null;
	}
	

	@Override
	public ImpTable<ClassScope> visit(IdentifierExp n) {
		return null;
	}
	
	@Override
	public ImpTable<ClassScope> visit(Conditional n) {
		n.e1.accept(this);
		n.e2.accept(this);
		n.e3.accept(this);
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(BooleanType n) {
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(IntegerType n) {
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(Print n) {
		n.exp.accept(this);
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(LessThan n) {
		n.e1.accept(this);
		n.e2.accept(this);
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(Plus n) {
		n.e1.accept(this);
		n.e2.accept(this);
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(Minus n) {
		n.e1.accept(this);
		n.e2.accept(this);
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(Times n) {
		n.e1.accept(this);
		n.e2.accept(this);
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(IntegerLiteral n) {
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(Not not) {
		not.e.accept(this);
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(UnknownType n) {
		return null;
	}

	@Override 
	public ImpTable<ClassScope> visit(FunctionDecl n) {
		return null;
	}
	
	@Override 
	// This is a formal parameter to the current function
	public ImpTable<ClassScope> visit(VarDecl n) {
		if(thisMethod != null)
			defField(thisMethod, n.name, n.type);
		else
			defField(thisClass, n.name, n.type);
		return null;
	}
	
	@Override 
	public ImpTable<ClassScope> visit(FunctionType n) {
		return null;
	}
	
	@Override
	public ImpTable<ClassScope> visit(Call n) {
		return null;
	}
	
	@Override
	public ImpTable<ClassScope> visit(MainClass n) {
		defClass(n.className, new ClassScope(null));
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(ClassDecl n) {
		ClassScope parent = null;
		if(n.superName != null)
			parent = classes.lookup(n.superName);
		if(parent == null) {
			parent = wishList.get(n.superName);
			if(parent == null) {
				parent = new ClassScope(null);
				wishList.put(n.superName, parent);
			}
		}
		
		if(wishList.containsKey(n.name))
			thisClass = wishList.remove(n.name);
		else
			thisClass = new ClassScope(parent);
			
		n.vars.accept(this);
		n.methods.accept(this);
		thisClass = null;
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(MethodDecl n) {
		FunctionType ft = new FunctionType();
		thisMethod = new MethodScope(thisClass, ft);
		ft.locals = new ImpTable<Type>();
		ft.formals = n.formals;
		ft.returnType = n.returnType;
		n.formals.accept(this);
		n.statements.accept(this);
		n.returnExp.accept(this);
		n.returnType = ft;
		defMethod(thisClass, n.name, thisMethod);
		thisMethod = null;
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(IntArrayType n) {
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(ObjectType n) {
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(Block n) {
		n.statements.accept(this);
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(If n) {
		n.tst.accept(this);
		n.thn.accept(this);
		n.els.accept(this);
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(While n) {
		n.tst.accept(this);
		n.body.accept(this);
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(ArrayAssign n) {
		n.value.accept(this);
		n.index.accept(this);
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(And n) {
		n.e1.accept(this);
		n.e2.accept(this);
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(ArrayLookup n) {
		n.array.accept(this);
		n.index.accept(this);
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(ArrayLength n) {
		n.array.accept(this);
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(BooleanLiteral n) {
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(This n) {
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(NewArray n) {
		n.size.accept(this);
		return null;
	}

	@Override
	public ImpTable<ClassScope> visit(NewObject n) {
		return null;
	}
	
	
	///////////////////// Helpers ///////////////////////////////////////////////	
	private void defClass(String name, ClassScope classScope) {
		try {
			classes.put(name, classScope);
		} catch (DuplicateException e) {
			errors.duplicateDefinition(name);
		}
	}
	
	private void defField(Scope scope, String name, Type value) {
		try {
			scope.defField(name, value);
		} catch (DuplicateException e) {
			errors.duplicateDefinition(name);
		}
	}
	
	private void defMethod(ClassScope classScope, String name, MethodScope value) {
		try {
			classScope.defMethod(name, value);
		} catch (DuplicateException e) {
			errors.duplicateDefinition(name);
		}
	}
	

}