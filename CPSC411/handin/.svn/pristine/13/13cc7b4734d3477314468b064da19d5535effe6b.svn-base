package translate.implementation;

import static ir.tree.IR.*;
import static translate.TranslatorLabels.*;
import ast.AST;
import ast.And;
import ast.ArrayAssign;
import ast.ArrayLength;
import ast.ArrayLookup;
import ast.Assign;
import ast.Block;
import ast.BooleanLiteral;
import ast.BooleanType;
import ast.Call;
import ast.ClassDecl;
import ast.Conditional;
import ast.Expression;
import ast.FunctionDecl;
import ast.FunctionType;
import ast.IdentifierExp;
import ast.If;
import ast.IntArrayType;
import ast.IntegerLiteral;
import ast.IntegerType;
import ast.LessThan;
import ast.MainClass;
import ast.MethodDecl;
import ast.Minus;
import ast.NewArray;
import ast.NewObject;
import ast.NodeList;
import ast.Not;
import ast.ObjectType;
import ast.Plus;
import ast.Print;
import ast.Program;
import ast.Statement;
import ast.This;
import ast.Times;
import ast.Type;
import ast.UnknownType;
import ast.VarDecl;
import ast.While;
import translate.DataFragment;
import translate.Fragments;
import translate.ProcFragment;
import translate.implementation.Cx;
import translate.implementation.Ex;
import translate.implementation.Nx;
import translate.implementation.TRExp;
import util.FunTable;
import util.List;
import util.Lookup;
import visitor.Visitor;
import ir.frame.Access;
import ir.frame.Frame;
import ir.temp.Label;
import ir.temp.Temp;
import ir.tree.CJUMP;
import ir.tree.IR;
import ir.tree.IRData;
import ir.tree.IRExp;
import ir.tree.IRStm;
import ir.tree.TEMP;
import ir.tree.BINOP.Op;
import ir.tree.CJUMP.RelOp;



/**
 * This visitor builds up a collection of IRTree code fragments for the body
 * of methods in a Functions program.
 * <p>
 * Methods that visit statements and expression return a TRExp, other methods 
 * just return null, but they may add Fragments to the collection by means
 * of a side effect.
 * 
 * @author kdvolder
 */
public class TranslateVisitor implements Visitor<TRExp> {
	/**
	 * We build up a list of Fragment (pieces of stuff to be converted into
	 * assembly) here.
	 */
	private Fragments frags;

	/**
	 * We use this factory to create Frame's, without making our code dependent
	 * on the target architecture.
	 */
	private Frame frameFactory;

	/**
	 * The symbol table may be needed to find information about classes being 
	 * instantiated, or methods being called etc.
	 */
	private Frame frame;

	private FunTable<IRExp> currentEnv;

	public TranslateVisitor(Lookup<Type> table, Frame frameFactory) {
		this.frags = new Fragments(frameFactory);
		this.frameFactory = frameFactory;
	}

	/////// Helpers //////////////////////////////////////////////

	private boolean atGlobalScope() {
		return frame.getLabel().equals(L_MAIN);
	}

	/**
	 * Create a frame with a given number of formals. We assume that
	 * no formals escape, which is the case in MiniJava.
	 */
	private Frame newFrame(Label name, int nFormals) {
		return frameFactory.newFrame(name, nFormals);
	}

	/**
	 * Creates a label for a function (used by calls to that method).
	 * The label name is simply the function name.
	 */
	private Label functionLabel(String functionName) {
		return Label.get("_"+functionName);
	}


	private void putEnv(String name, Access access) {
		currentEnv = currentEnv.insert(name, access.exp(frame.FP()));
	}

	private void putEnv(String name, IRExp exp) {
		currentEnv = currentEnv.insert(name, exp);
	}

	////// Visitor ///////////////////////////////////////////////

	@Override
	public <T extends AST> TRExp visit(NodeList<T> ns) {
		IRStm result = IR.NOP;
		for (int i = 0; i < ns.size(); i++) {
			AST nextStm = ns.elementAt(i);
			TRExp e = nextStm.accept(this);
			// e will be null if the statement was in fact a function declaration
			// just ignore these as they generate Fragments
			if (e != null)
				result = IR.SEQ(result, e.unNx());
		}
		return new Nx(result);
	}

	@Override
	public TRExp visit(Program n) {
		frame = newFrame(L_MAIN, 0);
		currentEnv = FunTable.theEmpty();
		TRExp statements = n.statements.accept(this);
		TRExp print = n.print.accept(this);
		IRStm body = IR.SEQ(
				statements.unNx(),
				print.unNx());
		body = frame.procEntryExit1(body);
		frags.add(new ProcFragment(frame, body));
		return null;
	}

	@Override
	public TRExp visit(BooleanType n) {
		throw new Error("Not implemented");
	}

	@Override
	public TRExp visit(IntegerType n) {
		throw new Error("Not implemented");
	}

	@Override
	public TRExp visit(UnknownType n) {
		throw new Error("Not implemented");
	}

	private TRExp visitStatements(NodeList<Statement> statements) {
		IRStm result = IR.NOP;
		for (int i = 0; i < statements.size(); i++) {
			Statement nextStm = statements.elementAt(i);
			result = IR.SEQ(result, nextStm.accept(this).unNx());
		}
		return new Nx(result);
	}

	@Override
	public TRExp visit(Conditional n) {
		Temp temp = new Temp();
		Label t = Label.gen();
		Label f = Label.gen();
		Label join = Label.gen();
		IRStm tst = n.e1.accept(this).unCx(t, f);
		IRStm thn = IR.SEQ(
				IR.LABEL(t),
				new Nx(IR.MOVE(temp, n.e2.accept(this).unEx())).unNx(),
				IR.JUMP(join));
		IRStm els = IR.SEQ(
				IR.LABEL(f),
				new Nx(IR.MOVE(temp, n.e3.accept(this).unEx())).unNx());

		return new Ex( IR.ESEQ(IR.SEQ( tst, thn, els, IR.LABEL(join)), IR.TEMP(temp)));
	}

	@Override
	public TRExp visit(Print n) {
		TRExp arg = n.exp.accept(this);
		return new Ex(IR.CALL(L_PRINT, arg.unEx()));
	}

	@Override
	public TRExp visit(Assign n) {
		IRExp lhs;
		if (atGlobalScope()) {
			Label g = Label.get(n.name.name);
			IRExp zero = IR.CONST(0);
			IRData data = new IRData(g, List.list(zero));
			DataFragment decl = new DataFragment(frame, data);
			frags.add(decl);
			lhs = IR.MEM(IR.NAME(g));
			putEnv(n.name.name, lhs);
		} else {
			Access var = frame.allocLocal(false);
			putEnv(n.name.name, var);
			lhs = var.exp(frame.FP());
		}
		TRExp val = n.value.accept(this);
		return new Nx(IR.MOVE(lhs, val.unEx()));
	}

	private TRExp relOp(final CJUMP.RelOp op, AST ltree, AST rtree) {
		final TRExp l = ltree.accept(this);
		final TRExp r = rtree.accept(this);
		return new TRExp() {
			@Override
			public IRStm unCx(Label t, Label f) {
				return IR.CJUMP(op, l.unEx(), r.unEx(), t, f);
			}

			@Override
			public IRStm unCx(IRExp dst, IRExp src) {
				return IR.CMOVE(op, l.unEx(), r.unEx(), dst, src);
			}
			@Override
			public IRExp unEx() {
				TEMP v = TEMP(new Temp());
				return ESEQ( SEQ( 
						MOVE(v, FALSE),
						CMOVE(op, l.unEx(), r.unEx(), v, TRUE)),
						v);
			}

			@Override
			public
			IRStm unNx() {
				Label end = Label.gen();
				return SEQ(  unCx(end, end),
						LABEL(end) );
			}

		};

	}
	@Override
	public TRExp visit(LessThan n) {
		return relOp(RelOp.LT, n.e1, n.e2);
	}

	//////////////////////////////////////////////////////////////

	private TRExp numericOp(Op op, Expression e1, Expression e2) {
		TRExp l = e1.accept(this);
		TRExp r = e2.accept(this);
		return new Ex(IR.BINOP(op, l.unEx(), r.unEx()));
	}

	@Override
	public TRExp visit(Plus n) {
		return numericOp(Op.PLUS,n.e1,n.e2);
	}

	@Override
	public TRExp visit(Minus n) {
		return numericOp(Op.MINUS,n.e1,n.e2);
	}

	@Override
	public TRExp visit(Times n) {
		return numericOp(Op.MUL,n.e1,n.e2);
	}

	//////////////////////////////////////////////////////////////////

	@Override
	public TRExp visit(IntegerLiteral n) {
		return new Ex(IR.CONST(n.value));
	}

	@Override
	public TRExp visit(IdentifierExp n) {
		IRExp var = currentEnv.lookup(n.name);
		return new Ex(var);
	}

	@Override
	public TRExp visit(Not n) {
		final TRExp negated = n.e.accept(this);
		return new Cx() {
			@Override
			public IRStm unCx(Label ifTrue, Label ifFalse) {
				return negated.unCx(ifFalse, ifTrue);
			}
			@Override
			public IRStm unCx(IRExp dst, IRExp src) {
				return new Ex(IR.BINOP(Op.MINUS, IR.CONST(1), negated.unEx())).unCx(dst, src);
			}
			@Override
			public IRExp unEx() {
				return new Ex(IR.BINOP(Op.MINUS, IR.CONST(1), negated.unEx())).unEx();
			}
		};
	}

	@Override
	public TRExp visit(FunctionDecl n) {
		Frame oldframe = frame;
		frame = newFrame(functionLabel(n.name), n.formals.size());
		FunTable<IRExp> saveEnv = currentEnv;

		//Get the access information for each regular formal and add it to the environment.
		for (int i = 0; i < n.formals.size(); i++) {
			putEnv(n.formals.elementAt(i).name, frame.getFormal(i));
		}

		TRExp stats = visitStatements(n.statements);
		TRExp exp = n.returnExp.accept(this);

		IRStm body = IR.SEQ( 
				stats.unNx(), 
				IR.MOVE(frame.RV(), exp.unEx()));
		body = frame.procEntryExit1(body);
		frags.add(new ProcFragment(frame, body));

		frame = oldframe;
		currentEnv = saveEnv;

		return null;
	}


	@Override
	public TRExp visit(VarDecl n) {
		Access var = frame.getInArg(n.index);
		putEnv(n.name, var);
		return null;
	}


	@Override
	public TRExp visit(Call n) {
		String functionName = "unknown";
		if (n.name instanceof IdentifierExp) {
			functionName = ((IdentifierExp) n.name).name;
		}
		List<IRExp> args = List.list();

		for (int i = 0; i < n.rands.size(); i++) {
			TRExp arg = n.rands.elementAt(i).accept(this);
			args.add(arg.unEx());
		}
		return new Ex(IR.CALL(functionLabel(functionName), args));
	}


	@Override
	public TRExp visit(FunctionType n) {
		throw new Error("Not implemented");
	}

	/**
	 * After the visitor successfully traversed the program, 
	 * retrieve the built-up list of Fragments with this method.
	 */
	public Fragments getResult() {
		return frags;
	}

	@Override
	public TRExp visit(MainClass n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TRExp visit(ClassDecl n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TRExp visit(MethodDecl n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TRExp visit(IntArrayType n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TRExp visit(ObjectType n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TRExp visit(Block n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TRExp visit(If n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TRExp visit(While n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TRExp visit(ArrayAssign n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TRExp visit(And n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TRExp visit(ArrayLookup n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TRExp visit(ArrayLength n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TRExp visit(BooleanLiteral n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TRExp visit(This n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TRExp visit(NewArray n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TRExp visit(NewObject n) {
		// TODO Auto-generated method stub
		return null;
	}
}
