package ast;

import java.io.PrintWriter;
import java.io.StringWriter;

import visitor.CountVisitor;
import visitor.DepthVisitor;
import visitor.PrettyPrintVisitor;
import visitor.StructurePrintVisitor;
import visitor.Visitor;




public abstract class AST {
	
	public abstract <R> R accept(Visitor<R> v);
	
	@Override
	public String toString() {
		StringWriter out = new StringWriter();
		this.accept(new PrettyPrintVisitor(new PrintWriter(out)));
		return out.toString();
	}
	public Integer getCount() {
		StringWriter out = new StringWriter();
		return this.accept(new CountVisitor(new PrintWriter(out)));
	}
	
	public Integer getDepth() {
		StringWriter out = new StringWriter();
		return this.accept(new DepthVisitor(new PrintWriter(out)));
	}

	
	public String dump() {
		StringWriter out = new StringWriter();
		this.accept(new StructurePrintVisitor(new PrintWriter(out)));
		return out.toString();		
	}
}
