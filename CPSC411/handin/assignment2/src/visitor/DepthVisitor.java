package visitor;

import java.io.PrintWriter;

import ast.AST;
import ast.Assign;
import ast.BooleanType;
import ast.Conditional;
import ast.IdentifierExp;
import ast.IntegerLiteral;
import ast.IntegerType;
import ast.LessThan;
import ast.Minus;
import ast.NodeList;
import ast.Not;
import ast.Plus;
import ast.Print;
import ast.Program;
import ast.Times;
import ast.UnknownType;

import util.IndentingWriter;



/**
 * This is an adaptation of the PrettyPrintVisitor from the textbook
 * online material, but updated to work with the "modernized" 
 * Visitor and our own versions of the AST classes.
 * <p>
 * This version is also cleaned up to actually produce *properly* indented
 * output.
 * 
 * @author kdvolder
 */
public class DepthVisitor implements Visitor<Integer> {

	/**
	 * Where to send out.print output.
	 */
	private IndentingWriter out;
	
	public DepthVisitor(PrintWriter out) {
		this.out = new IndentingWriter(out);
	}
	
	///////////// Visitor methods /////////////////////////////////////////

	@Override
	public Integer visit(Program n) {
		return Math.max(1 + n.statements.accept(this), 1 + n.print.accept(this));
	}

	@Override
	public Integer visit(BooleanType n) {
		return 1;
	}

	@Override
	public Integer visit(UnknownType n) {
		return 1;
	}

	@Override
	public Integer visit(IntegerType n) {
		return 1;
	}

  	@Override
	public Integer visit(Conditional n) {
		int depth = 1;
  		depth = Math.max(depth, 1 + n.e1.accept(this));
  		depth = Math.max(depth, 1 + n.e2.accept(this));
  		depth = Math.max(depth, 1 + n.e3.accept(this));
		return depth;
	}

	@Override
	public Integer visit(Print n) {
		return 1 + n.exp.accept(this);
	}

	@Override
	public Integer visit(Assign n) {
		return 1 + n.value.accept(this);
	}

	@Override
	public Integer visit(LessThan n) {
		int depth = 1;
  		depth = Math.max(depth, 1 + n.e1.accept(this));
  		depth = Math.max(depth, 1 + n.e2.accept(this));
		return depth;
	}

	@Override
	public Integer visit(Plus n) {
		int depth = 1;
  		depth = Math.max(depth, 1 + n.e1.accept(this));
  		depth = Math.max(depth, 1 + n.e2.accept(this));
		return depth;
}

	@Override
	public Integer visit(Minus n) {
		int depth = 1;
  		depth = Math.max(depth, 1 + n.e1.accept(this));
  		depth = Math.max(depth, 1 + n.e2.accept(this));
		return depth;
}

	@Override
	public Integer visit(Times n) {
		int depth = 1;
  		depth = Math.max(depth, 1 + n.e1.accept(this));
  		depth = Math.max(depth, 1 + n.e2.accept(this));
		return depth;
}

	@Override
	public Integer visit(IntegerLiteral n) {
		return 1;
	}

	@Override
	public Integer visit(IdentifierExp n) {
		return 1;
	}

	@Override
	public Integer visit(Not n) {
		return 1+ n.e.accept(this);
	}

	@Override
	public <T extends AST> Integer visit(NodeList<T> nodes) {
		int depth = 0;
		for (int i = 0; i < nodes.size(); i++) {
			return Math.max(depth, nodes.elementAt(i).accept(this));
		}
		return depth;
	}
}
