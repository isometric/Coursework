package ca.ubc.cs411.assignment1;

import java.io.*;

public class WordCount {

    public static void main(String[] args) {

        int total = 0;
    	
        for (int i = 0; i < args.length; i++){
            try {
                File file = new File(args[i]);
                if (file.exists() && file.isFile()) {
                    System.out.println(String.format("%16d", file.length()) + "\t" + file.getName());
                    total += file.length();
                } else if (file.exists() && file.isDirectory()) {
                    System.out.println("Directory" + "\t\t" + file.getName());
                } else {
                    System.out.println("Does Not Exist" + "\t\t" + file.getName());
                }
            } catch (Exception e) {
                System.out.println("Error Opening " + "\t\t" + args[i]);
            }
        }
        System.out.println(String.format("%16d", total) + "\t" + "total");
    }
}