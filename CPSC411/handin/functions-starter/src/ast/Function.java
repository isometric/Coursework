package ast;

import visitor.Visitor;

public class Function extends Statement {
	
	public final Type retType;
	public final String name;
	public final NodeList<Parameter> params;
	public final NodeList<Assign> assigns;
	public final Expression exp;

	public Function(Type retType, String name, NodeList<Parameter> params, NodeList<Assign> assigns,Expression exp) {
		this.retType = retType;
		this.name = name;
		this.params = params;
		this.assigns = assigns;
		this.exp = exp;
	}


	@Override
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}

}
