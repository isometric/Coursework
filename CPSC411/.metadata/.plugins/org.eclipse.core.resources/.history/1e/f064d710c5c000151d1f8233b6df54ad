package test.parser;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;

import parser.Parser;

import ast.Program;

import test.SampleCode;


/**
 * The tests in this class correspond more or less to the work in Chapter 3.
 * <p>
 * These tests try to call your parser to parse Expression programs, but they do 
 * not check the AST produced by the parser. As such you should be able to get
 * these tests to pass without inserting any semantic actions into your
 * parser specification file.
 * <p>
 * The tests in this file are written in an order that can be followed if
 * you want to develop your parser incrementally, staring with the tests at the
 * top of the file, and working your way down.
 * 
 * @author kdvolder
 */
public class Test3Parse {
	
	/**
	 * All testing is supposed to go through calling this method to one of the
	 * accept methods, to see whether some input is accepted by the parser. The subclass 
	 * Test4Parse refines these tests by overriding this accept method to also verify the 
	 * parse tree structure.
	 */
	protected void accept(String input, Integer expCount, Integer expDepth) throws Exception {
		System.out.println("parsing string: "+ input);
		Program p = Parser.parse(input);
		System.out.println("Parse tree:");
		System.out.println(p.dump());
		Assert.assertEquals(expCount, p.getCount());
		Assert.assertEquals(expDepth, p.getDepth());
	}
	
	protected void accept(File file/*, Integer expCount, Integer expDepth*/) throws Exception {
		System.out.println("parsing file: "+file);
		Program p = Parser.parse(file);
		System.out.println("Parse tree:");
		System.out.println(p.dump());
//		Assert.assertEquals(p.getCount(), expCount);
//		Assert.assertEquals(p.getDepth(), expDepth);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// First let's ensure we can parse the "simplest possible" program:

	@Test
	public void testSmallest() throws Exception {
		// The smallest program has one print statement with the smallest expression.
		accept( "print 1", 3, 3);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Next: let's work on making the expression parsing complete.
	
	// Note: some of the created test programs here are not correct. They use undeclared variables
	// and may have type errors... but they should pass by the parser. The parser doesn't
	// check for these kinds of errors.
	
	void acceptExpression(String exp, Integer expCount, Integer expDepth) throws Exception {
		accept( "print " + exp, 2 + expCount, 2 + expDepth);
	}
	
	@Test
	public void testIdentifier() throws Exception {
		acceptExpression("x", 1, 1);
		acceptExpression("y", 1, 1);
		acceptExpression("xy123", 1, 1);
		acceptExpression("x_y_123", 1, 1);
		acceptExpression("x_y_123", 1, 1);
	}
	
	@Test
	public void testThis() throws Exception {
		acceptExpression("this", 1, 1);
	}
	
	@Test
	public void testNot() throws Exception {
		acceptExpression("!x", 2, 2);
		acceptExpression("!!!!!!x", 7, 7);
	}

	@Test
	public void testParens() throws Exception {
		acceptExpression("(1)", 2, 2);
		acceptExpression("((((((1))))))", 7, 7);
	}

	@Test
	public void testMult() throws Exception {
		acceptExpression("10*9", 3, 2);
		acceptExpression("10*9*8", 5, 3);
		acceptExpression("foo*length", 3, 2);
		acceptExpression("10*9*8*7*x*y*foo", 13, 7);
	}
	
	@Test
	public void testAdd() throws Exception {
		acceptExpression("10+9", 3, 2);
		acceptExpression("10-9", 3, 2);
		acceptExpression("10+9+8", 5, 3);
		acceptExpression("10-9-8", 5, 3);
		acceptExpression("length+length", 3, 2);
		acceptExpression("length-length", 3, 2);
		acceptExpression("foo+foo", 3, 2);
		acceptExpression("foo+(foo)", 3, 2);
		acceptExpression("10+9+x*length-foo+array", 11 , 5 );
		acceptExpression("(a-b)*(a+b)", 7, 3);
	}

	@Test
	public void testComp() throws Exception {
		acceptExpression("10<9", 3 , 2);
		acceptExpression("10+a*3<9-4+2", 11, 4);
		acceptExpression("length<1", 3, 2);
		acceptExpression("i<foo", 3, 2);
		acceptExpression("10<9", 3, 2);
		acceptExpression("10+a*3<9-4+2", 11, 4);
		acceptExpression("length<1", 3 , 2);
		acceptExpression("i<foo", 3, 2);
	}
	
	@Test
	public void testConditional() throws Exception {
		acceptExpression("10<9?x:y", 5, 6);
		acceptExpression("10+a*3<9-4+2 ? 3 + 4 : 5 * 7", 13, 13);
		acceptExpression("1 ? 2 ? 3 ? 4 : 5 : 6 : 7", 13, 13);
		acceptExpression("1 ? 2 ? 3 : 4 ? 5 : 6 : 7 ? 8 : 9", 13, 13);
	}

	/////////////////////////////////////////////////////////////////////////////////
	// Now let's work on making statement parsing complete.
	
	void acceptStatement(String statement, Integer expCount, Integer expDepth) throws Exception {
		accept( statement + "\n" + "print 1", expCount , expDepth);
	}
	
	@Test public void testAssign() throws Exception {
		acceptStatement("numbers = numbers + 1;", 3, 3);
		acceptStatement("foo = foo+1;", 3, 3);
	}
	
	/////////////////////////////////////////////////////////////////////////////////
	// Finally, check whether the parser accepts all the sample code.
	@Test 
	public void testParseSampleCode() throws Exception {
		File[] files = SampleCode.sampleFiles();
		for (File file : files) {
			accept(file);
		}
	}
	
}
