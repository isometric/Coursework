-module(mini1).

-export([who_am_i/0, registration/0, flatten/1, reverse_hr/1, reverse_tr/1,
         time_rev/0, time_rev/1]).
-export([fib_hr/1, fib_tr/1, time_fib/1, time_fib/0]).

-export([fib_pair_hr/1, fib_pair_tr/3]).


who_am_i() ->
  { "James Deng",
    13340112,
    "jamesdeng@alumni.ubc.ca"
  }.

registration() -> wait.

% flatten(X) ->
%   if X == []        -> X;
%      is_list(X)     -> flatten(hd(X)) ++ flatten(tl(X));
%      not is_list(X) -> [X]
%   end.

flatten([]) -> [];
flatten([H | T]) -> flatten(H) ++ flatten(T);
flatten(X) -> [X].

reverse_hr([]) -> [];
reverse_hr([H | T]) -> reverse_hr(T) ++ [H].

% write a tail-recursive version below
% reverse_tr(_) -> ok.
reverse_tr(L) -> reverse_tr(L, []).
reverse_tr([], R) -> R;
reverse_tr([H | T], R) -> reverse_tr(T, [H | R]).

elapsed(T0, T1) ->
  1.0e-9*erlang:convert_time_unit(T1-T0, native, nano_seconds).

time_rev(N) when is_integer(N) ->
  L = lists:seq(1,N),
  T0 = erlang:monotonic_time(),
  R_hr = reverse_hr(L),
  T1 = erlang:monotonic_time(),
  R_tr = reverse_tr(L),
  T2 = erlang:monotonic_time(),
  R_tr = R_hr, % make sure they match
  io:format("N = ~8B: time reverse_hr = ~10.6f, time reverse_tr = ~10.6f~n",
            [N, elapsed(T0, T1), elapsed(T1, T2)]),
  ok;
time_rev(L) when is_list(L) ->
  [time_rev(N) || N <- L],
  ok.
time_rev() -> time_rev([1000, 10000, 20000, 30000, 40000, 50000]).

% Let fib(N) denote the N^th fibonacci number.
% Here's a head-recursive implementation
fib_hr(0) -> 0;
fib_hr(N) -> element(1, fib_pair_hr(N)).

% fib_pair_hr(N) -> {fib(N), fib(N-1)}
fib_pair_hr(1) -> {1, 0};
fib_pair_hr(N) ->
  {F1, F2} = fib_pair_hr(N-1), % F1 = fib(N-1), F2 = fib(N-1)
  {F1 + F2, F1}.

% Now for the tail-recursive version.
fib_tr(N) -> fib_pair_tr(N, 0, 1).

fib_pair_tr(0, X, _) -> X;
fib_pair_tr(N, Fib1, Fib2) -> fib_pair_tr(N-1, Fib2, Fib1 + Fib2).



time_fib(N) when is_integer(N) ->
  T0 = erlang:monotonic_time(),
  F_hr = fib_hr(N),
  T1 = erlang:monotonic_time(),
  F_tr = fib_tr(N),
  T2 = erlang:monotonic_time(),
  F_tr = F_hr, % make sure they match
  io:format("N = ~8B: time fib_hr = ~10.6f, time fib_tr = ~10.6f ratio = ~10.6f~n",
            [N, elapsed(T0, T1), elapsed(T1, T2), elapsed(T0, T1)/elapsed(T1, T2)]),
  ok;
time_fib(L) when is_list(L) ->
  [time_fib(N) || N <- L],
  ok.
time_fib() -> time_fib([100, 300, 1000, 3000, 10000, 30000, 100000, 300000, 1000000, 3000000, 10000000, 30000000]).

% DATA:
% N =      100 time fib_hr =   0.000021 time fib_tr =   0.000005 ratio =   3.872308
% N =      300 time fib_hr =   0.000031 time fib_tr =   0.000018 ratio =   1.710941
% N =     1000 time fib_hr =   0.000113 time fib_tr =   0.000071 ratio =   1.579918
% N =     3000 time fib_hr =   0.000454 time fib_tr =   0.000284 ratio =   1.599770
% N =    10000 time fib_hr =   0.003099 time fib_tr =   0.001899 ratio =   1.631613
% N =    30000 time fib_hr =   0.014567 time fib_tr =   0.009466 ratio =   1.538946
% N =   100000 time fib_hr =   0.149443 time fib_tr =   0.108590 ratio =   1.376213
% N =   300000 time fib_hr =   1.418805 time fib_tr =   1.061039 ratio =   1.337185
% N =  1000000 time fib_hr =  15.308419 time fib_tr =   9.606813 ratio =   1.593496
% N =  3000000 time fib_hr = 149.246527 time fib_tr =  98.335108 ratio =   1.517734

% The performance diffrences between tail recursive and head rescursive implementations appear
% to be around 1.5 and independent from N. This is in linne with the official efficency guide
% which states that on x86, tail recursion is about 30% faster than non. 1/0.7 = 1.43 which is
% approximatly equal to the 1.3 to 1.5 ratios I observed with time fib_hr/time fib_tr. The
% extra cost is then likely due to more difficult garbage collection versus tail recursion
% which can be trivially genarized into a loop.
% Source: http://www.erlang.org/doc/efficiency_guide/myths.html

% However, closer inspection also reveals that average cost of each iteration increaces
% exponentially with respect to the size of N for both methods. Indeed, plotting log(time)/N
% reveals a very linear relation ship starting around N = 100000. The 100000th fibonacci
% number is far greater than any x86 native integral types so I think it's likely due to the
% cost of big integer processing. As fibbonacci grows exponentially with respect to the
% golden ratio, so too should the bytes representing it. Sum takes linear time so it makes
% sense that a step that is dependant on addition is tied to the exponent of N.




