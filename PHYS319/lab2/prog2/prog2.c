/*
* PHYS319 Lab 3 Interrupt Example in C
*
* Written by Ryan Wicks
* 16 Jan 2012
*
* This program is a C version of the assembly program that formed part of
* lab 2.
*
*
*/
#include <msp430g2231.h>

int count = 0;

void main(void) {
    WDTCTL = WDTPW + WDTHOLD;     // Stop watchdog timer

    P1DIR = 0xF7;
    P1OUT = 0x41;

    // Enable input at P1.3 as an interrupt
    P1IE = 0x08;

    // Turn on interrupts and go into the lowest
    // power mode (the program stops here)
    // Notice the strange format of the function, it is an "intrinsic"
    // ie. not part of C; it is specific to this chipset
    // Port 1 interrupt service routine
    _BIS_SR(LPM4_bits + GIE);
}


// #pragma vector = PORT1_VECTOR
// __interrupt void PORT1_ISR(void) {


__attribute__((interrupt(PORT1_VECTOR)))
void PORT1_ISR(void) {
    if (count == 0) {
        P1OUT = 0x0;
        count++;
    } else if (count == 1) {
        P1OUT = 0x1;
        count++;
    } else if (count == 2) {
        P1OUT = 0x40;
        count++;
    } else if (count == 3) {
        P1OUT = 0x41;
        count = 0;
    }
    // Clear P1.3 IFG. If you don't, it just happens again.
    P1IFG &= ~0x08;
}
