	.file	"prog2.c"
	.arch msp430g2231
	.cpu 430
	.mpy none

	.text
.Ltext0:
.global	count
.global	count
	.section	.bss
	.type	count,@object
	.size	count,2
count:
	.skip 2,0
	.section	.init9,"ax",@progbits
	.p2align 1,0
.global	main
	.type	main,@function
/***********************
 * Function `main' 
 ***********************/
main:
.LFB0:
	.file 1 "prog2.c"
	.loc 1 16 0
	.loc 1 17 0
	mov	#23168, &__WDTCTL
	.loc 1 19 0
	mov.b	#llo(-9), &__P1DIR
	.loc 1 20 0
	mov.b	#65, &__P1OUT
	.loc 1 23 0
	mov.b	#8, &__P1IE
	.loc 1 30 0
	bis	#248, r2
	.loc 1 31 0
.LIRD0:
.LFE0:
.Lfe1:
	.size	main,.Lfe1-main
;; End of function 

	.text
	.p2align 1,0
.global	PORT1_ISR
	.type	PORT1_ISR,@function
/***********************
 * Interrupt Vector 2 Service Routine `PORT1_ISR' 
 ***********************/
PORT1_ISR:
.global	__isr_2
__isr_2:
.LFB1:
	.loc 1 39 0
	push	r15
.LCFI0:
	.loc 1 40 0
	mov	&count, r15
	cmp	#0, r15
	jne	.L3
	.loc 1 41 0
	mov.b	#0, &__P1OUT
	.loc 1 42 0
	mov	&count, r15
	add	#1, r15
	mov	r15, &count
	jmp	.L4
.L3:
	.loc 1 43 0
	mov	&count, r15
	cmp	#1, r15
	jne	.L5
	.loc 1 44 0
	mov.b	#1, &__P1OUT
	.loc 1 45 0
	mov	&count, r15
	add	#1, r15
	mov	r15, &count
	jmp	.L4
.L5:
	.loc 1 46 0
	mov	&count, r15
	cmp	#2, r15
	jne	.L6
	.loc 1 47 0
	mov.b	#64, &__P1OUT
	.loc 1 48 0
	mov	&count, r15
	add	#1, r15
	mov	r15, &count
	jmp	.L4
.L6:
	.loc 1 49 0
	mov	&count, r15
	cmp	#3, r15
	jne	.L4
	.loc 1 50 0
	mov.b	#65, &__P1OUT
	.loc 1 51 0
	mov	#0, &count
.L4:
	.loc 1 54 0
	mov.b	&__P1IFG, r15
	and.b	#llo(-9), r15
	mov.b	r15, &__P1IFG
	.loc 1 55 0
	pop	r15
	reti
.LFE1:
.Lfe2:
	.size	PORT1_ISR,.Lfe2-PORT1_ISR
;; End of function 

	.section	.debug_frame,"",@progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.string	""
	.uleb128 0x1
	.sleb128 -2
	.byte	0
	.byte	0xc
	.uleb128 0x1
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x1
	.p2align 1,0
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.2byte	.LFB0
	.2byte	.LFE0-.LFB0
	.p2align 1,0
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.2byte	.LFB1
	.2byte	.LFE1-.LFB1
	.byte	0x4
	.4byte	.LCFI0-.LFB1
	.byte	0xe
	.uleb128 0x4
	.byte	0x8f
	.uleb128 0x2
	.p2align 1,0
.LEFDE2:
	.text
.Letext0:
	.file 2 "/usr/lib/gcc/msp430/4.6.3/../../../../msp430/include/msp430g2231.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.4byte	0xcc
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x2
	.uleb128 0x1
	.4byte	.LASF14
	.byte	0x1
	.4byte	.LASF15
	.4byte	.LASF16
	.2byte	0
	.2byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF2
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF0
	.byte	0x1
	.byte	0x10
	.byte	0x1
	.2byte	.LFB0
	.2byte	.LFE0
	.byte	0x2
	.byte	0x71
	.sleb128 0
	.uleb128 0x4
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0x27
	.byte	0x1
	.2byte	.LFB1
	.2byte	.LFE1
	.4byte	.LLST0
	.uleb128 0x5
	.4byte	.LASF4
	.byte	0x2
	.2byte	0x19b
	.4byte	.LASF6
	.4byte	0x5f
	.byte	0x1
	.byte	0x1
	.uleb128 0x6
	.4byte	0x64
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF3
	.uleb128 0x5
	.4byte	.LASF5
	.byte	0x2
	.2byte	0x19d
	.4byte	.LASF7
	.4byte	0x5f
	.byte	0x1
	.byte	0x1
	.uleb128 0x5
	.4byte	.LASF8
	.byte	0x2
	.2byte	0x19f
	.4byte	.LASF9
	.4byte	0x5f
	.byte	0x1
	.byte	0x1
	.uleb128 0x5
	.4byte	.LASF10
	.byte	0x2
	.2byte	0x1a3
	.4byte	.LASF11
	.4byte	0x5f
	.byte	0x1
	.byte	0x1
	.uleb128 0x5
	.4byte	.LASF12
	.byte	0x2
	.2byte	0x27c
	.4byte	.LASF13
	.4byte	0xb3
	.byte	0x1
	.byte	0x1
	.uleb128 0x6
	.4byte	0x25
	.uleb128 0x7
	.4byte	.LASF17
	.byte	0x1
	.byte	0xe
	.4byte	0xc8
	.byte	0x1
	.byte	0x3
	.byte	0x3
	.2byte	count
	.uleb128 0x8
	.byte	0x2
	.byte	0x5
	.string	"int"
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x2007
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LLST0:
	.2byte	.LFB1
	.2byte	.LCFI0
	.2byte	0x2
	.byte	0x71
	.sleb128 0
	.2byte	.LCFI0
	.2byte	.LFE1
	.2byte	0x2
	.byte	0x71
	.sleb128 2
	.2byte	0
	.2byte	0
	.section	.debug_aranges,"",@progbits
	.4byte	0x14
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x2
	.byte	0
	.2byte	.Ltext0
	.2byte	.Letext0-.Ltext0
	.2byte	.LFB0
	.2byte	.LFE0-.LFB0
	.2byte	0
	.2byte	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.2byte	.Ltext0
	.2byte	.Letext0
	.2byte	.LFB0
	.2byte	.LFE0
	.2byte	0
	.2byte	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF5:
	.string	"P1DIR"
.LASF2:
	.string	"unsigned int"
.LASF8:
	.string	"P1IFG"
.LASF13:
	.string	"__WDTCTL"
.LASF6:
	.string	"__P1OUT"
.LASF14:
	.string	"GNU C 4.6.3 20120301 (mspgcc LTS 20120406 unpatched)"
.LASF7:
	.string	"__P1DIR"
.LASF1:
	.string	"PORT1_ISR"
.LASF12:
	.string	"WDTCTL"
.LASF10:
	.string	"P1IE"
.LASF3:
	.string	"unsigned char"
.LASF9:
	.string	"__P1IFG"
.LASF0:
	.string	"main"
.LASF16:
	.string	"/home/james/Insync/UBC/Courses/PHYS319/lab2/prog2"
.LASF15:
	.string	"prog2.c"
.LASF11:
	.string	"__P1IE"
.LASF4:
	.string	"P1OUT"
.LASF17:
	.string	"count"
