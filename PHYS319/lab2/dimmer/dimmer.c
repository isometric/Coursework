#include <msp430g2231.h>
void main(void) {
    WDTCTL = WDTPW + WDTHOLD;
    ADC10CTL0 = ADC10SHT_2 + ADC10ON;
    ADC10CTL1 = INCH_1;
    ADC10AE0 |= 0x02;
    P1DIR |= 0x45;
    unsigned i;


    P1DIR |= BIT2;              // P1.2 to output
    P1SEL |= BIT2;              // P1.2 to TA0.1

    CCR0 = 1000-1;              // PWM period
    CCTL1 = OUTMOD_7;           // CCR1 reset/set


    while (1) {
        ADC10CTL0 |= ENC + ADC10SC;
        while (ADC10CTL1 & ADC10BUSY);
            CCR1  = ADC10MEM;                 // CCR1 PWM duty cycle
            TACTL = TASSEL_2 + MC_1;    // SMCLK, up mode

    for (i = 0xFFFF; i > 0; i--);
    }
}
