	.file	"dimmer.c"
	.arch msp430g2231
	.cpu 430
	.mpy none

	.text
.Ltext0:
	.section	.init9,"ax",@progbits
	.p2align 1,0
.global	main
	.type	main,@function
/***********************
 * Function `main' 
 ***********************/
main:
.LFB0:
	.file 1 "dimmer.c"
	.loc 1 2 0
	sub	#2, r1
.LCFI0:
	.loc 1 3 0
	mov	#23168, &__WDTCTL
	.loc 1 4 0
	mov	#4112, &__ADC10CTL0
	.loc 1 5 0
	mov	#4096, &__ADC10CTL1
	.loc 1 6 0
	mov.b	&__ADC10AE0, r15
	bis.b	#2, r15
	mov.b	r15, &__ADC10AE0
	.loc 1 7 0
	mov.b	&__P1DIR, r15
	bis.b	#69, r15
	mov.b	r15, &__P1DIR
	.loc 1 11 0
	mov.b	&__P1DIR, r15
	bis.b	#4, r15
	mov.b	r15, &__P1DIR
	.loc 1 12 0
	mov.b	&__P1SEL, r15
	bis.b	#4, r15
	mov.b	r15, &__P1SEL
	.loc 1 14 0
	mov	#999, &__TACCR0
	.loc 1 15 0
	mov	#224, &__TACCTL1
.L5:
	.loc 1 19 0
	mov	&__ADC10CTL0, r15
	bis	#3, r15
	mov	r15, &__ADC10CTL0
	.loc 1 20 0
	nop
.L2:
	.loc 1 20 0 is_stmt 0 discriminator 1
	mov	&__ADC10CTL1, r15
	and	#1, r15
	mov.b	r15, r15
	cmp.b	#0, r15
	jne	.L2
	.loc 1 21 0 is_stmt 1
	mov	&__ADC10MEM, r15
	mov	r15, &__TACCR1
	.loc 1 22 0
	mov	#528, &__TACTL
	.loc 1 24 0
	mov	#llo(-1), @r1
	jmp	.L3
.L4:
	.loc 1 24 0 is_stmt 0 discriminator 2
	add	#llo(-1), @r1
.L3:
	.loc 1 24 0 discriminator 1
	cmp	#0, @r1
	jne	.L4
	.loc 1 25 0 is_stmt 1
	jmp	.L5
.LIRD0:
.LFE0:
.Lfe1:
	.size	main,.Lfe1-main
;; End of function 

	.section	.debug_frame,"",@progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.string	""
	.uleb128 0x1
	.sleb128 -2
	.byte	0
	.byte	0xc
	.uleb128 0x1
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x1
	.p2align 1,0
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.2byte	.LFB0
	.2byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.p2align 1,0
.LEFDE0:
	.text
.Letext0:
	.file 2 "/usr/lib/gcc/msp430/4.6.3/../../../../msp430/include/msp430g2231.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.4byte	0x11e
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x2
	.uleb128 0x1
	.4byte	.LASF24
	.byte	0x1
	.4byte	.LASF25
	.4byte	.LASF26
	.2byte	0
	.2byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF27
	.byte	0x1
	.byte	0x2
	.byte	0x1
	.2byte	.LFB0
	.2byte	.LFE0
	.4byte	.LLST0
	.4byte	0x4e
	.uleb128 0x4
	.string	"i"
	.byte	0x1
	.byte	0x8
	.4byte	0x25
	.byte	0x2
	.byte	0x91
	.sleb128 -2
	.byte	0
	.uleb128 0x5
	.4byte	.LASF2
	.byte	0x2
	.byte	0x9f
	.4byte	.LASF4
	.4byte	0x5f
	.byte	0x1
	.byte	0x1
	.uleb128 0x6
	.4byte	0x64
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x5
	.4byte	.LASF3
	.byte	0x2
	.byte	0xa2
	.4byte	.LASF5
	.4byte	0x7c
	.byte	0x1
	.byte	0x1
	.uleb128 0x6
	.4byte	0x25
	.uleb128 0x5
	.4byte	.LASF6
	.byte	0x2
	.byte	0xa4
	.4byte	.LASF7
	.4byte	0x7c
	.byte	0x1
	.byte	0x1
	.uleb128 0x5
	.4byte	.LASF8
	.byte	0x2
	.byte	0xa6
	.4byte	.LASF9
	.4byte	0x7c
	.byte	0x1
	.byte	0x1
	.uleb128 0x7
	.4byte	.LASF10
	.byte	0x2
	.2byte	0x19d
	.4byte	.LASF12
	.4byte	0x5f
	.byte	0x1
	.byte	0x1
	.uleb128 0x7
	.4byte	.LASF11
	.byte	0x2
	.2byte	0x1a5
	.4byte	.LASF13
	.4byte	0x5f
	.byte	0x1
	.byte	0x1
	.uleb128 0x7
	.4byte	.LASF14
	.byte	0x2
	.2byte	0x1c2
	.4byte	.LASF15
	.4byte	0x7c
	.byte	0x1
	.byte	0x1
	.uleb128 0x7
	.4byte	.LASF16
	.byte	0x2
	.2byte	0x1c6
	.4byte	.LASF17
	.4byte	0x7c
	.byte	0x1
	.byte	0x1
	.uleb128 0x7
	.4byte	.LASF18
	.byte	0x2
	.2byte	0x1ca
	.4byte	.LASF19
	.4byte	0x7c
	.byte	0x1
	.byte	0x1
	.uleb128 0x7
	.4byte	.LASF20
	.byte	0x2
	.2byte	0x1cc
	.4byte	.LASF21
	.4byte	0x7c
	.byte	0x1
	.byte	0x1
	.uleb128 0x7
	.4byte	.LASF22
	.byte	0x2
	.2byte	0x27c
	.4byte	.LASF23
	.4byte	0x7c
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x2007
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x2007
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LLST0:
	.2byte	.LFB0
	.2byte	.LCFI0
	.2byte	0x2
	.byte	0x71
	.sleb128 0
	.2byte	.LCFI0
	.2byte	.LFE0
	.2byte	0x2
	.byte	0x71
	.sleb128 2
	.2byte	0
	.2byte	0
	.section	.debug_aranges,"",@progbits
	.4byte	0x10
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x2
	.byte	0
	.2byte	.LFB0
	.2byte	.LFE0-.LFB0
	.2byte	0
	.2byte	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.2byte	.LFB0
	.2byte	.LFE0
	.2byte	0
	.2byte	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF0:
	.string	"unsigned int"
.LASF17:
	.string	"__TACCTL1"
.LASF12:
	.string	"__P1DIR"
.LASF15:
	.string	"__TACTL"
.LASF9:
	.string	"__ADC10MEM"
.LASF10:
	.string	"P1DIR"
.LASF24:
	.string	"GNU C 4.6.3 20120301 (mspgcc LTS 20120406 unpatched)"
.LASF14:
	.string	"TACTL"
.LASF20:
	.string	"TACCR1"
.LASF3:
	.string	"ADC10CTL0"
.LASF6:
	.string	"ADC10CTL1"
.LASF1:
	.string	"unsigned char"
.LASF8:
	.string	"ADC10MEM"
.LASF27:
	.string	"main"
.LASF5:
	.string	"__ADC10CTL0"
.LASF7:
	.string	"__ADC10CTL1"
.LASF11:
	.string	"P1SEL"
.LASF26:
	.string	"/home/james/Insync/UBC/Courses/PHYS319/lab2/dimmer"
.LASF4:
	.string	"__ADC10AE0"
.LASF13:
	.string	"__P1SEL"
.LASF22:
	.string	"WDTCTL"
.LASF19:
	.string	"__TACCR0"
.LASF21:
	.string	"__TACCR1"
.LASF18:
	.string	"TACCR0"
.LASF23:
	.string	"__WDTCTL"
.LASF2:
	.string	"ADC10AE0"
.LASF16:
	.string	"TACCTL1"
.LASF25:
	.string	"dimmer.c"
