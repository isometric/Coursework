;  this application blinks P1.0 and P1.6

.include "msp430g2x31.inc"

  org 0xf800
start:
  ;mov.w #0x5a80, &WDTCTL
  mov.w #WDTPW|WDTHOLD, &WDTCTL
  mov.b #0x41, &P1DIR  ; 01000001
  mov.w #0x01, r8      ; 00000001
repeat:
  mov.b r8, &P1OUT
  xor.b #0x41, r8      ; 01000001
  mov.w #10000, r9     ; spin n times 

waiter:                ; spin 
  dec r9
  jnz waiter
  jmp repeat

  org 0xfffe
  dw start             ; set reset vector to 'init' label

