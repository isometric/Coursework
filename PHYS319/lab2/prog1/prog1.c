/*
* PHYS319 Lab3 Timing example in C
*
* Written by Ryan Wicks
* 16 January 2012
*
* This program is a C version of the assembly program that formed part of lab 2.
* This is not the best way to implement timing, or to organize your code.
* It is simply one way.
*
* This will almost certainly not give exactly the same timing as the assembly
* program from lab 2, and the output assembly will also be very different, even
* though the task is similar.
*/
#include <msp430g2231.h>
void main(void) {
    // Notice the label volatile. What happens if you remove this?
    volatile unsigned int count;
    WDTCTL = WDTPW + WDTHOLD;
    P1DIR = 0x41;
    P1OUT = 0x01;
    while (1) {
        count = 10000;
        while (count != 0) {
        count--;
        }
    P1OUT = P1OUT ^ 0x41;
    }
}
