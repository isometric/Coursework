	.file	"adc.c"
	.arch msp430g2231
	.cpu 430
	.mpy none

	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.text
.Ltext0:
	.section	.init9,"ax",@progbits
	.p2align 1,0
.global	main
	.type	main,@function
/***********************
 * Function `main' 
 ***********************/
main:
.LFB0:
	.file 1 "adc.c"
	.loc 1 2 0
	sub	#2, r1
.LCFI0:
	.loc 1 3 0
	mov	#23168, &__WDTCTL
	.loc 1 4 0
	mov	#4112, &__ADC10CTL0
	.loc 1 5 0
	mov	#4096, &__ADC10CTL1
	.loc 1 6 0
	mov.b	&__ADC10AE0, r15
	bis.b	#2, r15
	mov.b	r15, &__ADC10AE0
	.loc 1 7 0
	mov.b	&__P1DIR, r15
	bis.b	#69, r15
	mov.b	r15, &__P1DIR
.L8:
	.loc 1 11 0
	mov	&__ADC10CTL0, r15
	bis	#3, r15
	mov	r15, &__ADC10CTL0
	.loc 1 12 0
	nop
.L2:
	.loc 1 12 0 is_stmt 0 discriminator 1
	mov	&__ADC10CTL1, r15
	and	#1, r15
	cmp.b	#0, r15
	jne	.L2
	.loc 1 13 0 is_stmt 1
	mov	&__ADC10MEM, r15
	cmp	#769, r15
	jlo	.L3
	.loc 1 14 0
	mov.b	#1, &__P1OUT
	jmp	.L4
.L3:
	.loc 1 15 0
	mov	&__ADC10MEM, r15
	cmp	#257, r15
	jlo	.L5
	.loc 1 16 0
	mov.b	#4, &__P1OUT
	jmp	.L4
.L5:
	.loc 1 18 0
	mov.b	#64, &__P1OUT
.L4:
	.loc 1 19 0
	mov	#llo(-1), @r1
	jmp	.L6
.L7:
	.loc 1 19 0 is_stmt 0 discriminator 2
	add	#llo(-1), @r1
.L6:
	.loc 1 19 0 discriminator 1
	cmp	#0, @r1
	jne	.L7
	.loc 1 20 0 is_stmt 1
	jmp	.L8
.LFE0:
.Lfe1:
	.size	main,.Lfe1-main
;; End of function 

	.section	.debug_frame,"",@progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.string	""
	.uleb128 0x1
	.sleb128 -2
	.byte	0x0
	.byte	0xc
	.uleb128 0x1
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x1
	.p2align 1,0
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.2byte	.LFB0
	.2byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.p2align 1,0
.LEFDE0:
	.text
.Letext0:
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LLST0:
	.2byte	.LFB0
	.2byte	.LCFI0
	.2byte	0x2
	.byte	0x71
	.sleb128 2
	.2byte	.LCFI0
	.2byte	.LFE0
	.2byte	0x2
	.byte	0x71
	.sleb128 4
	.2byte	0x0
	.2byte	0x0
	.file 2 "/usr/lib/gcc/msp430/4.5.3/../../../../msp430/include/msp430g2231.h"
	.section	.debug_info
	.4byte	0x150
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x2
	.uleb128 0x1
	.4byte	.LASF16
	.byte	0x1
	.4byte	.LASF17
	.4byte	.LASF18
	.2byte	0x0
	.2byte	0x0
	.4byte	.Ldebug_ranges0+0x0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.4byte	.LASF19
	.byte	0x1
	.byte	0x2
	.byte	0x1
	.2byte	.LFB0
	.2byte	.LFE0
	.4byte	.LLST0
	.4byte	0x47
	.uleb128 0x3
	.string	"i"
	.byte	0x1
	.byte	0x8
	.4byte	0x47
	.byte	0x2
	.byte	0x91
	.sleb128 -4
	.byte	0x0
	.uleb128 0x4
	.byte	0x2
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x5
	.4byte	.LASF2
	.byte	0x2
	.byte	0x9d
	.4byte	.LASF4
	.4byte	0x5f
	.byte	0x1
	.byte	0x1
	.uleb128 0x6
	.4byte	0x64
	.uleb128 0x4
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x5
	.4byte	.LASF3
	.byte	0x2
	.byte	0xa0
	.4byte	.LASF5
	.4byte	0x7c
	.byte	0x1
	.byte	0x1
	.uleb128 0x6
	.4byte	0x47
	.uleb128 0x5
	.4byte	.LASF6
	.byte	0x2
	.byte	0xa2
	.4byte	.LASF7
	.4byte	0x7c
	.byte	0x1
	.byte	0x1
	.uleb128 0x5
	.4byte	.LASF8
	.byte	0x2
	.byte	0xa4
	.4byte	.LASF9
	.4byte	0x7c
	.byte	0x1
	.byte	0x1
	.uleb128 0x7
	.4byte	.LASF10
	.byte	0x2
	.2byte	0x199
	.4byte	.LASF12
	.4byte	0x5f
	.byte	0x1
	.byte	0x1
	.uleb128 0x7
	.4byte	.LASF11
	.byte	0x2
	.2byte	0x19b
	.4byte	.LASF13
	.4byte	0x5f
	.byte	0x1
	.byte	0x1
	.uleb128 0x7
	.4byte	.LASF14
	.byte	0x2
	.2byte	0x269
	.4byte	.LASF15
	.4byte	0x7c
	.byte	0x1
	.byte	0x1
	.uleb128 0x5
	.4byte	.LASF2
	.byte	0x2
	.byte	0x9d
	.4byte	.LASF4
	.4byte	0x5f
	.byte	0x1
	.byte	0x1
	.uleb128 0x5
	.4byte	.LASF3
	.byte	0x2
	.byte	0xa0
	.4byte	.LASF5
	.4byte	0x7c
	.byte	0x1
	.byte	0x1
	.uleb128 0x5
	.4byte	.LASF6
	.byte	0x2
	.byte	0xa2
	.4byte	.LASF7
	.4byte	0x7c
	.byte	0x1
	.byte	0x1
	.uleb128 0x5
	.4byte	.LASF8
	.byte	0x2
	.byte	0xa4
	.4byte	.LASF9
	.4byte	0x7c
	.byte	0x1
	.byte	0x1
	.uleb128 0x7
	.4byte	.LASF10
	.byte	0x2
	.2byte	0x199
	.4byte	.LASF12
	.4byte	0x5f
	.byte	0x1
	.byte	0x1
	.uleb128 0x7
	.4byte	.LASF11
	.byte	0x2
	.2byte	0x19b
	.4byte	.LASF13
	.4byte	0x5f
	.byte	0x1
	.byte	0x1
	.uleb128 0x7
	.4byte	.LASF14
	.byte	0x2
	.2byte	0x269
	.4byte	.LASF15
	.4byte	0x7c
	.byte	0x1
	.byte	0x1
	.byte	0x0
	.section	.debug_abbrev
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0x0
	.byte	0x0
	.uleb128 0x2
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0x0
	.byte	0x0
	.uleb128 0x3
	.uleb128 0x34
	.byte	0x0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0x0
	.byte	0x0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0x0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0x0
	.byte	0x0
	.uleb128 0x5
	.uleb128 0x34
	.byte	0x0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x2007
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0x0
	.byte	0x0
	.uleb128 0x6
	.uleb128 0x35
	.byte	0x0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0x0
	.byte	0x0
	.uleb128 0x7
	.uleb128 0x34
	.byte	0x0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x2007
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0x0
	.byte	0x0
	.byte	0x0
	.section	.debug_pubnames,"",@progbits
	.4byte	0x17
	.2byte	0x2
	.4byte	.Ldebug_info0
	.4byte	0x154
	.4byte	0x25
	.string	"main"
	.4byte	0x0
	.section	.debug_aranges,"",@progbits
	.4byte	0x10
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x2
	.byte	0x0
	.2byte	.LFB0
	.2byte	.LFE0-.LFB0
	.2byte	0x0
	.2byte	0x0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.2byte	.LFB0
	.2byte	.LFE0
	.2byte	0x0
	.2byte	0x0
	.section	.debug_str,"MS",@progbits,1
.LASF10:
	.string	"P1OUT"
.LASF0:
	.string	"unsigned int"
.LASF4:
	.string	"*__ADC10AE0"
.LASF8:
	.string	"ADC10MEM"
.LASF1:
	.string	"unsigned char"
.LASF5:
	.string	"*__ADC10CTL0"
.LASF7:
	.string	"*__ADC10CTL1"
.LASF6:
	.string	"ADC10CTL1"
.LASF11:
	.string	"P1DIR"
.LASF9:
	.string	"*__ADC10MEM"
.LASF13:
	.string	"*__P1DIR"
.LASF12:
	.string	"*__P1OUT"
.LASF2:
	.string	"ADC10AE0"
.LASF14:
	.string	"WDTCTL"
.LASF17:
	.string	"adc.c"
.LASF18:
	.string	"/home/james/Dropbox/UBC/PHYS319/lab2/adc"
.LASF19:
	.string	"main"
.LASF16:
	.string	"GNU C 4.5.3"
.LASF15:
	.string	"*__WDTCTL"
.LASF3:
	.string	"ADC10CTL0"
