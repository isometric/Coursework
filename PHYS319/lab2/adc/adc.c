#include <msp430g2231.h>
void main(void) {
    WDTCTL = WDTPW + WDTHOLD;
    ADC10CTL0 = ADC10SHT_2 + ADC10ON;
    ADC10CTL1 = INCH_1;
    ADC10AE0 |= 0x02;
    P1DIR |= 0x45;
    unsigned i;

    while (1) {
        ADC10CTL0 |= ENC + ADC10SC;
        while (ADC10CTL1 & ADC10BUSY);
        if (ADC10MEM > 0x300)
            P1OUT = 0x01;
        else if (ADC10MEM > 0x100)
            P1OUT = 0x04;
        else
            P1OUT = 0x40;
    for (i = 0xFFFF; i > 0; i--);
    }
}
