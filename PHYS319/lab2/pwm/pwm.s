	.file	"pwm.c"
	.arch msp430g2231
	.cpu 430
	.mpy none

	.text
.Ltext0:
	.section	.init9,"ax",@progbits
	.p2align 1,0
.global	main
	.type	main,@function
/***********************
 * Function `main' 
 ***********************/
main:
.LFB0:
	.file 1 "pwm.c"
	.loc 1 2 0
	.loc 1 3 0
	mov	#23168, &__WDTCTL
	.loc 1 5 0
	mov.b	&__P1DIR, r15
	bis.b	#4, r15
	mov.b	r15, &__P1DIR
	.loc 1 6 0
	mov.b	&__P1SEL, r15
	bis.b	#4, r15
	mov.b	r15, &__P1SEL
	.loc 1 8 0
	mov	#999, &__TACCR0
	.loc 1 9 0
	mov	#224, &__TACCTL1
	.loc 1 10 0
	mov	#250, &__TACCR1
	.loc 1 11 0
	mov	#528, &__TACTL
	.loc 1 12 0
	bis	#16, r2
	.loc 1 13 0
.LIRD0:
.LFE0:
.Lfe1:
	.size	main,.Lfe1-main
;; End of function 

	.section	.debug_frame,"",@progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.string	""
	.uleb128 0x1
	.sleb128 -2
	.byte	0
	.byte	0xc
	.uleb128 0x1
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x1
	.p2align 1,0
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.2byte	.LFB0
	.2byte	.LFE0-.LFB0
	.p2align 1,0
.LEFDE0:
	.text
.Letext0:
	.file 2 "/usr/lib/gcc/msp430/4.6.3/../../../../msp430/include/msp430g2231.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.4byte	0xc8
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x2
	.uleb128 0x1
	.4byte	.LASF16
	.byte	0x1
	.4byte	.LASF17
	.4byte	.LASF18
	.2byte	0
	.2byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF19
	.byte	0x1
	.byte	0x2
	.byte	0x1
	.2byte	.LFB0
	.2byte	.LFE0
	.byte	0x2
	.byte	0x71
	.sleb128 0
	.uleb128 0x4
	.4byte	.LASF2
	.byte	0x2
	.2byte	0x19d
	.4byte	.LASF4
	.4byte	0x4e
	.byte	0x1
	.byte	0x1
	.uleb128 0x5
	.4byte	0x53
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x4
	.4byte	.LASF3
	.byte	0x2
	.2byte	0x1a5
	.4byte	.LASF5
	.4byte	0x4e
	.byte	0x1
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF6
	.byte	0x2
	.2byte	0x1c2
	.4byte	.LASF7
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x5
	.4byte	0x25
	.uleb128 0x4
	.4byte	.LASF8
	.byte	0x2
	.2byte	0x1c6
	.4byte	.LASF9
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF10
	.byte	0x2
	.2byte	0x1ca
	.4byte	.LASF11
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF12
	.byte	0x2
	.2byte	0x1cc
	.4byte	.LASF13
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.uleb128 0x4
	.4byte	.LASF14
	.byte	0x2
	.2byte	0x27c
	.4byte	.LASF15
	.4byte	0x7e
	.byte	0x1
	.byte	0x1
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x2007
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",@progbits
	.4byte	0x10
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x2
	.byte	0
	.2byte	.LFB0
	.2byte	.LFE0-.LFB0
	.2byte	0
	.2byte	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.2byte	.LFB0
	.2byte	.LFE0
	.2byte	0
	.2byte	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF2:
	.string	"P1DIR"
.LASF0:
	.string	"unsigned int"
.LASF11:
	.string	"__TACCR0"
.LASF3:
	.string	"P1SEL"
.LASF9:
	.string	"__TACCTL1"
.LASF13:
	.string	"__TACCR1"
.LASF7:
	.string	"__TACTL"
.LASF16:
	.string	"GNU C 4.6.3 20120301 (mspgcc LTS 20120406 unpatched)"
.LASF4:
	.string	"__P1DIR"
.LASF8:
	.string	"TACCTL1"
.LASF12:
	.string	"TACCR1"
.LASF5:
	.string	"__P1SEL"
.LASF1:
	.string	"unsigned char"
.LASF17:
	.string	"pwm.c"
.LASF19:
	.string	"main"
.LASF10:
	.string	"TACCR0"
.LASF15:
	.string	"__WDTCTL"
.LASF14:
	.string	"WDTCTL"
.LASF6:
	.string	"TACTL"
.LASF18:
	.string	"/home/james/Insync/Courses/PHYS319/lab2/pwm"
