/* Copyright James Deng 2014 */

#include <main.h>
#include <msp430g2553.h>


volatile unsigned int timer = 0;
volatile unsigned int linePeriod = 0;
volatile unsigned int line = 0;

volatile unsigned int step = 0;

/* Sets PWM on P1.2 to 1087 Hz, 25% */
void setup() {
    /* disable watchdog timer */
    WDTCTL = WDTPW + WDTHOLD;


    /* config system clock */
    BCSCTL1 = CALBC1_16MHZ;
    DCOCTL  = CALDCO_16MHZ;
    BCSCTL2 &= ~(DIVS_3);


    /* config PWM on P1.2 */
    P1DIR |= BIT2;              // P1.2 to output
    P1SEL |= BIT2;              // P1.2 to TA0.1
    TA0CCR0 = 65535;            // PWM Period
    TA0CCTL1 = OUTMOD_7;        // CCR1 reset/set
    TA0CCR1 = 9000;             // CCR1 PWM duty cycle
    TA0CTL = TASSEL_2 + MC_1;   // SMCLK, up mode


    /* setup laser on P1.3*/
    P1DIR |= BIT3;              // P1.3 to output
    P1OUT |= BIT3;              // P1.3 default on


    /* setup line detect on P1.4*/
    P1DIR &= ~BIT4;             // P1.4 to input
    P1IE  |= BIT4;
    P1REN |= BIT4;              // P1.4 to pull up
    P1OUT |= BIT4;


    /* DIAGNOSTICS */
    P1DIR |= BIT0;              // P1.1 to output
    P1OUT |= BIT0;              // P1.1 default on
    P1DIR |= BIT6;              // P1.6 to output
    P1OUT |= BIT6;              // P1.6 default on
    P1OUT |= BIT7;
    P1DIR |= BIT7;

    /* sanity check */
    // while (1) {
    //     P1OUT ^= BIT7;
    //     __delay_cycles(1000000);
    // }
}


int image_pixel(int column, int row){
    int index = (row * IMG_X + column)/8;          // basic big endian, 8 pixels per byte
    unsigned int offset = column;                   // bit selector
    offset %= 8;

    return ((image[index] << offset) & BIT7);
}


void pairs_pattern() {
    if (timer % 3) P1OUT |= BIT3;
    if (timer % 4) P1OUT &= ~BIT3;
}


void blank_pattern(){
    P1OUT &= ~BIT3;
}


void temporal_pattern(){
    int temp = timer + line;
    P1OUT &= ~BIT3;
    if ( (temp % 16) > 8 ) P1OUT ^= BIT3;
}


void static_pattern(){
    if (line % 100){
        P1OUT &= ~BIT3;
    } else {
        if (timer % 3) P1OUT |= BIT3;
        if (timer % 4) P1OUT &= ~BIT3;
    }
}


void main(void) {
    /* config */
    setup();                    // setup pins/clock
    __delay_cycles(8000000);    // delay 0.5 sec to let motor spin up


    /* main loop */
    __enable_interrupt();
    while (1){
        timer++; // keep time
        __delay_cycles(256);
        // long cycle to hedge against main loop cycle time variance
        // pattern drifts in precision towards end without


        /* display code */

        float column = IMG_X * (((timer/linePeriod) - 2.0/16.0) * 16.0/13.0);  // normalize

        P1OUT &= ~BIT3;
        if (image_pixel(column,line)) P1OUT |= BIT3;

        /* sanity tests */
        // pairs_pattern();
        // blank_pattern();
        // temporal_pattern();
        // static_pattern();

        // manage display window
        if (timer < ((linePeriod * 2)/16) ) P1OUT |= BIT3;
        if (timer > ((linePeriod * 15)/16) )  P1OUT |= BIT3;

        // overflow warning
        if ((linePeriod > 2048) || (timer > 2048)) P1OUT &= ~BIT0;
        //if (linePeriod > 650) P1OUT &= ~BIT6;
    }
}


#pragma vector = PORT1_VECTOR
__interrupt void PORT1_ISR(void) {
    /* timing calibration*/
    linePeriod = (linePeriod * 63 + timer)/64;
    // long decay/influence to counteract random interrupts and timing issues during startup
    timer = 0;

    /* set line speed */
    step =  (step+1) % 2; // change the % part to change line speed
    if (step == 0) line = (line+1) % IMG_Y ;

    /* clear P1.3 interrupt */
    P1IFG &= ~BIT4;
}


// TODO(isometric): port 2 interrupt to time frames
