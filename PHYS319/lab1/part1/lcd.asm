.include "msp430g2x31.inc" 
CPUOFF equ 0x0010                 ; creates compiler alias
  org 0xf800
start: 
  mov #0x0280, SP                 ; set stack pointer
  mov.w #WDTPW|WDTHOLD, &WDTCTL   ; disable watchdog,symbol
  mov.b #11110111b, &P1DIR        ; turns on port 1.x


;        DDDD-AAS  2014
  mov.b #00100110b, &P1OUT
  mov.b #00100111b, &P1OUT

  mov.b #00000100b, &P1OUT
  mov.b #00000101b, &P1OUT

  mov.b #00010010b, &P1OUT
  mov.b #00010011b, &P1OUT

  mov.b #01000000b, &P1OUT
  mov.b #01000001b, &P1OUT

  bis.w #CPUOFF, SR               ; disable CPU 

org 0xfffe                    
dw start                          ; set reset addr with program start

 