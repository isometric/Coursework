; (RG) => 00, 01 -> 10 -> 11 ->
.include "msp430g2x31.inc" ; # instead of ;
  CPUOFF equ 0x0010                 ; creates alias
  org 0xf800                        ; program start location 

RESET:
  mov.w #0x280, sp                  ; set SP
  mov.w #WDTPW|WDTHOLD,&WDTCTL      ; disable watchdog,symbol
  mov.b #11110111b, &P1DIR          ; turns on P1.x (except P1.3)
  mov.b #01001001b, &P1OUT          ; sets P1.6, P1.3, P1.0
  mov.b #00001000b, &P1REN          ; resistor enable on R3 (not working?) 
  mov.b #00001000b, &P1IE           ; enable interupts from P1.3
  mov.b #00001000b, R7              ; set intial state to 00 
  mov.b #00001001b, R8              ; set state 2 to      01
  mov.b #01001000b, R9              ; set state 3 to      10
  mov.b #01001001b, R10             ; set state 4 to      11
  mov.b         R7, &P1OUT          ; activate state 1
  EINT                              ; enable interupt mode 
  bis.w    #CPUOFF, SR              ; turn cpu off 

PUSH:
  mov.b         R7, R11             ; save R7, cycle states down
  mov.b         R8, R7            
  mov.b         R9, R8
  mov.b        R10, R9
  mov.b        R11, R10
  mov.b         R7, &P1OUT          ; activate new state
  bic.b #00001000b, &P1IFG          ; (clear? interupt flag)
  reti                              ; pops SP, pops PC essentially killing state

  org 0xffe4                        ; on P1.3 interupt jump addr, store location of PUSH
  dw PUSH
  org 0xfffe                        ; on RESET interupt's jump addr, store location of RESET
  dw RESET