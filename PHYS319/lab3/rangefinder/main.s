	.file	"main.c"
	.arch msp430g2231
	.cpu 430
	.mpy none

	.text
.Ltext0:
	.comm BitCnt,1
	.comm TXByte,2,2
	.comm Mode,2,2
	.section	.init9,"ax",@progbits
	.p2align 1,0
.global	main
	.type	main,@function
/***********************
 * Function `main' 
 ***********************/
main:
.LFB0:
	.file 1 "main.c"
	.loc 1 86 0
	sub	#2, r1
.LCFI0:
	.loc 1 87 0
	mov	#23168, &__WDTCTL
	.loc 1 90 0
	mov.b	&__CALBC1_1MHZ, r15
	mov.b	r15, &__BCSCTL1
	.loc 1 91 0
	mov.b	&__CALDCO_1MHZ, r15
	mov.b	r15, &__DCOCTL
	.loc 1 92 0
	mov.b	&__BCSCTL2, r15
	and.b	#llo(-7), r15
	mov.b	r15, &__BCSCTL2
	.loc 1 94 0
	call	#InitializeButton
	.loc 1 97 0
	mov.b	&__P1DIR, r15
	bis.b	#81, r15
	mov.b	r15, &__P1DIR
	.loc 1 98 0
	mov.b	&__P1OUT, r15
	and.b	#llo(-82), r15
	mov.b	r15, &__P1OUT
	.loc 1 104 0
	mov.b	&__P1DIR, r15
	and.b	#llo(-3), r15
	mov.b	r15, &__P1DIR
	.loc 1 105 0
	mov.b	&__P1OUT, r15
	bis.b	#2, r15
	mov.b	r15, &__P1OUT
	.loc 1 106 0
	mov.b	&__P1REN, r15
	bis.b	#2, r15
	mov.b	r15, &__P1REN
	.loc 1 111 0
	mov	#0, &Mode
	.loc 1 112 0
	call	#PreApplicationMode
	.loc 1 114 0
	eint
.L7:
.LBB2:
	.loc 1 118 0
	bis	#24, r2
	.loc 1 125 0
	call	#ConfigureTimerUart
	.loc 1 140 0
	mov.b	&__P1OUT, r15
	bis.b	#16, r15
	mov.b	r15, &__P1OUT
	.loc 1 141 0
	mov	#2, r15
.L2:
	dec	r15
	cmp	#0, r15
	jne	.L2
	nop
	nop
	nop
	.loc 1 142 0
	mov.b	&__P1OUT, r15
	mov.b	#0, &__P1OUT
	.loc 1 144 0
	mov	#0, @r1
	.loc 1 146 0
	nop
.L3:
	.loc 1 146 0 is_stmt 0 discriminator 1
	mov.b	&__P1IN, r15
	mov.b	r15, r15
	and	#2, r15
	cmp	#0, r15
	jeq	.L3
	.loc 1 147 0 is_stmt 1
	jmp	.L4
.L5:
	.loc 1 148 0
	add	#1, @r1
.L4:
	.loc 1 147 0 discriminator 1
	mov.b	&__P1IN, r15
	mov.b	r15, r15
	and	#2, r15
	cmp	#0, r15
	jne	.L5
	.loc 1 155 0
	mov	@r1, r15
	mov	#11, r14
	call	#__divhi3
	mov.b	r15, r15
	mov.b	r15, r15
	mov	r15, &TXByte
	.loc 1 156 0
	call	#Transmit
	.loc 1 159 0
	mov	#51, r15
	mov	#llo(-9055), r14
.L6:
	dec	r14
	cmp	#0, r14
	jne	.L6
	dec	r15
	cmp	#0, r15
	jne	.L6
	.loc 1 171 0
	bis	#216, r2
.LBE2:
	.loc 1 175 0
	jmp	.L7
.LIRD0:
.LFE0:
.Lfe1:
	.size	main,.Lfe1-main
;; End of function 

	.text
	.p2align 1,0
.global	PreApplicationMode
	.type	PreApplicationMode,@function
/***********************
 * Function `PreApplicationMode' 
 ***********************/
PreApplicationMode:
.LFB1:
	.loc 1 178 0
	.loc 1 179 0
	mov.b	&__P1DIR, r15
	bis.b	#65, r15
	mov.b	r15, &__P1DIR
	.loc 1 180 0
	mov.b	&__P1OUT, r15
	bis.b	#1, r15
	mov.b	r15, &__P1OUT
	.loc 1 181 0
	mov.b	&__P1OUT, r15
	and.b	#llo(-65), r15
	mov.b	r15, &__P1OUT
	.loc 1 186 0
	mov.b	&__BCSCTL1, r15
	bis.b	#16, r15
	mov.b	r15, &__BCSCTL1
	.loc 1 187 0
	mov.b	&__BCSCTL3, r15
	bis.b	#32, r15
	mov.b	r15, &__BCSCTL3
	.loc 1 195 0
	mov	#1200, &__TACCR0
	.loc 1 196 0
	mov	#272, &__TACTL
	.loc 1 197 0
	mov	#112, &__TACCTL1
	.loc 1 198 0
	mov	#600, &__TACCR1
	.loc 1 199 0
	bis	#216, r2
	.loc 1 201 0
	ret
.LFE1:
.Lfe2:
	.size	PreApplicationMode,.Lfe2-PreApplicationMode
;; End of function 

	.p2align 1,0
.global	ConfigureTimerUart
	.type	ConfigureTimerUart,@function
/***********************
 * Function `ConfigureTimerUart' 
 ***********************/
ConfigureTimerUart:
.LFB2:
	.loc 1 203 0
	.loc 1 204 0
	mov	#4, &__TACCTL0
	.loc 1 205 0
	mov	#736, &__TACTL
	.loc 1 206 0
	mov.b	&__P1SEL, r15
	bis.b	#6, r15
	mov.b	r15, &__P1SEL
	.loc 1 207 0
	mov.b	&__P1DIR, r15
	bis.b	#2, r15
	mov.b	r15, &__P1DIR
	.loc 1 208 0
	ret
.LFE2:
.Lfe3:
	.size	ConfigureTimerUart,.Lfe3-ConfigureTimerUart
;; End of function 

	.p2align 1,0
.global	Transmit
	.type	Transmit,@function
/***********************
 * Function `Transmit' 
 ***********************/
Transmit:
.LFB3:
	.loc 1 215 0
	.loc 1 216 0
	mov.b	#10, &BitCnt
	.loc 1 217 0
	mov	&TXByte, r15
	bis	#256, r15
	mov	r15, &TXByte
	.loc 1 218 0
	mov	&TXByte, r15
	rla	r15
	mov	r15, &TXByte
	.loc 1 235 0
	mov	&__TAR, r15
	add	#52, r15
	mov	r15, &__TACCR0
	.loc 1 236 0
	mov	#4144, &__TACCTL0
	.loc 1 239 0
	nop
.L11:
	.loc 1 239 0 is_stmt 0 discriminator 1
	mov	&__TACCTL0, r15
	and	#16, r15
	cmp	#0, r15
	jne	.L11
	.loc 1 240 0 is_stmt 1
	ret
.LFE3:
.Lfe4:
	.size	Transmit,.Lfe4-Transmit
;; End of function 

	.p2align 1,0
.global	Timer_A
	.type	Timer_A,@function
/***********************
 * Interrupt Vector 9 Service Routine `Timer_A' 
 ***********************/
Timer_A:
.global	__isr_9
__isr_9:
.LFB4:
	.loc 1 245 0
	push	r15
.LCFI1:
	.loc 1 246 0
	mov	&__TACCR0, r15
	add	#52, r15
	mov	r15, &__TACCR0
	.loc 1 247 0
	mov.b	&BitCnt, r15
	cmp.b	#0, r15
	jne	.L13
	.loc 1 248 0
	mov.b	&__P1SEL, r15
	and.b	#llo(-7), r15
	mov.b	r15, &__P1SEL
	.loc 1 249 0
	mov	&__TACCTL0, r15
	and	#llo(-17), r15
	mov	r15, &__TACCTL0
	jmp	.L12
.L13:
	.loc 1 258 0
	mov	&__TACCTL0, r15
	bis	#128, r15
	mov	r15, &__TACCTL0
	.loc 1 259 0
	mov	&TXByte, r15
	and	#1, r15
	mov.b	r15, r15
	cmp.b	#0, r15
	jeq	.L15
	.loc 1 260 0
	mov	&__TACCTL0, r15
	and	#llo(-129), r15
	mov	r15, &__TACCTL0
.L15:
	.loc 1 261 0
	mov	&TXByte, r15
	clrc
	rrc	r15
	mov	r15, &TXByte
	.loc 1 262 0
	mov.b	&BitCnt, r15
	add.b	#llo(-1), r15
	mov.b	r15, &BitCnt
.L12:
	.loc 1 264 0
	pop	r15
	reti
.LFE4:
.Lfe5:
	.size	Timer_A,.Lfe5-Timer_A
;; End of function 

	.p2align 1,0
.global	ta1_isr
	.type	ta1_isr,@function
/***********************
 * Interrupt Vector 8 Service Routine `ta1_isr' 
 ***********************/
ta1_isr:
.global	__isr_8
__isr_8:
.LFB5:
	.loc 1 268 0
	push	r15
.LCFI2:
	.loc 1 269 0
	mov	&__TACCTL1, r15
	and	#llo(-2), r15
	mov	r15, &__TACCTL1
	.loc 1 270 0
	mov	&Mode, r15
	cmp	#0, r15
	jne	.L17
	.loc 1 271 0
	mov.b	&__P1OUT, r15
	xor.b	#65, r15
	mov.b	r15, &__P1OUT
	jmp	.L16
.L17:
	.loc 1 273 0
	mov	#0, &__TACCTL1
	.loc 1 274 0
	bic	#208, 2(r1)
.L16:
	.loc 1 276 0
	pop	r15
	reti
.LFE5:
.Lfe6:
	.size	ta1_isr,.Lfe6-ta1_isr
;; End of function 

	.p2align 1,0
.global	InitializeButton
	.type	InitializeButton,@function
/***********************
 * Function `InitializeButton' 
 ***********************/
InitializeButton:
.LFB6:
	.loc 1 278 0
	.loc 1 279 0
	mov.b	&__P1DIR, r15
	and.b	#llo(-9), r15
	mov.b	r15, &__P1DIR
	.loc 1 280 0
	mov.b	&__P1OUT, r15
	bis.b	#8, r15
	mov.b	r15, &__P1OUT
	.loc 1 281 0
	mov.b	&__P1REN, r15
	bis.b	#8, r15
	mov.b	r15, &__P1REN
	.loc 1 282 0
	mov.b	&__P1IES, r15
	bis.b	#8, r15
	mov.b	r15, &__P1IES
	.loc 1 283 0
	mov.b	&__P1IFG, r15
	and.b	#llo(-9), r15
	mov.b	r15, &__P1IFG
	.loc 1 284 0
	mov.b	&__P1IE, r15
	bis.b	#8, r15
	mov.b	r15, &__P1IE
	.loc 1 285 0
	ret
.LFE6:
.Lfe7:
	.size	InitializeButton,.Lfe7-InitializeButton
;; End of function 

	.p2align 1,0
.global	PORT1_ISR
	.type	PORT1_ISR,@function
/***********************
 * Interrupt Vector 2 Service Routine `PORT1_ISR' 
 ***********************/
PORT1_ISR:
.global	__isr_2
__isr_2:
.LFB7:
	.loc 1 289 0
	push	r15
.LCFI3:
	.loc 1 293 0
	mov.b	#0, &__P1IFG
	.loc 1 294 0
	mov.b	&__P1IE, r15
	and.b	#llo(-9), r15
	mov.b	r15, &__P1IE
	.loc 1 295 0
	mov	#23069, &__WDTCTL
	.loc 1 296 0
	mov.b	&__IFG1, r15
	and.b	#llo(-2), r15
	mov.b	r15, &__IFG1
	.loc 1 297 0
	mov.b	&__IE1, r15
	bis.b	#1, r15
	mov.b	r15, &__IE1
	.loc 1 299 0
	mov	#0, &__TACCTL1
	.loc 1 300 0
	mov.b	&__P1OUT, r15
	and.b	#llo(-66), r15
	mov.b	r15, &__P1OUT
	.loc 1 301 0
	mov	#1, &Mode
	.loc 1 302 0
	bic	#208, 2(r1)
	.loc 1 303 0
	pop	r15
	reti
.LFE7:
.Lfe8:
	.size	PORT1_ISR,.Lfe8-PORT1_ISR
;; End of function 

	.p2align 1,0
.global	WDT_ISR
	.type	WDT_ISR,@function
/***********************
 * Interrupt Vector 10 Service Routine `WDT_ISR' 
 ***********************/
WDT_ISR:
.global	__isr_10
__isr_10:
.LFB8:
	.loc 1 307 0
	push	r15
.LCFI4:
	.loc 1 308 0
	mov.b	&__IE1, r15
	and.b	#llo(-2), r15
	mov.b	r15, &__IE1
	.loc 1 309 0
	mov.b	&__IFG1, r15
	and.b	#llo(-2), r15
	mov.b	r15, &__IFG1
	.loc 1 310 0
	mov	#23168, &__WDTCTL
	.loc 1 311 0
	mov.b	&__P1IE, r15
	bis.b	#8, r15
	mov.b	r15, &__P1IE
	.loc 1 312 0
	pop	r15
	reti
.LFE8:
.Lfe9:
	.size	WDT_ISR,.Lfe9-WDT_ISR
;; End of function 

	.p2align 1,0
.global	ADC10_ISR
	.type	ADC10_ISR,@function
/***********************
 * Interrupt Vector 5 Service Routine `ADC10_ISR' 
 ***********************/
ADC10_ISR:
.global	__isr_5
__isr_5:
.LFB9:
	.loc 1 316 0
	.loc 1 317 0
	bic	#16, @r1
	.loc 1 318 0
	reti
.LFE9:
.Lfe10:
	.size	ADC10_ISR,.Lfe10-ADC10_ISR
;; End of function 

	.section	.debug_frame,"",@progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.string	""
	.uleb128 0x1
	.sleb128 -2
	.byte	0
	.byte	0xc
	.uleb128 0x1
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x1
	.p2align 1,0
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.2byte	.LFB0
	.2byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x4
	.p2align 1,0
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.2byte	.LFB1
	.2byte	.LFE1-.LFB1
	.p2align 1,0
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.2byte	.LFB2
	.2byte	.LFE2-.LFB2
	.p2align 1,0
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.2byte	.LFB3
	.2byte	.LFE3-.LFB3
	.p2align 1,0
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.2byte	.LFB4
	.2byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI1-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8f
	.uleb128 0x2
	.p2align 1,0
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.2byte	.LFB5
	.2byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI2-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8f
	.uleb128 0x2
	.p2align 1,0
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.2byte	.LFB6
	.2byte	.LFE6-.LFB6
	.p2align 1,0
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.2byte	.LFB7
	.2byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI3-.LFB7
	.byte	0xe
	.uleb128 0x4
	.byte	0x8f
	.uleb128 0x2
	.p2align 1,0
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.2byte	.LFB8
	.2byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI4-.LFB8
	.byte	0xe
	.uleb128 0x4
	.byte	0x8f
	.uleb128 0x2
	.p2align 1,0
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.2byte	.LFB9
	.2byte	.LFE9-.LFB9
	.p2align 1,0
.LEFDE18:
	.text
.Letext0:
	.file 2 "/usr/msp430/include/msp430g2231.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.4byte	0x2d5
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x2
	.uleb128 0x1
	.4byte	.LASF55
	.byte	0x1
	.4byte	.LASF56
	.4byte	.LASF57
	.2byte	0
	.2byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF58
	.byte	0x1
	.byte	0x56
	.byte	0x1
	.2byte	.LFB0
	.2byte	.LFE0
	.4byte	.LLST0
	.4byte	0x56
	.uleb128 0x4
	.2byte	.LBB2
	.2byte	.LBE2
	.uleb128 0x5
	.4byte	.LASF59
	.byte	0x1
	.byte	0x90
	.4byte	0x56
	.byte	0x2
	.byte	0x91
	.sleb128 -2
	.byte	0
	.byte	0
	.uleb128 0x6
	.byte	0x2
	.byte	0x5
	.string	"int"
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF1
	.byte	0x1
	.byte	0xb2
	.byte	0x1
	.2byte	.LFB1
	.2byte	.LFE1
	.byte	0x2
	.byte	0x71
	.sleb128 2
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0xcb
	.byte	0x1
	.2byte	.LFB2
	.2byte	.LFE2
	.byte	0x2
	.byte	0x71
	.sleb128 2
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xd7
	.byte	0x1
	.2byte	.LFB3
	.2byte	.LFE3
	.byte	0x2
	.byte	0x71
	.sleb128 2
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xf5
	.byte	0x1
	.2byte	.LFB4
	.2byte	.LFE4
	.4byte	.LLST1
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.2byte	0x10c
	.byte	0x1
	.2byte	.LFB5
	.2byte	.LFE5
	.4byte	.LLST2
	.uleb128 0xa
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.2byte	0x116
	.byte	0x1
	.2byte	.LFB6
	.2byte	.LFE6
	.byte	0x2
	.byte	0x71
	.sleb128 2
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x121
	.byte	0x1
	.2byte	.LFB7
	.2byte	.LFE7
	.4byte	.LLST3
	.uleb128 0x9
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x133
	.byte	0x1
	.2byte	.LFB8
	.2byte	.LFE8
	.4byte	.LLST4
	.uleb128 0xa
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x13c
	.byte	0x1
	.2byte	.LFB9
	.2byte	.LFE9
	.byte	0x2
	.byte	0x71
	.sleb128 0
	.uleb128 0xb
	.string	"IE1"
	.byte	0x2
	.byte	0x87
	.4byte	.LASF60
	.4byte	0x107
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	0x10c
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF10
	.uleb128 0xd
	.4byte	.LASF11
	.byte	0x2
	.byte	0x8e
	.4byte	.LASF61
	.4byte	0x107
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF12
	.byte	0x2
	.2byte	0x111
	.4byte	.LASF14
	.4byte	0x107
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF13
	.byte	0x2
	.2byte	0x113
	.4byte	.LASF15
	.4byte	0x107
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF16
	.byte	0x2
	.2byte	0x115
	.4byte	.LASF17
	.4byte	0x107
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF18
	.byte	0x2
	.2byte	0x117
	.4byte	.LASF19
	.4byte	0x107
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF20
	.byte	0x2
	.2byte	0x199
	.4byte	.LASF21
	.4byte	0x17e
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	0x107
	.uleb128 0xe
	.4byte	.LASF22
	.byte	0x2
	.2byte	0x19b
	.4byte	.LASF23
	.4byte	0x107
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF24
	.byte	0x2
	.2byte	0x19d
	.4byte	.LASF25
	.4byte	0x107
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF26
	.byte	0x2
	.2byte	0x19f
	.4byte	.LASF27
	.4byte	0x107
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF28
	.byte	0x2
	.2byte	0x1a1
	.4byte	.LASF29
	.4byte	0x107
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF30
	.byte	0x2
	.2byte	0x1a3
	.4byte	.LASF31
	.4byte	0x107
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF32
	.byte	0x2
	.2byte	0x1a5
	.4byte	.LASF33
	.4byte	0x107
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF34
	.byte	0x2
	.2byte	0x1a7
	.4byte	.LASF35
	.4byte	0x107
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF36
	.byte	0x2
	.2byte	0x1c2
	.4byte	.LASF37
	.4byte	0x213
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	0x25
	.uleb128 0xe
	.4byte	.LASF38
	.byte	0x2
	.2byte	0x1c4
	.4byte	.LASF39
	.4byte	0x213
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF40
	.byte	0x2
	.2byte	0x1c6
	.4byte	.LASF41
	.4byte	0x213
	.byte	0x1
	.byte	0x1
	.uleb128 0x10
	.string	"TAR"
	.byte	0x2
	.2byte	0x1c8
	.4byte	.LASF62
	.4byte	0x213
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF42
	.byte	0x2
	.2byte	0x1ca
	.4byte	.LASF43
	.4byte	0x213
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF44
	.byte	0x2
	.2byte	0x1cc
	.4byte	.LASF45
	.4byte	0x213
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF46
	.byte	0x2
	.2byte	0x27c
	.4byte	.LASF47
	.4byte	0x213
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF48
	.byte	0x2
	.2byte	0x2ac
	.4byte	.LASF49
	.4byte	0x17e
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	.LASF50
	.byte	0x2
	.2byte	0x2ae
	.4byte	.LASF51
	.4byte	0x17e
	.byte	0x1
	.byte	0x1
	.uleb128 0x11
	.4byte	.LASF52
	.byte	0x1
	.byte	0x4c
	.4byte	0x10c
	.byte	0x1
	.byte	0x3
	.byte	0x3
	.2byte	BitCnt
	.uleb128 0x11
	.4byte	.LASF53
	.byte	0x1
	.byte	0x4d
	.4byte	0x25
	.byte	0x1
	.byte	0x3
	.byte	0x3
	.2byte	TXByte
	.uleb128 0x11
	.4byte	.LASF54
	.byte	0x1
	.byte	0x4e
	.4byte	0x213
	.byte	0x1
	.byte	0x3
	.byte	0x3
	.2byte	Mode
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x2007
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x2007
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x2007
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x2007
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LLST0:
	.2byte	.LFB0
	.2byte	.LCFI0
	.2byte	0x2
	.byte	0x71
	.sleb128 0
	.2byte	.LCFI0
	.2byte	.LFE0
	.2byte	0x2
	.byte	0x71
	.sleb128 2
	.2byte	0
	.2byte	0
.LLST1:
	.2byte	.LFB4
	.2byte	.LCFI1
	.2byte	0x2
	.byte	0x71
	.sleb128 0
	.2byte	.LCFI1
	.2byte	.LFE4
	.2byte	0x2
	.byte	0x71
	.sleb128 2
	.2byte	0
	.2byte	0
.LLST2:
	.2byte	.LFB5
	.2byte	.LCFI2
	.2byte	0x2
	.byte	0x71
	.sleb128 0
	.2byte	.LCFI2
	.2byte	.LFE5
	.2byte	0x2
	.byte	0x71
	.sleb128 2
	.2byte	0
	.2byte	0
.LLST3:
	.2byte	.LFB7
	.2byte	.LCFI3
	.2byte	0x2
	.byte	0x71
	.sleb128 0
	.2byte	.LCFI3
	.2byte	.LFE7
	.2byte	0x2
	.byte	0x71
	.sleb128 2
	.2byte	0
	.2byte	0
.LLST4:
	.2byte	.LFB8
	.2byte	.LCFI4
	.2byte	0x2
	.byte	0x71
	.sleb128 0
	.2byte	.LCFI4
	.2byte	.LFE8
	.2byte	0x2
	.byte	0x71
	.sleb128 2
	.2byte	0
	.2byte	0
	.section	.debug_aranges,"",@progbits
	.4byte	0x14
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x2
	.byte	0
	.2byte	.Ltext0
	.2byte	.Letext0-.Ltext0
	.2byte	.LFB0
	.2byte	.LFE0-.LFB0
	.2byte	0
	.2byte	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.2byte	.Ltext0
	.2byte	.Letext0
	.2byte	.LFB0
	.2byte	.LFE0
	.2byte	0
	.2byte	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF4:
	.string	"Timer_A"
.LASF47:
	.string	"__WDTCTL"
.LASF11:
	.string	"IFG1"
.LASF37:
	.string	"__TACTL"
.LASF32:
	.string	"P1SEL"
.LASF48:
	.string	"CALDCO_1MHZ"
.LASF53:
	.string	"TXByte"
.LASF28:
	.string	"P1IES"
.LASF25:
	.string	"__P1DIR"
.LASF5:
	.string	"ta1_isr"
.LASF44:
	.string	"TACCR1"
.LASF34:
	.string	"P1REN"
.LASF33:
	.string	"__P1SEL"
.LASF30:
	.string	"P1IE"
.LASF27:
	.string	"__P1IFG"
.LASF9:
	.string	"ADC10_ISR"
.LASF62:
	.string	"__TAR"
.LASF10:
	.string	"unsigned char"
.LASF36:
	.string	"TACTL"
.LASF60:
	.string	"__IE1"
.LASF31:
	.string	"__P1IE"
.LASF13:
	.string	"BCSCTL1"
.LASF16:
	.string	"BCSCTL2"
.LASF18:
	.string	"BCSCTL3"
.LASF26:
	.string	"P1IFG"
.LASF21:
	.string	"__P1IN"
.LASF52:
	.string	"BitCnt"
.LASF38:
	.string	"TACCTL0"
.LASF40:
	.string	"TACCTL1"
.LASF23:
	.string	"__P1OUT"
.LASF51:
	.string	"__CALBC1_1MHZ"
.LASF43:
	.string	"__TACCR0"
.LASF58:
	.string	"main"
.LASF6:
	.string	"InitializeButton"
.LASF24:
	.string	"P1DIR"
.LASF57:
	.string	"/home/james/Insync/UBC/Courses/PHYS319/lab3/rangefinder"
.LASF15:
	.string	"__BCSCTL1"
.LASF20:
	.string	"P1IN"
.LASF0:
	.string	"unsigned int"
.LASF19:
	.string	"__BCSCTL3"
.LASF7:
	.string	"PORT1_ISR"
.LASF59:
	.string	"dist"
.LASF22:
	.string	"P1OUT"
.LASF3:
	.string	"Transmit"
.LASF1:
	.string	"PreApplicationMode"
.LASF42:
	.string	"TACCR0"
.LASF56:
	.string	"main.c"
.LASF55:
	.string	"GNU C 4.6.3 20120301 (mspgcc LTS 20120406 unpatched)"
.LASF14:
	.string	"__DCOCTL"
.LASF8:
	.string	"WDT_ISR"
.LASF17:
	.string	"__BCSCTL2"
.LASF49:
	.string	"__CALDCO_1MHZ"
.LASF54:
	.string	"Mode"
.LASF29:
	.string	"__P1IES"
.LASF45:
	.string	"__TACCR1"
.LASF39:
	.string	"__TACCTL0"
.LASF41:
	.string	"__TACCTL1"
.LASF35:
	.string	"__P1REN"
.LASF61:
	.string	"__IFG1"
.LASF46:
	.string	"WDTCTL"
.LASF12:
	.string	"DCOCTL"
.LASF50:
	.string	"CALBC1_1MHZ"
.LASF2:
	.string	"ConfigureTimerUart"
