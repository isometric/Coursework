	.file	"main.c"
	.arch msp430g2231
	.cpu 430
	.mpy none

	.text
.Ltext0:
	.comm BitCnt,1
	.comm TXByte,2,2
	.comm Mode,2,2
	.section	.init9,"ax",@progbits
	.p2align 1,0
.global	main
	.type	main,@function
/***********************
 * Function `main' 
 ***********************/
main:
.LFB0:
	.file 1 "main.c"
	.loc 1 86 0
	sub	#4, r1
.LCFI0:
	.loc 1 89 0
	mov	#23168, &__WDTCTL
	.loc 1 92 0
	mov.b	&__CALBC1_1MHZ, r15
	mov.b	r15, &__BCSCTL1
	.loc 1 93 0
	mov.b	&__CALDCO_1MHZ, r15
	mov.b	r15, &__DCOCTL
	.loc 1 94 0
	mov.b	&__BCSCTL2, r15
	and.b	#llo(-7), r15
	mov.b	r15, &__BCSCTL2
	.loc 1 96 0
	call	#InitializeButton
	.loc 1 99 0
	mov.b	&__P1DIR, r15
	bis.b	#65, r15
	mov.b	r15, &__P1DIR
	.loc 1 100 0
	mov.b	&__P1OUT, r15
	and.b	#llo(-66), r15
	mov.b	r15, &__P1OUT
	.loc 1 102 0
	mov.b	&__P1DIR, r15
	bis.b	#2, r15
	mov.b	r15, &__P1DIR
	.loc 1 103 0
	mov.b	&__P1OUT, r15
	bis.b	#2, r15
	mov.b	r15, &__P1OUT
	.loc 1 105 0
	mov	#0, &Mode
	.loc 1 106 0
	call	#PreApplicationMode
	.loc 1 109 0
	mov	#llo(-24480), &__ADC10CTL1
	.loc 1 110 0
	mov	#14392, &__ADC10CTL0
	.loc 1 112 0
	mov	#332, r15
.L2:
	dec	r15
	cmp	#0, r15
	jne	.L2
	nop
	nop
	.loc 1 114 0
	eint
.L5:
	.loc 1 119 0 discriminator 1
	mov	&__ADC10CTL0, r15
	bis	#3, r15
	mov	r15, &__ADC10CTL0
	.loc 1 120 0 discriminator 1
	bis	#24, r2
	.loc 1 125 0 discriminator 1
	mov	&__ADC10MEM, r15
	mov	r15, @r1
	mov	#0, 2(r1)
	.loc 1 128 0 discriminator 1
	call	#ConfigureTimerUart
	.loc 1 131 0 discriminator 1
	mov	@r1, r12
	mov	2(r1), r13
	mov	r12, r14
	mov	r13, r15
	rla	r14
	rlc	r15
	add	r12, r14
	addc	r13, r15
	rla	r14
	rlc	r15
	rla	r14
	rlc	r15
	rla	r14
	rlc	r15
	rla	r14
	rlc	r15
	rla	r14
	rlc	r15
	sub	r12, r14
	subc	r13, r15
	rla	r14
	rlc	r15
	rla	r14
	rlc	r15
	rla	r14
	rlc	r15
	add	r12, r14
	addc	r13, r15
	add	#llo(-479430), r14
	addc	#lhi(-479430), r15
	cmp	#0, r15
	jl	.L4
	cmp	#1, r15
	jge	.L3
	cmp	#0, r14
	jhs	.L3
.L4:
	add	#1023, r14
	addc	#lhi(1023), r15
.L3:
	swpb	r14
	swpb	r15
	xor.b	r15, r14
	xor	r15, r14
	sxt	r15
	rra	r15
	rrc	r14
	rra	r15
	rrc	r14
	mov.b	r14, r15
	mov.b	r15, r15
	mov	r15, &TXByte
	.loc 1 132 0 discriminator 1
	call	#Transmit
	.loc 1 134 0 discriminator 1
	mov.b	&__P1OUT, r15
	xor.b	#1, r15
	mov.b	r15, &__P1OUT
	.loc 1 137 0 discriminator 1
	mov	#2400, &__TACCR0
	.loc 1 138 0 discriminator 1
	mov	#272, &__TACTL
	.loc 1 139 0 discriminator 1
	mov	#2400, &__TACCR1
	.loc 1 140 0 discriminator 1
	mov	#16, &__TACCTL1
	.loc 1 143 0 discriminator 1
	bis	#216, r2
	.loc 1 148 0 discriminator 1
	jmp	.L5
.LIRD0:
.LFE0:
.Lfe1:
	.size	main,.Lfe1-main
;; End of function 

	.text
	.p2align 1,0
.global	PreApplicationMode
	.type	PreApplicationMode,@function
/***********************
 * Function `PreApplicationMode' 
 ***********************/
PreApplicationMode:
.LFB1:
	.loc 1 152 0
	.loc 1 153 0
	mov.b	&__P1DIR, r15
	bis.b	#65, r15
	mov.b	r15, &__P1DIR
	.loc 1 154 0
	mov.b	&__P1OUT, r15
	bis.b	#1, r15
	mov.b	r15, &__P1OUT
	.loc 1 155 0
	mov.b	&__P1OUT, r15
	and.b	#llo(-65), r15
	mov.b	r15, &__P1OUT
	.loc 1 160 0
	mov.b	&__BCSCTL1, r15
	bis.b	#16, r15
	mov.b	r15, &__BCSCTL1
	.loc 1 161 0
	mov.b	&__BCSCTL3, r15
	bis.b	#32, r15
	mov.b	r15, &__BCSCTL3
	.loc 1 169 0
	mov	#1200, &__TACCR0
	.loc 1 170 0
	mov	#272, &__TACTL
	.loc 1 171 0
	mov	#112, &__TACCTL1
	.loc 1 172 0
	mov	#600, &__TACCR1
	.loc 1 173 0
	bis	#216, r2
	.loc 1 175 0
	ret
.LFE1:
.Lfe2:
	.size	PreApplicationMode,.Lfe2-PreApplicationMode
;; End of function 

	.p2align 1,0
.global	ConfigureTimerUart
	.type	ConfigureTimerUart,@function
/***********************
 * Function `ConfigureTimerUart' 
 ***********************/
ConfigureTimerUart:
.LFB2:
	.loc 1 177 0
	.loc 1 178 0
	mov	#4, &__TACCTL0
	.loc 1 179 0
	mov	#736, &__TACTL
	.loc 1 180 0
	mov.b	&__P1SEL, r15
	bis.b	#6, r15
	mov.b	r15, &__P1SEL
	.loc 1 181 0
	mov.b	&__P1DIR, r15
	bis.b	#2, r15
	mov.b	r15, &__P1DIR
	.loc 1 182 0
	ret
.LFE2:
.Lfe3:
	.size	ConfigureTimerUart,.Lfe3-ConfigureTimerUart
;; End of function 

	.p2align 1,0
.global	Transmit
	.type	Transmit,@function
/***********************
 * Function `Transmit' 
 ***********************/
Transmit:
.LFB3:
	.loc 1 190 0
	.loc 1 192 0
	mov.b	#10, &BitCnt
	.loc 1 193 0
	mov	&TXByte, r15
	bis	#256, r15
	mov	r15, &TXByte
	.loc 1 194 0
	mov	&TXByte, r15
	rla	r15
	mov	r15, &TXByte
	.loc 1 211 0
	mov	&__TAR, r15
	add	#52, r15
	mov	r15, &__TACCR0
	.loc 1 212 0
	mov	#4144, &__TACCTL0
	.loc 1 215 0
	nop
.L9:
	.loc 1 215 0 is_stmt 0 discriminator 1
	mov	&__TACCTL0, r15
	and	#16, r15
	cmp	#0, r15
	jne	.L9
	.loc 1 216 0 is_stmt 1
	ret
.LFE3:
.Lfe4:
	.size	Transmit,.Lfe4-Transmit
;; End of function 

	.p2align 1,0
.global	Timer_A
	.type	Timer_A,@function
/***********************
 * Interrupt Vector 9 Service Routine `Timer_A' 
 ***********************/
Timer_A:
.global	__isr_9
__isr_9:
.LFB4:
	.loc 1 222 0
	push	r15
.LCFI1:
	.loc 1 223 0
	mov	&__TACCR0, r15
	add	#52, r15
	mov	r15, &__TACCR0
	.loc 1 224 0
	mov.b	&BitCnt, r15
	cmp.b	#0, r15
	jne	.L11
	.loc 1 225 0
	mov.b	&__P1SEL, r15
	and.b	#llo(-7), r15
	mov.b	r15, &__P1SEL
	.loc 1 226 0
	mov	&__TACCTL0, r15
	and	#llo(-17), r15
	mov	r15, &__TACCTL0
	jmp	.L10
.L11:
	.loc 1 236 0
	mov	&__TACCTL0, r15
	bis	#128, r15
	mov	r15, &__TACCTL0
	.loc 1 237 0
	mov	&TXByte, r15
	and	#1, r15
	mov.b	r15, r15
	cmp.b	#0, r15
	jeq	.L13
	.loc 1 238 0
	mov	&__TACCTL0, r15
	and	#llo(-129), r15
	mov	r15, &__TACCTL0
.L13:
	.loc 1 239 0
	mov	&TXByte, r15
	clrc
	rrc	r15
	mov	r15, &TXByte
	.loc 1 240 0
	mov.b	&BitCnt, r15
	add.b	#llo(-1), r15
	mov.b	r15, &BitCnt
.L10:
	.loc 1 243 0
	pop	r15
	reti
.LFE4:
.Lfe5:
	.size	Timer_A,.Lfe5-Timer_A
;; End of function 

	.p2align 1,0
.global	ta1_isr
	.type	ta1_isr,@function
/***********************
 * Interrupt Vector 8 Service Routine `ta1_isr' 
 ***********************/
ta1_isr:
.global	__isr_8
__isr_8:
.LFB5:
	.loc 1 248 0
	push	r15
.LCFI2:
	.loc 1 249 0
	mov	&__TACCTL1, r15
	and	#llo(-2), r15
	mov	r15, &__TACCTL1
	.loc 1 250 0
	mov	&Mode, r15
	cmp	#0, r15
	jne	.L15
	.loc 1 251 0
	mov.b	&__P1OUT, r15
	xor.b	#65, r15
	mov.b	r15, &__P1OUT
	jmp	.L14
.L15:
	.loc 1 254 0
	mov	#0, &__TACCTL1
	.loc 1 255 0
	bic	#208, 2(r1)
.L14:
	.loc 1 258 0
	pop	r15
	reti
.LFE5:
.Lfe6:
	.size	ta1_isr,.Lfe6-ta1_isr
;; End of function 

	.p2align 1,0
.global	InitializeButton
	.type	InitializeButton,@function
/***********************
 * Function `InitializeButton' 
 ***********************/
InitializeButton:
.LFB6:
	.loc 1 261 0
	.loc 1 262 0
	mov.b	&__P1DIR, r15
	and.b	#llo(-9), r15
	mov.b	r15, &__P1DIR
	.loc 1 263 0
	mov.b	&__P1OUT, r15
	bis.b	#8, r15
	mov.b	r15, &__P1OUT
	.loc 1 264 0
	mov.b	&__P1REN, r15
	bis.b	#8, r15
	mov.b	r15, &__P1REN
	.loc 1 265 0
	mov.b	&__P1IES, r15
	bis.b	#8, r15
	mov.b	r15, &__P1IES
	.loc 1 266 0
	mov.b	&__P1IFG, r15
	and.b	#llo(-9), r15
	mov.b	r15, &__P1IFG
	.loc 1 267 0
	mov.b	&__P1IE, r15
	bis.b	#8, r15
	mov.b	r15, &__P1IE
	.loc 1 268 0
	ret
.LFE6:
.Lfe7:
	.size	InitializeButton,.Lfe7-InitializeButton
;; End of function 

	.p2align 1,0
.global	PORT1_ISR
	.type	PORT1_ISR,@function
/***********************
 * Interrupt Vector 2 Service Routine `PORT1_ISR' 
 ***********************/
PORT1_ISR:
.global	__isr_2
__isr_2:
.LFB7:
	.loc 1 277 0
	push	r15
.LCFI3:
	.loc 1 286 0
	mov.b	#0, &__P1IFG
	.loc 1 287 0
	mov.b	&__P1IE, r15
	and.b	#llo(-9), r15
	mov.b	r15, &__P1IE
	.loc 1 288 0
	mov	#23069, &__WDTCTL
	.loc 1 289 0
	mov.b	&__IFG1, r15
	and.b	#llo(-2), r15
	mov.b	r15, &__IFG1
	.loc 1 290 0
	mov.b	&__IE1, r15
	bis.b	#1, r15
	mov.b	r15, &__IE1
	.loc 1 292 0
	mov	#0, &__TACCTL1
	.loc 1 293 0
	mov.b	&__P1OUT, r15
	and.b	#llo(-66), r15
	mov.b	r15, &__P1OUT
	.loc 1 294 0
	mov	#1, &Mode
	.loc 1 295 0
	bic	#208, 2(r1)
	.loc 1 297 0
	pop	r15
	reti
.LFE7:
.Lfe8:
	.size	PORT1_ISR,.Lfe8-PORT1_ISR
;; End of function 

	.p2align 1,0
.global	WDT_ISR
	.type	WDT_ISR,@function
/***********************
 * Interrupt Vector 10 Service Routine `WDT_ISR' 
 ***********************/
WDT_ISR:
.global	__isr_10
__isr_10:
.LFB8:
	.loc 1 302 0
	push	r15
.LCFI4:
	.loc 1 303 0
	mov.b	&__IE1, r15
	and.b	#llo(-2), r15
	mov.b	r15, &__IE1
	.loc 1 304 0
	mov.b	&__IFG1, r15
	and.b	#llo(-2), r15
	mov.b	r15, &__IFG1
	.loc 1 305 0
	mov	#23168, &__WDTCTL
	.loc 1 306 0
	mov.b	&__P1IE, r15
	bis.b	#8, r15
	mov.b	r15, &__P1IE
	.loc 1 307 0
	pop	r15
	reti
.LFE8:
.Lfe9:
	.size	WDT_ISR,.Lfe9-WDT_ISR
;; End of function 

	.p2align 1,0
.global	ADC10_ISR
	.type	ADC10_ISR,@function
/***********************
 * Interrupt Vector 5 Service Routine `ADC10_ISR' 
 ***********************/
ADC10_ISR:
.global	__isr_5
__isr_5:
.LFB9:
	.loc 1 312 0
	.loc 1 313 0
	bic	#16, @r1
	.loc 1 314 0
	reti
.LFE9:
.Lfe10:
	.size	ADC10_ISR,.Lfe10-ADC10_ISR
;; End of function 

	.section	.debug_frame,"",@progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.string	""
	.uleb128 0x1
	.sleb128 -2
	.byte	0
	.byte	0xc
	.uleb128 0x1
	.uleb128 0x2
	.byte	0x80
	.uleb128 0x1
	.p2align 1,0
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.2byte	.LFB0
	.2byte	.LFE0-.LFB0
	.byte	0x4
	.4byte	.LCFI0-.LFB0
	.byte	0xe
	.uleb128 0x6
	.p2align 1,0
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.2byte	.LFB1
	.2byte	.LFE1-.LFB1
	.p2align 1,0
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.2byte	.LFB2
	.2byte	.LFE2-.LFB2
	.p2align 1,0
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.2byte	.LFB3
	.2byte	.LFE3-.LFB3
	.p2align 1,0
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.2byte	.LFB4
	.2byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI1-.LFB4
	.byte	0xe
	.uleb128 0x4
	.byte	0x8f
	.uleb128 0x2
	.p2align 1,0
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.2byte	.LFB5
	.2byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI2-.LFB5
	.byte	0xe
	.uleb128 0x4
	.byte	0x8f
	.uleb128 0x2
	.p2align 1,0
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.2byte	.LFB6
	.2byte	.LFE6-.LFB6
	.p2align 1,0
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.2byte	.LFB7
	.2byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI3-.LFB7
	.byte	0xe
	.uleb128 0x4
	.byte	0x8f
	.uleb128 0x2
	.p2align 1,0
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.2byte	.LFB8
	.2byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI4-.LFB8
	.byte	0xe
	.uleb128 0x4
	.byte	0x8f
	.uleb128 0x2
	.p2align 1,0
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.2byte	.LFB9
	.2byte	.LFE9-.LFB9
	.p2align 1,0
.LEFDE18:
	.text
.Letext0:
	.file 2 "/usr/lib/gcc/msp430/4.6.3/../../../../msp430/include/msp430g2231.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.4byte	0x2ef
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x2
	.uleb128 0x1
	.4byte	.LASF61
	.byte	0x1
	.4byte	.LASF62
	.4byte	.LASF63
	.2byte	0
	.2byte	0
	.4byte	.Ldebug_ranges0+0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x3
	.byte	0x1
	.4byte	.LASF64
	.byte	0x1
	.byte	0x55
	.byte	0x1
	.2byte	.LFB0
	.2byte	.LFE0
	.4byte	.LLST0
	.4byte	0x50
	.uleb128 0x4
	.4byte	.LASF65
	.byte	0x1
	.byte	0x57
	.4byte	0x50
	.byte	0x2
	.byte	0x91
	.sleb128 -4
	.byte	0
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF1
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF2
	.byte	0x1
	.byte	0x97
	.byte	0x1
	.2byte	.LFB1
	.2byte	.LFE1
	.byte	0x2
	.byte	0x71
	.sleb128 2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF3
	.byte	0x1
	.byte	0xb1
	.byte	0x1
	.2byte	.LFB2
	.2byte	.LFE2
	.byte	0x2
	.byte	0x71
	.sleb128 2
	.uleb128 0x5
	.byte	0x1
	.4byte	.LASF4
	.byte	0x1
	.byte	0xbd
	.byte	0x1
	.2byte	.LFB3
	.2byte	.LFE3
	.byte	0x2
	.byte	0x71
	.sleb128 2
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF5
	.byte	0x1
	.byte	0xdd
	.byte	0x1
	.2byte	.LFB4
	.2byte	.LFE4
	.4byte	.LLST1
	.uleb128 0x6
	.byte	0x1
	.4byte	.LASF6
	.byte	0x1
	.byte	0xf7
	.byte	0x1
	.2byte	.LFB5
	.2byte	.LFE5
	.4byte	.LLST2
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF7
	.byte	0x1
	.2byte	0x104
	.byte	0x1
	.2byte	.LFB6
	.2byte	.LFE6
	.byte	0x2
	.byte	0x71
	.sleb128 2
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF8
	.byte	0x1
	.2byte	0x114
	.byte	0x1
	.2byte	.LFB7
	.2byte	.LFE7
	.4byte	.LLST3
	.uleb128 0x8
	.byte	0x1
	.4byte	.LASF9
	.byte	0x1
	.2byte	0x12d
	.byte	0x1
	.2byte	.LFB8
	.2byte	.LFE8
	.4byte	.LLST4
	.uleb128 0x7
	.byte	0x1
	.4byte	.LASF10
	.byte	0x1
	.2byte	0x137
	.byte	0x1
	.2byte	.LFB9
	.2byte	.LFE9
	.byte	0x2
	.byte	0x71
	.sleb128 0
	.uleb128 0x9
	.string	"IE1"
	.byte	0x2
	.byte	0x87
	.4byte	.LASF66
	.4byte	0x100
	.byte	0x1
	.byte	0x1
	.uleb128 0xa
	.4byte	0x105
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF11
	.uleb128 0xb
	.4byte	.LASF12
	.byte	0x2
	.byte	0x8e
	.4byte	.LASF14
	.4byte	0x100
	.byte	0x1
	.byte	0x1
	.uleb128 0xb
	.4byte	.LASF13
	.byte	0x2
	.byte	0xa2
	.4byte	.LASF15
	.4byte	0x12e
	.byte	0x1
	.byte	0x1
	.uleb128 0xa
	.4byte	0x25
	.uleb128 0xb
	.4byte	.LASF16
	.byte	0x2
	.byte	0xa4
	.4byte	.LASF17
	.4byte	0x12e
	.byte	0x1
	.byte	0x1
	.uleb128 0xb
	.4byte	.LASF18
	.byte	0x2
	.byte	0xa6
	.4byte	.LASF19
	.4byte	0x12e
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF20
	.byte	0x2
	.2byte	0x111
	.4byte	.LASF22
	.4byte	0x100
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF21
	.byte	0x2
	.2byte	0x113
	.4byte	.LASF23
	.4byte	0x100
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF24
	.byte	0x2
	.2byte	0x115
	.4byte	.LASF25
	.4byte	0x100
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF26
	.byte	0x2
	.2byte	0x117
	.4byte	.LASF27
	.4byte	0x100
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF28
	.byte	0x2
	.2byte	0x19b
	.4byte	.LASF29
	.4byte	0x100
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF30
	.byte	0x2
	.2byte	0x19d
	.4byte	.LASF31
	.4byte	0x100
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF32
	.byte	0x2
	.2byte	0x19f
	.4byte	.LASF33
	.4byte	0x100
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF34
	.byte	0x2
	.2byte	0x1a1
	.4byte	.LASF35
	.4byte	0x100
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF36
	.byte	0x2
	.2byte	0x1a3
	.4byte	.LASF37
	.4byte	0x100
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF38
	.byte	0x2
	.2byte	0x1a5
	.4byte	.LASF39
	.4byte	0x100
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF40
	.byte	0x2
	.2byte	0x1a7
	.4byte	.LASF41
	.4byte	0x100
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF42
	.byte	0x2
	.2byte	0x1c2
	.4byte	.LASF43
	.4byte	0x12e
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF44
	.byte	0x2
	.2byte	0x1c4
	.4byte	.LASF45
	.4byte	0x12e
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF46
	.byte	0x2
	.2byte	0x1c6
	.4byte	.LASF47
	.4byte	0x12e
	.byte	0x1
	.byte	0x1
	.uleb128 0xd
	.string	"TAR"
	.byte	0x2
	.2byte	0x1c8
	.4byte	.LASF67
	.4byte	0x12e
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF48
	.byte	0x2
	.2byte	0x1ca
	.4byte	.LASF49
	.4byte	0x12e
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF50
	.byte	0x2
	.2byte	0x1cc
	.4byte	.LASF51
	.4byte	0x12e
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF52
	.byte	0x2
	.2byte	0x27c
	.4byte	.LASF53
	.4byte	0x12e
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.4byte	.LASF54
	.byte	0x2
	.2byte	0x2ac
	.4byte	.LASF55
	.4byte	0x2ab
	.byte	0x1
	.byte	0x1
	.uleb128 0xe
	.4byte	0x100
	.uleb128 0xc
	.4byte	.LASF56
	.byte	0x2
	.2byte	0x2ae
	.4byte	.LASF57
	.4byte	0x2ab
	.byte	0x1
	.byte	0x1
	.uleb128 0xf
	.4byte	.LASF58
	.byte	0x1
	.byte	0x4c
	.4byte	0x105
	.byte	0x1
	.byte	0x3
	.byte	0x3
	.2byte	BitCnt
	.uleb128 0xf
	.4byte	.LASF59
	.byte	0x1
	.byte	0x4d
	.4byte	0x25
	.byte	0x1
	.byte	0x3
	.byte	0x3
	.2byte	TXByte
	.uleb128 0xf
	.4byte	.LASF60
	.byte	0x1
	.byte	0x4e
	.4byte	0x12e
	.byte	0x1
	.byte	0x3
	.byte	0x3
	.2byte	Mode
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x2007
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x2007
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x2007
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x2007
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LLST0:
	.2byte	.LFB0
	.2byte	.LCFI0
	.2byte	0x2
	.byte	0x71
	.sleb128 0
	.2byte	.LCFI0
	.2byte	.LFE0
	.2byte	0x2
	.byte	0x71
	.sleb128 4
	.2byte	0
	.2byte	0
.LLST1:
	.2byte	.LFB4
	.2byte	.LCFI1
	.2byte	0x2
	.byte	0x71
	.sleb128 0
	.2byte	.LCFI1
	.2byte	.LFE4
	.2byte	0x2
	.byte	0x71
	.sleb128 2
	.2byte	0
	.2byte	0
.LLST2:
	.2byte	.LFB5
	.2byte	.LCFI2
	.2byte	0x2
	.byte	0x71
	.sleb128 0
	.2byte	.LCFI2
	.2byte	.LFE5
	.2byte	0x2
	.byte	0x71
	.sleb128 2
	.2byte	0
	.2byte	0
.LLST3:
	.2byte	.LFB7
	.2byte	.LCFI3
	.2byte	0x2
	.byte	0x71
	.sleb128 0
	.2byte	.LCFI3
	.2byte	.LFE7
	.2byte	0x2
	.byte	0x71
	.sleb128 2
	.2byte	0
	.2byte	0
.LLST4:
	.2byte	.LFB8
	.2byte	.LCFI4
	.2byte	0x2
	.byte	0x71
	.sleb128 0
	.2byte	.LCFI4
	.2byte	.LFE8
	.2byte	0x2
	.byte	0x71
	.sleb128 2
	.2byte	0
	.2byte	0
	.section	.debug_aranges,"",@progbits
	.4byte	0x14
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x2
	.byte	0
	.2byte	.Ltext0
	.2byte	.Letext0-.Ltext0
	.2byte	.LFB0
	.2byte	.LFE0-.LFB0
	.2byte	0
	.2byte	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.2byte	.Ltext0
	.2byte	.Letext0
	.2byte	.LFB0
	.2byte	.LFE0
	.2byte	0
	.2byte	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF5:
	.string	"Timer_A"
.LASF53:
	.string	"__WDTCTL"
.LASF12:
	.string	"IFG1"
.LASF43:
	.string	"__TACTL"
.LASF38:
	.string	"P1SEL"
.LASF54:
	.string	"CALDCO_1MHZ"
.LASF59:
	.string	"TXByte"
.LASF34:
	.string	"P1IES"
.LASF13:
	.string	"ADC10CTL0"
.LASF16:
	.string	"ADC10CTL1"
.LASF15:
	.string	"__ADC10CTL0"
.LASF17:
	.string	"__ADC10CTL1"
.LASF6:
	.string	"ta1_isr"
.LASF50:
	.string	"TACCR1"
.LASF40:
	.string	"P1REN"
.LASF39:
	.string	"__P1SEL"
.LASF65:
	.string	"tempMeasured"
.LASF36:
	.string	"P1IE"
.LASF33:
	.string	"__P1IFG"
.LASF10:
	.string	"ADC10_ISR"
.LASF67:
	.string	"__TAR"
.LASF11:
	.string	"unsigned char"
.LASF42:
	.string	"TACTL"
.LASF66:
	.string	"__IE1"
.LASF37:
	.string	"__P1IE"
.LASF21:
	.string	"BCSCTL1"
.LASF24:
	.string	"BCSCTL2"
.LASF26:
	.string	"BCSCTL3"
.LASF18:
	.string	"ADC10MEM"
.LASF32:
	.string	"P1IFG"
.LASF58:
	.string	"BitCnt"
.LASF44:
	.string	"TACCTL0"
.LASF46:
	.string	"TACCTL1"
.LASF19:
	.string	"__ADC10MEM"
.LASF29:
	.string	"__P1OUT"
.LASF49:
	.string	"__TACCR0"
.LASF64:
	.string	"main"
.LASF7:
	.string	"InitializeButton"
.LASF30:
	.string	"P1DIR"
.LASF23:
	.string	"__BCSCTL1"
.LASF0:
	.string	"unsigned int"
.LASF57:
	.string	"__CALBC1_1MHZ"
.LASF8:
	.string	"PORT1_ISR"
.LASF63:
	.string	"/home/james/Insync/UBC/PHYS319/lab3/temperature_demo2"
.LASF28:
	.string	"P1OUT"
.LASF4:
	.string	"Transmit"
.LASF2:
	.string	"PreApplicationMode"
.LASF48:
	.string	"TACCR0"
.LASF62:
	.string	"main.c"
.LASF61:
	.string	"GNU C 4.6.3 20120301 (mspgcc LTS 20120406 unpatched)"
.LASF22:
	.string	"__DCOCTL"
.LASF9:
	.string	"WDT_ISR"
.LASF25:
	.string	"__BCSCTL2"
.LASF27:
	.string	"__BCSCTL3"
.LASF31:
	.string	"__P1DIR"
.LASF55:
	.string	"__CALDCO_1MHZ"
.LASF60:
	.string	"Mode"
.LASF35:
	.string	"__P1IES"
.LASF51:
	.string	"__TACCR1"
.LASF45:
	.string	"__TACCTL0"
.LASF47:
	.string	"__TACCTL1"
.LASF1:
	.string	"long int"
.LASF41:
	.string	"__P1REN"
.LASF14:
	.string	"__IFG1"
.LASF52:
	.string	"WDTCTL"
.LASF20:
	.string	"DCOCTL"
.LASF56:
	.string	"CALBC1_1MHZ"
.LASF3:
	.string	"ConfigureTimerUart"
