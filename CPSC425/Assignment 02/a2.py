#!/usr/bin/env python
"""
Name:   James Deng
ID:     13340112
"""

from __future__ import division

import math
import numpy as np
from PIL import Image
from scipy import signal

#   pylint: disable=E1101


def main():
    """
    shows a black and white test.jpeg before and after a gaussian blur
    """
    img = Image.open("lenna.jpg").convert('L')
    img.show()

    img_array = gaussconvolve2d(np.asarray(img), 3.0)
    img_array = img_array.astype('uint8')

    img_blurred = Image.fromarray(img_array)
    img_blurred.show()
    img_blurred.save("test_blurred.jpg", "JPEG")


def boxfilter(size):
    """
    2d boxfilter kernel as numpy array
    """
    # check that size is odd
    assert size % 2, "size must be odd"

    normalized_kernel = np.array([(size/(size**2)) for _ in range(size**2)])

    return normalized_kernel


def gauss1d(sigma):
    """
    1d kernel array
    """
    gauss = np.vectorize(lambda x: math.exp(-x**2/(2*sigma**2)))

    # int() truncates input so floor is for clarity only
    x_max = int(math.floor(math.ceil(6*sigma)/2))
    kernel = gauss(np.arange(-x_max, (x_max+1)))

    kernel_sum = kernel.sum()
    normalized_kernel = kernel/kernel_sum

    return normalized_kernel


def gauss2d(sigma):
    """
    2d gaussian kernel as numpy array
    """
    gauss_x = gauss1d(sigma)[np.newaxis]
    gauss_y = np.swapaxes(gauss_x, 0, 1)

    normalized_kernel = signal.convolve2d(gauss_x, gauss_y)

    return normalized_kernel


def gaussconvolve2d(array, sigma):
    """
    applies gaussian filter of order sigma to array
    """
    return signal.convolve2d(array, gauss2d(sigma), 'same')


if __name__ == '__main__':
    main()
