#!/usr/bin/env python
"""
Name:   James Deng
ID:     13340112
"""

from __future__ import division

from PIL import Image, ImageDraw
import numpy as np
import math
from scipy import signal
import ncc

#   pylint: disable=E1101


def main():
    """
    displays template match across the template
    minsize defaults to size of template
    """
    templateScale = 1/2
    nccThreshold = 0.5

    template = Image.open("./faces/template.jpg").convert('L')
    images = [Image.open("./faces/judybats.jpg").convert('L'),
              Image.open("./faces/students.jpg").convert('L'),
              Image.open("./faces/tree.jpg").convert('L')]

    templateX = int(template.size[0]*templateScale)
    templateY = int(template.size[1]*templateScale)
    template = template.resize((templateX, templateY), Image.BICUBIC)
    template.resize((templateX*5, templateY*5), Image.NEAREST).show()
    print templateX, " ", templateY

    pyramids = [MakePyramid(img, min(template.size)) for img in images]
    for pyramid in pyramids:
        # ShowPyramid(pyramid)
        FindTemplate(pyramid, template, nccThreshold).show()


def MakePyramid(image, minsize):
    """
    makes the pyramid
    scalling based on last instead of source
    """
    scale = 0.9
    pyramid = []
    img = image
    while (min(img.size) >= minsize):
        pyramid.append(img)
        img = img.resize((int(img.size[0] * scale),
                          int(img.size[1] * scale)),
                         Image.BICUBIC)

    return pyramid


def ShowPyramid(pyramid):
    """
    shows pyramids side by side, aligns to bottom
    """
    width = 0
    height = pyramid[0].size[1]
    for img in pyramid:
        width += img.size[0]

    image = Image.new("L", (width, height), "white")

    offset = 0
    for img in pyramid:
        image.paste(img, (offset, height-img.size[1]))
        offset += img.size[0]

    image.show()
    return image


def FindTemplate(pyramid, template, threshold):
    """
    draws box arond matches found by ncc
    """
    templateX = template.size[0]
    templateY = template.size[1]

    image = pyramid[0].convert('RGB')
    imageX = image.size[0]
    imageY = image.size[1]

    offsetX = int(templateX/2)
    offsetY = int(templateY/2)

    nccPyramid = []
    draw = ImageDraw.Draw(image)
    for img in pyramid:
        result = Image.fromarray(ncc.normxcorr2D(img, template))
        nccPyramid.append(result)
        resultX = result.size[0]
        resultY = result.size[1]
        for x in range(resultX):
            for y in range(resultY):
                if (result.getpixel((x, y)) >= threshold):
                    scale = imageX/resultX
                    draw.rectangle([(scale*(x+offsetX), scale*(y+offsetY)),
                                    (scale*(x-offsetX), scale*(y-offsetY))],
                                   outline='red')
    # ShowPyramid(nccPyramid)
    del draw
    return image


if __name__ == '__main__':
    main()
