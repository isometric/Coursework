#!/usr/bin/env python
"""
Name:   James Deng
ID:     13340112
"""

from __future__ import division

from random import randint
from PIL import Image, ImageDraw
import numpy as np
import csv
import math

#   pylint: disable=E1101


def main():
    # Test run...
    # match("scene", "basmati")
    # match("scene", "book")
    match('library2', 'library')

def read_keys(image):
    """
    Input an image and its associated SIFT keypoints.

    The argument image is the image file name (without an extension).
    The image is read from the PGM format file image.pgm and the
    keypoints are read from the file image.key.

    read_keys returns the following 3 arguments:

    image: the image (in PIL 'RGB' format)

    keypoints: K-by-4 array, in which each row has the 4 values specifying
    a keypoint (row, column, scale, orientation).  The orientation
    is in the range [-PI, PI] radians.

    descriptors: a K-by-128 array, where each row gives a descriptor
    for one of the K keypoints.  The descriptor is a 1D array of 128
    values with unit length.
    """
    img = Image.open("./"+image+".pgm").convert("RGB")
    keypoints = []
    descriptors = []
    first = True
    with open("./"+image+".key", "rb") as key_file:
        reader = csv.reader(key_file, delimiter=' ', quoting=csv.QUOTE_NONNUMERIC, skipinitialspace=True)
        descriptor = []
        for row in reader:
            if len(row) == 2:
                assert first, "Invalid keypoint file header."
                assert row[1] == 128, "Invalid keypoint descriptor length in header (should be 128)."
                count = row[0]
                first = False
            if len(row) == 4:
                keypoints.append(np.array(row))
            if len(row) == 20:
                descriptor += row
            if len(row) == 8:
                descriptor += row
                assert len(descriptor) == 128, "Keypoint descriptor length invalid (should be 128)."
                # normalize the key to unit length
                descriptor = np.array(descriptor)
                descriptor = descriptor / math.sqrt(np.sum(np.power(descriptor, 2)))
                descriptors.append(descriptor)
                descriptor = []
    assert len(keypoints) == count, "Incorrect total number of keypoints read."
    print "Number of keypoints read:", int(count)
    return [img, keypoints, descriptors]


def append_images(im1, im2):
    """
    Create a new image that appends two images side-by-side.

    The arguments, im1 and im2, are PIL images of type RGB
    """
    im1cols, im1rows = im1.size
    im2cols, im2rows = im2.size
    im3 = Image.new('RGB', (im1cols+im2cols, max(im1rows, im2rows)))
    im3.paste(im1, (0, 0))
    im3.paste(im2, (im1cols, 0))
    return im3


def display_matches(im1, im2, matched_pairs):
    """
    Display matches on a new image with the two input images placed side by side.

    Arguments:
     im1           1st image (in PIL 'RGB' format)
     im2           2nd image (in PIL 'RGB' format)
     matched_pairs list of matching keypoints, im1 to im2

    Displays and returns a newly created image (in PIL 'RGB' format)
    """
    im3 = append_images(im1, im2)
    offset = im1.size[0]
    draw = ImageDraw.Draw(im3)
    for item in matched_pairs:
        draw.line((item[0][1], item[0][0], offset+item[1][1], item[1][0]), fill="red", width=2)
    im3.show()
    return im3


def angle_between_angles(x,y):
  return min(y-x, y-x+2*math.pi, y-x-2*math.pi, key=abs)


def match(image_1, image_2):
    """
    Input two images and their associated SIFT keypoints.
    Display lines connecting the first 5 keypoints from each image.
    Note: These 5 are not correct matches, just randomly chosen points.

    The arguments image_1 and image_2 are file names without file extensions.

    Returns the number of matches displayed.

    Example: match('scene','book')
    """
    sift_threshold = 0.99
    ransac_samples = 50
    ransac_tol_scale = 0.05
    ransac_tol_angle = np.radians(5)

    im_1, keypoints_1, descriptors_1 = read_keys(image_1)
    im_2, keypoints_2, descriptors_2 = read_keys(image_2)

    assert len(descriptors_1) >= 2 & len(descriptors_2) >= 2

    matched_pairs = []
    for i in range(len(descriptors_1)):
        distances = []
        for j in range(len(descriptors_2)):
            #dist = math.acos(sum(p*q for p, q in zip(descriptors_1[i], descriptors_2[j])))
            dist = math.acos(np.dot(descriptors_1[i], descriptors_2[j]))
            distances.append(dist)
        sorted_distances = sorted(distances)
        if (sorted_distances[0] / sorted_distances[1]) < sift_threshold:
            matched_pairs.append([keypoints_1[i], keypoints_2[distances.index(sorted_distances[0])]])

    optimal_subset = []
    for _ in range(ransac_samples):
        random_subset = []
        random_index = randint(0, len(matched_pairs)-1)

        _, _, rand_scale_1, rand_up_1 = matched_pairs[random_index][0]
        _, _, rand_scale_2, rand_up_2 = matched_pairs[random_index][1]
        random_scale = rand_scale_1/rand_scale_2
        random_rotation = angle_between_angles(rand_up_1, rand_up_2)

        for i in range(len(matched_pairs)):
            if i == random_index:
                    continue

            _, _, samp_scale_1, samp_up_1 = matched_pairs[i][0]
            _, _, samp_scale_2, samp_up_2 = matched_pairs[i][1]
            sample_scale = samp_scale_1/samp_scale_2
            sample_rotation = angle_between_angles(samp_up_1, samp_up_2)

            if math.fabs(random_scale - sample_scale)/random_scale < ransac_tol_scale:
                if math.fabs(angle_between_angles(sample_rotation, random_rotation)) < ransac_tol_angle:
                    random_subset.append(matched_pairs[i])

        if len(random_subset) > len(optimal_subset):
            optimal_subset = random_subset

    matched_pairs = optimal_subset

    return display_matches(im_1, im_2, matched_pairs)



if __name__ == '__main__':
    main()
