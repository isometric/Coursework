#!/usr/bin/env python
"""
Name:   James Deng
ID:     13340112
"""
from __future__ import division

from PIL import Image, ImageDraw
import numpy as np
from scipy.cluster.vq import vq, kmeans
from scipy.spatial.distance import cdist
import matplotlib.pyplot as plt

# START OF FUNCTIONS CARRIED FORWARD FROM ASSIGNMENT 2
#   pylint: disable=E1101

# Computes the cost of given boundaries. Good boundaries have zero cost.
def get_boundaries_cost( boundaries, good_boundaries ):
    return np.sum( boundaries != good_boundaries )

# Finds the indices of color_histograms given a series of cluster centres.
def cluster2boundaries(histograms, centres):

    # Find the cluster assignment of each histogram
    distances = cdist( histograms, centres )
    idx       = np.argmin( distances, 1 )

    # Find the points where the index changes
    boundaries = np.zeros( len(idx)+1, dtype = np.bool )

    for i in range( len(idx)-1 ):
        boundaries[i+1] = idx[i] != idx[i+1]

    return boundaries

# Computes histograms from gray images
def compute_gray_histograms( grays, nbins ):
    gray_hs = np.zeros(( nframes, nbins ), dtype = np.uint16 )

    for i in range( len(grays) ):
        gray_im = grays[i]
        v1 = np.histogram(gray_im.flatten(),bins=nbins, range=(0,255))
        gray_hs[i] = v1[0]

    return gray_hs


def compute_color_histograms( colors, nbins ):
    r_hs = np.zeros(( nframes, nbins ), dtype = np.uint16 )
    for i in range( len(colors) ): # for each frame
            colour_im = colors[i,:,:,0]
            v1 = np.histogram(colour_im.flatten(),bins=nbins, range=(0,255))
            r_hs[i] = v1[0]
    g_hs = np.zeros(( nframes, nbins ), dtype = np.uint16 )
    for i in range( len(colors) ): # for each frame
            colour_im = colors[i,:,:,1]
            v1 = np.histogram(colour_im.flatten(),bins=nbins, range=(0,255))
            g_hs[i] = v1[0]
    b_hs = np.zeros(( nframes, nbins ), dtype = np.uint16 )
    for i in range( len(colors) ): # for each frame
            colour_im = colors[i,:,:,2]
            v1 = np.histogram(colour_im.flatten(),bins=nbins, range=(0,255))
            b_hs[i] = v1[0]
    return np.hstack((r_hs, g_hs, b_hs))

# === Main code starts here ===
fname     = 'colours' # folder name
nframes   = 151       # number of frames
im_height = 90        # image height
im_width  = 120       # image width

# define the list of (manually determined) shot boundaries here
good_boundaries = [33, 92, 143]

# convert good_boundaries list to a binary array
gb_bool = np.zeros( nframes+1, dtype = np.bool )
gb_bool[ good_boundaries ] = True

# Create some space to load the images into memory
colors = np.zeros(( nframes, im_height, im_width, 3), dtype = np.uint8)
grays  = np.zeros(( nframes, im_height, im_width   ), dtype = np.uint8)

# Read the images and store them in color and grayscale formats
for i in range( nframes ):
    imname    = '%s/dwc%03d.png' % ( fname, i+1 )
    im        = Image.open( imname ).convert( 'RGB' )
    colors[i] = np.asarray(im, dtype = np.uint8)
    grays[i]  = np.asarray(im.convert( 'L' ))

# Initialize color histogram
nclusters   = 4
nbins       = range(2,13)
gray_costs  = np.zeros( len(nbins) )
color_costs = np.zeros( len(nbins) )

# === GRAY HISTOGRAMS ===
for n in nbins:
    grey_histograms = compute_gray_histograms(grays, n)
    [codebook, distortion] = kmeans(grey_histograms, 4)
    boundaries = cluster2boundaries(grey_histograms, codebook)
    # print nbins.index(n)
    gray_costs[nbins.index(n)] = get_boundaries_cost(boundaries, gb_bool)

# === END GRAY HISTOGRAM CODE ===

plt.figure(1)
plt.xlabel('Number of bins')
plt.ylabel('Error in boundary detection')
plt.title('Boundary detection using gray histograms')
plt.plot(nbins, gray_costs)
plt.axis([2, 13, -1, 10])
plt.grid(True)
plt.show()

# === COLOR HISTOGRAMS ===
for n in nbins:
    colour_histograms = compute_color_histograms(colors, n)
    [codebook, distortion] = kmeans(colour_histograms, 4)
    boundaries = cluster2boundaries(colour_histograms, codebook)
    # print nbins.index(n)
    color_costs[nbins.index(n)] = get_boundaries_cost(boundaries, gb_bool)

# === END COLOR HISTOGRAM CODE ===

plt.figure(2)
plt.xlabel('Number of bins')
plt.ylabel('Error in boundary detection')
plt.title('Boundary detection using color histograms')
plt.plot(nbins, color_costs)
plt.axis([2, 13, -1, 10])
plt.grid(True)
plt.show()

fdiffs = np.zeros( nframes )
# === ABSOLUTE FRAME DIFFERENCES ===
for i in range(nframes-1):
    length, width = grays[i].shape
    diff = 0
    for j in range( length ):
        for k in range( width ):
            diff += abs(int(grays[i,j,k])-int(grays[i+1,j,k]))
    fdiffs[i] = diff

plt.figure(4)
plt.xlabel('Frame number')
plt.ylabel('Absolute frame difference')
plt.title('Absolute frame differences')
plt.plot(fdiffs)
plt.show()

sqdiffs = np.zeros( nframes )
# === SQUARED FRAME DIFFERENCES ===
for i in range(nframes-1):
    length, width = grays[i].shape
    diff = 0
    for j in range( length ):
        for k in range( width ):
            diff += abs(int(grays[i,j,k])-int(grays[i+1,j,k]))**2
    sqdiffs[i] = diff

plt.figure(5)
plt.xlabel('Frame number')
plt.ylabel('Squared frame difference')
plt.title('Squared frame differences')
plt.plot(sqdiffs)
plt.show()

avgs = np.zeros( nframes )
avgdiffs = np.zeros( nframes )
# === AVERAGE GRAY DIFFERENCES ===
for i in range(nframes):
    length, width = grays[i].shape
    diff = 0
    for j in range( length ):
        for k in range( width ):
            diff += int(grays[i,j,k])
    avgs[i] = diff /(length * width)

for i in range(nframes-1):
    avgdiffs[i] = abs(avgs[i]-avgs[i+1])

plt.figure(6)
plt.xlabel('Frame number')
plt.ylabel('Average gray frame difference')
plt.title('Average gray frame differences')
plt.plot(avgdiffs)
plt.show()

histdiffs = np.zeros( nframes )
# === HISTOGRAM DIFFERENCES ===
grey_histograms = []
for i in range(nframes):
        ghs = compute_gray_histograms(grays[i],10);
        grey_histograms.append(ghs)
for i in range(nframes-1):
    histdiffs[i] = np.linalg.norm(grey_histograms[i+1]-grey_histograms[i])
plt.figure(7)
plt.xlabel('Frame number')
plt.ylabel('Histogram frame difference')
plt.title('Histogram frame differences')
plt.plot(histdiffs)
plt.show()
