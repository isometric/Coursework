
This file is to include your testing documentation. The file is to be
in plain text ASCII and properly spaced and edited so that when it is
viewed on a Linux machine it is readable. Not all ASCII files created
on a Windows machine display as expected due to the differences with
respect to how new lines and carriage returns are treated. Line widths
are to be limited to 80 characters.

Table of contents

I.      Test 1 - Memory manager will allocate/free large amounts of memory
II.     Test 2 - Memory manager handles coalescing
III.    Test 3 - Process creation is limited by available memory
IV.     Test 4 - Processes end up at the end of the ready queue

I Test 1 - Memory manager will allocate/free large amounts of memory

I.1 Summary of test
This is a black box test that tests whether the memory manager is able to:
* allocate close to theoretical max of available
* stop allocating more memory than is availble
* return freed memory to the free list
* handle various sized requests
* coalesce freed memory
* handle large malloc/free requests
* handle large numbers of malloc/free requests

Note: this test is run by uncommenting run_kmemtests in init.c.

I.2 Test methodology
A request is made for memory larger than the freemem before the hole and just
small enough to fit in the second half of freemem is made it is then repeated
to make sure malloc is not allocating too much memory.

Various sized requests are then made and successfully requested sizes are
recorded. When malloc first fails the sum of the malloced memory is printed.

A bunch of consequtive and non consecutively requested pointers are freed
including the first pointer with the large request. The freed sizes are
subtracted from the allocated size which is agian printed.

Various sized requestes are made again until freemem is exhausted aka malloc
fails as before. Requested sizes are generally larger than in the first pass.
Allocated size is updated and pritned again.

The theoretically availble bytes should be the largest value.

The theoretically availble bytes, first allocation pass bytes and second
allocation pass bytes should be within the same ballpark but not necessacarily
equal.

First pass and theoretical should be within a few percent from header overhead
and request granuarity.

Memory after free pass should be very small but non zero relative to max.

Second pass should be much greater than after free pass but can be significantly
smaller than first pass depending on sizes used.

I.3 Sample output

theoretical sizeof freemem 3142975 bytes
allocated 333 pointers, 3134290 bytes in use
freed 57 pointers, 537498bytes in use
allocated 647 pointers, 3108433 bytes in use

II Test 2 - Memory manager handles coalescing

II.1 Summary of test

This test shows that coalescing works.
* Create a many blocks of memory
* Free every other block of memory
* Free remaining memory
* Check that there are only two blocks of free memory remaining

Note: this test is written in mem.c as test_2(); In order to use
this function inside initproc() the function must be declared in
xeroskernel.h using the keyword extern. Then test_2() can be used
immediately following the call to kmeminit().

II.2 Test methodology

Create a function called test_2 in mem.c. Inside this function allocate
32-bytes 100 times. Maintain a pointer to each call to kmalloc. Now
count the number of nodes in the free list by passing freemem to
count_nodes. Print out the result. There should be two nodes.

Next, use kfree to free every other pointer. Now, call count_nodes with
freemem as the argument. Again, print out the results. There should
be 52 nodes in the free-node list.

Now, use kfree to free the remaining pointers. Count the remaining
nodes and print out the result. There should be 2 nodes in freenodes
list.

Now that test2 has been written it should be called by the kernel once
kmeminit has been called.

Expect to see the change in free-list size as it grows and then shrinks
again.

II.3 Sample output

    ... (content above test) ...
    nodes in free-list: 2
    nodes in free-list: 52
    nodes in free-list: 2
    ... (content below test) ...

III Test 3 - Process creation is limited by available memory

III.1 Summary of test

Checks that process creation interacts with the memory manager
in the expected fashion.
* Create a process with a large stack
* Attempt to make more processes while expecting a failure
* The original process stops freeing up memory
* Create another process with a large stack

Note: An implementation of this test is available in user.c. The function
is referred to as test_3(). In this case, the test_3() proc can be created
from root and the producer and consumers commented out. The test_3 proc
will contain everything necessary to run this test.

III.2 Test methodology

In users.c create a function, called test_3. test_3 that takes nothing and 
returns nothing. This function contains a syscreate() call, enters a 
while-loop and then stops after a couple of iterations.

The root function inside user.c should make a syscreate call for test_3.
Inside of test_3 a large function, named fn (takes nothing and returns nothing),
is created with a large stack size. For this test use a stack with a
size of 0x400000 - 0x196000 - 300, (note the change in base). This size is
almost as large as the area after the hole. Print out the result from
syscreate. It should be non-zero.

Have root attempt to make another process. The result should be zero and
the kernel should print "stack malloc fail". This error message means that
the memory manager failed to make an allocation. In this instance it was for
creating memory for a process.

Have root attempt to create processes  until the result from syscreate is 
non-zero. This will occur because the old process stopped freeing up the 
memory. These multiple attempts can be encapsulated inside of a while-loop. 
For example, the following code would test this.

    int counter = 0;
    while (syscreate(fn, LARGE_NUM) == 0) {
        counter++;
        sysyield();
    }

   KASSERT( counter > 0 );

Once root is able to get past the while loop with counter > 0 it's apparent
that there were multiple attempts to create a new process but each failed
except for the last one.

Expect to see multiple attempts at creating a process fail due to a stack
malloc fail. After a list of failures there should be a success due the large
chunk of memory being returned to the allocator.

III.3 Sample output

The sample shows the attempt to make multiple processes and failing due to
insufficient memory.

created proc
stack malloc fail
stack malloc fail
failed creating proc
stack malloc fail
failed creating proc
stack malloc fail
failed creating proc
stack malloc fail
failed creating proc
created proc
done

IV Test 4 - Processes end up at the end of the ready queue

IV.1 Summary of test

This test checks multiple things.
* yielded processes are placed at the end of the ready queue
* processes don't jump in line
* new processes are placed at the end of the ready queue
* processes are run in the same order they are yielded

IV.2 Test methodology

Create 3 functions in user.c. Name them fn1, fn2, and fn3. Each should have an
infinite loop. On each iteration the function prints its name and then yields.
fn1 could look like the following,

    void fn1(void) {
        for(;;) {
            kprintf("(fn1)");
            sysyield();
        }
    }

Now, have root create the three functions in lexographic order before
root enters its infite loop. The screen should show a repeated string
of the functions' print statements.

See IV.3 for the expected output.

IV.3 Sample output

The following sample output should be a repreated pattern that appears
on the screen. The order should be fn1, fn2, fn3, repeating.

... (fn1)(fn2)(fn3)(fn1)(fn2)(fn3)(fn1) ...
