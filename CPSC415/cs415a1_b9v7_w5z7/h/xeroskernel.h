/* xeroskernel.h - disable, enable, halt, restore, isodd, min, max */

#ifndef XEROSKERNEL_H
#define XEROSKERNEL_H

#define SANITY_VAL 0xDEADCAFE
/* Symbolic constants used throughout Xinu */

typedef	char    Bool;        /* Boolean type                  */
typedef unsigned int size_t; /* Something that can hold the value of
                              * theoretical maximum number of bytes
                              * addressable in this architecture.
                              */
#define	FALSE   0       /* Boolean constants             */
#define	TRUE    1
#define	EMPTY   (-1)    /* an illegal gpq                */
#define	NULL    0       /* Null pointer for linked lists */
#define	NULLCH '\0'     /* The null character            */


/* Universal return constants */

#define	OK            1         /* system call ok               */
#define	SYSERR       -1         /* system call failed           */
#define	EOF          -2         /* End-of-file (usu. from read)	*/
#define	TIMEOUT      -3         /* time out  (usu. recvtim)     */
#define	INTRMSG      -4         /* keyboard "intr" key pressed	*/
                                /*  (usu. defined as ^B)        */
#define	BLOCKERR     -5         /* non-blocking op would block  */

/* Test framework */
#define DONTDEBUG 1             /* set to 0 to log fn start/end */
#define KASSERT( exp ) ( (exp)? (void)0 : __assert_fail( __LINE__ ) )
#define KDEBUG( exp ) ( (DONTDEBUG)?  (void)0 : __print_plz( exp ) )

/* Data Definitions */
typedef enum {STOPPED = 0xCAFE0, RUNNING, READY, BLOCKED} proc_state;
typedef enum {CREATE = 0xBB0, YIELD = 0xBEBE0, STOP = 0xBEEBEEE0} context_request;

/* Process control block */
typedef struct PCB {
    int PID;                // Process ID
    proc_state state;       // Status of processes
    int parent_pid;         // PID of parent
    unsigned long esp;      // Stack pointer
    unsigned long start;    // Lowest address in process
    struct PCB *next;       // Next PCB in queue
} PCB;

/* Freememory node header */
typedef struct header{
    unsigned long size;         // Number of bytes allocated
    struct header *prev;        // Previous entry in free list
    struct header *next;        // Next entry in free list
    const char *sanityCheck;    // Sanity check firled
    unsigned char dataStart[0]; // Beginning of allocated memory
} memHeader;

/* Registers and flags pushed onto stack */
typedef struct context_frame {
    unsigned int   edi;
    unsigned int   esi;
    unsigned int   ebp;
    unsigned int   esp;
    unsigned int   ebx;
    unsigned int   edx;
    unsigned int   ecx;
    unsigned int   eax;
    unsigned int   iret_eip;
    unsigned int   iret_cs;
    unsigned int   eflags;
} context_frame;

/* Functions defined by startup code */

void            bzero(void *base, int cnt);
void            bcopy(const void *src, void *dest, unsigned int n);
void            disable(void);
unsigned short  getCS(void);
unsigned char   inb(unsigned int);
void            init8259(void);
int             kprintf(char * fmt, ...);
void            lidt(void);
void            outb(unsigned int, unsigned char);
void            set_evec(unsigned int xnum, unsigned long handler);
extern void     kmeminit(void);
extern void     *kmalloc(unsigned int size);
extern void     run_kmemtests(void);
extern void     kfree(void *ptr);
extern void     dispatch(void);
extern void     contextinit(void);
extern          context_request contextswitch(PCB *process);
extern void     pcbinit(void);
extern void     run_pcbtests(void);
extern int      create(void (*func) (void), int stack);
extern void     __assert_fail(int line);
extern void     __print_plz(char*);
extern void     freeze(void);
extern void     ready(PCB*);
extern int      syscall(int call, ...);
extern unsigned int syscreate( void (*func)(void), int stack );
extern void     sysyield( void );
extern void     sysstop( void );
extern void     root(void);

/* Anything you add must be between the #define and this comment */

#endif
