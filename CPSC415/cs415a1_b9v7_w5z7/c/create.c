/* create.c : create a process
 */

#include <xeroskernel.h>
#include <i386.h>

extern PCB* stopped_head;
extern long freemem;

/* Create a new process */
int create(void (*func) (void), int stack) {
    KDEBUG( "R" );
    
    /* using size of context_frame for buffer */
    unsigned long stack_size = stack + 2 * sizeof(context_frame);
    unsigned long stack_addr = (unsigned long) kmalloc(stack_size);

    if (!stack_addr) {
        kprintf("stack malloc fail\n");
        KDEBUG( "r" );
        return 0;
    }

    if (!stopped_head) {
        kprintf("no free pcb\n");
        kfree(stack_addr);
        KDEBUG( "r" );
        return 0;
    }

    if ((int) func < HOLESTART && (int) func > HOLEEND) {
        kprintf("func points to hole hole\n");
        kfree(stack_addr);
        kprintf("r");
        return 0;
    }

    memHeader* currHead = (memHeader*) freemem;
    while (currHead != NULL) {

        if ((currHead->dataStart < func) && (func < (currHead->dataStart + currHead->size))) {
            kprintf("func points to freemem\n");
            kprintf("r");
            return 0;
        }
        checksanity(currHead);
        currHead = currHead->next;
    }


    PCB *pcb = stopped_head;
    stopped_head =  stopped_head->next;

    context_frame * buffer  = ((context_frame *) (stack_addr + stack)) - 1;
    memset(buffer, SANITY_VAL, sizeof(context_frame));

    context_frame * cf      = ((context_frame *) (stack_addr + stack)) - 2;
    memset(cf, 0, sizeof(context_frame));
    cf->iret_eip = (unsigned int) func;
    cf->iret_cs  = (unsigned int) getCS();

    pcb->esp = (unsigned long)cf;
    pcb->start = stack_addr;

    ready(pcb);

    KDEBUG( "r" );
    return 1;
}

