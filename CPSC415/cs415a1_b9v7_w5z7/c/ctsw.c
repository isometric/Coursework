/* ctsw.c : context switcher
 */

#include <xeroskernel.h>

/* Your code goes here - You will need to write some assembly code. You must
   use the gnu conventions for specifying the instructions. (i.e this is the
   format used in class and on the slides.) You are not allowed to change the
   compiler/assembler options or issue directives to permit usage of Intel's
   assembly language conventions.
*/

void _ISREntryPoint();
static void *k_stack;
static unsigned long *ESP;

/* Switch between processes and the kernel */
extern context_request contextswitch(PCB *p) {
    KDEBUG ("C");
    context_request result;
    ESP = p->esp;
    asm volatile(" \
        pushf \n\
        pusha \n\
        movl %%esp, k_stack \n\
        movl ESP, %%esp \n\
        popa \n\
        iret \n\
    _ISREntryPoint: \n\
        pusha \n\
        movl %%esp, ESP \n\
        movl k_stack, %%esp \n\
        popa \n\
        popf \n\
            "
        :
        :
        : "%eax");
    p->esp = ESP;

    /* Get the request type from the process's stack.
     * The value is below the saved registers and flags. */
    int *reqaddr = p->esp + sizeof(context_frame);
    result = *((context_request*)reqaddr);
    KDEBUG ("c");
    return result;
}

/* Setup for the context switcher */
void contextinit(void) {
    set_evec(49, _ISREntryPoint);
}
