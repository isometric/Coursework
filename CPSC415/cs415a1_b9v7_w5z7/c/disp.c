
/* disp.c : dispatcher
 */

#include <xeroskernel.h>

// Global Variables
#define PCB_SIZE 16
PCB pcbs[PCB_SIZE];
PCB *ready_head = NULL;
PCB *blocked_head = NULL;
PCB *stopped_head = NULL;

/* Forward Declarations */

/* Initialize the Process Control Block Table.
 * Assign the first entries to the head of the ready queue.
 * Assign the remaining entries to the blocked table. */
void pcbinit(void);

/* Return head of ready queue and remove it from the queue. */
PCB *next(void);

/* Chooses next process to run. */
void dispatch(void);

/* Adds a process control block to the end of the ready queue. */
void ready(PCB*);

/* Set PCB struct. */
void set_pcb(PCB*,
        int,
        proc_state,
        int,
        unsigned long,
        unsigned long,
        PCB*);

/* Free process control block and held resources. */
void cleanup(PCB*);

/* Runs tests on PCB methods */
void run_pcbtests(void);

/* Counts the number of elements in the PCB list. */
int count_list(PCB *head);

/* Gets end of PCB list */
PCB* last_pcb(PCB *head);

/* Definitions */
/* Chooses next process to run. */
void dispatch(void) {

    int *argp;
    context_request request;
    PCB *pcb = next(), *newpcb;

    for(;;) {
        KDEBUG( "D" );
        request = contextswitch(pcb);

        switch(request) {
            case CREATE:
                argp = (int*)(pcb->esp + sizeof(context_frame));
                argp = argp[1]; // point to beginning of args
                int result = create((void*)argp[0], argp[1]);
                if (result) {
                    newpcb = last_pcb(ready_head);
                    newpcb->parent_pid = pcb->PID;
                    result = newpcb->PID; // return new PID
                }
                ((context_frame*)pcb->esp)->eax = result;
                break;
            case YIELD:
                ready(pcb);
                pcb = next();
                break;
            case STOP:
                cleanup(pcb);
                pcb = next();
                break;
            default:
                kprintf("ERROR unrecognized request\n");
                break;
        }
        KDEBUG( "d" );
    }
}

/* Initialize the Process Control Block Table.
 * Assign the first entries to the head of the ready queue.
 * Assign the remaining entries to the blocked table. */
void pcbinit(void) {
    int i;
    for(i = 0; i < PCB_SIZE-1; i++) {
        set_pcb(pcbs+i, i+1, STOP, 0, NULL, NULL, pcbs+i+1);
}
    set_pcb(pcbs+i, i+1, STOP, 0, NULL, NULL, NULL);

    ready_head   = NULL;
    blocked_head = NULL;
    stopped_head = pcbs;

    KASSERT( count_list(ready_head) == 0 );
    KASSERT( count_list(blocked_head) == 0 );
    KASSERT( count_list(stopped_head) == PCB_SIZE );
}


/* Free process control block and held resources. */
void cleanup(PCB *pcb) {
    // Free memory used for process
    kfree((void*)(pcb->start));

    // Add process to end of stopped Q.
    if (stopped_head)
        pcb->next = stopped_head;
    stopped_head = pcb;

    // Reset values
    set_pcb(pcb, pcb->PID, STOP, pcb->parent_pid, NULL, NULL, NULL);
}

/* Adds a process control block to the end of the ready queue. */
void ready(PCB *pcb) {
    int len = count_list(ready_head);
    if (!ready_head) {
        pcb->next = NULL;
        pcb->state = READY;
        ready_head = pcb;
    } else {
        PCB *current = ready_head;
        while (current->next) {
            current = current->next;
        }
        current->next = pcb;
        pcb->next = NULL;
        pcb->state = READY;
    }
    KASSERT( len+1 == count_list(ready_head) );
}

/* Counts the number of elements in the PCB list. */
int count_list(PCB *head) {
    int count = 0;
    while (head) {
        head = head->next;
        count++;
    }
    return count;
}

/* Gets end of PCB list */
PCB* last_pcb(PCB *head) {
    while (head && head->next)
        head = head->next;
    return head;
}

/* Set PCB struct. */
void set_pcb(PCB *entry,
        int pid,
        proc_state state,
        int parent_pid,
        unsigned long esp,
        unsigned long start,
        PCB *next) {
    entry->PID = pid;
    entry->state = state;
    entry->parent_pid = parent_pid;
    entry->esp = esp;
    entry->start = start;
    entry->next = next;
}

/* Return head of ready queue and remove it from the queue. */
PCB *next(void) {
    if (!ready_head) {
        return NULL;
    } else {
        PCB *result = ready_head;
        ready_head = ready_head->next;
        return result;
    }
}

/* Runs tests on PCB methods */
void run_pcbtests(void) {
    pcbinit();

    ready_head = &(pcbs[0]);
    ready_head->next = NULL;
    stopped_head = &(pcbs[1]);

    // Test next()
    // Test emptying initial next queue
    KASSERT( next() != NULL );
    KASSERT( next() == NULL );
    KASSERT( count_list(ready_head) == 0 );
    KASSERT( count_list(stopped_head) == PCB_SIZE-1 );

    ready(&(pcbs[1]));
    stopped_head = &(pcbs[2]);

    KASSERT( count_list(ready_head) == 1 );
    KASSERT( count_list(stopped_head) == PCB_SIZE-2 );

    ready(&(pcbs[2]));
    ready(&(pcbs[3]));
    ready(&(pcbs[4]));
    ready(&(pcbs[5]));
    ready(&(pcbs[6]));
    ready(&(pcbs[7]));
    ready(&(pcbs[8]));
    ready(&(pcbs[9]));
    stopped_head = &(pcbs[10]);

    KASSERT( count_list(ready_head) == 9 );
    KASSERT( count_list(stopped_head) == PCB_SIZE-10 );
    PCB *ptr = next();
    KASSERT( ptr != NULL );
    KASSERT( next() != ptr );
    KASSERT( count_list(ready_head) == 7 );

    pcbinit();
}
