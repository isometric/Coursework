/* user.c : User processes
 */

#include <xeroskernel.h>

/* Consumer */
void consumer(void) {
    int i = 0;
    for(; i < 15; i++) {
        kprintf(" Birthday UBC\n");
        sysyield();
    }
    sysstop();
}

/* Producer */
void producer(void) {
    int i = 0;
    for(; i < 12; i++) {
        kprintf("Happy 100th");
        sysyield();
    }
    sysstop();
}

/* Used by test_3() */
void fn(void) {
    int i;
    for (i = 0; i < 3; i++) {
        sysyield();
    }
    sysstop();
}

/* Test fn corresponding to Test 3 in Testing.txt */
void test_3(void) {
    long stacksize = 0x400000 - 0x196000 - 300;
    int result = syscreate(fn, stacksize);
    int counter = 0;
    kprintf("created proc\n");
    KASSERT( result != 0 );
    
    result = syscreate(fn, stacksize);
    while (syscreate(fn, stacksize) == 0) {
        kprintf("failed creating proc\n");
        counter++;
        sysyield();
    }

    kprintf("created proc\n");
    KASSERT( counter > 0 );
    kprintf("done");

    sysstop();
}

/* First process run by kernel */
extern void root(void) {
    syscreate(producer, 4000);
    syscreate(consumer, 4000);
    for (;;) sysyield();

    kprintf("root shouldn't finish");
    freeze();
    sysstop();
}


