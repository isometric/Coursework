/* syscall.c : syscalls
 */

#include <xeroskernel.h>

/* Make a system call */
int syscall(int call, ...) {
    KDEBUG( "S" );
    int result;
    /* pointer to arguments following int call */
    int *argptr = (int*)(((void*)&call) + sizeof(int));
    asm volatile(" \
            push %1 \n\
            push %2 \n\
            int $49 \n\
            movl %%eax, %0 \n\
            pop %%eax \n\
            pop %%eax \n\
                "
            :"=r"(result)
            :"r"(argptr), "r"(call)
            : "%eax");
    KDEBUG( "s" );
    return result;
}

/* Create a process */
unsigned int syscreate( void (*func)(void), int stack ) {
    int result = syscall(CREATE, func, stack);
    return result;
}

/* Current process yields using the CPU */
void sysyield( void ) {
    syscall(YIELD);
}

/* Stop current process and clean it from memory */
void sysstop( void ) {
    syscall(STOP);
}

