/* xeroskernel.h - disable, enable, halt, restore, isodd, min, max */


#ifndef __XEROSKERNEL_H__
#define __XEROSKERNEL_H__

#include <stdarg.h>

/* Symbolic constants used throughout Xinu */

typedef	char    Bool;   /* Boolean type                  */
#define	FALSE   0       /* Boolean constants             */
#define	TRUE    1
#define	EMPTY   (-1)    /* an illegal gpq                */
#define	NULL    0       /* Null pointer for linked lists */
#define	NULLCH '\0'     /* The null character            */


/* Universal return constants */

#define	OK            1         /* system call ok               */
#define	SYSERR       -1         /* system call failed           */
#define	EOF          -2         /* End-of-file (usu. from read)	*/
#define	TIMEOUT      -3         /* time out  (usu. recvtim)     */
#define	INTRMSG      -4         /* keyboard "intr" key pressed	*/
                                /*  (usu. defined as ^B)        */
#define	BLOCKERR     -5         /* non-blocking op would block  */

/* Functions defined by startup code */


void           bzero(void *base, int cnt);
void           bcopy(const void *src, void *dest, unsigned int n);
void           disable(void);
unsigned short getCS(void);
unsigned char  inb(unsigned int);
void           init8259(void);
int            kprintf(char * fmt, ...);
void           lidt(void);
void           outb(unsigned int, unsigned char);

#define FREEZE          do {                                    \
    kprintf("FREEZE IN %s on LINE %d\n", __FILE__, __LINE__);   \
    freeze();                                                   \
} while (0)

#define MAX_PROC        64
#define KERNEL_INT      80
#define TIMER_INT       (TIMER_IRQ + 32)
#define KEYBOARD_INT    (KEYBOARD_IRQ + 32)
#define PROC_STACK      (4096 * 4)

#define MAX_DEV         4

#define STATE_STOPPED   0
#define STATE_READY     1
#define STATE_SLEEP     2

#define UNBLOCKED_BY_SIGNAL -362

#define SYS_STOP        0
#define SYS_YIELD       1
#define SYS_CREATE      2
#define SYS_TIMER       3
#define SYS_SIGRETURN   4
#define SYS_KILL        5
#define SYS_SIGHANDLER  6
#define SYS_KEYBOARD    7
#define SYS_GETPID      100
#define SYS_PUTS        103
#define SYS_SLEEP       105

#define SYS_OPEN        21
#define SYS_CLOSE       22
#define SYS_READ        23
#define SYS_WRITE       24
#define SYS_IOCTL       25

/* make sure interrupts are armed later on in the kernel development  */
#define STARTING_EFLAGS         0x00003000
#define ARM_INTERRUPTS          0x00000200

#define DEV_KEYBOARD            0x0
#define DEV_ECHO_KEYBOARD       0x1

#define KEYBOARD_IN             0x60
#define KEYBOARD_CNTL          0x64


typedef void    (*funcptr)(void);
typedef void    (*funcptr_w_arg)(void*);

typedef struct pcb {
    struct pcb * next;
    struct pcb * prev;
    long        esp;
    int         state;
    int         pid;
    int         otherpid;
    void        *buffer;
    int         bufferlen;
    int         ret;
    int         sleepdiff;
    long        args;
    long        signals;
    int         in_handler;
    funcptr_w_arg   handlers[32];
    int         fdt[MAX_DEV];
} pcb;


/* The actual space is set aside in create.c */
extern pcb     proctab[MAX_PROC];

typedef struct devsw {
    char *dvname;
    int owner_blocked;
    pcb *owner_pcb;
    int (*dvopen)(void);
    int (*dvclose)(void);
    int (*dvread)(void* buff, int bufflen);
    int (*dvwrite)(void* buff, int bufflen);
    int (*dvioctl)(int fd, unsigned long command, va_list ap);
    char *dv_read_buff;
    int dv_read_buff_len;
    int dv_read_buff_pos;
    void (*dvint)(void);
} devsw;

extern devsw  devtab[MAX_DEV];

#pragma pack(1)

typedef struct context_frame {
  unsigned int        edi;
  unsigned int        esi;
  unsigned int        ebp;
  unsigned int        esp;
  unsigned int        ebx;
  unsigned int        edx;
  unsigned int        ecx;
  unsigned int        eax;
  unsigned int        iret_eip;
  unsigned int        iret_cs;
  unsigned int        eflags;
  unsigned int        stackSlots[0];
} context_frame;

void            kfree(void *ptr);
void            kmeminit( void );
void            *kmalloc( int size );
int             in_free( void* p );
int             in_hole( void *p );
void            memorydump( void* ptr );
void            dispatch( void );
void            dispatchinit( void );
void            ready( pcb *p );
pcb             *next( void );
void            contextinit( void );
int             contextswitch( pcb *p );
int             create( funcptr fp, int stack );
void            printCF (void * stack);
void            root( void );
void            set_evec(unsigned int xnum, unsigned long handler);
void            sleep(pcb *, int);
void            tick( void );
void            freeze( void );
int             signal(int pid, int sig_no);
void            default_sig_handler( void* );
pcb             *findPCB( int pid );
int             reg_handler( pcb *p, int signal, funcptr_w_arg handler, funcptr_w_arg *old_addr);
void            unwind( pcb *p, long old_sp );
void            process_signals( pcb *p );
void            sigtramp(void (*handler)(void *), void *cntx);
void            remove_sleep( pcb *p );
void            devinit(void);
void            enable_irq( unsigned int irq, int disable );
unsigned int    kbtoa( unsigned char code );

/* Syscalls */
int             syscall(int call, ...);
int             syscreate( funcptr fp, int stack );
int             sysgetpid( void );
unsigned int    syssleep( unsigned int t );
int             sysstop( void );
void            sysputs( char *str );
int             sysyield( void );
int             syskill( int PID, int signalNumber );
int             syssighandler( int signal, void (*newhandler)(void *), void (** oldhandler)(void *) );
void            syssigreturn( void *old_sp );
int             sysopen(int dev_no);
int             sysclose(int fd);
int             sysread(int fd, void* buff, int bufflen);
int             syswrite(int fd, void* buff, int bufflen);
int             sysioctl(int fd, unsigned long command, ...);

/* Device Interface */
int di_open(pcb *p, int dev_no);
int di_close(pcb *p, int fd);
int di_read(pcb *p, int fd, void* buff, int bufflen);
int di_write(pcb *p, int fd, void* buff, int bufflen);
int di_flush_int(int fd, void* buff, int bufflen, int finished);
void di_unblock(pcb * p, int fd, int ret_code);
void di_sig_int(pcb * pcb, int fd);
void di_int(int fd);

#endif
