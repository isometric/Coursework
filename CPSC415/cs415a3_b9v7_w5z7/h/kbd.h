
#ifndef __KDB_H__
#define __KDB_H__

#include <xeroskernel.h>

#define BUFFER_SIZE 4

#define NONE        -1
#define KB          0
#define ECHO_KB     1


int kb_open(void);
int kb_close(void);
void kb_init(devsw * dev_blk);
void kb_int(void);

int echo_kb_open(void);
int echo_kb_close(void);
void echo_kb_init(devsw * dev_blk);
void echo_kb_int(void);

int keyboard_open(int device);
int keyboard_close(int device);
void keyboard_init(int echo, devsw * dev_blk);
void keyboard_int(void);

int keyboard_read(void* buff, int bufflen);
int keyboard_write(void* buff, int bufflen);
int keyboard_ioctl(int fd, unsigned long command, va_list ap);

int keyboard_flush(void);


#endif
