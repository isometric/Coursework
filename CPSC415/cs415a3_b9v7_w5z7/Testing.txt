
This file is to include your testing documentation. The file is to be
in plain text ASCII and properly spaced and edited so that when it is
viewed on a Linux machine it is readable. Not all ASCII files created
on a Windows machine display as expected due to the differences with
respect to how new lines and carriage returns are treated. Line widths
are to be limited to 80 characters.

=======================================================================

                      NOTES FOR RUNNING TEST

For most of these tests modify the choice variable at the bottom
of user.c. By doing this you can select which test to run.

=======================================================================

                        TABLE OF CONTENTS

1. Test showing prioritization of signals
2. syssighandler() test case
3. syssigkill() test case
4. sysopen() with invalid arguments
5. syswrite() with invalid file descriptor
6. sysioctl() test for invalid commands
7. sysread() when there are more characters buffered in kernel than 
    the read requests
8. Two test cases for scenarios not covered here or in the test 
    program.

=======================================================================

1. Test showing prioritization of signals

This test shows that higher priority signals are delivered before 
lower priority signals.

In user.c set choice to 6. This will run test6. test6() creates a 
process that creates a new process and then yields a couple times.
After yielding a few times the process sends two signals to the newly
created process. The two signals sent are 24 and 25 in that order.
The order that handlers should be executed in 25 and then 24, since
higher valued signals are executed first.

After creation, the new process registers a handler for 24 and 25.

Expect to see the following output

handler6b()
...
handler6a()

handler6b() should be shown first since it is the handler for 25.
This output demonstrates prioritization works because both signals
should be delivered in the same quantum. This means that when the
signalled process is next set to use the CPU both signals will be
seen; however, since the signal handler respects signal priority
the higher priority signal will be handled first.

After seeing the correct output shutdown the kernel since fn6, the
process created, enters an infinite loop.

=======================================================================

2. syssighandler() test case

This test shows that signal handler is run.

Test 1 already shows that syssighandler will register a new handler,
what remains to be shown is that the old handler is written back
and that passing NULL results in loss of the old handler.

Follow a similar setup to Test 1, instead set choice to 2.

This test tries calling syssighandler with a variety of arguments,
most of them return error values. There are two things to note

1) the lines following the third "Valid signal" should show the address
of handler_fn to be equal to old_addr

2) the line following the fourth "Valid signal" should show the same
address as previous.

Number (2) means that even if the user passes in NULL for the old 
address that it doesn't write anything. This effectively throws
away the old address.

Here is the expected output.

Invalid signal, valid handler, valid old_addr, result: -1
0xa0000 in hole
Valid signal, invalid handler, valid old_addr, result: -2
Valid signal, valid handler, valid old_addr, result: 0
old_addr contains 0x0
Valid signal, valid handler, valid old_addr, result: 0
handler_fn addr 0x1de3
old_addr contains 0x1de3
Valid signal, valid handler, old_addr == NULL, result: 0
old_addr contains 0x1de3

=======================================================================

3. syssigkill() test case

Test 1 already demonstrates that syssigkill delivers a signal to a
process. This test will demonstrate that syssigkill handles invalid 
arguments.

First, set choice to 1. Test 1 creates two processes. The first
process yields a couple of times while to give the second process time
to spin up and enter it's infinite for-loop. The first process then
goes and proceeds to call syskill with a combination of incorrect
arguments. Finally, the first process makes a valid call. Since
the second function has no handlers for that signal nothing happens.

Here is the expected output

Invalid pid, valid signal, result: -712
Invalid pid, valid signal, result: -712
Valid pid, invalid signal, result: -651
Valid pid, invalid signal, result: -651
Valid pid, valid signal, result: 0

The -712 indicates that no process exists with the pid sent syskill.
The -651 indicates that the signal sent to the process is invalid.
Finally, 0 indicates the delivery was successful.

=======================================================================

4. sysopen() with invalid arguments

Test 12 Shows that sysopen will fail when called with invalid 
descriptors or descriptors that aren't registered.

First, set choice to 12. This test, in the test12 function tries a
couple arguments
1) a negative number - this should return -1
2) a valid number that isn't registered - this should return -1
3) a valid number of a device that's already been opened - this should
    return -1
4) a number larger than the number of entries in the file descriptor 
    table

Expect the following output

test12()
Tried to call sysopen with -1, result : -1
Tried to call sysopen with 2 a non-existent device, result : -1
Called system open with valid device number, result: 0
Called system open with valid device, but device is already open, result: -1
Tried to call sysopen with 10, result: -1
test12() passed

The 0 is the file descriptor for the regular non-echo keyboard.
Everything else should be -1 since they will fail.

=======================================================================

5. syswrite() with invalid file descriptor

This test checks that regardless of the arguments passed to syswrite,
the syscall always returns -1

To run this test, set choice to 13. This test checks a variety of
valid and invalid file descriptors.

1) Valid file descriptor - to check the appropriate driver code is 
    available.
2) A file descriptor less than 0
3) A file descriptor that shouldn't exist
4) A file descriptor greater than the size of a file descriptor table

Expect the following output

test13()
kb_write()
Attempted to write to open keyboard on fd 0, saw result of -1
Attempted to write to open keyboard on fd -1, saw result of -1
Attempted to write to open keyboard on fd 2, saw result of -1
Attempted to write to open keyboard on fd 10, saw result of -1
test13() passed

=======================================================================

6. sysioctl() test for invalid commands

This test checks that sysioctl responds with -1 when provided with
invalid commands.










=======================================================================

7. sysread() when there are more characters buffered in kernel than 
    the read requests

This test checks that typing more than 4 characters before calling a 
will cause the additional characters to be lost.

To run this test, set choice to 16. This test turns on, opens a
connection to the echo keyboard and asks you to type 4 characters
quickly. Only 2 characters should be displayed. When the timer is up
the four characters in the buffer should be printed out.

Expect the following output.

test16()
quickly type 4 characters
qwertoo late

read: qw
should have seen 2 characters
test16() passed

the line that says "qwertoo late" is due to typing qwer and then having
the keyboard driver stop since the buffer is full.

=======================================================================

8. Two test cases for scenarios not covered here or in the test 

1) More characters input into lower buffer than can be stored, on next
read only first 4 characters are returned.

This test is run by setting choice to 15. Here is the expected output

test15()
quickly type 5 characters
qwertoo late if you haven't already typed the characters

read: qwer
should have seen first 4 characters typed
test15() passed

2) Sending a signal to a sleeping process wakes it up and causes it to
have an errorneous error code for returning.

This is run by setting choice to 10. This test creates a couple processes
and then sends signals to those processes while they are asleep. The
processes should wake up and return an error code of -362.
