#include <xeroskernel.h>
#include <xeroslib.h>

/* ========================= Globals =============================== */
extern pcb *sleepQ;

/* ========================= Prototypes ============================ */
int get_sig( pcb *p );
void remove_sleep( pcb *p );

/* ========================== Definitions ========================== */

/*  Runs the signal handler and then trampolines the process
 *  back into its old state.
 */
void sigtramp(void (*handler)(void *), void *cntx) {
    handler(cntx);
    syssigreturn(cntx);
}

/*  Registers a signal with a process. If the target process is blocked
 *  then the target is unblocked and the blocking system call that
 *  caused the process to block is returned an error.
 *
 *  pid     the process to receive the signal
 *  sig_no  the signal to deliver
 *
 *  returns -1 if the PID is invalid
 *  returns -2 if a signal is invalid
 *  returns 0 otherwise
 */
int signal(int pid, int sig_no) {

    pcb *target = NULL;

    if (sig_no > 31 || sig_no < 0)
        return -2;

    if ( (target = findPCB(pid)) == NULL )
        return -1;

    // handles blocked processes
    if ( target->state != STATE_READY ) {
        int i;
        for (i = 0; i < MAX_DEV; i++) {
            if (target->fdt[i])
                di_sig_int(target, target->fdt[i]);
        }
        remove_sleep( target );
        target->ret = UNBLOCKED_BY_SIGNAL;
        ready( target );
    }

    // Don't deliver signals if the handler is ignored
    if (target->handlers[sig_no] == NULL)
        return 0;

    target->signals |= 1 << sig_no;
    return 0;
}

/* Unwinds stack to address at old_sp. This is used for undoing the
 * stack built up for a signal handler.
 *
 * This function should also set the return value to that of any
 * saved values.
 *
 * Finally, reenable signals for the process.
 *
 * p        the process whose stack will be unwound
 * old_sp   where the stack will be unwound to
 */
void unwind( pcb *p, long old_sp ) {
    long *stack;

    // unwind stack
    p->esp = old_sp;

    // Mark as ready to handle signal
    p->in_handler = 0;

    // retrieve saved return value
    stack = (long*)p->esp;
    p->ret = stack[-1];
    kprintf("retrieved old ret value of 0x%x\n", p->ret);
}

 /* check for invalid fn address
  *
  * fn  ptr to a function
 */
int valid_fn(funcptr fn) {

    if (in_hole((void*)fn)) {
        kprintf("0x%x in hole\n", fn);
        return 0;
    }

    if (in_free((void*)fn)) {
        kprintf("0x%x in free memory\n", fn);
        return 0;
    }
    return 1;
}

// TODO check that address is inside the process
// TODO check if this is even necessary
int inside_proc(void *addr) {
    return 0;
}

/* Registers a handler for the process. Puts the address of the old
 * handler at old_addr.
 *
 * p            the process to add the handler to
 * sig          the signal the handler is registered for
 * handler      the handler to be called when the proc is delivered sig
 * old_addr     a location to write the address of the old handler to
 *              this is useful if the process wishes to reinstall the
 *              old handler
 *
 * if old_addr is NULL then the original handler is lost.
 *
 * returns  0 if handler installed successfully
 * returns -1 if signal is invalid
 * returns -2 if either handler resides at an invalid address
 */
int reg_handler( pcb *p, int signal, funcptr_w_arg handler, funcptr_w_arg *old_handler) {
    if (signal < 0 || signal > 31)
        return -1;
    if (!valid_fn((void*)handler) || inside_proc((void*)old_handler))
        return -2;
    if (old_handler != NULL)
        *old_handler = p->handlers[signal];

    p->handlers[signal] = handler;

    return 0;
}

/* If a process has a pending signal the highest priority signal is
 * handled; otherwise, do nothing
 *
 * p    the next process to run
 */
void process_signals( pcb *p ) {
    int sig_num;

    // Exit if currently processing a signal or no signals are pending
    if (p->in_handler || (sig_num = get_sig( p ))== 0)
        return;

    void *handler = p->handlers[sig_num];

    // Mark signal as received
    p->signals ^= 1 << sig_num;
    p->in_handler = 1;

    // Save return value of pcb
    long *stack = (long*)p->esp;

    // Save current return value.
    // Expect sigreturn to reset this value.
    stack[-1] = p->ret;

    // Place args for sigtramp on stack
    stack[-2] = p->esp;
    stack[-3] = (long)handler;
    stack[-4] = 0xB0B0; // Garbage return address

    // Create new context frame
    stack = &stack[-4];
    context_frame *cf = (context_frame*)stack;
    --cf;

    // Setup new frame
    memset((void*)cf, 0, sizeof(context_frame));
    cf->iret_cs = getCS();
    cf->iret_eip = (unsigned int)sigtramp;

    // Disable interrupts while handling signal
    cf->eflags = STARTING_EFLAGS | ARM_INTERRUPTS;

    // Update process' esp
    p->esp = (unsigned int)cf;
}

/* Return signal number if a signal is waiting for service by proc
 *
 * p    process
 */
int get_sig( pcb *p ) {
    int i;
    for (i = 31; i >= 0; i--){
        if ( (1 << i) & p->signals )
            return i;
    }
    return 0;
}
