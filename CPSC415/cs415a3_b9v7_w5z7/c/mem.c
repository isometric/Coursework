/* mem.c : memory manager
 */

#include <xeroskernel.h>
#include <i386.h>

/* Your code goes here */
/*Why do you need a paragraph mask?*/
#define PARAGRAPH_MASK  (~(0xf))

extern long     freemem;

typedef struct struct_mem mem;
struct struct_mem {
    mem         *next;
    mem         *prev;
    int         size;
};

static mem      *head;

 void kmeminit( void ) {
/****************************/

     long       s;

     s = ( freemem + 0x10 ) & PARAGRAPH_MASK;

     head = (mem *)s;
     head->size = HOLESTART - s;
     head->prev = NULL;

     s = HOLEEND;

     head->next = (mem *)s;
     head->next->next = NULL;
     head->next->prev = head;
     head->next->size = (1024 * 1024 * 4) - HOLEEND;
}

void kfree(void * mem) {

}


 void *kmalloc( int size ) {
/********************************/

    mem         *p;
    mem         *r;

    if( size & 0xf ) {
        size = ( size + 0x10 ) & PARAGRAPH_MASK;
    }

    for( p = head; p && ( p->size < size ); p = p->next );

    if( !p ) {
        return( 0 );
    }

    if( ( p->size - size ) < sizeof( mem ) ) {
       if( p->next ) {
            p->next->prev = p->prev;
        }

        if( p->prev ) {
            p->prev->next = p->next;
        } else {
            head = p->next;
        }
    } else {
        r = (mem *) ( (int)p + size );
        *r = *p;

        if( p->next ) {
            p->next->prev = r;
        }

        if( p->prev ) {
            p->prev->next = r;
        } else {
            head = r;
        }
    }

    return( p );
}

/* see if pointer is in free memory
 *
 * p ptr to region
 *
 * returns 1 if ptr is in free memory block, 0 otherwise
 */
int in_free(void* p) {
    mem *tmp = head;
    while (tmp) {
        if (tmp <= (mem*)p && (mem*)p <= tmp + tmp->size)
            return 1;
        tmp = tmp->next;
    }
    return 0;
}

/* check if ptr points to addr in hole
 *
 * p    a ptr
 *
 * returns 1 if ptr points to hole region, 0 otherwise
 */
int in_hole(void *p) {
    return (HOLESTART <= (long)p && (long)p <= HOLEEND);
}

/* displays memory around ptr
 *
 * ptr  address to begin printing around
 */
void memorydump(void* ptr) {
    kprintf("displaying memory at 0x%08x\n", (int*)ptr);

    int * i;
    int prevBytes = 4*1, postBytes = 4*10;
    for (i = ((int *) ptr)-prevBytes ; i < ((int*) ptr)+postBytes; i++) {
        !((int) i % 16) ? kprintf("\n%08x :", i) : 0 ; 
        i == (int *) ptr ? kprintf("       >%08x", *i) : kprintf("  \t%08x", *i);
    }   
    kprintf("\n");
}
