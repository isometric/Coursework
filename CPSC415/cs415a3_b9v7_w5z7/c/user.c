/* user.c : User processes
 */

#include <xeroskernel.h>
#include <xeroslib.h>

static int pids[10];
static int g_var = FALSE;

/* Test - Try sending invalid arguments to syskill, and then finally
 * send correct arguments.
 */

void fn1a( void ) {
    int         ret;

    sysyield();
    sysyield();
    sysyield();

    // Invalid target pid
    ret = syskill( 100, 5 );
    kprintf("Invalid pid, valid signal, result: %d\n", ret);
    ret = syskill( -1, 5 );
    kprintf("Invalid pid, valid signal, result: %d\n", ret);
    ret = syskill( pids[1], -1 );
    kprintf("Valid pid, invalid signal, result: %d\n", ret);
    ret = syskill( pids[1], 100 );
    kprintf("Valid pid, invalid signal, result: %d\n", ret);
    ret = syskill( pids[1], 5 );
    kprintf("Valid pid, valid signal, result: %d\n", ret);
    sysstop();
}

void fn1b( void ) {
    for(;;) {
        // Nothing happens
    }
    sysstop();
}

void test1( void ) {
    pids[0] = syscreate( fn1a, 4096 );
    pids[1] = syscreate( fn1b, 4096 );
}

/* Test - Look at handler registering behaviour */

void handler_fn( void *old_esp ) {
    sysstop();
}

void test2( void ) {
    funcptr_w_arg old = (funcptr_w_arg)1;
    int res;
    // Expect -1
    res = syssighandler(-1, NULL, &old);
    kprintf("Invalid signal, valid handler, valid old_addr, result: %d\n", res);

    // Expect -2 since at HOLE start
    res = syssighandler(0, (funcptr_w_arg)(640*1024), &old);
    kprintf("Valid signal, invalid handler, valid old_addr, result: %d\n", res);

    // Expect old to be NULL
    res = syssighandler(0, NULL, &old);
    kprintf("Valid signal, valid handler, valid old_addr, result: %d\n", res);
    kprintf("old_addr contains 0x%x\n", *old);

    // Expect old to be addr of handler_fn
    res = syssighandler(0, handler_fn , &old);
    res = syssighandler(0, NULL , &old);
    kprintf("Valid signal, valid handler, valid old_addr, result: %d\n", res);
    kprintf("handler_fn addr 0x%x\n", handler_fn);
    kprintf("old_addr contains 0x%x\n", *old);

    // Expect old not to change
    res = syssighandler(0, handler_fn, NULL);
    kprintf("Valid signal, valid handler, old_addr == NULL, result: %d\n", res);
    kprintf("old_addr contains 0x%x\n", *old);
}

/* Test - Make sure signals 0 - 31 are delivered */

void fn3a( void ) {
    for(;;) {
        // Nothing happens
    }
    sysstop();
}

void test3( void ) {
    kprintf("-- Test 3 --\n");
    int res;
    pids[0] = syscreate( fn3a, 4096 );
    sysyield();
    sysyield();
    // Expect invalid signal
    res = syskill(pids[0], -1);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 0);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 1);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 2);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 3);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 4);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 5);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 6);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 7);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 8);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 9);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 10);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 11);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 12);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 13);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 14);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 15);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 16);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 17);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 18);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 19);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 20);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 21);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 22);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 23);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 24);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 25);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 26);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 27);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 28);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 29);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 30);
    kprintf("(result: %d)", res);
    res = syskill(pids[0], 31);
    kprintf("(result: %d)", res);
    // Expect invalid signal
    res = syskill(pids[0], 32);
    kprintf("(result: %d)", res);
    sysstop();
}

/* Test - Check that handler entry set ignore does nothing */

void fn4( void ) {
    for (;;) {}
    sysstop();
}
void test4( void ) {
    pids[0] = syscreate( fn4, 4096 );
    sysyield();
    sysyield();
    syskill( pids[0], 30 );
    for (;;) {}
    sysstop();
}

/* Test - Check that a handler is called when a signal is delivered
 * to a process and there is a handler fn assigned.
 */

void handler5( void *ctx ) {
    kprintf("handler5() called\n");
}

void fn5( void ) {
    void (* old)(void*);
    char buf[100];

    sprintf( buf, "fn5's pid is 0x%x\n", sysgetpid() );
    sysputs( buf );

    sprintf( buf, "fn5's handler is at 0x%x\n", &handler5 );
    sysputs( buf );

    syssighandler( 30, handler5, &old );

    for (;;) {
        if (g_var == TRUE) {
            kprintf("The handler was called successfully and returned successfully\n");
            FREEZE;
        }
    }
    sysstop();
}

void test5( void ) {
    int i;
    pids[0] = syscreate( fn5, 4096 );

    for (i = 0; i < 100; i++)  // Kill time

    sysyield();
    syskill( pids[0], 30 );
    for (;;) {}
    sysstop();
}

/* Test - Tests 2 things
 * 1) Test that only one handler is run at a time.
 * 2) Highest priority signal should be handled first
 *      Expect handler6b to be run first since 25 > 24
 *      and priority is ordered by increasing order,
 *      with higher priority signals having a larger
 *      number than lower priority signals.
 */

void handler6a( void *ctx ) {
    int i;
    kprintf("handler6a()\n");
    for ( i = 0; i < 25; i++ )
        sysyield();

    kprintf("handler6a() done\n");
}

void handler6b( void *ctx ) {
    int i;
    kprintf("handler6b()\n");
    for ( i = 0; i < 25; i++ )
        sysyield();

    kprintf("handler6b() done\n");
}

void fn6 ( void ) {
    void (* old_a)(void*);
    void (* old_b)(void*);
    syssighandler( 24, handler6a, &old_a );
    syssighandler( 25, handler6b, &old_b );

    for (;;) {}
    sysstop();
}

void test6( void ) {
    int i;

    pids[0] = syscreate( fn6, 4096 );

    for ( i = 0; i < 25; i++)
        sysyield();

    syskill( pids[0], 24 );
    syskill( pids[0], 25 );

    sysstop();
}

/* Test - If a signal is received while the same signal value is being
 * processed then the new signal should delivered but not handled until
 * the current handler is done firing. */

void handle7( void *ctx ) {
    int i;

    kprintf("handle7()\n");

    for (i = 0; i < 50; i++)
        sysyield();

    kprintf("handle7() done\n");
}

void fn7( void ) {
    void (*old)(void*);

    syssighandler( 10, &handle7, &old );
    for (;;) {}
    sysstop();
}

void test7( void ) {
    pids[0] = syscreate( fn7, 4096 );

    // WAIT
    int i;
    for (i = 0; i < 5; i++)
        sysyield();

    syskill( pids[0], 10 );

    // Smoke break
    for (i = 0; i < 5; i++)
        sysyield();

    // Expect to see this message between the first
    // "handle7()" and it's matching "handle7() done"
    sysputs("Sending second signal 10 to fn7\n");
    syskill( pids[0], 10 );
}

/* Test - A received signal while sleeping should:
 * 1) cause the process to be unblocked
 * 2) have an error code set for its return value
 * 3) the signal should be processed
 * 4) the process should return from sleeping with an error
 */

void handle8( void *ctx) {
    kprintf("handle8() ran\n");
}

void fn8( void ) {
    int res;
    void (*old)(void*);

    syssighandler( 1, &handle8, &old );

    // Try and take a nice long nap.
    res = (int)syssleep( 3 * 60 * 1000 );

    kprintf("Tried to sleep for 3 minutes. syssleep recieved result %d\n", res);

    if (res != UNBLOCKED_BY_SIGNAL)
        FREEZE;

    sysstop();
}

void test8( void ) {
    int i, res;

    pids[0] = syscreate( fn8, 4096 );

    // Kill some time. Want to come back while fn8 is sleeping.
    for (i = 0; i < 5; i++)
        sysyield();

    res = syskill( pids[0], 1 );
    kprintf("Tried to kill proc 0x%x, result of %d\n", pids[0], res);
}

/* Test - A received signal while sleeping should:
 * 1) cause the process to be unblocked
 * 2) have an error code set for its return value
 * 3) the signal should be processed
 * 4) the process should return from sleeping with an error
 *
 * This test checks 3 things
 * 1) Removing node in middle of sleep queue
 * 2) Removing node at the end of the sleep queue
 * 3) Removing a node when the sleep queue is of size 1
 */

void fn9a( void ) {
    int res;
    res = syssleep( 10 * 1000 );
    kprintf("fn9a (0x%x), result %d\n", sysgetpid(), res);
    sysstop();
}

void fn9b( void ) {
    int res;
    res = syssleep( 20 * 1000 );
    kprintf("fn9b (0x%x), result %d\n", sysgetpid(), res);
    sysstop();
}

void fn9c( void ) {
    int res;
    res = syssleep( 30 * 1000 );
    kprintf("fn9c (0x%x), result %d\n", sysgetpid(), res);
    sysstop();
}

void test9( void ) {
    int i;
    pids[0] = syscreate( fn9a, 4096 );
    pids[1] = syscreate( fn9b, 4096 );
    pids[2] = syscreate( fn9c, 4096 );

    // Kill some time. Want to come back while fns are sleeping.
    for (i = 0; i < 5; i++)
        sysyield();

    sysputs("signal to fn9b proc\n");
    syskill( pids[1], 1 );
    sysputs("signal to fn9c proc\n");
    syskill( pids[2], 1 );
    sysputs("signal to fn9a proc\n");
    syskill( pids[0], 1 );
}

/* Test - A received signal while sleeping should:
 * 1) cause the process to be unblocked
 * 2) have an error code set for its return value
 * 3) the signal should be processed
 * 4) the process should return from sleeping with an error
 *
 * This test checks removing a node at the beginning of the sleep queue
 */

void fn10a( void ) {
    int res;
    res = syssleep( 1 * 1000 );
    // Expect to see this print 1st
    kprintf("fn10a result %d\n", res);
    sysstop();
}

void fn10b( void ) {
    int res;
    res = syssleep( 2 * 1000 );
    kprintf("fn10b result %d\n", res);
    sysstop();
}

void fn10c( void ) {
    int res;
    res = syssleep( 3 * 1000 );
    kprintf("fn10c result %d\n", res);
    sysstop();
}

void test10( void ) {
    int i;
    pids[0] = syscreate( fn10a, 4096 );
    pids[1] = syscreate( fn10b, 4096 );
    pids[2] = syscreate( fn10c, 4096 );

    // Kill some time. Want to come back while fns are sleeping.
    for (i = 0; i < 5; i++)
        sysyield();

    syskill( pids[0], 1 );
    syskill( pids[1], 1 );
    syskill( pids[2], 1 );
}

/* Test - Open a device, reads 10 characters, then closes the device */
void test11( void ) {
    sysputs("test11()\n");

    int res, fd;
    char buff[120];
    sysputs(buff);
    fd = sysopen(DEV_ECHO_KEYBOARD);
    if (fd == -1)
        FREEZE;
    sprintf(buff, "echo keyboard opened with fd %d\n", fd);
    sysputs(buff);

    memset(buff, '\0', 120);
    res = sysread(fd, (void*) buff, 10);
    sysputs("\n");
    int i;
    for (i = 0; i < res; i++) {
      sysputs("-");
    }
    sysputs("\n");
    sysputs(buff);
    sysputs("\n");

    res = sysclose(fd);
    if (res == -1) {
      sysputs("returns -1\n");
    }
}

/* Test - 2.6 in Assignment */

void a_new_process(void) {
    syssleep(1000);
    syskill(pids[0], 20);
    syskill(pids[0], 18);
    sysstop();
}

void another_new_process(void) {
    syssleep(5000);
    syskill(pids[0], 18);
    sysstop();
}

void yet_another_new_process(void) {
    syssleep(5000);
    syskill(pids[0], 20);
    sysstop();
}

void prints_that_it_was_called(void * _){
    sysputs("that it was called\n");
}

void a_signal_handler(void * _){
    sysputs("that it is the 2nd signal handler and was called\n");
}

void a_test_program(void) {
    int fd, ret_val;
    char buff[256];
    sysputs("a greeting\n");
    fd = sysopen(DEV_ECHO_KEYBOARD);
    if (ret_val == -1) sysputs("ERR: open echo keyboard ret -1\n");
    else sysputs("opened echo keyboard\n");


    memset(buff, '\0', 256);
    ret_val = sysread(fd, buff, 10);
    sysputs("sysread read:\n");
    sysputs(buff);
    sysputs("\n");


    ret_val = sysopen(DEV_KEYBOARD);
    if (ret_val == -1) sysputs("open keyboard ret -1\n");
    else sysputs("ERR: opened keyboard\n");

    ret_val = sysclose(fd);

    if (ret_val == -1) sysputs("ERR close ret -1\n");
    else sysputs("echo keyboard closed\n");

    fd = sysopen(DEV_KEYBOARD);
    if (ret_val == -1) sysputs("ERR: open keyboard ret -1\n");
    else sysputs("opened keyboard\n");

    int i;
    sysputs("reading 3x 10 chars\n");
    for (i = 0; i < 3; i++) {
        memset(buff, '\0', 256);
        ret_val = sysread(fd, (void*) buff, 10);
        sysputs(buff);
    }

    ret_val = sysclose(fd);

    if (ret_val == -1) sysputs("ERR close ret -1\n");
    else sysputs("echo keyboard closed\n");

    pids[0] = sysgetpid();

    void (* old_handler)(void*);
    syssighandler( 18, prints_that_it_was_called, &old_handler);
    syscreate(a_new_process, 4096);

    memset(buff, '\0', 256);
    ret_val = sysread(fd, (void*) buff, 10);
    if (ret_val == -362) sysputs("sysread returned -362\n");
    else sysputs("ERR: -362 not returned by sysread");

    syssighandler( 18, a_signal_handler, &old_handler);
    syscreate(another_new_process, 4096);

    memset(buff, '\0', 256);
    ret_val = sysread(fd, (void*) buff, 10);
    if (ret_val == -362) sysputs("sysread returned -362\n");
    else sysputs("ERR: -362 not returned by sysread\n");

    syssighandler( 20, old_handler, &old_handler);
    syscreate(yet_another_new_process, 4096);

    memset(buff, '\0', 256);
    ret_val = sysread(fd, (void*) buff, 10);
    if (ret_val == -362) sysputs("sysread returned -362\n");
    else sysputs("ERR: -362 not returned by sysread\n");

    memset(buff, '\0', 256);
    ret_val = sysread(fd, (void*) buff, 256);
    sysputs("sysread read:\n");
    sysputs(buff);
    sysputs("\n");

    sysputs("reading again\n");
    memset(buff, '\0', 256);
    ret_val = sysread(fd, (void*) buff, 256);
    sysputs("sysread read:\n");
    sysputs(buff);
    sysputs("\n");

    sysputs("termination message and exit\n");
    syssleep(60*1000);
}

/* Test - Calling sysopen with invalid arguments returns -1. */
void test12( void ) {
    sysputs("test12()\n");

    int res;
    char buff[120];

    res = sysopen(-1);
    sprintf(buff, "Tried to call sysopen with -1, result : %d\n", res);
    sysputs(buff);
    if (res != -1)
        FREEZE;

    res = sysopen(2);
    sprintf(buff, "Tried to call sysopen with 2 a non-existent device, result : %d\n", res);
    sysputs(buff);
    if (res != -1)
        FREEZE;

    res = sysopen(DEV_KEYBOARD);
    sprintf(buff, "Called system open with valid device number, result: %d\n", res);
    sysputs(buff);
    if (res == -1)
        FREEZE;

    res = sysopen(DEV_ECHO_KEYBOARD);
    sprintf(buff, "Called system open with valid device, but device is already open, result: %d\n", res);
    sysputs(buff);
    if (res != -1)
        FREEZE;

    res = sysopen(10);
    sprintf(buff, "Tried to call sysopen with 10, result: %d\n", res);
    sysputs(buff);
    if (res != -1)
        FREEZE;


    sysputs("test12() passed\n");
}

/* Test - Tests that syswrite returns -1 when given an invalid file descriptor */
void test13( void ) {
    sysputs("test13()\n");

    int res, fd;
    char buff[100];
    buff[0] = 'a';
    buff[1] = 'b';
    buff[2] = 'c';
    buff[3] = 'd';
    buff[4] = 'e';
    buff[5] = '\0';

    fd = sysopen(DEV_KEYBOARD);
    if (fd != DEV_KEYBOARD) FREEZE;

    res = syswrite(fd, (void*)buff, 5);
    if (res != -1) FREEZE;

    sprintf(buff, "Attempted to write to open keyboard on fd %d, saw result of %d\n", fd, res);
    sysputs(buff);

    res = syswrite(-1, (void*)buff, 5);
    if (res != -1) FREEZE;

    sprintf(buff, "Attempted to write to open keyboard on fd %d, saw result of %d\n", -1, res);
    sysputs(buff);

    res = syswrite(2, (void*)buff, 5);
    if (res != -1) FREEZE;

    sprintf(buff, "Attempted to write to open keyboard on fd %d, saw result of %d\n", 2, res);
    sysputs(buff);

    res = syswrite(10, (void*)buff, 5);
    if (res != -1) FREEZE;

    sprintf(buff, "Attempted to write to open keyboard on fd %d, saw result of %d\n", 10, res);
    sysputs(buff);

    sysputs("test13() passed\n");
}

void test14(void) {
    int res, fd;
    char buff[120];
    sysputs(buff);
    fd = sysopen(DEV_ECHO_KEYBOARD);
    if (fd == -1)
        FREEZE;
    sprintf(buff, "echo keyboard opened with fd %d\n", fd);
    sysputs(buff);

    res = sysioctl(fd, 53, 'q');
    if (res == -1) {
      sysputs("sysioctl returned -1\n");
    }
    sysputs("EOF char set to 'q'\n");

    int i;
    for (i = 0; i < 3; i++) {
        memset(buff, '\0', 120);
        res = sysread(fd, (void*) buff, 120);
        sysputs("\n");
        int i;
        for (i = 0; i < res; i++) {
          sysputs("-");
        }
        sysputs("\n");
        sysputs(buff);
        sysputs("\n");
    }

    res = sysclose(fd);
    if (res == -1) {
      sysputs("stsclose returned -1\n");
    }
}

/* Test - Part of test Check that extra characters aren't buffered */
void test15( void ) {
    sysputs("test15()\n");

    int fd;
    char buff[100];
    char out[100];

    memset((void*)buff, '\0', sizeof(char)*100);

    fd = sysopen(DEV_ECHO_KEYBOARD);

    sysputs("quickly type 5 characters\n");
    syssleep(10 * 1000);
    sysputs("too late if you haven't already typed the characters\n");

    sysread(fd, (void*) buff, 4);
    sprintf(out, "\nread: %s\n", buff);
    sysputs(out);
    sysputs("should have seen first 4 characters typed\n");

    sysputs("test15() passed\n");
}


/* Test - When kernel has more chars buffered than requested */
void test16( void ) {
    sysputs("test16()\n");
    int fd;
    char buff[100];
    char out[100];
    
    memset((void*)buff, '\0', sizeof(char)*100);

    fd = sysopen(DEV_ECHO_KEYBOARD);

    sysputs("quickly type 4 characters\n");
    syssleep(10 * 1000);
    sysputs("too late\n");

    // Expect this to print immediately
    sysread(fd, (void*) buff, 2);
    sprintf(out, "\nread: %s\n", buff);
    sysputs(out);
    sysputs("should have seen 2 characters\n");
    sysputs("test16() passed\n");
}

/* Test - When a kernel has more chars bfufered than requested. 
 * When there are two reads they should both return immediately. */

void test17( void ) {
    sysputs("test17()\n");
    int fd;
    char buff[100];
    char out[100];

    memset((void*)buff, '\0', sizeof(char)*100);

    fd = sysopen(DEV_ECHO_KEYBOARD);

    sysputs("quickly type 4 characters\n");
    syssleep(10 * 1000);
    sysputs("too late\n");

    // Expect this to print immediately
    sysread(fd, (void*) buff, 2);
    sprintf(out, "\nread: %s\n", buff);
    sysputs(out);
    sysputs("should have seen 2 characters\n");

    memset((void*)buff, '\0', sizeof(char)*100);

    // Expect this to print immediately
    sysread(fd, (void*) buff, 2);
    sprintf(out, "\nread: %s\n", buff);
    sysputs(out);
    sysputs("should have seen 2 characters\n");

}

void     root( void ) {
/****************************/


    char  buff[100];

    sysyield();
    sysyield();

    int choice = 0;
    switch (choice) {
        default:
            a_test_program();
            break;
        case 1:
            test1();
            break;
        case 2:
            test2();
            break;
        case 3:
            test3();
            break;
        case 4:
            test4();
            break;
        case 5:
            test5();
            break;
        case 6:
            test6();
            break;
        case 7:
            test7();
            break;
        case 8:
            test8();
            break;
        case 9:
            test9();
            break;
        case 10:
            test10();
            break;
        case 11:
            test11();
            break;
        case 12:
            test12();
            break;
        case 13:
            test13();
            break;
        case 14:
            test14();
            break;
        case 15:
            test15();
            break;
        case 16:
            test16();
            break;
        case 17:
            test17();
            break;
    };

    sprintf(buff, "Root finished\n");
    sysputs( buff );
    for (;;) {
    }
    sysstop();
}
