/* syscall.c : syscalls
 */

#include <xeroskernel.h>

int syscall( int req, ... ) {
    va_list     ap;
    int         rc;

    va_start( ap, req );

    __asm __volatile( " \
        movl %1, %%eax \n\
        movl %2, %%edx \n\
        int  %3 \n\
        movl %%eax, %0 \n\
        "
        : "=g" (rc)
        : "g" (req), "g" (ap), "i" (KERNEL_INT)
        : "%eax"
    );

    va_end( ap );

    return( rc );
}

 int syscreate( funcptr fp, int stack ) {
    return( syscall( SYS_CREATE, fp, stack ) );
}

 int sysyield( void ) {
    return( syscall( SYS_YIELD ) );
}

 int sysstop( void ) {
    return( syscall( SYS_STOP ) );
}

int sysgetpid( void ) {
    return( syscall( SYS_GETPID ) );
}

void sysputs( char *str ) {
    syscall( SYS_PUTS, str );
}

unsigned int syssleep( unsigned int t ) {
    return syscall( SYS_SLEEP, t );
}

/*  Registers newhandler for the indicated signal
 *
 *  signal          the signal to handle. must be between 0 - 31
 *  newhandler      a fn to handle the new signal
 *  oldhandler      a ptr to a location to write the address of the old
 *                  handler. this is useful to reinstall the old handler.
 *
 *  if oldhandler is NULL then the old handler is lost.
 *
 *  returns         -1 if signal is invalid
 *  returns         -2 if newhandler is at an invalid address
 *  returns         0 if the handler is successfully installed
 */
int syssighandler(int signal, void (*newhandler)(void *), void (** oldhandler)(void *)) {
    return syscall( SYS_SIGHANDLER, signal, newhandler, oldhandler );
}

/*  Not to be called by user code. Used for signal handling. */
void syssigreturn(void *old_sp) {
    syscall( SYS_SIGRETURN, old_sp );
    // Shouldn't return
    freeze();
}

/*  Delivers a signal to process with PID
 *
 *  PID             the PID of the process to deliver the signal to
 *  signalNumber    the signal to deliver, [0, 31].
 *
 *  returns         0 on success, or if the signal has already
 *                  been delivered.
 *  returns         -712 if the process doesn't exist
 *  returns         -651 if signalNumber is invalid.
 */
int syskill(int pid, int signalNumber) {
    int ret = syscall( SYS_KILL, pid, signalNumber );
    if (ret == -1) // PID invalid
        return -712;
    if (ret == -2) // sig invalid
        return -651;
    return 0;
}

/* Open a device. May fail for one of the following reasons
 *
 * 1) invalid number
 * 2) already assigned number
 * 3) valid number with no device
 *
 * dev_no   major device number
 *
 * returns  the file descriptor in [0, 3] if successful
 * returns  -1 if the open fails
 */
int sysopen(int dev_no) {
    return syscall(SYS_OPEN, dev_no);
}

/* Closes the file descriptor.
 *
 * fd   an open file descriptor
 *
 * returns 0 on success, -1 on failure
 */
int sysclose(int fd) {
    return syscall(SYS_CLOSE, fd);
}

/* reads up to bufflen bytes from the open device into the buffer
 *
 * fd       file descriptor to read from
 * buff     buffer to read into
 * bufflen  the maximum amount of bytes to read into buff
 *
 * returns  -1 on error
 * returns  the number of bytes on success
 */
int sysread(int fd, void* buff, int bufflen) {
    return syscall(SYS_READ, fd, buff, bufflen);
}

/* write bytes from the buffer into fd
 *
 * fd       the file descriptor to write to
 * buff     the bytes to write
 * bufflen  the len of the msg in buff
 *
 * returns -1 on error, otherwise the number of bytes written
 */
int syswrite(int fd, void* buff, int bufflen) {
    return syscall(SYS_WRITE, fd, buff, bufflen);
}

/* Executes command on fd
 *
 * fd       file descriptor to send command ot
 * command  the command to implement
 * ...      additional parameters (device specific)
 *
 * returns  -1 on error, 0 otherwise
 */
int sysioctl(int fd, unsigned long command, ...) {
    va_list ap;
    va_start(ap, command);
    return syscall(SYS_IOCTL, fd, command, ap);
}
