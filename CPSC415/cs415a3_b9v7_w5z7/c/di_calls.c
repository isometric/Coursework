/* di_calls.c : messaging system (assignment 2)
 */

#include <xeroskernel.h>

#include <kbd.h>

devsw devtab[MAX_DEV];

void devinit(void) {
    memset((void*)devtab, 0, sizeof(devsw)*MAX_DEV);
    kb_init(&devtab[DEV_KEYBOARD]);
    echo_kb_init(&devtab[DEV_ECHO_KEYBOARD]);
}

int di_open(pcb * pcb, int dev_no) {
    if (dev_no & 0xFFFFFFFC) return -1;

    if (devtab[dev_no].owner_pcb == 0 &&
        (devtab[dev_no].dvopen &&
        devtab[dev_no].dvopen() > 0)) {

        devtab[dev_no].owner_pcb = pcb;
        devtab[dev_no].owner_blocked = 0;
        pcb->fdt[dev_no] = dev_no;
        return dev_no;
    }

    return -1;
}

int di_close(pcb * pcb, int fd) {
    if (fd & 0xFFFFFFFC) return -1;

    if (pcb->fdt[fd] == fd &&
        (pcb == devtab[fd].owner_pcb) &&
        (devtab[fd].dvclose &&
        devtab[fd].dvclose() > 0)) {

        devtab[fd].owner_pcb = 0;
        devtab[fd].owner_blocked = 0;
        return 0;
    }

    return -1;
}

// -1 error, 1 good, only propogate error
int di_read(pcb * pcb, int fd, void* buff, int bufflen){
    if (fd & 0xFFFFFFFC) return -1;
    if (pcb->fdt[fd] == fd &&
        (pcb == devtab[fd].owner_pcb)) {

        if (!devtab[fd].dvread) {
            di_unblock(pcb, fd, 0);
            return 1;
        } else {
            devtab[fd].dv_read_buff =  buff;
            devtab[fd].dv_read_buff_len = bufflen;
            devtab[fd].owner_blocked = 1;
            devtab[fd].dv_read_buff_pos = 0;
            devtab[fd].dv_read_buff_pos = devtab[fd].dvread(buff, bufflen);

            return 1;
        }
    }

    return -1;
}

int di_write(pcb * pcb, int fd, void* buff, int bufflen){
    if (fd & 0xFFFFFFFC) return -1;
    int status;
    if (pcb->fdt[fd] == fd &&
        (pcb == devtab[fd].owner_pcb) &&
        (devtab[fd].dvwrite &&
        (status = devtab[fd].dvwrite(buff, bufflen)) > 0)) {

        return status;
    }

    return -1;
}

void di_int(int fd) {
    if (fd & 0xFFFFFFFC) return;
    if (!devtab[fd].owner_pcb) return;
    devtab[fd].dvint();
}

/* Called outside of device */
void di_sig_int(pcb * pcb, int fd) {
    if (fd & 0xFFFFFFFC) return;
    if (pcb->fdt[fd] == fd &&
        (pcb == devtab[fd].owner_pcb) &&
        devtab[fd].owner_blocked) {

        di_unblock(pcb, fd, -362);
    }
}

/* Only called by device functions. */
void di_unblock(pcb * pcb, int fd, int ret_code) {
    if (pcb->fdt[fd] == fd &&
        (pcb == devtab[fd].owner_pcb) &&
        devtab[fd].owner_blocked &&
        devtab[fd].owner_pcb->state != STATE_SLEEP) {

        pcb->ret = ret_code;
        ready(pcb);

        devtab[fd].dv_read_buff = NULL;
        devtab[fd].dv_read_buff_len = 0;
        devtab[fd].dv_read_buff_pos = 0;

        devtab[fd].owner_blocked = 0;
    }
}

int di_flush_int(int fd, void* buff, int bufflen, int finished) {
    int bytes_copied = 0, i = 0;
    char *cbuff = (char*)buff;

    for (; devtab[fd].dv_read_buff_pos < devtab[fd].dv_read_buff_len && bytes_copied < bufflen;
        devtab[fd].dv_read_buff_pos++, bytes_copied++) {
        devtab[fd].dv_read_buff[devtab[fd].dv_read_buff_pos] = cbuff[bytes_copied];
    }

    if (devtab[fd].dv_read_buff_pos == devtab[fd].dv_read_buff_len ||
        finished) {
        di_unblock(devtab[fd].owner_pcb, fd, devtab[fd].dv_read_buff_pos);
    }

    return bytes_copied;
}

int di_ioctl(pcb * pcb, int fd, int request_no, va_list ap){
    if (fd & 0xFFFFFFFC) return -1;
    int status;
    if (pcb->fdt[fd] == fd &&
        (pcb == devtab[fd].owner_pcb) &&
        (!devtab[fd].dvioctl ||
        (devtab[fd].dvioctl &&
        (status = devtab[fd].dvioctl(fd, (unsigned long)request_no, ap) > 0)))) {
        return status;
    }


    return -1;
}
