/* sleep.c : sleep device (assignment 2)
 */

#include <xeroskernel.h>


#define MILLISECONDS_TICK 10
static pcb	*sleepQ;


// Len is the length of time to sleep

void	sleep( pcb *p, int len ) {
/****************************************/

    pcb	*tmp;


    if( len < 1 ) {
        ready( p );
        return;
    }

    // Convert the length of time to sleep in ticks
    // each tick is 10ms
    len = len / MILLISECONDS_TICK;

    p->state = STATE_SLEEP;
    p->next = NULL;
    p->prev = NULL;
    if( !sleepQ ) {
        sleepQ = p;
        p->sleepdiff = len;
    } else if( sleepQ->sleepdiff > len ) {
        p->next = sleepQ;
        sleepQ->sleepdiff -= len;
        p->sleepdiff = len;
        sleepQ = p;
    } else {
        len -= sleepQ->sleepdiff;
        for( tmp = sleepQ; tmp->next; tmp = tmp->next ) {
            if( len < tmp->next->sleepdiff ) {
                break;
            } else {
                len -= tmp->next->sleepdiff;
            }
        }

        p->next = tmp->next;
        p->prev = tmp;
        p->sleepdiff = len;
        tmp->next = p;
        if( p->next ) {
            p->next->prev = p;
            p->next->sleepdiff -= len;
        }
    }
}

extern void	tick( void ) {
/****************************/

    pcb	*tmp;

    if( !sleepQ ) {
        return;
    }

    for( sleepQ->sleepdiff--; sleepQ && !sleepQ->sleepdiff; ) {
        tmp = sleepQ;
        sleepQ = tmp->next;

        tmp->state = STATE_READY;
        tmp->next = NULL;
        tmp->prev = NULL;
        tmp->ret = 0;
        ready( tmp );
    }
}

/* Removes process from sleepQ. The process has an error code set
 * to that indicates to the syscall it has been unblocked.  The
 *
 * Assume: pcb is on the sleepQ
 *
 * p    process to remove from sleep queue
 */
extern void remove_sleep( pcb *p ) {
    if (p->state != STATE_SLEEP)
        return;

    // Remove node from sleepQ

    // Edge case where p is head of sleepQ
    // Here, change the head of sleepQ
    if (sleepQ == p)
        sleepQ = p->next;

    // Tell the previous element to point to element after p
    if (p->prev)
        p->prev->next = p->next;

    // Tell the next element to point to the element before p
    if (p->next)
        p->next->prev = p->prev;

    p->next = NULL;
    p->prev = NULL;
}
