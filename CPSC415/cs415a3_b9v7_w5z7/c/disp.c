/* disp.c : dispatcher
*/

#include <xeroskernel.h>
#include <i386.h>
#include <xeroslib.h>

pcb *findPCB( int pid );

static pcb      *head = NULL;
static pcb      *tail = NULL;

void     dispatch( void ) {
    /********************************/

    pcb             *p;
    int             r;
    funcptr         fp;
    funcptr_w_arg   handler;
    funcptr_w_arg   *handler_loc;
    int             stack;
    va_list         ap;
    char            *str;
    void            *buf;
    int             len;
    int             sig;
    int             target;
    unsigned long   cmd;

    for( p = next(); p; ) {

        process_signals(p);

        r = contextswitch(p);
        switch(r) {

            case(SYS_CREATE):
                ap     = (va_list)p->args;
                fp     = (funcptr)(va_arg(ap, int));
                stack  = va_arg(ap, int);
                p->ret = create(fp, stack);
                break;

            case(SYS_YIELD):
                ready(p);
                p = next();
                break;

            case(SYS_STOP):
                p->state = STATE_STOPPED;
                p        = next();
                break;

            case(SYS_KILL):
                ap      = (va_list)p->args;
                target  = va_arg(ap, int);
                sig     = va_arg(ap, int);
                p->ret  = signal(target, sig);
                break;

            case(SYS_PUTS):
                ap                  = (va_list)p->args;
                str                 = va_arg(ap, char*);
                kprintf("%s", str);
                p->ret              = 0;
                break;

            case(SYS_GETPID):
                p->ret = p->pid;
                break;

            case(SYS_SLEEP):
                ap             = (va_list)p->args;
                len            = va_arg(ap, int);
                sleep(p, len);
                p              = next();
                break;

            case(SYS_TIMER):
                tick();
                ready(p);
                p = next();
                end_of_intr();
                break;

            case(SYS_SIGRETURN):
                ap      = (va_list)p->args;
                stack   = va_arg(ap, long);
                unwind(p, stack);
                break;

            case(SYS_OPEN):
                ap     = (va_list)p->args;
                target = va_arg(ap, int);
                p->ret = di_open(p, target);
                break;

            case(SYS_CLOSE):
                ap     = (va_list)p->args;
                target = va_arg(ap, int);
                p->ret = di_close(p, target);
                
            case( SYS_KEYBOARD ):
                keyboard_int();
                break;

            case(SYS_READ):
                ap          = (va_list)p->args;
                target      = va_arg(ap, int);
                buf         = va_arg(ap, void*);
                len         = va_arg(ap, int);
                if ((p->ret      = di_read(p, target, buf, len)) != -1)
                    p = next();
                break;

            case(SYS_WRITE):
                ap           = (va_list)p->args;
                target       = va_arg(ap, int);
                buf          = va_arg(ap, void*);
                len          = va_arg(ap, int);
                p->ret       = di_write(p, target, buf, len);
                break;

            case(SYS_IOCTL):
                ap           = (va_list)p->args;
                target       = va_arg(ap, int);
                cmd          = va_arg(ap, unsigned long);
                ap           = va_arg(ap, va_list); // TODO will this work?

                break;

            case(SYS_SIGHANDLER):
                ap          = (va_list)p->args;
                sig         = va_arg(ap, int);
                handler     = va_arg(ap, funcptr_w_arg);
                handler_loc = va_arg(ap, funcptr_w_arg*);
                p->ret      = reg_handler(p, sig, handler, handler_loc);
                break;

            default:
                kprintf("Bad Sys request %d, pid = %d\n", r, p->pid);
        }
    }

    kprintf("Out of processes: dying\n");

    for(;;);
}

extern void dispatchinit( void ) {
    /********************************/

    //bzero( proctab, sizeof( pcb ) * MAX_PROC );
    memset(proctab, 0, sizeof( pcb ) * MAX_PROC);
}



extern void     ready( pcb *p ) {
    /*******************************/

    p->next = NULL;
    p->state = STATE_READY;

    if( tail ) {
        tail->next = p;
    } else {
        head = p;
    }

    tail = p;
}

extern pcb      *next( void ) {
    /*****************************/

    pcb *p;

    p = head;

    if( p ) {
        head = p->next;
        if( !head ) {
            tail = NULL;
        }
    } else {
        kprintf( "BAD\n" );
        for(;;);
    }

    p->next = NULL;
    p->prev = NULL;
    return( p );
}


extern pcb *findPCB( int pid ) {
    /******************************/

    int	i;

    for( i = 0; i < MAX_PROC; i++ ) {
        if( proctab[i].pid == pid ) {
            return( &proctab[i] );
        }
    }

    return( NULL );
}

/* Stop the kernel */
extern void freeze( void ) {
    kprintf("Freezing system\n");
    for (;;) {}
}
