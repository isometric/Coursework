/* kbd.c : messaging system (assignment 2)
 */

#include <kbd.h>
#include <i386.h>
#include <xeroskernel.h>

#define DATA_PORT 0x60
#define CNTL_PORT 0x64

char kb_name[] = "keyboard";
char echo_kb_name[] = "echo keyboard";

char keyboard_buff[BUFFER_SIZE];
int keyboard_buff_len = BUFFER_SIZE;
int keyboard_buff_pos;

int active_device;

int new_char_in_buff;

char keyboard_eof;
int last_char_eof;

int kb_open(void){
    return keyboard_open(KB);
}

int echo_kb_open(void) {
    return keyboard_open(ECHO_KB);
}

int keyboard_open(int device){
    if (active_device == NONE) {
        enable_irq(KEYBOARD_IRQ, 0);
        active_device = device;
        keyboard_eof = '\0';
        return 1;
    }
    return -1;
}

int kb_close(void){
    return keyboard_close(KB);
}

int echo_kb_close(void) {
    return keyboard_close(ECHO_KB);
}

int keyboard_close(int device) {
    if (active_device != NONE) {
        active_device = NONE;
        return 1;
    }
    return -1;
}

int keyboard_read(void* buff, int bufflen) {
    return keyboard_flush();
}

int keyboard_write(void* buff, int bufflen){
    return -1;
}

int keyboard_ioctl(int fd, unsigned long command, va_list ap) {
    if (command == 53) {
        keyboard_eof = (char)va_arg(ap, int);
        return 0;
    }
    return -1;
}

void kb_int(void) {
    new_char_in_buff = 0;
}

void echo_kb_int(void) {
    if (new_char_in_buff)
        kprintf("%c", keyboard_buff[keyboard_buff_pos - 1]);
    new_char_in_buff = 0;
}

/*  Moves data from keyboard hardware to lower level buffer. */
void keyboard_int(void) {
    if (!(inb(CNTL_PORT))) return;

    new_char_in_buff = 0;
    unsigned char byte = kbtoa(inb(DATA_PORT));

    if(byte == keyboard_eof) {
        last_char_eof = 1;
    } else if  (keyboard_buff_pos < 4) {
        keyboard_buff[keyboard_buff_pos++] = byte;
        new_char_in_buff = 1;

        if (active_device != NONE)
            di_int(active_device);
    }
    keyboard_flush();
    end_of_intr();
}

void kb_init(devsw * dev_blk) {
    keyboard_init(KB, dev_blk);
}

void echo_kb_init(devsw * dev_blk) {
    keyboard_init(ECHO_KB, dev_blk);
}

void keyboard_init(int echo, devsw * dev_blk) {
    if (echo){
        dev_blk->dvopen = echo_kb_open;
        dev_blk->dvclose = echo_kb_close;
        dev_blk->dvname = echo_kb_name;
        dev_blk->dvint = echo_kb_int;
    } else {
        dev_blk->dvopen = kb_open;
        dev_blk->dvclose = kb_close;
        dev_blk->dvname = kb_name;
        dev_blk->dvint = kb_int;
    }

    dev_blk->owner_pcb = NULL;
    dev_blk->dvread = keyboard_read;
    dev_blk->dvioctl = keyboard_ioctl;
    dev_blk->dvwrite = keyboard_write;

    keyboard_buff_pos = 0;
    keyboard_eof = '\0';
    active_device = NONE;
}

// return >0
int keyboard_flush(void) {
    if (active_device == NONE) return 0;

    int bytes_flushed, i = 0;

    if (keyboard_buff[keyboard_buff_pos] == '\n' || keyboard_eof) {
        bytes_flushed = di_flush_int(active_device, keyboard_buff, keyboard_buff_pos, 1);
    }
    else {
        bytes_flushed = di_flush_int(active_device, keyboard_buff, keyboard_buff_pos, 0);
    }

    int new_keyboard_buff_pos = keyboard_buff_pos - bytes_flushed;

    if (bytes_flushed < keyboard_buff_pos) {
        for (keyboard_buff_pos = bytes_flushed;
            keyboard_buff_pos < keyboard_buff_len && i < bytes_flushed;
            keyboard_buff_len++, i++) {
            keyboard_buff[i] = keyboard_buff[keyboard_buff_pos];
        }
    }

    keyboard_buff_pos = new_keyboard_buff_pos;
    keyboard_eof = 0;
    return bytes_flushed;
}
