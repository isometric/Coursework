
/* disp.c : dispatcher
 */

#include <xeroskernel.h>

// Global Variables
#define PCB_SIZE 0x10
#define PCB_INDEX_MASK_SIZE 0x8
#define PCB_INDEX_MASK 0xFF
#define RECV_ANY_NUM 0
PCB pcbs[PCB_SIZE];
PCB *ready_head = NULL;
PCB *blocked_head = NULL;
PCB *stopped_head = NULL;
PCB *sleep_head = NULL;
PCB *idle_process = NULL;
static int pid_counter = 0;

/* ======================= Declarations ======================== */

/* TODO REMOVE
 * Prints out PIDS of elements in list
 */
void print_list(PCB *head);

/* Sets the return value for a process upon returning from a syscall
 *
 * pcb  the process whose eax value will be modified
 * val  the value to set eax */
void setret(PCB *pcb, int val);

/* TODO REMOVE THIS
 * Provides interface for another module to check the state of disp.c */
int check(void);

/* Initialize the Process Control Block Table.
 * Assign the first entries to the head of the ready queue.
 * Assign the remaining entries to the blocked table. */
void pcbinit(void);

/* Print str on the monitor. */
void puts(char *str);

/* Kill process with pid */
int kill(int, int);

/* Print PIDs of PCB table */
void printpcbs(void);

/* Returns addr of PCB with pid or NULL if no PCB is found */
PCB *select_pcb(int);

/* 1 if pcb in queue, otherwise 0 */
int in_queue(PCB *pcb, PCB *head);

/* Return head of ready queue and remove it from the queue. */
PCB* next_proc(void);

/* Chooses next process to run. */
void dispatch(void);

/* Adds a process control block to the end of the ready queue. */
void ready(PCB*);

/* Remove pcb from queue. Modifies queue head pointed to by queueptr.
 * Assume that pcb is in queue. */
void remove_next(PCB*, PCB**);

/* Free senders/receivers waiting to contact pcb. */
void cleansenders(PCB *);
void cleanreceivers(PCB *);

/* Remove src from target's sender queue.
 *
 * src          the pcb to remove
 * target       element with queue to modify
 *
 * Assume that src is in target's sender queue.
 */
void remove_sender(PCB*, PCB*);

/* Remove dest from target's receiver queue.
 *
 * dest         the pcb to remove
 * target       element with queue to modify
 *
 * Assume that dest is in target's receiver queue.
 */
void remove_receiver(PCB*, PCB*);

/* Place pcb at end of queue using next field */
void enqueue_next(PCB*, PCB**);

/* Place pcb at end of sender queue */
void enqueue_sender(PCB*, PCB*);

/* Place pcb at end of receiver queue */
void enqueue_receiver(PCB*, PCB*);


/* Set PCB struct. */
// TODO add nextSender and nextReceiver fields
void set_pcb(PCB *entry,
        int pid,
        proc_state state,
        int parent_pid,
        unsigned long esp,
        unsigned long start,
        PCB *next,
        PCB *sender,
        PCB *receiver);

/* Free process control block and held resources. */
void cleanup(PCB*);

/* Runs tests on PCB methods */
void run_pcbtests(void);

/* Test enqueue_next */
void test_enqueue_next(void);

/* Counts the number of elements in the PCB list. */
int count_list(PCB *head);
int count_sender(PCB *head);
int count_receiver(PCB *head);

/* Counts PCB occurences in list */
int count_appearance(PCB*, PCB*);

/* Prints out pcbs in queue */
void print_ready();

/* Gets end of PCB list */
PCB* last_pcb(PCB *head);

/* ======================= Definitions ======================== */

/* Chooses next process to run. */
void dispatch(void) {

    int *argp, result;
    ctx_req request;
    PCB *pcb = next_proc(), *tmppcb;

    for(;;) {
        request = context_switch(pcb);


        argp = (int*)(pcb->esp + sizeof(context_frame));
        argp = (int*)argp[1]; // point to beginning of args

        KASSERT(!check());

        switch(request) {
            case CREATE:
                result = create((void*)argp[0], argp[1]);
                if (result >= 0) {
                    KASSERT( count_appearance(select_pcb(result), ready_head) == 1 );
                    KASSERT( count_appearance(select_pcb(result), blocked_head) == 0 );
                    KASSERT( count_appearance(select_pcb(result), stopped_head) == 0 );
                }
                setret(pcb, result);
                break;
            case YIELD:

                KASSERT( count_appearance(pcb, ready_head) == 0 );
                KASSERT( count_appearance(pcb, blocked_head) == 0 );
                KASSERT( count_appearance(pcb, stopped_head) == 0 );

                ready(pcb);

                KASSERT( count_appearance(pcb, ready_head) == 1 );
                KASSERT( count_appearance(pcb, blocked_head) == 0 );
                KASSERT( count_appearance(pcb, stopped_head) == 0 );

                pcb = next_proc();

                KASSERT( count_appearance(pcb, ready_head) == 0 );
                KASSERT( count_appearance(pcb, blocked_head) == 0 );
                KASSERT( count_appearance(pcb, stopped_head) == 0 );
                break;
            case STOP:
                cleanup(pcb);
                pcb = next_proc();

                KASSERT( count_appearance(pcb, ready_head) == 0 );
                KASSERT( count_appearance(pcb, blocked_head) == 0 );
                KASSERT( count_appearance(pcb, stopped_head) == 0 );
                break;
            case PUTS:
                puts((char*)argp[0]);
                break;
            case KILL:
                // TODO test kill with a process that was asleep. Verify that it is removed from it's dependee's senders/receiver's list
                result = kill((int)argp[0], pcb->PID);
                setret(pcb, result);
                break;
            case GETPID:
                setret(pcb, pcb->PID);
                break;
            case SEND:
                result = send((int)argp[0], (void*)argp[1], (int)argp[2], pcb);
                if (result == BLOCKED) { // TODO does BLOCKED need to be negative?

                    block(pcb);

                    KASSERT( count_appearance(pcb, blocked_head) == 1 );
                    KASSERT( count_appearance(pcb, ready_head) == 0 );
                    KASSERT( count_appearance(pcb, stopped_head) == 0 );

                    tmppcb = pcb;
                    pcb = next_proc();

                    KASSERT( count_appearance(pcb, ready_head) == 0 );
                    KASSERT( count_appearance(pcb, blocked_head) == 0 );
                    KASSERT( count_appearance(pcb, stopped_head) == 0 );

                    KASSERT( count_appearance(tmppcb, blocked_head) == 1 );
                    KASSERT( count_appearance(tmppcb, ready_head) == 0 );
                    KASSERT( tmppcb != pcb );
                } else if (result >= 0) { // TODO is it > or >=?
                    // Set return values for sender and receiver
                    PCB *destPCB = select_pcb((int)argp[0]);
                    setret(pcb, result);
                    setret(destPCB, result);

                    KASSERT( count_appearance(destPCB, ready_head) == 0 );
                    KASSERT( count_appearance(destPCB, blocked_head) == 1 );
                    KASSERT( count_appearance(destPCB, stopped_head) == 0 );

                    remove_next(destPCB, &blocked_head);
                    ready(destPCB);
                    ready(pcb);
                    pcb = next_proc();

                    KASSERT( count_appearance(destPCB, ready_head) == 1 );
                    KASSERT( count_appearance(destPCB, blocked_head) == 0 );
                    KASSERT( count_appearance(destPCB, stopped_head) == 0 );
                } else if (result < 0) {
                    setret(pcb, result);
                }
                break;
            case RECV:
                result = receive((int*)argp[0], (void*)argp[1], (int)argp[2], pcb);
                if (result == BLOCKED) {
                    KASSERT( count_appearance(tmppcb, ready_head) == 0 ); // TODO Failling here
                    block(pcb);

                    KASSERT( count_appearance(pcb, ready_head) == 0 );
                    KASSERT( count_appearance(pcb, blocked_head) == 1 );
                    KASSERT( count_appearance(pcb, stopped_head) == 0 );

                    tmppcb = pcb;
                    pcb = next_proc();

                    KASSERT( count_appearance(pcb, ready_head) == 0 );
                    KASSERT( count_appearance(pcb, blocked_head) == 0 );
                    KASSERT( count_appearance(pcb, stopped_head) == 0 );
                    // TODO fails on next line. Easy fix is to just remove
                    // node from ready queue after it's been blocked. That's
                    // not the best answer but an answer.
                    KASSERT( count_appearance(tmppcb, blocked_head) == 1 );
                    KASSERT( count_appearance(tmppcb, ready_head) == 0 ); // TODO Failling here
                    KASSERT( tmppcb != pcb );
                } else if (result >= 0) {
                    // Set return values for sender and receiver
                    PCB *srcPCB = select_pcb(*((int*)argp[0]));
                    setret(srcPCB, result);
                    setret(pcb, result);

                    KASSERT( count_appearance(srcPCB, ready_head) == 0 );
                    KASSERT( count_appearance(srcPCB, blocked_head) == 1 );
                    KASSERT( count_appearance(srcPCB, stopped_head) == 0 );

                    remove_next(srcPCB, &blocked_head);
                    ready(srcPCB); // Unblock sender
                    ready(pcb); // Unblock sender
                    pcb = next_proc();

                    KASSERT( count_appearance(srcPCB, ready_head) == 1 );
                    KASSERT( count_appearance(srcPCB, blocked_head) == 0 );
                    KASSERT( count_appearance(srcPCB, stopped_head) == 0 );
                } else if (result < 0) {
                    setret(pcb, result);
                }
                break;
            case TIMER_INT:

                KASSERT( count_appearance(pcb, ready_head) == 0 );
                KASSERT( count_appearance(pcb, blocked_head) == 0 );
                KASSERT( count_appearance(pcb, stopped_head) == 0 );

                ready(pcb);

                KASSERT( count_appearance(pcb, ready_head) == 1 );
                KASSERT( count_appearance(pcb, blocked_head) == 0 );
                KASSERT( count_appearance(pcb, stopped_head) == 0 );

                pcb = next_proc(); // Does this need to go after tick?

                KASSERT( count_appearance(pcb, ready_head) == 0 );
                KASSERT( count_appearance(pcb, blocked_head) == 0 );
                KASSERT( count_appearance(pcb, stopped_head) == 0 );

                tick();
                end_of_intr();
                break;
            case SLEEP:
                sleep(pcb, (unsigned int)argp[0]);
                pcb = next_proc();

                KASSERT( count_appearance(pcb, ready_head) == 0 );
                KASSERT( count_appearance(pcb, blocked_head) == 0 );
                KASSERT( count_appearance(pcb, stopped_head) == 0 );

                break;
            default:
                kprintf("ERROR %d made unrecognized request: 0x%x\n", pcb->PID, request);
                break;
        }
        tmppcb = NULL;
    }
}

/* Initialize the Process Control Block Table.
 * Assign the first entries to the head of the ready queue.
 * Assign the remaining entries to the blocked table. */
void pcbinit(void) {
    int i;

    for(i = 0; i < PCB_SIZE-1; i++)
        set_pcb(pcbs+i, 0, STOPPED, 0, NULL, NULL, pcbs+i+1, NULL, NULL);

    set_pcb(pcbs+i, 0, STOPPED, 0, NULL, NULL, NULL, NULL, NULL);

    ready_head   = NULL;
    blocked_head = NULL;
    stopped_head = pcbs;

    KASSERT( count_list(ready_head) == 0 );
    KASSERT( count_list(blocked_head) == 0 );
    KASSERT( count_list(stopped_head) == PCB_SIZE );
}

/* Free process control block and held resources. */
void cleanup(PCB *pcb) {
    // Free memory used for process
    kfree((void*)(pcb->start));

    if (pcb->state == BLOCKED && pcb->blockedBy) {
        remove_sender(pcb, pcb->blockedBy);
        remove_receiver(pcb, pcb->blockedBy);
        pcb->blockedBy = NULL;
    }

    // Free waiting senders and receivers
    cleansenders(pcb);
    cleanreceivers(pcb);

    // Add process to end of stopped Q.
    if (stopped_head)
        pcb->next = stopped_head;
    stopped_head = pcb;

    // Reset values
    set_pcb(pcb, 0, STOPPED, pcb->parent_pid, NULL, NULL, NULL, NULL, NULL);
}

/* Free senders waiting to contact pcb. */
void cleansenders(PCB *pcb) {
    PCB *head = pcb->sender;
    while (head) {
        PCB *tmp = head->nextSender;
        head->nextSender = NULL;
        // Error for process ending before it's terminated
        setret(head, -1);

        if (head->state != STOPPED) {
            ready(head);
            remove_next(head, &blocked_head);

            KASSERT( count_appearance(head, ready_head) == 1 );
            KASSERT( count_appearance(head, blocked_head) == 0 );
            KASSERT( count_appearance(head, stopped_head) == 0 );
        }

        head = tmp;
    }
}

/* Free senders waiting to contact pcb. */
void cleanreceivers(PCB *pcb) {
    PCB *head = pcb->receiver,
        *tmp = NULL;
    while (head) {
        KASSERT( head->state != ASLEEP );

        // Error for process ending before it's terminated
        setret(head, -1);

        if (head->state != STOPPED) {
            // Assume a process is blocked for only one reason.
            remove_next(head, &blocked_head);
            ready(head);

            KASSERT( count_appearance(head, ready_head) == 1 );
            KASSERT( count_appearance(head, blocked_head) == 0 );
            KASSERT( count_appearance(head, stopped_head) == 0 );

        }

        if (head->state == STOPPED) {
            cleanup(head); // TODO test
        }

        // Remove head and select next element
        tmp = head->nextReceiver;
        head->nextReceiver = NULL;
        head = tmp;
    }
}

/* Adds a process control block to the end of the ready queue. */
void ready(PCB *pcb) {
    KASSERT(!check());

    int len = count_list(ready_head);
    enqueue_next(pcb, &ready_head);
    pcb->state = READY;
}

/* Adds a process control block to the end of the blocked queue. */
void block(PCB *pcb) {
    int len = count_list(blocked_head);
    KASSERT( pcb->state == READY );
    KASSERT( pcb->state != STOPPED );
    KASSERT( count_appearance(pcb, ready_head) == 0 );
    KASSERT( count_appearance(pcb, blocked_head) == 0 );
    KASSERT( count_appearance(pcb, stopped_head) == 0 );

    enqueue_next(pcb, &blocked_head);
    pcb->state = BLOCKED;

    KASSERT( count_appearance(pcb, ready_head) == 0 );
    KASSERT( count_appearance(pcb, blocked_head) == 1 );
    KASSERT( count_appearance(pcb, stopped_head) == 0 );
    KASSERT( len+1 == count_list(blocked_head) );
}

/* Counts the number of elements in the PCB list. */
int count_list(PCB *head) {
    int count = 0;
    while (head) {
        head = head->next;
        count++;
    }
    return count;
}

int count_sender(PCB *head) {
    int count = 0;
    PCB *curr = head->sender;
    while (curr) {
        curr = curr->nextSender;
        count++;
    }
    return count;
}

int count_receiver(PCB *head) {
    int count = 0;
    PCB *curr = head->receiver;
    while (curr) {
        curr = curr->nextReceiver;
        count++;
    }
    return count;
}


/* Gets end of PCB list */
PCB* last_pcb(PCB *head) {
    while (head && head->next)
        head = head->next;
    return head;
}

/* Set PCB struct. */
void set_pcb(PCB *entry,
        int pid,
        proc_state state,
        int parent_pid,
        unsigned long esp,
        unsigned long start,
        PCB *next,
        PCB *sender,
        PCB *receiver) {
    entry->PID = pid;
    entry->state = state;
    entry->parent_pid = parent_pid;
    entry->esp = esp;
    entry->start = start;
    entry->next = next;
    entry->sender = sender;
    entry->receiver = receiver;
}

/* Return head of ready queue and remove it from the queue. */
PCB* next_proc(void) {
    if (!ready_head) {
        KASSERT(idle_process);
        return idle_process;
    } else {
        PCB *result = ready_head;
        KASSERT( result->state == READY );
        ready_head = ready_head->next;
        KASSERT( count_appearance(result, ready_head) == 0 );
        return result;
    }
}

/* Print str on the monitor. */
void puts(char *str) {
    kprintf("%s", str);
}

/* Returns addr of PCB with pid or NULL if no PCB is found */
PCB* select_pcb(int pid) {
    return &(pcbs[pid & PCB_INDEX_MASK]);
}

int index_of_pcb(PCB* pcb) {
    return pcb - pcbs;
}

/* Kill process with targetPID
 *
 * returns -1 if the process is STOPPED
 * returns -2 if the process tries to kill itself
 */
int kill(int targetPID, int currPID) {
    PCB *pcb = select_pcb(targetPID),
        **head = NULL;

    /* Check process exists */
    if (!pcb || pcb->state == STOPPED)
        return -1;

    /* Process cannot kill itself */
    if (targetPID == currPID)
        return -2;

    KASSERT( pcb->state == READY || pcb->state == BLOCKED );

    if (pcb->state == READY) {
        head = &ready_head;
    } else if (pcb->state == BLOCKED) {
        head = &blocked_head;
    } else if (pcb->state == ASLEEP) {
        head == &sleep_head;
    } else {
        kprintf("!invalid queue head!\n"); freeze(__LINE__);
    }

    KASSERT( in_queue(pcb, *head) );

    remove_next(pcb, head);
    cleanup(pcb);
    enqueue_next(pcb, &stopped_head);

    KASSERT( pcb->state == STOPPED );
    KASSERT( !in_queue(pcb, *head) );
    KASSERT( count_list(ready_head) > 0 );

    return 1;
}

/* 1 if pcb in queue, otherwise 0 */
int in_queue(PCB *pcb, PCB *head) {
    while (head) {
        if (head->PID == pcb->PID) {
            return TRUE;
        }
        head = head->next;
    }
    return FALSE;
}

/* Generates a new PID */
int next_pid(int index) {

    // ssets pid_counter to natrual 1.. 2^(32 - PCB_IDNEX_SIZE);

    int num_pids_requested = (1 + pid_counter++ % ((1 << (32 - PCB_INDEX_MASK_SIZE)) -1));
    int pid = (num_pids_requested << PCB_INDEX_MASK_SIZE) | (index & PCB_INDEX_MASK);

    return pid;
}

/* Remove pcb from queue. Modifies queue head pointed to by queueptr. */
void remove_next(PCB *pcb, PCB **queueptr) {
    /* TODO TESTS
     * Test only 1 element on list removed (ready list)
     * Test only 1 element on list removed (blocked)
     * Test 2 elements on ready list
     * Test 2+ elements on ready list
     */

    int count = count_list(*queueptr);
    if (*queueptr == pcb) {     // remove head
        *queueptr = pcb->next;
        KASSERT ( count_list(*queueptr) == count - 1 );
    } else {                    // remove after head
        PCB *curr = *queueptr;
        while (curr && curr->next != pcb)
            curr = curr->next;
        if (curr && curr->next == pcb) {
            curr->next = pcb->next;
            KASSERT ( count_list(*queueptr) == count - 1 );
        }
    }
}

/* Place pcb at end of queue using next field */
/* TODO test with empty queues */
void enqueue_next(PCB *pcb, PCB **head) {
    if (*head == NULL) {
        *head = pcb;
    } else {
        PCB *curr = *head;
        while (curr->next != NULL)
            curr = curr->next;
        curr->next = pcb;
    }
    pcb->next = NULL;
}

/* Place receiver at end of src's receiver queue
 *
 * receiver     pcb to add to receiver list
 * src          head of receiver list to add pcb to
 *
 * Assume src && receiver are a valid PCBs
 */
void enqueue_receiver(PCB *receiver, PCB *src) {

    // receiver wants to know who its blocked by
    receiver->blockedBy = src;

    if (!src->receiver) {
        // Empty list
        src->receiver = receiver;
        receiver->nextReceiver = NULL;
    } else {
        // Arbitrary length linked-list
        PCB *curr = src->receiver;
        while (curr->nextReceiver != NULL) {
            curr = curr->nextReceiver;
        }
        curr->nextReceiver = receiver;
    }

    // Receiver is end of the list
    receiver->nextReceiver = NULL;
}

/* Place sender at end of dest's sender queue
 *
 * sender   pcb to add to sender list
 * dest     head of sender list to add pcb to
 *
 * Assume sender && dest are a valid PCBs
 */
void enqueue_sender(PCB* sender, PCB *dest) {

    // sender wants to know who its blocked by
    sender->blockedBy = dest;

    if (!dest->sender) {
        // Empty list
        dest->sender = sender;
        sender->nextSender = NULL;
    } else {
        // Arbitrary length linked-list
        PCB *curr = dest->sender;
        while (curr->nextSender != NULL) {
            curr = curr->nextSender;
        }
        curr->nextSender = sender;
    }

    // Sender is end of the list
    sender->nextSender = NULL;
}

/* Remove src from target's sender queue.
 *
 * src          the pcb to remove
 * target       element with queue to modify
 */
void remove_sender(PCB *src, PCB *target) {
    /* TODO TEST */
    int count = count_sender(target);
    if (target->sender == src) {     // remove head
        target->sender = src->nextSender;
    } else {                    // remove after head
        PCB *curr = target->sender;
        while (curr && curr->nextSender && curr->nextSender != src) {
            curr = curr->nextSender;
        }

        if (curr && curr->nextSender)
            curr->nextSender = src->nextSender;
    }
}

/* Remove dest from target's receiver queue.
 *
 * dest         the pcb to remove
 * target       element with queue to modify
 */
void remove_receiver(PCB *dest, PCB *target) {
    /* TODO TEST */
    int count = count_receiver(target);
    if (target->receiver == dest) {     // remove head
        target->receiver = dest->nextReceiver;
    } else {                    // remove after head
        PCB *curr = target->receiver;
        while (curr && curr->nextReceiver && curr->nextReceiver != dest) {
            curr = curr->nextReceiver;
        }
        if (curr && curr->nextReceiver)
            curr->nextReceiver = dest->nextReceiver;
    }
}


/* Runs tests on PCB methods */
void run_pcbtests(void) {
    pcbinit();

    ready_head = &(pcbs[0]);
    ready_head->next = NULL;
    stopped_head = &(pcbs[1]);

    // Test next_proc()
    // Test emptying initial next queue
    KASSERT( count_list(ready_head) == 0 );
    KASSERT( count_list(stopped_head) == PCB_SIZE-1 );

    ready(&(pcbs[1]));
    stopped_head = &(pcbs[2]);

    KASSERT( count_list(ready_head) == 1 );
    KASSERT( count_list(stopped_head) == PCB_SIZE-2 );

    ready(&(pcbs[2]));
    ready(&(pcbs[3]));
    ready(&(pcbs[4]));
    ready(&(pcbs[5]));
    ready(&(pcbs[6]));
    ready(&(pcbs[7]));
    ready(&(pcbs[8]));
    ready(&(pcbs[9]));
    stopped_head = &(pcbs[10]);

    KASSERT( count_list(ready_head) == 9 );
    KASSERT( count_list(stopped_head) == PCB_SIZE-10 );
    PCB *ptr = next_proc();
    KASSERT( ptr != NULL );
    KASSERT( next_proc() != ptr );
    KASSERT( count_list(ready_head) == 7 );

    test_enqueue_next();
    test_enqueue_sender();

    pcbinit();
}

/* Test enqueue_next */
void test_enqueue_next(void) {
    pcbinit();

    // insert into empty list
    KASSERT( blocked_head == NULL );
    KASSERT( count_list(blocked_head) == 0 );
    enqueue_next(&(pcbs[1]), &blocked_head);
    KASSERT( count_list(blocked_head) == 1 );

    // insert into list with 1
    enqueue_next(&(pcbs[2]), &blocked_head);
    KASSERT( count_list(blocked_head) == 2 );

    // insert into list with 2
    enqueue_next(&(pcbs[3]), &blocked_head);
    KASSERT( count_list(blocked_head) == 3 );

    pcbinit();
}

/* Test enqueue_sender */
void test_enqueue_sender(void) {
    pcbinit();

    KASSERT( count_sender(pcbs[0].sender) == 0 );

    // insert into empty list
    enqueue_sender(&(pcbs[1]), &(pcbs[0]));
    KASSERT( count_sender(pcbs[0].sender) == 1 );

    // insert into list with 1
    enqueue_sender(&(pcbs[2]), &(pcbs[0]));
    KASSERT( count_sender(pcbs[0].sender) == 2 );

    // insert into list with 2
    enqueue_sender(&(pcbs[3]), &(pcbs[0]));
    KASSERT( count_sender(pcbs[0].sender) == 3 );

    pcbinit();
}

/* Print PIDs of PCB table */
void printpcbs(void) {
    int i;
    kprintf("(PCBs:");
    for (i = 0; i < PCB_SIZE; i++) {
        kprintf("%d,", pcbs[i].PID);
    }
    kprintf(")");

}

/* Decrements time remaining on sleeping processes. Moves any that have
 * slept long enough to the ready queue. */
void tick(void) {
    PCB *curr = sleep_head;
    while(curr) {
        // move completed procs to ready queue
        if (curr->sleepTicks <= 0) {
            PCB *tmp = curr;
            curr = curr->next;
            sleep_head = curr;
            ready(tmp);

            KASSERT( count_appearance(tmp, ready_head) == 1 );
            KASSERT( count_appearance(tmp, blocked_head) == 0 );
            KASSERT( count_appearance(tmp, stopped_head) == 0 );
            KASSERT( count_appearance(tmp, sleep_head) == 0);

            KASSERT( count_appearance(curr, ready_head) == 0);
            KASSERT( count_appearance(curr, blocked_head) == 0);
            KASSERT( count_appearance(curr, stopped_head) == 0);
            KASSERT( count_appearance(curr, sleep_head) == 1 || !sleep_head);

            // return value for syssleep
            setret(tmp, 0);
        } else {
            curr->sleepTicks--;
            curr = curr->next;
        }
        // decrement tick counter
    }
}

/* Put a process to sleep for a number of milliseconds
 * pcb              the process that requested to sleep
 * milliseconds     the number of milliseconds requested
 * */
void sleep(PCB *pcb, unsigned int milliseconds) {
    pcb->sleepTicks = (milliseconds / TICKS_PER_MS) + 1;
    enqueue_sleep(pcb);
    pcb->state == ASLEEP;
}

/* Puts a PCB on the sleep queue. 3 cases.
 * Empty sleep queue                            => pcb is new head
 * pcb has fewer/equal ticks than first element => pcb is new head
 * pcb has more ticks than the first element    => insert in middle */
void enqueue_sleep(PCB *pcb) {
    PCB *curr = sleep_head;
    if (!sleep_head) {
        sleep_head = pcb;
        pcb->next = NULL;
    } else if (curr->sleepTicks >= pcb->sleepTicks) {
        sleep_head = pcb;
        pcb->next = curr;
    } else {
        while (curr->next && curr->next->sleepTicks <= pcb->sleepTicks)
            curr = curr->next;
        PCB *tmpNext = curr->next;
        curr->next = pcb;
        pcb->next = tmpNext;
    }
}

/* Idle Process */
void idle_proc(void) {
    // while (1) {
    //     kprintf("idle");
    //     asm volatile("hlt\n\t":::);
    // }
   asm volatile("loop:\n\t" "hlt\n\t" "jmp loop\n\t" :::);
}

// Assumes only only root on ready Q
void init_idle_proc(void) {
    KASSERT(count_list(ready_head) == 1);      // only root process should be ready
    KASSERT(count_list(blocked_head) == 0);      // root is ready to run, no other proc
    KASSERT(count_list(stopped_head) == 15);      // only root process should be ready
    // additional stack size should be 0 but shomehow fails under around 40

    idle_process = select_pcb(create(idle_proc, 500));
    ready_head->next = NULL;

    KASSERT(count_list (ready_head) == 1);      // only root process should be ready
    KASSERT(count_list(blocked_head) == 0);      // idle proc is never on blocked queue
    KASSERT(count_list(stopped_head) == 14);      // 1 less than before becauseo f idle_proc
}

/* Prints out pcbs in queue */
void print_ready() {
    PCB *pcb = ready_head;
    kprintf("(ready:");
    while (pcb) {
        kprintf("%x:", pcb->PID);
        pcb = pcb->next;
    }
    kprintf(")");
}

/* Counts PCB occurences in list */
int count_appearance(PCB *pcb, PCB *head) {
    int count = 0;
    while (head) {
        if (head->PID == pcb->PID)
            count++;
        head = head->next;
    }
    return count;
}

/* TODO REMOVE THIS
 * Provides interface for another module to check the state of disp.c */
int check(void) {
    return (ready_head == stopped_head && ready_head != NULL);
}

/* TODO REMOVE
 * Prints out PIDS of elements in list
 */
void print_list(PCB *head) {
    kprintf("(");
    while (head) {
        kprintf("%d,", head->sleepTicks);
        head = head->next;
    }
    kprintf(")");
}

/* Sets the return value for a process upon returning from a syscall
 *
 * pcb  the process whose eax value will be modified
 * val  the value to set eax */
void setret(PCB *pcb, int val) {
    ((context_frame*)(pcb->esp))->eax = val;
}
