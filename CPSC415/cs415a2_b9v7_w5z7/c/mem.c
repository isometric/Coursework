/* mem.c : memory manager
 */

#include <xeroskernel.h>
#include <i386.h>

extern long freemem;
extern char *maxaddr;

static const unsigned int BYTE_ALIGN = 16;
static const char FREED_MEM = 0;
static const char *IS_FREE = (const char*)0xDEADBEEF;

/*
 * ================================= FUNCTION DECLARATIONS ===================================
 */

/* Run at kernel startup. This subroutine creates two large free memory nodes. */
void kmeminit(void);

/* Allocates memory with length of size or returns 0 if not enough memory. */
void *kmalloc(unsigned int size);

/* Free the block started at ptr.
 * If ptr doesn't point to a valid piece of allocated memory then do nothing.
 * ptr points to the beginning of an allocated block of memory. */
void kfree(void *ptr);

/* validates sanity value in header. Halts kernel on invalid memory. */
void checksanity(memHeader* header);

/* sets values in memory header. */
void set_memheader(void *header, unsigned long size, memHeader *prev, memHeader *next, const char *sanity);

/* displays value of ptr-1, ptr, and ptr+1 */
void memorydump(void* ptr);

/* Returns 1 if h1 is immediately left of h2. Else, 0. */
int memcontig(memHeader* h1, memHeader* h2);

/* Coalesces h1 with h2 only if h1 is immediately left of h2. */
void coalesce_header(memHeader *h1, memHeader *h2);

/* Insert h1 next to h2. Otherwise, do nothing. */
void insert(memHeader *h1, memHeader *h2);

/* Counts the number of nodes in the linked list with head of start. */
unsigned long count_freenodes(memHeader* start);

/* Prints the addresses of the nodes in the linked list. */
void print_freenodes(void);

/* Print out values in a memHeader */
void print_node(memHeader* node) {
    kprintf("addr: 0x%x size: %d, prev: 0x%x, next: 0x%x, sanity: 0x%x\n",
            node, node->size, node->prev, node->next, node->sanityCheck);
}

/* Prints string. Used for debugging logging macro. */
void __print_plz(char *str);

/* Says what kind failed and halts the kernel. */
void __assert_fail(int line);

/* Methods to run tests */
void run_kmemtests(void);
void run_memtests(void);
void run_memtests_large(void);

/*
 * ================================= DEFINITIONS ===================================
 */

/* Returns 1 if h1 is immediately left of h2. Else, 0. */
int memcontig(memHeader* h1, memHeader* h2) {
    return ((unsigned long)(h1->dataStart) + h1->size) == (unsigned long)h2;
}

/* Allocates memory with length of size or returns 0 if not enough memory. */
void *kmalloc(unsigned int size) {

    /* Calculates the number of bytes aligned to a 2-byte aligned
     * address rounded up. 16 bytes are included for the header. */
    unsigned int allocSize = ((size >> 4) + ((size & 0xf) ? 1 : 0)) << 4,
                 searchSize = allocSize + sizeof(memHeader);
    memHeader* currHead = (memHeader*) freemem;
    void* data = NULL;

    while (currHead != NULL) {
        if ((unsigned int)(currHead->size) >= searchSize) {
            // Create new header at end of allocd data.
            void *newFree = ((void*)(currHead->dataStart)) + allocSize;
            unsigned int newSize = currHead->size - allocSize - sizeof(memHeader);
            set_memheader(newFree, newSize, currHead->prev, currHead->next, IS_FREE);

            // Update previous and next nodes in linked list.
            if (currHead->prev) {
                currHead->prev->next = (memHeader*)newFree;
            } else {
                freemem = ((long) newFree); // newHeader becomes head of linked list.
                ((memHeader*) freemem)->prev = NULL;
            }

            if (currHead->next)
                currHead->next->prev = newFree;

            data = currHead->dataStart;

            // Cleanup alloc'd header
            set_memheader(currHead, allocSize, NULL, NULL, IS_FREE);
            break;
        } else {
            checksanity(currHead);
            currHead = currHead->next;
        }
    }

    if ((int) data & 0xf) {
        memorydump(currHead);
        kprintf("%d pointer of allocated header\n", currHead);
        kprintf("%d not aligned \n", data);
        freeze(__LINE__);
    }
    return (void*)data;
}

/* displays value of ptr-1, ptr, and ptr+1 */
void memorydump(void* ptr) {
    kprintf("displaying memory at 0x%08x\n", (int*)ptr);

    int * i;
    int prevBytes = 4, postBytes = 4*7;
    for (i = ((int *) ptr)-prevBytes ; i < ((int*) ptr)+postBytes; i++) {
        !((int) i % 16) ? kprintf("\n%08x :", i) : 0 ;
        i == (int *) ptr ? kprintf("       >%08x", *i) : kprintf("  \t%08x", *i);
    }
    kprintf("\n");
}

/* Runs an infinite loop. this stops the kernel executing. */
void freeze(int line) {
    kprintf("(freeze %d)", line);
    for (;;);
}

/* Coalesces h1 with h2 only if h1 is immediately left of h2. */
void coalesce_header(memHeader *h1, memHeader *h2) {
    if (memcontig(h1, h2)) {
        h1->next = h2->next;
        h1->size = h1->size + h2->size + sizeof(memHeader);
        if (h1->next)
            h1->next->prev = h1;
    }
}

/* Insert h1 next to h2. Otherwise, do nothing. */
void insert(memHeader *h1, memHeader *h2) {
    KASSERT( h1 != h2 );
    if (h1 < h2) { // h1 comes before h2
        set_memheader(h1, h1->size, h2->prev, h2, IS_FREE);
        set_memheader(h2, h2->size, h1, h2->next, IS_FREE);
    } else if (h1 > h2) { // h1 comes after h2
        set_memheader(h1, h1->size, h2, h2->next, IS_FREE);
        set_memheader(h2, h2->size, h2->prev, h1, IS_FREE);
    }
}

/* Free the block started at ptr.
 * If ptr doesn't point to a valid piece of allocated memory then do nothing.
 * ptr points to the beginning of an allocated block of memory. */
void kfree(void *ptr) {
    memHeader *currNode = ptr - sizeof(memHeader),
              *prevNode = (memHeader*)freemem,
              *nextNode = NULL;

    // Check that the ptr points to just after a prevNode
    if (((int)ptr & 0xf) || // Assert ptr on 16-bit boundary
        ((int)ptr < HOLESTART && (int)ptr > HOLEEND) ||// ptr not in hole
        prevNode->sanityCheck != IS_FREE)// sanityCheck passes
    {
        kprintf("unable to free pointer looking at 0x%x\n", ptr);
        return;
    }

    // Find previous and next nodes
    while (prevNode->next && prevNode->next < currNode) {
        checksanity(prevNode);
        prevNode = prevNode->next;
    }
    nextNode = prevNode->next;

    KASSERT( prevNode );
    KASSERT( !nextNode || nextNode >= ((memHeader*)ptr) );
    KASSERT( currNode != prevNode );

    insert(currNode, prevNode);
    if (currNode > prevNode) {
        insert(currNode, nextNode);
    }

    if (currNode < prevNode) {
        coalesce_header(currNode, prevNode);
    } else {
        coalesce_header(currNode, nextNode);
        coalesce_header(prevNode, currNode);
    }

    // Update start of link-list
    unsigned long min = (unsigned long)((currNode < prevNode) ? currNode : prevNode);
    freemem = (freemem < min) ? freemem : min;
    ((memHeader*) freemem)->prev = NULL;

    // Clean up freed data space
    int i;
    for (i = 0; i < currNode->size; i++) {
        currNode->dataStart[i] = FREED_MEM;
    }
}

/* Prints string. Used for debugging logging macro. */
void __print_plz(char *str) {
    kprintf(str);
}

/* Says what kind failed and halts the kernel. */
void __assert_fail(int line) {
    kprintf("Check on line %d failed\n", line);
    freeze(__LINE__);
}

/* validates sanity value in header. Halts kernel on invalid memory. */
void checksanity(memHeader* header) {
    if (header->sanityCheck != IS_FREE) {
        kprintf("Address 0x%x failed sanity check, contained %d\n",
                header,
                header->sanityCheck);
        memorydump((void*)header);
        freeze(__LINE__);
    }
}

/* sets values in memory header. */
void set_memheader(void *header,
        unsigned long size,
        memHeader *prev,
        memHeader *next,
        const char *sanity) {
    ((memHeader*)header)->size = size;
    ((memHeader*)header)->prev = prev;
    ((memHeader*)header)->next = next;
    ((memHeader*)header)->sanityCheck = sanity;
}

/* Initializes memory management */
void kmeminit(void) {
    freemem = (freemem >> 4) + ((freemem & 0xf) ? 1 : 0);
    freemem = freemem << 4;
    memHeader *node1 = (memHeader*) freemem;
    memHeader *node2 = (memHeader*) HOLEEND;

    set_memheader(node1,
            (unsigned long)HOLESTART - (unsigned long)node1->dataStart,
            NULL,
            node2,
            IS_FREE);
    set_memheader(node2,
            (unsigned long)maxaddr - (unsigned long)HOLEEND,
            node1,
            NULL,
            IS_FREE);
}

/* Run at kernel startup.
 * Assume nothing has been allocated yet. */
void run_kmemtests(void) {
    const long freemem_start = freemem;
    memHeader *node1 = NULL;
    memHeader *node2 = NULL;

    run_memtests();

    freemem = freemem_start;
    node1 = (memHeader*) freemem_start;
    node2 = (memHeader*) HOLEEND;
    set_memheader(node1,
            (unsigned long)HOLESTART - (unsigned long)node1->dataStart,
            NULL,
            node2,
            IS_FREE);
    set_memheader(node2,
            (unsigned long)maxaddr - (unsigned long)HOLEEND,
            node1,
            NULL,
            IS_FREE);
    KASSERT( count_freenodes((memHeader*)freemem_start) == 2 );

    run_memtests_large();

    freemem = freemem_start;
    node1 = (memHeader*) freemem;
    node2 = (memHeader*) HOLEEND;
    set_memheader(node1,
            (unsigned long)HOLESTART - (unsigned long)node1->dataStart,
            NULL,
            node2,
            IS_FREE);
    set_memheader(node2,
            (unsigned long)maxaddr - (unsigned long)HOLEEND,
            node1,
            NULL,
            IS_FREE);
    KASSERT( count_freenodes((memHeader*)freemem) == 2 );

    run_memtests_large();

    freemem = freemem_start;
    node1 = (memHeader*) freemem;
    node2 = (memHeader*) HOLEEND;
    set_memheader(node1,
            (unsigned long)HOLESTART - (unsigned long)node1->dataStart,
            NULL,
            node2,
            IS_FREE);
    set_memheader(node2,
            (unsigned long)maxaddr - (unsigned long)HOLEEND,
            node1,
            NULL,
            IS_FREE);
    KASSERT( count_freenodes((memHeader*)freemem) == 2 );

    test_2();

}

/* Run large test on kmalloc and kfree */
void run_memtests_large(void) {
  int arrSize = 1024;
  void * pointer[arrSize];
  int size[arrSize];
  int i, j, k, allocated = 0, freed = 0;

  int available = HOLESTART - (int) freemem + (int) maxaddr - HOLEEND;
  kprintf("theoretical sizeof freemem %d bytes\n", available);

  size[0] = 0x260000; // leaves 0xA000 or 40k free at end of second block which les less than 4k
  pointer[0] = kmalloc(size[0]);
  if (pointer[0]) allocated += size[0];
  if (kmalloc(size[0])) {
    kprintf("ERROR: allocated more size than available");
    for(;;);
  }


  for(i = 1; i < arrSize; i++) {
    size[i] = (i * 7919) % 3727;
    pointer[i] = kmalloc(size[i]);
    allocated += size[i];
    if (!pointer[i]) {
      allocated -= size[i];
      size[i] = 0;
      break;
    }
  }

  kprintf("allocated %d pointers, %d bytes in use \n", i, allocated);

  // free every 13th, 17th and 23'rd allocated pointer
  for(j = 0; j < arrSize; j++) {
    if (!pointer[j]) break;
    if (!(j%13) || !(j%17) || !(j%23)) {
      freed ++;
      kfree(pointer[j]);
      allocated -= size[j];
    }
  }

  kprintf("freed %d pointers, %d bytes in use \n", freed, allocated);

  // i is the first free index after the first pass
  for(k = i; k < arrSize; k++) {
    size[k] = (k * 6343) % 7919;
    allocated += size[k];
    pointer[k] = kmalloc(size[k]);
    if (!pointer[k]) {
      allocated -= size[k];
      size[k] = 0;
      break;
    }
  }

  kprintf("allocated %d pointers, %d bytes in use \n", k-i, allocated);

  // i-1 is index of last pointer successfull allocated in first loop so near end of memory
  // i is initial k value which should always succeed so near initial freemem
  if (pointer[i-1] < pointer[i]) {
    kprintf("ERR: error1");
  }

  // pointer[2] is not factor of free loop, should be near init freemem
  // pointer[j-1] should be near memory end
  if (pointer[2] > pointer[j-1]) {
    kprintf("ERR: error2\n");
  }
}

/* Run tests */
void run_memtests(void) {
    kprintf("Testing kmalloc & kfree\n");
    kprintf("freemem start: 0x%x\n", freemem);
    const long freemem_start = freemem;
    void *ptr = NULL, *ptr2 = NULL;
    kprintf("number of nodes: %d\n", count_freenodes((memHeader*) freemem));
    print_freenodes();

    // Test A1 == 0
    // Expect freemem == freemem_start
    KASSERT( freemem_start == freemem );

    kprintf("Test: %d\n", __LINE__);
    ptr = kmalloc(0);

    KASSERT( freemem_start + sizeof(memHeader) == ((unsigned long)ptr) );
    KASSERT( count_freenodes((memHeader*)freemem) == 2 );

    kfree(ptr);

    KASSERT( freemem_start == freemem );
    KASSERT( count_freenodes((memHeader*)freemem) == 2 );

    // Test A1 < 16
    // Expect freemem == freemem_start + 32
    KASSERT( freemem_start == freemem );

    kprintf("Test: %d\n", __LINE__);
    ptr = kmalloc(8);

    KASSERT( freemem_start + BYTE_ALIGN + sizeof(memHeader) == freemem );
    KASSERT( count_freenodes((memHeader*)freemem) == 2);

    kfree(ptr);

    KASSERT( freemem_start == freemem );
    KASSERT( count_freenodes((memHeader*)freemem) == 2 );

    // Test A1 == 16
    // Expect freemem == freemem_start + 32
    KASSERT( freemem_start == freemem );

    kprintf("Test: %d\n", __LINE__);
    ptr = kmalloc(BYTE_ALIGN);

    KASSERT( freemem_start + BYTE_ALIGN + sizeof(memHeader) == freemem );
    KASSERT( count_freenodes((memHeader*)freemem) == 2);

    kfree(ptr);

    KASSERT( freemem_start == freemem );
    KASSERT( count_freenodes((memHeader*)freemem) == 2 );

    // Test A1 > 16
    // Expect freemem == freemem_start + 48
    KASSERT( freemem_start == freemem );

    kprintf("Test: %d\n", __LINE__);
    ptr = kmalloc(BYTE_ALIGN + 1);

    KASSERT( freemem_start + 2*BYTE_ALIGN + sizeof(memHeader) == freemem );
    KASSERT( count_freenodes((memHeader*)freemem) == 2);

    kfree(ptr);

    KASSERT( freemem_start == freemem );
    KASSERT( count_freenodes((memHeader*)freemem) == 2 );

    // Test A1(HOLE_START - freemem)
    // Expect freemem => HOLEEND because header + size is greater than first region
    KASSERT( freemem_start == freemem );

    kprintf("Test: %d\n", __LINE__);
    ptr = kmalloc(HOLESTART - freemem);
    KASSERT( ( (memHeader*) freemem)->next != ((memHeader*)HOLEEND) );

    KASSERT( (int)ptr > HOLEEND );
    KASSERT( freemem_start == freemem );
    KASSERT( count_freenodes((memHeader*)freemem) == 2);

    kfree(ptr);

    KASSERT( freemem_start == freemem );
    KASSERT( count_freenodes((memHeader*)freemem) == 2 );

    // Test A1(small), A2(small)
    // Expect freemem == freemem_start + 2*sizeof(memHeader) + 2*small
    KASSERT( freemem_start == freemem );
    kprintf("Test: %d\n", __LINE__);

    ptr = kmalloc(BYTE_ALIGN - 1); // <-- Issue is here

    KASSERT( ptr != NULL );
    KASSERT( ((memHeader*)((void*)freemem_start))->prev == NULL );

    ptr2 = kmalloc(BYTE_ALIGN - 1);

    KASSERT( (void*)freemem == ptr2 + BYTE_ALIGN );
    KASSERT( ptr + BYTE_ALIGN == ptr2 - sizeof(memHeader) );
    KASSERT( count_freenodes((memHeader*)freemem) == 2);
    KASSERT( freemem == freemem_start + 2*sizeof(memHeader) + 2*BYTE_ALIGN );

    kfree(ptr);

    KASSERT( freemem == freemem_start );
    //memorydump((void*) freemem_start);
    KASSERT( count_freenodes((memHeader*) freemem) == 3 );

    kfree(ptr2);

    KASSERT( freemem_start == freemem );
    KASSERT( count_freenodes((memHeader*) freemem) == 2 );

    // Test A1(HOLE_START - freemem), A2(small)
    // Expect freemem => HOLE_END + small + sizeof(memHeader)
    KASSERT( freemem_start == freemem );
    KASSERT( count_freenodes((memHeader*)freemem) == 2);

    kprintf("Test: %d\n", __LINE__);
    ptr = kmalloc(HOLESTART - freemem);
    ptr2 = kmalloc(BYTE_ALIGN);

    KASSERT( (int)ptr > HOLEEND );
    KASSERT( (int)ptr2 < HOLESTART && ptr2 < (void*)freemem );
    KASSERT( count_freenodes((memHeader*)freemem) == 2);

    kfree(ptr);
    kfree(ptr2);

    KASSERT( freemem_start == freemem );
    KASSERT( count_freenodes((memHeader*)freemem) == 2 );

    // Test A1(small), A2(small), free A1
    // Expect num of nodes == 3
    KASSERT( freemem_start == freemem );

    kprintf("Test: %d\n", __LINE__);
    ptr = kmalloc(BYTE_ALIGN - 1);
    ptr2 = kmalloc(BYTE_ALIGN - 1);
    KASSERT( ptr + BYTE_ALIGN == ptr2 - sizeof(memHeader) );
    KASSERT( freemem_start < freemem );

    kfree(ptr);

    KASSERT( freemem_start == freemem );
    KASSERT( count_freenodes((memHeader*)freemem) == 3 );

    kfree(ptr2);

    // Test A1(small), A2(small), free A2
    // Expect num of nodes == 2
    KASSERT( freemem_start == freemem );

    kprintf("Test: %d\n", __LINE__);
    ptr = kmalloc(BYTE_ALIGN - 1);
    ptr2 = kmalloc(BYTE_ALIGN - 1);

    kfree(ptr2);

    KASSERT( freemem_start + sizeof(memHeader) + BYTE_ALIGN == freemem );
    KASSERT( count_freenodes((memHeader*)freemem) == 2 );

    kfree(ptr);
    KASSERT( count_freenodes((memHeader*)freemem) == 2 );

    // Test A1, free A1, A1: expect freemem == freemem_start + sizeof(memHeader) + 16
    KASSERT( freemem_start == freemem );

    ptr = kmalloc(BYTE_ALIGN - 1);
    kfree(ptr);
    ptr = kmalloc(BYTE_ALIGN - 1);

    KASSERT( freemem_start + sizeof(memHeader) + BYTE_ALIGN == freemem );
    KASSERT( count_freenodes((memHeader*)freemem) == 2 );
    kfree(ptr);

    // Test A1(16 bytes), A2(16 bytes), free A1, A3(16 bytes): expect free to be at A1
    ptr = kmalloc(BYTE_ALIGN - 1);
    *((int*)ptr) = 0xAA;
    ptr2 = kmalloc(BYTE_ALIGN - 1);
    *((int*)ptr2) = 0xBB;
    kfree(ptr);
    ptr = kmalloc(BYTE_ALIGN - 1);
    *((int*)ptr) = 0xCC;
    KASSERT( freemem_start == freemem );
    KASSERT( count_freenodes((memHeader*)freemem) == 3 );

}

/* Counts the number of nodes in the linked list with head of start. */
unsigned long count_freenodes(memHeader* start) {
    // Check that start is valid head node
    KASSERT( start->sanityCheck == IS_FREE );
    KASSERT( start != NULL );
    KASSERT( !(start->prev) && start->next); // check it's a start node

    unsigned long count = 0;
    memHeader* current = start;
    while (current != NULL) {
        checksanity(start);
        current = current->next;
        count++;
    }

    return count;
}

/* Prints the addresses of the nodes in the linked list. */
void print_freenodes(void) {
    memHeader* current = (memHeader*)freemem;

    /* Check freemem is a valid header node */
    KASSERT( current->sanityCheck == IS_FREE );
    KASSERT( current != NULL );
    KASSERT( current->prev == NULL );

    kprintf("nodelist:\n");
    while (current != NULL) {
        print_node(current);
        checksanity(current);
        current = current->next;
    }
    kprintf("\n");

}

/* Test corresponding with second test in Testing.txt */
void test_2(void) {
    int i;
    void *ptrs[100];
    for (i = 0; i < 100; i++) {
        ptrs[i] = kmalloc(32);
        KASSERT( ptrs[i] != NULL );
    }

    kprintf("nodes in free-list: %d\n", count_freenodes((memHeader*)freemem));
    KASSERT( count_freenodes((memHeader*)freemem) == 2 );

    // Free every other allocated block of memory
    for (i = 0; i < 100; i += 2) {
        kfree(ptrs[i]);
    }

    kprintf("nodes in free-list: %d\n", count_freenodes((memHeader*)freemem));
    KASSERT( count_freenodes((memHeader*)freemem) == 52 );

    // Free remaining allocated blocks of memory
    for(i = 1; i < 100; i += 2) {
        kfree(ptrs[i]);
    }

    kprintf("nodes in free-list: %d\n", count_freenodes((memHeader*)freemem));
    KASSERT( count_freenodes((memHeader*)freemem) == 2 );
}

