/* user.c : User processes
 */
#include <xeroskernel.h>

int testPIDs[10];

/* ======================= Tests ========================= */
/* ======================= Test 1 ========================
 * A receiver waits for a msg from a sender. The message
 * is eventually sent. The receiver should unblock and
 * print the message. */

void recv1(void) {
    char buf[20];
    int result;

    result = sysrecv(&testPIDs[1], buf, 20);
    KASSERT( result == 15 );
    sysputs(buf);

    result = sysrecv(&testPIDs[1], buf, 20);
    KASSERT( result == 10 );
    sysputs(buf);

    sysputs("test1 passed\n");
}

void send1(void) {
    char buf[15];
    int result;

    sprintf(buf, "string1\n");
    result = syssend(testPIDs[0], buf, 15);
    KASSERT( result == 15 );

    sprintf(buf, "string2\n");
    result = syssend(testPIDs[0], buf, 10);
    KASSERT( result == 10 );
}

void test1(void) {
    testPIDs[0] = syscreate(recv1, 4000);
    testPIDs[1] = syscreate(send1, 4000);
}

/* ======================= Test 2 ========================
 * A sender waits for a msg from a sender. The message
 * is eventually sent. The sender should unblock and
 * print the message. */

void recv2(void) {
    char buf[20];
    int result = sysrecv(&testPIDs[1], buf, 20);
    KASSERT( result == 15 );
}

void send2(void) {
    char buf[15];
    sprintf(buf, "test");
    int result = syssend(testPIDs[0], buf, 15);
    KASSERT( result == 15 );
    sysputs("test2 passed\n");
}

void test2(void) {
    testPIDs[1] = syscreate(send2, 4000);
    testPIDs[0] = syscreate(recv2, 4000);
}

/* ======================= Test 3 ========================
 * A receiver is waiting for a sender. The sender is
 * cleaned up. The receiver should unblock and return -1. */

void recv3(void) {
    char buf[20];
    int result = sysrecv(&testPIDs[1], buf, 20);
    KASSERT( result == -1 );
    sysputs("test3 passed\n");
}

void send3(void) {
    int i;
    for (i = 0; i < 3000; i++)
        sysyield();
}

void test3(void) {
    testPIDs[0] = syscreate(recv3, 4000);
    testPIDs[1] = syscreate(send3, 4000);
}

/* ======================= Test 4 ========================
 * A sender is waiting for a receiver. The receiver is
 * cleaned up. The sender should unblock and return -1. */

void recv4(void) {
    int i;
    for (i = 0; i < 3; i++)
        sysyield();
}

void send4(void) {
    char buf[20];
    int result = syssend(testPIDs[1], buf, 20);
    KASSERT( result == -1 );
    sysputs("test4 passed\n");
}

void test4(void) {
    testPIDs[0] = syscreate(send4, 4000);
    testPIDs[1] = syscreate(recv4, 4000);
}

/* ======================= Test 5 ========================
 * A receiver tries to receive from itself. The syscall
 * should exit immediately with -2. */

void recv5(void) {
    char buf[20];
    int result = sysrecv(&testPIDs[0], buf, 20);
    KASSERT( result == -2 );
    sysputs("test5 passed\n");
}

void test5(void) {
    testPIDs[0] = syscreate(recv5, 4000);
}

/* ======================= Test 6 ========================
 * A sender tries to send to itself. The syscall
 * should exit immediately with -2. */

void send6(void) {
    char buf[20];
    int result = syssend(testPIDs[0], buf, 20);
    KASSERT( result == -2 );
    sysputs("test6 passed\n");
}

void test6(void) {
    testPIDs[0] = syscreate(send6, 4000);
}

/* ======================= Test 7 ========================
 * Processes: recv7A, recv7B, recv7C
 * "<1> -<op>-> <2>" means process <1> calls <op>
 * against process <2>
 *
 * recv7A -RECV-> recv7B -RECV-> recv7C -KILL-> recv7B
 *
 * Kill recv7B. recv7A should get a -1 */

void recv7A(void) {
    char buf[20];
    int result = sysrecv(&testPIDs[1], buf, 20);
    KASSERT( result == -1 );
    syskill(testPIDs[2]);
    sysputs("test7 passed\n");
}

void recv7B(void) {
    char buf[20];
    int result = sysrecv(&testPIDs[2], buf, 20);
}

void recv7C(void) {
    int i = 0;
    for (; i < 10; i++)
        sysyield();
    int result = syskill(testPIDs[1]);
    KASSERT( result == 1 );
}

void test7(void) {
    testPIDs[0] = syscreate(recv7A, 4000);
    testPIDs[1] = syscreate(recv7B, 4000);
    testPIDs[2] = syscreate(recv7C, 4000);
}

/* ======================= Test 8 ========================
 * Processes: A, B, C
 * "<1> -<op>-> <2>" means process <1> calls <op>
 * against process <2>
 *
 * A -SEND-> B -SEND-> C
 *
 * Kill B. A should get a -1 */

/* First process run by kernel */

void recv8A(void) {
    char buf[30];
    sysputs("recv8A sending to recv8B\n");
    int result = syssend(testPIDs[1], buf, 30);
    sprintf(buf, "recv8A send reuslt: %d\n", result);
    sysputs(buf);
    KASSERT( result == -1 );
    sysputs("test 8 passed\n");
}

void recv8B(void) {
    char buf[20];
    sysputs("recv8B sending to recv8C\n");
    int result = syssend(testPIDs[2], buf, 20);
}

void recv8C(void) {
    syssleep(5* 1000);
    sysputs("recv8C killing recv8B\n");
    syskill(testPIDs[1]);
}

void test8(void) {
    testPIDs[0] = syscreate(recv8A, 4000);
    testPIDs[1] = syscreate(recv8B, 4000);
    testPIDs[2] = syscreate(recv8C, 4000);
}

/* ======================= Test 9 ========================
 * Create two processes, each has an infinite loop. Should
 * see printing interleaved.
 */

void fn9a(void) {
    for(;;)
        sysputs("( )");
}

void fn9b(void) {
    for(;;)
        sysputs("_");
}

void test9(void) {
    syscreate(fn9a, 4000);
    syscreate(fn9b, 4000);
}

/* ======================= Test 10 =======================
 * create a process. print "hello". sleep for 10 seconds.
 * print "world". Expect a delay between printing the .'s.
 */

void test10(void) {
    sysputs("Hello");
    int i = 0;
    for(; i < 10; i++) {
        syssleep(3*1000);
        sysputs(".");
    }
    sysputs("world\n");
    syssleep(3*1000);
    sysputs("test 10 passed\n");
}

/* ================ Consumer/Producer ====================
 * Setup for extended consumer/producer problem as
 * described in section 3.8.
 */

void fn(void) {
    unsigned int sleepTimeCopy,
                 pid = 0, // receive from any process
                 sleepTime;
    int myPID = sysgetpid();
    char buf[100];

    sprintf(buf, "Process %x is alive\n", myPID);
    sysputs(buf);

    int result = sysrecv(&pid, (void*)&sleepTime, sizeof(unsigned int));

    sprintf(buf, "Process %x received msg to sleep for %dms\n", myPID, sleepTime);
    sysputs(buf);

    syssleep(sleepTime);

    sprintf(buf, "Process %x has stopped sleeping and will exit\n", myPID);
    sysputs(buf);
}

void extended_consumer_producer(void) {
    char buf[75];
    unsigned int sleepTime = 0;
    int result = 0xCAFE;
    int i = 0;

    sysputs("Extended Consumer/Producer problem\n");
    sprintf(buf, "Process %x, root, is alive\n", sysgetpid());
    sysputs(buf);
    for (; i < 4; i++) {
        testPIDs[i] = syscreate(fn, 4000);
        sprintf(buf, "Process %x, root, created process with PID %x\n", sysgetpid(), testPIDs[i]);
        sysputs(buf);
    }

    //sleepTime = 4*1000;
    sleepTime = 4*1000;
    syssleep(sleepTime);

    sleepTime = 10 * 1000;
    result = syssend(testPIDs[2], (void*)&sleepTime, sizeof(unsigned int));
    sleepTime = 7 * 1000;
    result = syssend(testPIDs[1], (void*)&sleepTime, sizeof(unsigned int));
    sleepTime = 20 * 1000;
    result = syssend(testPIDs[0], (void*)&sleepTime, sizeof(unsigned int));
    sleepTime = 27 * 1000;
    result = syssend(testPIDs[3], (void*)&sleepTime, sizeof(unsigned int));

    result = sysrecv(&testPIDs[3], (void*)&sleepTime, sizeof(unsigned int));
    sprintf(buf, "Process %x received msg from to 4th, pid %x, proc result: %d\n",
            sysgetpid(), testPIDs[3], result);
    sysputs(buf);
    KASSERT(result == -1);

    result = syssend(testPIDs[2], (void*)&sleepTime, sizeof(unsigned int));
    sprintf(buf, "Process %x received msg from to 3rd proc, pid %x, result: %d\n",
            sysgetpid(), testPIDs[2], result);
    sysputs(buf);
    KASSERT(result == -1);
    sysstop();
}

/* ======================= Test 11 =======================
    tests idle process
 */

void test11(void) {
    int i;
    for(i = 0; i < 100; i++) {
        sysputs("----");
        sysyield();
    }
    sysstop();  // kill root
}

/* ======================= Test 12 =======================
 * If a process is blocked, waiting for a send, and it is killed then
 * the processes waiting around that process should still be queued
 * up to receive the process.
 */

void a12(void) {
    char buf[30];
    sprintf(buf, "a12 msg");

    int result = 0;
    sysputs("a12 is alive\n");

    syssleep(5 * 1000);
    syskill(testPIDs[2]);
    syssleep(5 * 1000);

    result = syssend(testPIDs[1], (void*)buf, 30);
    KASSERT (result >= 0);

    result = syssend(testPIDs[2], (void*)buf, 30);
    KASSERT (result == -1);

    result = syssend(testPIDs[3], (void*)buf, 30);
    KASSERT (result >= 0);
}

void b12(void) {
    char buf[30];
    char out[30];
    int result = 0;
    sysputs("b12 is alive\n");
    result = sysrecv(&testPIDs[0], (void*)buf, 30);
    sprintf(out, "b12 result: %s\n", buf);
    sysputs(out);
    KASSERT( result > 0 );
}

void c12(void) {
    char buf[30];
    int result = 0;
    sysputs("c12 is alive\n");
    result = sysrecv(&testPIDs[0], (void*)buf, 30);
    sysputs("Shouldn't get here\n");
    KASSERT( FALSE );
}

void d12(void) {
    char buf[30];
    char out[30];
    int result = 0;
    sysputs("d12 is alive\n");
    result = sysrecv(&testPIDs[0], (void*)buf, 30);
    sprintf(out, "d12 result: %s\n", buf);
    sysputs(out);
    KASSERT( result > 0 );
    sysputs("test 12 passed\n");
}

void test12(void) {
    testPIDs[0] = syscreate(a12, 4000);
    testPIDs[1] = syscreate(b12, 4000);
    testPIDs[2] = syscreate(c12, 4000);
    testPIDs[3] = syscreate(d12, 4000);
}

/* ======================= Test 13 =======================
 * If a process is blocked, waiting for a receive, and it is killed then
 * the processes waiting around that process should still be queued
 * up to receive the process.
 */

void a13(void) {
    char buf[100];
    char out[100];

    int result = 0;
    sysputs("a13 is alive\n");

    syssleep(10 * 1000);
    sysputs("kill c13\n");
    syskill(testPIDs[2]);

    sysputs("a13 trying to receive\n");
    result = sysrecv(&testPIDs[1], (void*)buf, 100);
    sprintf(out, "a13 recv %s from %x\n", buf, testPIDs[1]);
    sysputs(out);
    KASSERT (result >= 0);

    result = sysrecv(&testPIDs[2], (void*)buf, 100);
    KASSERT (result == -1);

    result = sysrecv(&testPIDs[3], (void*)buf, 100);
    sprintf(out, "a13 recv %s from %x\n", buf, testPIDs[3]);
    sysputs(out);
    KASSERT (result >= 0);

    sysputs("test 13 passed\n");
}

void b13(void) {
    char buf[100];
    char out[100];
    int result = 0;
    sysputs("b13 is alive\n");
    sprintf(buf, "BBB");

    sprintf(out, "b13:%x sending '%s'\n", testPIDs[1], buf);
    sysputs(out);

    result = syssend(testPIDs[0], (void*)buf, 100);
    KASSERT( result > 0 );
}

void c13(void) {
    char buf[100];
    char out[100];
    int result;
    sysputs("c13 is alive\n");
    sprintf(buf, "CCC");

    sprintf(out, "c13:%x sending '%s'\n", testPIDs[2], buf);
    sysputs(out);

    result = syssend(testPIDs[0], (void*)buf, 100);
    sysputs("Shouldn't get here\n");
    KASSERT( FALSE );
}

void d13(void) {
    char buf[100];
    char out[100];
    int result = 0;
    sysputs("d13 is alive\n");
    sprintf(buf, "DDD");

    sprintf(out, "d13:%x sending '%s'\n", testPIDs[3], buf);
    sysputs(out);

    result = syssend(testPIDs[0], (void*)buf, 100);
    KASSERT( result > 0 );
}

void test13(void) {
    testPIDs[0] = syscreate(a13, 4000);
    testPIDs[1] = syscreate(b13, 4000);
    testPIDs[2] = syscreate(c13, 4000);
    testPIDs[3] = syscreate(d13, 4000);
}

/* ======================= Test 14 =======================
 * If time sharing is working, multiple print statements should be seen
 * printing workers never yeild
 */

void worker14(void) {
    char buff[30];
    sprintf(buff, "%X", sysgetpid() & 0xFF);
    while(1) sysputs(buff);
}




void test14(void) {
    int i;
    for (i = 0; i <10; i++) syscreate(worker14, 1000);
}


extern void root(void) {
    int choice = 8;
    switch (choice) {
        case 1:
            test1();
            break;
        case 2:
            test2();
            break;
        case 3:
            test3();
            break;
        case 4:
            test4();
            break;
        case 5:
            test5();
            break;
        case 6:
            test6();
            break;
        case 7:
            test7();
            break;
        case 8:
            test8();
            break;
        case 9:
            test9();
            break;
        case 10:
            test10();
            break;
        case 11:
            test11();
            break;
        case 12:
            test12();
            break;
        case 13:
            test13();
            break;
        case 14:
            test14();
            break;
        default:
            sysputs("No test selected\n");
            break;
    };

    syssleep(10 * 1000);
    //The following is the "root" for section 3.8 on the assignment
    extended_consumer_producer();

    for (;;){
        sysyield();
    }

    sysputs("root shouldn't finish");
    freeze(__LINE__);
}
