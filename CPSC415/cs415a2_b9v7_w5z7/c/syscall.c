/* syscall.c : syscalls
 */

#include <xeroskernel.h>

/* Make a system call */
int syscall(int call, ...) {
    int result;
    /* pointer to arguments following int call */
    int *argptr = (int*)(((void*)&call) + sizeof(int));
    asm volatile(
        "push   %1\n\t"
        "push   %2\n\t"
        "int    $49\n\t"
        "movl   %%eax, %0\n\t"
        "pop    %%eax\n\t"
        "pop    %%eax\n\t"
        :"=r"(result)
        :"r"(argptr), "r"(call)
        : "%eax"
    );
    return result;
}

/* Create a process
 *
 * returns the pid of the process
 * returns 0 is there was an issue creating the process
 * */
unsigned int syscreate( void (*func)(void), int stack ) {
    int result = syscall(CREATE, func, stack);
    return result;
}

/* Current process yields using the CPU */
void sysyield( void ) {
    syscall(YIELD);
}

/* Stop current process and clean it from memory */
void sysstop( void ) {
    syscall(STOP);
}

/* Prints out a string */
void sysputs( char *str ) {
    syscall(PUTS, str);
}

/* Kill process with pid */
int syskill( int pid ) {
    return syscall(KILL, pid);
}

/* Process gets it's PID */
int sysgetpid( void ) {
    return syscall(GETPID);
}

/*  Send a message to dest_pid.
 *
    returns the number of bytes read by the receiving process
    returns -1 if the destination doesn't exist
    returns -1 if the destination terminates before receiving the message
    returns -2 if the process tries to send a message to itself
    returns -3 on any other sort of problem */
int syssend( int dest_pid, void *buffer, int buffer_len ) {
    return syscall(SEND, dest_pid, buffer, buffer_len);
}

/*  Receive a message from from_pid
    if from_pid is 0 this process listens for messages from other processes.

    returns the number of bytes received
    returns -1  if from_pid is invalid PID
    returns -1  if the sender terminates before receiving the message
    returns -2  if process tries to receive from itself
    returns -2  for any other problem (e.g.  size is 
                negative, buㄦ address is invalid, etc) */
int sysrecv( unsigned int *from_pid, void *buffer, int buffer_len ) {
    return syscall(RECV, from_pid, buffer, buffer_len);
}

/* Process sleeps for a number of milliseconds
 *
 * returns  0 if the process slept it's full amount of time
 * returns  greater than 0 if the process returned before sleeping the 
 *          requested amount. */
unsigned int syssleep( unsigned int milliseconds ) {
    return syscall(SLEEP, milliseconds);
}
