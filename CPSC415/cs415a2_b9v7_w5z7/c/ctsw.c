/* ctsw.c : context switcher
 */

#include <xeroskernel.h>

/* Your code goes here - You will need to write some assembly code. You must
   use the gnu conventions for specifying the instructions. (i.e this is the
   format used in class and on the slides.) You are not allowed to change the
   compiler/assembler options or issue directives to permit usage of Intel's
   assembly language conventions.
*/

void _ISREntryPoint();
void _InterruptEntryPoint();
void _CommonJump();
static void * k_stack;
static unsigned long *ESP;
static int rc, interrupt;

/* Switch between processes and the kernel */
extern ctx_req context_switch( PCB *p ) {
    ctx_req result;
    ESP = p->esp;

    asm volatile(
        "pushf\n\t"
        "pusha\n\t"
        "movl   %%esp,  k_stack\n\t"
        "movl   ESP,    %%esp\n\t"
        "popa\n\t"
        "iret\n\t"
    "_InterruptEntryPoint:\n\t"
        "cli\n\t"
        "pusha\n\t"
        "movl   $1,     %%ecx\n\t"
        "jmp    _CommonJump\n\t"
    "_ISREntryPoint:\n\t"
        "cli\n\t"
        "pusha\n\t"
        "movl   $0,     %%ecx\n\t"
    "_CommonJump:\n\t"
        "movl   %%esp,  ESP\n\t"
        "movl   k_stack, %%esp\n\t"
        "movl   %%eax,  28(%%esp)\n\t"    // kernel's eax
        "movl   %%ecx,  24(%%esp)\n\t"    // kernel's ecx
        "popa\n\t"
        "popf\n\t"
        "movl   %%eax,  rc\n\t"
        "movl   %%ecx,  interrupt\n\t"
      :
      :
      : "%eax" , "%ecx"
    );
    p->esp = ESP;

    /* Get the request type from the process's stack.
     * The value is below the saved registers and flags. */

    if (interrupt) {
        return TIMER_INT;
    } else {
        int *reqaddr = p->esp + sizeof(context_frame);
        result = *((ctx_req*)reqaddr);
        return result;
    }
}


/* Setup for the context switcher */
void context_init(void) {
    set_evec(49, _ISREntryPoint);
    set_evec(32, _InterruptEntryPoint);
    initPIT(SLICE);
}
