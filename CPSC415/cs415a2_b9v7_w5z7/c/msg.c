/* msg.c : messaging system (assignment 2)
 */

#include <xeroskernel.h>

/* ======================= Declarations ======================== */

/* Send msg to process. The actual message sent is the min of the
 * sender's requested length and the receiver's requested length.
 *
 * dest     the pid of the receiving process
 * buffer   the beginning of the stored msg
 * len      the nuber of bytes to be copied in buffer
 * pcb      the pcb of the calling process
 *
 * returns the number of bytes sent
 * returns TODO ERRORS*/
int send(int dest, void *buffer, int len, PCB *pcb);

/* Receive msg from process. The actual message received in the min
 * of the sender's requested length and the receiever's requested
 * length.
 *
 * src      a ptr to where the PID is written
 * buffer   an array to write into
 * len      the max number of bytes read
 * pcb      the process receiving a msg
 *
 * Note: when src is 0 receive will listen for a message from any
 * sender. After reading the message the memory pointed to by src
 * will have the PID of the sender who's message was received.
 */
int receive(int *src, void* buffer, int len, PCB *pcb);

/* Checks arguments to send
 * Returns TODO ERRORS */
int checksend(int dest, void *buffer, int len, PCB *pcb);

/* Checks if there is a valid sender waiting to send a msg
 *
 * returns 0 if there isn't
 * returns 1 if src == 0 and one sender OR pcb with src is waiting
 */
int waiting_send(int src, PCB *pcb);

/* Checks if there is a valid receiver waiting to recv a msg
 *
 * returns 1 if there is a proc on the receiver queue
 * returns 0 otherwise
 */
int waiting_recv(int dest, PCB *pcb);

/* Checks that buffer range is inside memory owned by pcb
 *
 * returns 1 if memory range covered is owned by pcb
 * returns 0 otherwise
 */
int validptr(void *buffer, int len, PCB *pcb);

/* Move msg from src to dest
 *
 * returns the number of bytes written
 */
int writemsg(void *srcBuf, void *destBuf, int srcLen, int destLen);

/* Returns the minimum of a and b */
int min(int a, int b);

/* ======================= Definitions ======================== */

/* Send msg to process. The actual message sent is the min of the
 * sender's requested length and the receiver's requested length.
 *
 * dest     the pid of the receiving process
 * buffer   the beginning of the stored msg
 * len      the nuber of bytes to be copied in buffer
 * pcb      the pcb of the calling process
 *
 * returns the number of bytes sent
 * returns TODO ERRORS*/
int send(int dest, void *buffer, int len, PCB *pcb) {
    int argsOk = checksend(dest, buffer, len, pcb),
        result = 0;
    if (argsOk != 0) {
        return argsOk;
    }

    PCB *destPCB = select_pcb(dest);
    KASSERT( destPCB->PID == dest );
    if (waiting_recv(dest, pcb)) {
        // Get Receiver's PCB
        PCB *destPCB = select_pcb(dest);
        remove_receiver(destPCB, pcb);
        
        // Clean blocked recv queue's
        destPCB->blockedBy = NULL;
        destPCB->nextReceiver = NULL;

        // Get receiver's arguments
        int *argp = (int*)(destPCB->esp + sizeof(context_frame));
        argp = (int*)argp[1];

        // Set dest's pid argument to current sender's PID
        *((int*)argp[0]) = pcb->PID;

        result = writemsg(buffer, argp[1], len, argp[2]);
    } else {
        enqueue_sender(pcb, destPCB);
        result = BLOCKED;
    }
    return result;
}

/* Receive msg from process. The actual message received in the min
 * of the sender's requested length and the receiever's requested
 * length.
 *
 * src      a ptr to where the PID is written
 * buffer   an array to write into
 * len      the max number of bytes read
 * pcb      the process receiving a msg
 *
 * Note: when src is 0 receive will listen for a message from any
 * sender. After reading the message the memory pointed to by src
 * will have the PID of the sender who's message was received.
 */
int receive(int *src, void* buffer, int len, PCB *pcb) {
    int argsOk = checkrecv(*src, buffer, len, pcb),
        result = 0;
    if (argsOk != 0)
        return argsOk;

    if (waiting_send(*src, pcb)) {
        // TODO test with multiple elements waiting
        // TODO test with receiveAny and 1 element
        // TODO test with receiveAny and 1+ elements

        // Get sender's PCB
        PCB *srcPCB = (*src == 0) ? pcb->sender : select_pcb(*src);
        remove_sender(srcPCB, pcb);
        srcPCB->blockedBy = NULL; // Clean blocked src's backpointer
        *src = srcPCB->PID; // Tell receiver the sender's PID

        // Get sender's arguments
        int *argp = (int*)(srcPCB->esp + sizeof(context_frame));
        argp = (int*)argp[1]; // point to beginning of args
        result = writemsg(argp[1], buffer, argp[2], len);
    } else if (*src == 0) {
        result = BLOCKED;
        pcb->blockedBy = NULL;
    } else {
        // TODO test with 1
        // TODO test with multiple
        enqueue_receiver(pcb, select_pcb(*src));
        result = BLOCKED;
    }
    return result;
}

/* Checks arguments to send
 *
 * returns -1 if the dest pcb doesn't exist
 * returns -2 if the process tries to send a msg to itself
 * returns -3 for any other problems
 */
int checksend(int dest, void *buffer, int len, PCB *pcb) {
    // TODO test
    PCB *destPCB = select_pcb(dest);
    if (dest == NULL) // Invalid PID
        return -1;
    if (destPCB->state == STOPPED)
        return -1;
    if (dest == pcb->PID)
        return -2;
    if (!validptr(buffer, len, pcb))
        return -3;
    return 0;
}

/* Checks arguments for recv
 *
 * returns 0 if the arguments are ok
 * returns -1 if src PID doesn't exist
 * returns -2 if process tries to receive from self
 * returns -2 otherwise
 */
int checkrecv(int src, void *buffer, int len, PCB *pcb) {
    // TODO test
    if (src == NULL) {
        if (!validptr(buffer, len, pcb))
            return -2;
    } else {
        if (select_pcb(src)->state == STOPPED)
            return -1;
        if (src == pcb->PID ||
                !validptr(buffer, len, pcb))
            return -2;
    }
    return 0;
}

/* Checks if there is a valid receiver waiting to recv a msg
 *
 * dest     pid of the process waiting to receive from pcb
 * pcb      the process sending a message
 *
 * returns 1 if dest is waiting for this pcb or any pcb
 * returns 0 otherwise
 */
int waiting_recv(int dest, PCB *pcb) {

    PCB *destPCB = select_pcb(dest);
    if (destPCB->state == BLOCKED && destPCB->blockedBy == NULL)
        return TRUE;

    if (pcb->receiver) {
        PCB *curr = pcb->receiver;
        while (curr) {
            if (curr->PID == dest)
                return TRUE;
            curr = curr->nextReceiver;
        }
    }
    return FALSE;
}

/* Checks if there is a valid sender waiting to send a msg
 * TODO check for anycase
 *
 * src      pid of proess waiting to send to pcb
 * pcb      the process receiving the msg
 *
 * returns 1 if src == 0 and one sender OR pcb with src is waiting
 * returns 0 if there isn't
 */
int waiting_send(int src, PCB *pcb) {
    if (pcb->sender) {
        PCB *curr = pcb->sender;
        while (curr) {
            if (curr->PID == src || src == 0)
                return TRUE;
            curr = curr->nextSender;
        }
    }
    return FALSE;
}

/* Checks that buffer range is inside memory owned by pcb
 *
 * returns 1 if memory range covered is owned by pcb
 * returns 0 otherwise
 */
int validptr(void *buffer, int len, PCB *pcb) {
    return (((unsigned long)buffer) >= pcb->start &&
        ((unsigned long)(buffer + len)) < pcb->end);
}

/* Move msg from src to dest. Assume that both src and dest made
 * send and recv calls.
 *
 * returns the number of bytes written
 */
int writemsg(void *srcBuf, void *destBuf, int srcLen, int destLen){
    int i = 0,
        minLen = min(srcLen, destLen);
    char *dest = (char*)destBuf, *src = (char*)srcBuf;
    for (; i < minLen; i++)
        dest[i] = src[i];
    return minLen;
}

/* Get the minimum of a and b */
int min(int a, int b) {
    return (a < b) ? a : b;
}
