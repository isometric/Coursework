/* create.c : create a process
 */

#include <xeroskernel.h>
#include <i386.h>

extern PCB * pcbs;
extern PCB * stopped_head;
extern long freemem;

/* Create a new process */
int create(void (*func) (void), int stack) {
    KASSERT( !check() );

    /* using size of context_frame for buffer */
    unsigned long stack_size = stack + 2 * sizeof(context_frame);
    unsigned long stack_addr = (unsigned long) kmalloc(stack_size);

    if (!stack_addr) {
        kprintf("stack malloc fail\n");
        return 0;
    }

    if (!stopped_head) {
        kprintf("no free pcb\n");
        kfree((void*)stack_addr);
        return 0;
    }

    if ((int) func < HOLESTART && (int) func > HOLEEND) {
        kprintf("func points to hole hole\n");
        kfree((void*)stack_addr);
        return 0;
    }

    memHeader* currHead = (memHeader*) freemem;
    while (currHead != NULL) {

        if ((currHead->dataStart < (char*)func) && ((char*)func < (currHead->dataStart + currHead->size))) {
            kprintf("func points to freemem\n");
            return 0;
        }
        checksanity(currHead);
        currHead = currHead->next;
    }


    PCB *pcb = stopped_head;
    stopped_head =  stopped_head->next;

    // Setup buffer and return address at bottom of the stack
    context_frame * buffer  = ((context_frame *) (stack_addr + stack)) - 1;
    memset(buffer, SANITY_VAL, sizeof(context_frame));
    *((int*)buffer) = (int)sysstop; // call sysstop when proc completes.

    // Setup context frame on stack
    context_frame * cf      = ((context_frame *) (stack_addr + stack)) - 2;
    memset(cf, 0, sizeof(context_frame));
    cf->iret_eip = (unsigned int) func;
    cf->iret_cs  = (unsigned int) getCS();
    cf->eflags = INTERRUPT_ENABLE_FLAG;

    // Try not to reuse PIDS for awhile

    pcb->PID = next_pid(index_of_pcb(pcb));
    pcb->esp = (unsigned long)cf;
    pcb->start = stack_addr;
    pcb->end = (unsigned long)buffer;
    pcb->blockedBy = NULL;

    ready(pcb);

    KASSERT( !check() );
    return pcb->PID;
}

