#version 330

uniform sampler2D TexSampler0;

in vec2 vTexCoord;
out vec4 FragColor;

void main() {
    vec4 texColor0 = texture(TexSampler0, vTexCoord);
    FragColor = texColor0;
}
