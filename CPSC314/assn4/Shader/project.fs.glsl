#version 330

uniform samplerCube TexSampler1;
uniform sampler2D TexSampler0;
uniform vec3 gem_pos;
in vec2 vTexCoord;
in vec3 vColor;
in vec3 vNormal;
in vec4 vPosition;


in vec4 gemPos;
in vec4 camPos;

out vec4 FragColor;


vec3 reflect(vec3 w, vec3 n) {
    return - w + n * (dot(w, n)*2.0);
}
void main(void) {

    vec3 reflected = reflect(normalize(vec3(-vPosition)), vNormal);
    vec4 texColor0 = (  texture(TexSampler1, reflected) +
                        texture(TexSampler1, reflected * vec3(1, 1, 0.5)) +
                        texture(TexSampler1, reflected * vec3(1, 0.5, 1)) +
                        texture(TexSampler1, reflected * vec3(0.5, 1, 1)) +

                        texture(TexSampler1, reflected * vec3(0.95, 1, 0.5)) +
                        texture(TexSampler1, reflected * vec3(1, 0.5, 0.95)) +
                        texture(TexSampler1, reflected * vec3(0.5, 0.95, 1)) +

                        texture(TexSampler1, reflected * vec3(0.95, 1, 0.95)) +
                        texture(TexSampler1, reflected * vec3(1, 0.95, 0.95)) +
                        texture(TexSampler1, reflected * vec3(0.95, 0.95, 1)) ) /10;


    vec4 texColor1 = (texture(TexSampler0, vTexCoord + 0.2 * vec2(texColor0)) +
                        texture(TexSampler0, vTexCoord + 0.21 * vec2(texColor0)) +
                        texture(TexSampler0, vTexCoord + 0.22 * vec2(texColor0)) +
                        texture(TexSampler0, vTexCoord + 0.23 * vec2(texColor0)) )/4 ;

    // vec3 colour  = vec3(texColor0 + texColor1);
    float mixfactor = pow(sin(length(gem_pos)),2);
    vec3 colour = (vec3(texColor0) * mixfactor) + (vec3(texColor1) * (1 - mixfactor));
    // colour = vec3(texColor0);


    FragColor = vec4(colour, 1.0);
}
