#version 330

layout (location = 0) in vec4 Position;
layout (location = 2) in vec2 aTexCoord;

uniform mat3 NormalMatrix;
uniform mat4 MVP;
uniform mat4 ModelViewMatrix;

uniform vec3 gem_pos;
uniform vec3 cameraPosition;

out vec2 vTexCoord;
out vec3 vColor;
// out vec3 vNormal;
out vec4 vPosition;

out vec4 camPos;
out vec4 gemPos;

void main() {
    vTexCoord   = aTexCoord;
    vColor      = vec3(1);
    // vNormal     = normaize(NormalMatrix * Normal);
    vPosition   = ModelViewMatrix * Position;

    camPos      = ModelViewMatrix * vec4(gem_pos, 1.0);
    gemPos      = ModelViewMatrix * vec4(cameraPosition, 1.0);

    gl_Position = MVP * Position;
}
