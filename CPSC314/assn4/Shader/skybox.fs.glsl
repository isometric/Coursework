#version 330

uniform samplerCube TexSampler1;

in vec2 vTexCoord;
in vec3 vColor;
in vec3 vNormal;
in vec4 vPosition;

in vec4 gemPos;
in vec4 camPos;

out vec4 FragColor;


vec3 reflect(vec3 w, vec3 n) {
    return - w + n * (dot(w, n)*2.0);
}
void main(void) {

    vec3 reflected = reflect(normalize(vec3(vPosition)), vNormal);
    vec4 texColor0 = texture(TexSampler1, reflected);

    FragColor = vec4(texColor0.r, texColor0.g, texColor0.b, 1.0);
}
