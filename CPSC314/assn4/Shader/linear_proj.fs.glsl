#version 330

uniform sampler2D TexSampler2;

in vec2 vTexCoord;
in vec3 vColor;
in vec3 vNormal;
in vec4 vPosition;

in vec4 gemPos;
in vec4 camPos;

out vec4 FragColor;



void main() {
    vec4 texColor0 = texture(TexSampler2, vTexCoord);
    FragColor = texColor0;
}