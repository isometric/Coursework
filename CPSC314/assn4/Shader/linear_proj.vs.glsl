#version 330

layout (location = 0) in vec4 aPosition;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoord;

uniform mat3 NormalMatrix;
uniform mat4 MVP;
uniform mat4 ModelViewMatrix;
uniform mat4 ProjectionMatrix;

uniform vec3 gem_pos;
uniform vec3 cameraPosition;

uniform mat4 UBC_MV;
uniform mat4 UBC_Proj;

out vec2 vTexCoord;
out vec3 vColor;
out vec3 vNormal;
out vec4 vPosition;

out vec4 camPos;
out vec4 gemPos;


void main() {


    vec4 temp   = ProjectionMatrix * UBC_MV * aPosition;
    vTexCoord   = 1.5 * vec2(temp.x/temp.w, temp.y/temp.w) + vec2(0.5, 0.7);

    vColor      = vec3(1);
    vNormal     = normalize(NormalMatrix * aNormal);
    //vNormal     = vec3(0,1,0);
    vPosition   = ModelViewMatrix * aPosition;


    // vPosition = aPosition;
    // vNormal = aNormal;

    camPos      = ModelViewMatrix * vec4(cameraPosition, 1.0);
    gemPos      = ModelViewMatrix * vec4(gem_pos, 1.0);

    gl_Position = MVP * aPosition;
}
