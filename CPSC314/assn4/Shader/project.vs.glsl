#version 330

layout (location = 0) in vec4 aPosition;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoord;

uniform mat3 NormalMatrix;
uniform mat4 MVP;
uniform mat4 ModelViewMatrix;
uniform sampler2D TexSampler0;
uniform samplerCube TexSampler1;

uniform vec3 gem_pos;
uniform vec3 cameraPosition;

out vec2 vTexCoord;
out vec3 vColor;
out vec3 vNormal;
out vec4 vPosition;

out vec4 camPos;
out vec4 gemPos;



vec3 reflect(vec3 w, vec3 n) {
    return - w + n * (dot(w, n)*2.0);
}

void main() {

    vec3 reflected = reflect(normalize(vec3(-vPosition)), vNormal);
    vTexCoord   = aTexCoord;
    vColor      = vec3(1);
    vNormal     = 0.8 * aNormal + 0.2 * normalize(vec3(texture(TexSampler0, vTexCoord)));
    vNormal     = normalize(NormalMatrix * vNormal);
//    vNormal     = vec3(texture(TexSampler0, vTexCoord));

    //vTexCoord   = 0.8 * aTexCoord + 0.2 * vec2(vNormal);
    vPosition   = ModelViewMatrix * aPosition;


    // vPosition = aPosition;
    // vNormal = aNormal;

    camPos      = ModelViewMatrix * vec4(gem_pos, 1.0);
    gemPos      = ModelViewMatrix * vec4(cameraPosition, 1.0);

    gl_Position = MVP * (aPosition + vec4(aNormal,1) * sin(gem_pos.z) * (texture(TexSampler0, vTexCoord)).x * 0.03);
}
