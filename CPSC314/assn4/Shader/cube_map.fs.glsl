#version 330

uniform samplerCube TexSampler1;

in vec2 vTexCoord;
in vec3 vColor;
in vec3 vNormal;
in vec4 vPosition;

in vec4 gemPos;
in vec4 camPos;

out vec4 FragColor;


vec3 reflect(vec3 w, vec3 n) {
    return - w + n * (dot(w, n)*2.0);
}
void main(void) {

    vec3 reflected = reflect(normalize(vec3(-vPosition)), vNormal);
    vec4 texColor0 = texture(TexSampler1, reflected);
    // vec4 texColor0 = (  texture(TexSampler1, reflected) +
    //                 texture(TexSampler1, reflected * vec3(1, 1, 0.95)) +
    //                 texture(TexSampler1, reflected * vec3(1, 0.95, 1)) +
    //                 texture(TexSampler1, reflected * vec3(0.95, 1, 1)) +

    //                 texture(TexSampler1, reflected * vec3(1.5, 1.05, 0.5)) +
    //                 texture(TexSampler1, reflected * vec3(1.05, 0.5, 1.5)) +
    //                 texture(TexSampler1, reflected * vec3(0.5, 1.5, 1.05)) +

    //                 texture(TexSampler1, reflected * vec3(0.9, 1.5, 0.5)) +
    //                 texture(TexSampler1, reflected * vec3(1.5, 0.5, 0.9)) +
    //                 texture(TexSampler1, reflected * vec3(0.5, 0.9, 1.5)) )/10;

    FragColor = vec4(texColor0.r, texColor0.g, texColor0.b, 1.0);
}
