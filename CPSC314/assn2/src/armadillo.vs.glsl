#version 330

layout (location = 0) in vec4 Position;
layout (location = 1) in vec3 Normal;

out vec3 LightIntensity;

struct LightInfo
{
    vec4 Position; // we are going to treat this as a direction to acheive directional lighting
    vec3 La;       // ambient light
    vec3 Ld;       // diffuse light
    vec3 Ls;       // specular light
};
uniform LightInfo Light;

struct MaterialInfo
{
    vec3 Ka;
    vec3 Kd;
    vec3 Ks;
    float Shininess;
};
uniform MaterialInfo Material;

uniform mat4 ModelViewMatrix;
uniform mat3 NormalMatrix;      // we keep a MV matrix without the translation component to apply to vectors
uniform mat4 ProjectionMatrix;
uniform mat4 MVP;               // ModelViewProjection Matrix

uniform vec3 gem_pos;     // the location of the gem in 3-space

uniform mat4 TRS;

uniform vec3 colours;

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// YOUR CODE HERE
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

void main()
{
    // determine vertex color
    vec3 tnorm     = normalize( NormalMatrix * Normal );
    vec3 s         = normalize( vec3(Light.Position) ); // incident vector

    LightIntensity = dot(s, tnorm) * Material.Ka * 0.4 + Material.Ka * 0.6;
    LightIntensity = dot(colours, tnorm) * colours;

    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    // YOUR CODE HERE
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    float timer = 0.5 * (1 + sin( length(Position) + length(vec2(colours.x, colours.y))));

    vec4 Max =  normalize(Position - vec4(gem_pos,1)) * length(Position) * 1.2 + vec4(gem_pos,1);

    mat4 scale = mat4(  1,0,0,0,
                        0,1,0,0,
                        0,1,1,0,
                        0,0,0,0.7);


    vec4 NPos = timer * Max + (1-timer) * scale * Max;//Max;
    //vec4 NPos = Position;


    if (distance(NPos, vec4(-0.10859, 1.1, 0.15, 1)) < 0.9) {
        float i = distance(NPos, vec4(-0.10859, 1.2, 0.5, 1));
        if ( i < 0.9) {
            gl_Position = MVP * TRS * NPos; // REPLACE ME
        } else {
            float f = 0.2 + 3.0 * (i - 1.0);
            gl_Position = MVP * ( f * NPos + (1-f) * TRS * NPos);
            //gl_NPos = MVP * NPos; // REPLACE ME
            if (f > 0.5) LightIntensity = i * vec3(0);

        }
    } else {
        gl_Position = MVP * NPos; // REPLACE ME
    }
}
