#version 330

// Copyright (c) Russell Gillette
// December 2013

layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 Normal;
out vec3 LightIntensity;

struct LightInfo
{
    vec4 Position; // we are going to treat this as a direction to acheive directional lighting
    vec3 La;       // ambient light
    vec3 Ld;       // diffuse light
    vec3 Ls;       // specular light
};
uniform LightInfo Light;

struct MaterialInfo
{
    vec3 Ka;
    vec3 Kd;
    vec3 Ks;
    float Shininess;
};
uniform MaterialInfo Material;

uniform mat3 NormalMatrix;      // we keep a MV matrix without the translation component to apply to vectors
uniform mat4 MVP;               // ModelViewProjection Matrix

uniform vec3 eye_pos;

uniform vec3 gem_pos;

uniform mat4 TRS;

void main()
{
    // determine vertex color
    vec3 tnorm     = normalize( NormalMatrix * Normal );
    vec3 s         = vec3(Light.Position); // incident vector
    LightIntensity = dot(s, tnorm) * Material.Ka * 0.1 + Material.Ka * 0.9;

/*
    mat4 translate = mat4(  1.0, 0.0, 0.0, 0.0,
                            0.0, 1.0, 0.0, 0.0,
                            0.0, 0.0, 1.0, 0.0,
                            eye_pos.x, eye_pos.y, eye_pos.z, 1.0);

    mat4 scale     = mat4(  1.0, 0.0, 0.0, 0.0,
                            0.0, 1.0, 0.0, 0.0,
                            0.0, 0.0, 1.0, 0.0,
                            0.0, 0.0, 0.0, 20.0);

    mat4 rotate    = LookAt(eye_pos, gem_pos, vec3(0,1,0));
 */
    if (length(Position) < 0.99) {
        LightIntensity = vec3(0,0,0);
    }
    gl_Position = MVP * TRS * vec4(vec3(Position.x,Position.z,-Position.y), 1.0);

}