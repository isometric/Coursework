#version 330

in vec3 vColor;
in vec3 vNormal;
in vec4 vPosition;
in vec4 lPos;


struct LightInfo
{
    vec4 LPosition; // we are going to treat this as a direction to achieve directional lighting
    vec3 La;       // ambient light
    vec3 Ld;       // diffuse light
    vec3 Ls;       // specular light
};
uniform LightInfo Light;

struct MaterialInfo
{
    vec3 Ka;            // Ambient reflectivity
    vec3 Kd;            // Diffuse reflectivity
    vec3 Ks;            // Specular reflectivity
    float Shininess;    // Specular shininess factor
};

uniform MaterialInfo Material;
uniform vec3 gem_pos;

layout (location = 0) out vec4 FragColor;

void main() {
    vec3 normal = normalize(vNormal);

    vec3 toLight = normalize(vec3(lPos) - vec3(vPosition));

    vec3 toV= -normalize(vec3(vPosition));

    vec3 h = normalize(toV + toLight);


    // human vision is logarithmic and
    float power = log(pow(length(vec3(lPos) - vec3(vPosition)), 2) + pow(length(vPosition),2));


    float specular  = pow(max(0.0, dot(h, normal)), 128);
    float diffuse   = max(0.0, dot(normal, toLight));

    //diffuse = pow(diffuse, 4);
    diffuse =(pow(1024, diffuse) - 1)/1024;


/*
         if (diffuse < 0.01) diffuse = 0.0;
    else if (diffuse < 0.02) diffuse = 0.01;
    else if (diffuse < 0.05) diffuse = 0.02;
    else if (diffuse < 0.10) diffuse = 0.05;

    else if (diffuse < 0.20) diffuse = 0.1;
    else if (diffuse < 0.40) diffuse = 0.3;
    else if (diffuse < 0.60) diffuse = 0.5;
    else if (diffuse < 0.85) diffuse = 0.7;
    else if (diffuse < 0.09) diffuse = 0.9;
    else                     diffuse = 1.0;
 */


         if (diffuse < 0.1) diffuse = 0.1;
    else if (diffuse < 0.2) diffuse = 0.2;
    else if (diffuse < 0.4) diffuse = 0.4;
    else if (diffuse < 0.8) diffuse = 0.8;
    else                    diffuse = 1.0;

    if      (specular < 0.7) specular = 0.0;
    else if (specular < 0.9) specular = 0.9;
    else                     specular = 1.0;
    vec3 intensity = normalize(gem_pos) * (0.3 * diffuse + 0.7 * specular);
    intensity += 0.1 * normalize(gem_pos);

    //vec3 intensity = normalize(gem_pos) * (vec3(0.05) + vColor * diffuse + vec3(0.6) * specular);


    intensity *= 4;
    intensity /= power;

    //intensity = vColor * (Light.La + Light.Ld * diffuse + Light.Ls * specular);
    //intensity = normalize(Light.La);

    FragColor = vec4(intensity, 1);

    if (dot(normal, toV) < 0.2) FragColor = 0.5 * vec4(0.2,0.2,0.05,1) + 0.5 * FragColor;

    float visability = 1/(pow(length(vPosition*0.5), 2));
    FragColor = visability * FragColor + (1 - visability) * vec4(0.2,0.2,0.05,1);

}