#version 330

// Copyright (c) Russell Gillette
// December 2013

layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 Normal;

out vec3 vColor;
out vec3 vNormal;
out vec4 vPosition;

out vec4 l0Pos;
out vec4 l1Pos;
out vec4 l2Pos;

struct LightInfo {
    vec4 Position;
    vec3 La;
    vec3 Ld;
    vec3 Ls;
};
// uniform LightInfo Light;


struct MaterialInfo {
    vec3 Ka;
    vec3 Kd;
    vec3 Ks;
    float Shininess;
};
uniform MaterialInfo Material;

uniform vec3 gem_pos;
uniform mat3 NormalMatrix;
uniform mat4 MVP;
uniform mat4 ModelViewMatrix;

uniform vec3 light0pos;
uniform vec3 light1pos;
uniform vec3 light2pos;


void main() {
    // determine vertex color
    // vec3 tnorm  = normalize( NormalMatrix * Normal );
    // vec3 s      = vec3(Light.Position); // incident vector
    // vColor      = dot(s, tnorm) * Material.Ka * 0.4 + Material.Ka * 0.6;


    vColor      = vec3(1);
    vNormal     = NormalMatrix * Normal;
    vPosition   = ModelViewMatrix * vec4(Position, 1.0);


    l0Pos        = ModelViewMatrix * vec4(light0pos, 1.0);
    l1Pos        = ModelViewMatrix * vec4(light1pos, 1.0);
    l2Pos        = ModelViewMatrix * vec4(light2pos, 1.0);


    gl_Position = MVP * vec4(Position, 1.0);
}
