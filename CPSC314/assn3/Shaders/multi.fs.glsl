#version 330

in vec3 vColor;
in vec3 vNormal;
in vec4 vPosition;


in vec4 l0Pos;
in vec4 l1Pos;
in vec4 l2Pos;


struct LightInfo
{
    vec4 LPosition; // we are going to treat this as a direction to achieve directional lighting
    vec3 La;       // ambient light
    vec3 Ld;       // diffuse light
    vec3 Ls;       // specular light
};
uniform LightInfo Light0;
uniform LightInfo Light1;
uniform LightInfo Light2;
uniform LightInfo Light3;


struct MaterialInfo
{
    vec3 Ka;            // Ambient reflectivity
    vec3 Kd;            // Diffuse reflectivity
    vec3 Ks;            // Specular reflectivity
    float Shininess;    // Specular shininess factor
};

uniform MaterialInfo Material;


layout (location = 0) out vec4 FragColor;

void main() {
    vec3 normal = normalize(vNormal);

    vec3 toV= -normalize(vec3(vPosition));



    // ligt0, blue spotlight at gem facing away from eye
    vec3 toLight = normalize(vec3(l0Pos) - vec3(vPosition));
    vec3 h = normalize(toV + toLight);
    float power = log(pow(length(vec3(l0Pos) - vec3(vPosition)), 2) + pow(length(vPosition),2));

    float specular  = pow(max(0.0, dot(h, normal)), 1024.0);
    float diffuse   = max(0.0, dot(normal, toLight));

    // vec3 iLight0 = normalize(vec3(0,0,1)) * (vec3(0.05) + vColor * diffuse + vec3(1) * specular);
    vec3 iLight0 = vec3(0,0,1) * (Light0.La * Material.Ka + Light0.Ld * Material.Kd * diffuse + Light0.Ls * Material.Ks * specular);

    iLight0 *= 1.5;
    iLight0 /= power;

    if (length(cross(vec3(0,0,1), toLight)) > 0.5 ) iLight0 = vec3(0);
    else {
        // float period = 16 * length(l0Pos - vPosition);
        // iLight0 += 0.2 * sin(length(period)) * vec3(1,0,1);
        iLight0 += pow((1 - length(cross(vec3(0,0,1), toLight))), 2) * 0.3 * vec3(1,1,0);
        if (length(cross(vec3(0,0,1), toLight)) < 0.1 ) iLight0 *= sqrt(length(cross(vec3(0,0,1), toLight)) * 10);

    }


    // light 1, directional red light from front
    toLight =  vec3(0,0,1); //normalize(vec3(l1Pos) - vec3(vPosition));
    h = normalize(toV + toLight);
    power = log(pow(length(vPosition),2));

    specular  = pow(max(0.0, dot(h, normal)), 4096.0);
    diffuse   = max(0.0, dot(normal, toLight));
    // vec3 iLight1 = vec3(1,0,0) * (vec3(0.05) + vColor * diffuse + vec3(0.6) * specular);
    vec3 iLight1 = vec3(1,0,0) * (Light1.La * Material.Ka + Light1.Ld * Material.Kd * diffuse + Light1.Ls * Material.Ks * specular);

    iLight1 *= 1;
    iLight1 /= power;


    // light 1, point light from somewhere
    toLight = normalize(vec3(l2Pos) - vec3(vPosition));
    h = normalize(toV + toLight);
    power = log(pow(length(vec3(l2Pos) - vec3(vPosition)), 2) + pow(length(vPosition),2));

    specular  = pow(max(0.0, dot(h, normal)), 1024.0);
    diffuse   = max(0.0, dot(normal, toLight));
    //vec3 iLight2 = vec3(0,1,0) * (vec3(0.05) + vColor * diffuse + vec3(0.6) * specular);
    vec3 iLight2 = vec3(0,1,0) * (Light2.La * Material.Ka + Light2.Ld * Material.Kd * diffuse + Light2.Ls * Material.Ks * specular);

    iLight2 *= 1;
    iLight2 /= power;



    vec3 intensity = iLight0 + iLight1 + iLight2;

    FragColor = vec4(intensity, 1);

}