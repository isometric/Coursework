#version 330

// Copyright (c) Russell Gillette
// December 2013

layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 Normal;

out vec3 vColor;
out vec3 vNormal;
out vec4 vPosition;
out vec4 lPos;

struct LightInfo
{
    vec4 Position; // we are going to treat this as a direction to acheive directional lighting
    vec3 La;       // ambient light
    vec3 Ld;       // diffuse light
    vec3 Ls;       // specular light
};
//uniform LightInfo Light0;

struct MaterialInfo
{
    vec3 Ka;
    vec3 Kd;
    vec3 Ks;
    float Shininess;
};
uniform MaterialInfo Material;

uniform vec3 gem_pos;
uniform mat3 NormalMatrix;      // we keep a MV matrix without the translation component to apply to vectors
uniform mat4 MVP;               // ModelViewProjection Matrix
uniform mat4 ModelViewMatrix;   // ModelView Matrix

void main()
{
    // determine vertex color
    vec3 tnorm      = normalize( NormalMatrix * Normal );
    vec3 s          = vec3(gem_pos); // incident vector
    vColor          = dot(s, tnorm) * Material.Ka * 0.4 + Material.Ka * 0.6;


    //vColor      = Position;
    vNormal     = NormalMatrix * Normal;
    vPosition   = ModelViewMatrix * vec4(Position, 1.0);
    // lPos        = ModelViewMatrix * Light0.Position;
    lPos        = ModelViewMatrix * vec4(gem_pos, 1.0);


    gl_Position = MVP * vec4(Position, 1.0);
}