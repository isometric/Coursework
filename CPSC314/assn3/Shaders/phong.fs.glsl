#version 330

in vec3 vColor;
in vec3 vNormal;
in vec4 vPosition;
in vec4 lPos;


struct LightInfo
{
    vec4 LPosition; // we are going to treat this as a direction to achieve directional lighting
    vec3 La;       // ambient light
    vec3 Ld;       // diffuse light
    vec3 Ls;       // specular light
};
uniform LightInfo Light0;

struct MaterialInfo
{
    vec3 Ka;            // Ambient reflectivity
    vec3 Kd;            // Diffuse reflectivity
    vec3 Ks;            // Specular reflectivity
    float Shininess;    // Specular shininess factor
};

uniform MaterialInfo Material;
uniform vec3 gem_pos;

layout (location = 0) out vec4 FragColor;

void main() {
    vec3 normal = normalize(vNormal);

    vec3 toLight = normalize(vec3(lPos) - vec3(vPosition));

    vec3 toV= -normalize(vec3(vPosition));

    vec3 h = normalize(toV + toLight);


    // human vision is logarithmic and
    float power = log(pow(length(vec3(lPos) - vec3(vPosition)), 2) + pow(length(vPosition),2));


    float specular  = pow(max(0.0, dot(h, normal)), 1024.0);
    float diffuse   = max(0.0, dot(normal, toLight));

    //diffuse = pow(diffuse, 4);
    diffuse =(pow(2048, diffuse) - 1)/2048;

    vec3 intensity = vColor * (Light0.La * Material.Ka + Light0.Ld * Material.Kd * diffuse + Light0.Ls * Material.Ks * specular);
    //vec3 intensity = normalize(gem_pos) * (0.3 * diffuse + 0.7 * specular);
    intensity += 0.05 * normalize(gem_pos);

    //vec3 intensity = normalize(gem_pos) * (vec3(0.05) + vColor * diffuse + vec3(0.6) * specular);


    intensity *= 4;
    intensity /= power;

    //intensity = normalize(Light.La);

    FragColor = vec4(intensity, 1);

    if (length(gem_pos) > 5) {
    float glow = pow(dot(normal, toV), 2);
    FragColor = (0 + glow) * FragColor + (1 - glow) * 0.2 * vec4(gem_pos,1);
    }

}