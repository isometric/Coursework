#version 330

layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 Normal;
//layout (location = 2 )in vec3 gem_pos;

out vec3 LightIntensity;

struct LightInfo
{
    vec4 Position; // we are going to treat this as a direction to acheive directional lighting
    vec3 La;       // ambient light
    vec3 Ld;       // diffuse light
    vec3 Ls;       // specular light
};
uniform LightInfo Light;
z
struct MaterialInfo
{
    vec3 Ka;
    vec3 Kd;
    vec3 Ks;
    float Shininess;
};
uniform MaterialInfo Material;

uniform mat4 ModelViewMatrix;
uniform mat3 NormalMatrix;      // we keep a MV matrix without the translation component to apply to vectors
uniform mat4 ProjectionMatrix;
uniform mat4 MVP;               // ModelViewProjection Matrix

uniform vec3 gem_position;
uniform float gem_radius;

void main()
{
    // determine vertex color
    vec3 tnorm     = normalize( NormalMatrix * Normal );
    vec3 s         = normalize( vec3(Light.Position) ); // incident vector

    /********************************
     * Your code goes here
     ********************************/
    LightIntensity = dot(s, tnorm)  + sqrt(gem_position + vec3(1,1,1)) + gem_position;    // REPLACE ME
    //LightIntensity = gem_position;

    if (distance(gem_position, Position) < gem_radius){
        gl_Position = MVP * vec4( normalize(Position - gem_position) * gem_radius + gem_position,1);
    } else {
        gl_Position = MVP * vec4(Position, 1); // everything is normal

    }


}