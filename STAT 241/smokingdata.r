smoking_data <- read.table("~/Desktop/STAT 241/smokingdata.txt")

boxplot(smoking_data$rate~smoking_data$type)


smokers <- subset(smoking_data, type =="smoker", rate)
non_smokers <- subset(smoking_data, type == "non-smoker", rate)


smoker_mean <- mean(smokers$rate)
smoker_var <- var(smokers$rate)

non_smoker_mean <- mean(non_smokers$rate)
non_smoker_var <- var(non_smokers$rate)


diffrence_of_means <- smoker_mean - non_smoker_mean
s2_pooled <- ((length(smokers$rate) - 1) * smoker_var + (length(non_smokers$rate) - 1) * non_smoker_var)/(length(smokers$rate) + length(non_smokers$rate) - 2)

se_pooled <- s2_pooled * sqrt( 1/length(smokers$rate) + 1/length(non_smokers$rate))
test_stat <- (diffrence_of_means)/sqrt(se_pooled)