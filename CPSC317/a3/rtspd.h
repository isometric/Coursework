#ifndef RTSPD_H
#define RTSPD_H

#include <cv.h>
#include <highgui.h>
#include <stdint.h>

// ... rest of header here
//
// #endif
/**
 * A lot of these return types and what not are probably wrong?
 * never sure if to return a pointer or struct. Fix as need be plzz
 */

typedef enum {
  SETUP = 0,
  PLAY,
  PAUSE,
  TEARDOWN,
} rtsp_state;


typedef struct rtsp_req {
  rtsp_state state;       //SETUP, PLAY, PAUSE, etc.
  char* file_path;    //movie.Mjpeg
  int seq;            //Cseq: 123
  int session;        //Session: 12356
} rtsp_req;

typedef struct rtsp_resp {
  char* status;
  int seq;
  int session;
} rtsp_resp;

typedef struct frame_timer {
  struct sigevent play_event;
  timer_t play_timer;
  struct itimerspec play_interval;
} frame_timer;

typedef struct rtsp_conn_data {
  int socket;
  rtsp_state state;       //SETUP, PLAY, PAUSE...
  CvCapture *video;
  frame_timer *timer;
  int frame;
} rtsp_conn_data;

typedef struct send_frame_data {
  int socket;
  CvCapture *video;
  int frame;
  int seq;
} send_frame_data;

typedef struct rtp_hdr_t {
  unsigned int dollar:8;
  unsigned int channel:8;
  unsigned int length:16;
  unsigned int version:2;   /* protocol version */
  unsigned int p:1;         /* padding flag */
  unsigned int x:1;         /* header extension flag */
  unsigned int cc:4;        /* CSRC count */
  unsigned int m:1;         /* marker bit */
  unsigned int pt:7;        /* payload type */
  unsigned int seq:16;      /* sequence number */
  u_int32_t ts;               /* timestamp */
  u_int32_t ssrc;             /* synchronization source */
  unsigned char *payload;
} rtp_hdr_t;

void process_request(rtsp_conn_data *conn, char *payload, char *response_str);
void *handle_client(void *data);

int parse(char* payload, rtsp_req* req);
void exec(rtsp_conn_data *conn, rtsp_req* req, rtsp_resp* resp);

void setup_player(rtsp_conn_data *conn, rtsp_req* req, rtsp_resp* resp);
void play_player(rtsp_conn_data *conn, rtsp_req* req, rtsp_resp* resp);
void pause_player(rtsp_conn_data *conn, rtsp_req* req, rtsp_resp* resp);
void teardown_player(rtsp_conn_data *conn, rtsp_req* req, rtsp_resp* resp);
void send_frame(union sigval sv_data);

void resp_to_str(rtsp_resp* resp, char *buff);
int contains_action(char *word);
int gen_session_num();
void error_resp(char *code, rtsp_req *req, rtsp_resp *resp);

#endif
