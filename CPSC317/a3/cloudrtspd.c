#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdint.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <semaphore.h>
#include <netdb.h>

//OpenCV
#include <cv.h>
#include <highgui.h>

//Timer
#include <sys/time.h>
#include <signal.h>
#include <time.h>

#include "cloudrtspd.h"
#include "cloud_helper.h"

void *handle_client(void *data) {

  sem_t lock;
  sem_init(&lock, 0, 1);
  int socket = (int) (intptr_t) data;

  rtsp_conn_data *conn = malloc(sizeof(rtsp_conn_data));
  conn->timer = malloc(sizeof(frame_timer));
  conn->socket = socket;
  conn->state = "INIT";
  conn->lock = lock;
  conn->cloud_conn_occupancy = 0;

  //rcv-send loop
  int len = 512;

  char *request = malloc(sizeof(char) * len);
  bzero(request, len);

  while(1) {

    if(read(socket, request, len) <= 0) {
      printf("Could not read\n");
      break;
    }

    printf("\nRequest <<<<<<<<<<<<<<<<<<\n%s\n", request);

    sem_wait(&conn->lock);

    int result;
    if((result = process_request(conn, request)) == -1) {
      printf("Unexpected Error. Exiting\n");
      break;
    }
    printf("Result %i\n", result);

    sem_post(&conn->lock);

    bzero(request, len);
  }

  printf("Exiting\n");

  close(socket);
  sem_destroy(&conn->lock);
  timer_delete(conn->timer->play_timer);
  free(conn->timer);
  free(conn);
  free(request);

  return NULL;
}

int process_request(rtsp_conn_data *conn, char* payload) {

  int result = 0;
  rtsp_req* request = malloc(sizeof(rtsp_req));
  rtsp_resp* response = malloc(sizeof(rtsp_resp));

  parse(payload, request);
  result = exec(conn, request, response);

  free(request);
  free(response);

  return result;
}

int parse(char *payload, rtsp_req *req) {
  char *line_sep = "\r\n";
  char *word_sep = " ";
  char *line, *word, *last_line, *last_word;

  char *action = NULL;
  char *file_path = NULL;
  char *seq = NULL;
  char *session = NULL;
  char *scale = NULL;

  for(line = strtok_r(payload, line_sep, &last_line);
      line;
      line = strtok_r(NULL, line_sep, &last_line)) {

    if(!line) break;

    for(word = strtok_r(line, word_sep, &last_word);
        word;
        word = strtok_r(NULL, word_sep, &last_word)) {

      if(!word) {
        break;
      } else if(contains_action(word)) {
        action = word;
        file_path = strtok_r(NULL, word_sep, &last_word);
      } else if(strstr(word, "CSeq:")) {
        seq = strtok_r(NULL, word_sep, &last_word);
      } else if(strstr(word, "Scale:")) {
        scale = strtok_r(NULL, word_sep, &last_word);
      } else if(strstr(word, "Session:")) {
        session = strtok_r(NULL, word_sep, &last_word);
      }

    }

  }

  if(!action || !file_path || !seq)
    return 1; //error

  req->action = action;
  req->file_path = file_path;
  req->seq = atoi(seq);
  req->session = (session) ? atoi(session) : -1;
  req->scale = (scale) ? atoi(scale) : 1;

  return 0;
}


int exec(rtsp_conn_data *conn, rtsp_req* req, rtsp_resp *resp) {

  if(strcmp("SETUP", req->action) == 0) {
      return setup_player(conn, req, resp);
  }

  if(strcmp("PLAY", req->action) == 0) {
      return play_player(conn, req, resp);
  }

  if(strcmp("PAUSE", req->action) == 0) {
      return pause_player(conn, req, resp);
  }

  if(strcmp("TEARDOWN", req->action) == 0) {
      return teardown_player(conn, req, resp);
  }

  return 1; //unsupported command

}

void init_timer(rtsp_conn_data *conn) {

  struct sigevent play_event;
  timer_t play_timer;
  struct itimerspec play_interval;

  memset(&play_event, 0, sizeof(play_event));
  play_event.sigev_notify = SIGEV_THREAD;
  play_event.sigev_value.sival_ptr = conn;
  play_event.sigev_notify_function = send_frame;

  timer_create(CLOCK_REALTIME, &play_event, &play_timer);

  frame_timer *timer = malloc(sizeof(frame_timer));
  timer->play_event = play_event;
  timer->play_timer = play_timer;
  timer->play_interval = play_interval;

  conn->timer = timer;
}

void start_timer(rtsp_conn_data *conn) {
  conn->timer->play_interval.it_interval.tv_sec = 0;
  conn->timer->play_interval.it_interval.tv_nsec = 40 * 1000000;
  conn->timer->play_interval.it_value.tv_sec = 0;
  conn->timer->play_interval.it_value.tv_nsec = 1;

  timer_settime(conn->timer->play_timer, 0, &(conn->timer->play_interval), NULL);
}

void pause_timer(rtsp_conn_data *conn) {
  conn->timer->play_interval.it_interval.tv_sec = 0;
  conn->timer->play_interval.it_interval.tv_nsec = 0;
  conn->timer->play_interval.it_value.tv_sec = 0;
  conn->timer->play_interval.it_value.tv_nsec = 0;
  timer_settime(conn->timer->play_timer, 0, &(conn->timer->play_interval), NULL);
  usleep(60000);
}

int send_response(int socket_fd, rtsp_resp *resp) {
  char raw_response[512];
  resp_to_str(resp, raw_response);
  int len = strlen(raw_response);

  printf("Response >>>>>>>>>>>>>>>>>>>>>>>\n%s\n", raw_response);
  return send_all(socket_fd, raw_response, &len);
}

int send_all(int s, char *buf, int *len) {
  int total = 0;
  int bytesleft = *len;
  int n;

  while(total < *len) {
    n = send(s, buf + total, bytesleft, 0);
    if (n == -1) { break; }
    total += n;
    bytesleft -= n;
  }

  *len = total;

  return (n == -1) ? -1 : 0;
}

int setup_player(rtsp_conn_data *conn, rtsp_req* req, rtsp_resp *resp) {

  if(strcmp(conn->state, "INIT") != 0) {
    error_resp("455 Method Not Valid in This State", req, resp);
    return send_response(conn->socket, resp);
  }

  if (strncmp(req->file_path, "cloud://", 8) == 0) {
    conn->cloud_path = malloc(sizeof(req->file_path)-8);
    strcpy(conn->cloud_path, req->file_path+8);
  } else {
    conn->cloud_path = 0;

    CvCapture *video;
    video = cvCaptureFromFile(req->file_path);

    if (!video) {
      error_resp("404 Not Found", req, resp);
      return send_response(conn->socket, resp);
    }
    conn->video = video;
  }

  conn->state = "READY";
  conn->frame = 0;
  conn->video_seq_num = 0;
  conn->seq = 0;

  resp->status = "200 OK";
  resp->seq = req->seq;
  resp->session = gen_session_num();

  if(send_response(conn->socket, resp) == -1) {
    printf("Error sending response");
    return -1;
  }

  init_timer(conn);
  return 0;
}


int play_player(rtsp_conn_data *conn, rtsp_req *req, rtsp_resp *resp) {

  if(strcmp(conn->state, "INIT") == 0) {
    error_resp("455 Method Not Valid in This State", req, resp);
    send_response(conn->socket, resp);
  }

  resp->status = "200 OK";
  resp->seq = req->seq;
  resp->session = req->session;

  if(send_response(conn->socket, resp) == -1) {
    puts("Error sending response");
    return -1;
  }

  conn->scale = req->scale;
  conn->state = "PLAYING";
  start_timer(conn);

  return 0;
}


int pause_player(rtsp_conn_data *conn, rtsp_req* req, rtsp_resp *resp) {

  if(strcmp(conn->state, "PLAYING") != 0) {
    error_resp("455 Method Not Valid in This State", req, resp);
    return send_response(conn->socket, resp);
  }

  pause_timer(conn);

  resp->status = "200 OK";
  resp->seq = req->seq;
  resp->session = req->session;

  if(send_response(conn->socket, resp) == -1) {
    puts("Error sending response");
    return -1;
  }

  conn->state = "READY";
  return 0;
}


int teardown_player(rtsp_conn_data *conn, rtsp_req* req, rtsp_resp *resp) {

  if(strcmp(conn->state, "INIT") == 0) {
    error_resp("455 Method Not Valid in This State", req, resp);
    return send_response(conn->socket, resp);
  }

  if(strcmp(conn->state, "PLAYING") == 0) {
    pause_timer(conn);
  }

  if(!conn->cloud_path) {
    cvReleaseCapture(&conn->video);
  }

  resp->status = "200 OK";
  resp->seq = req->seq;
  resp->session = req->session;

  if(send_response(conn->socket, resp) == -1) {
    puts("Error sending teardown response");
    return -1;
  }

  conn->state = "INIT";
  return 0;
}


void send_frame(union sigval sv_data) {
  rtsp_conn_data *conn = (rtsp_conn_data *) sv_data.sival_ptr;

  sem_wait(&conn->lock);

  if (conn->cloud_path != 0) {

    conn->frame += conn->scale;

    if(conn->frame < 0) {
      puts("no more frames. (attempted < 0)");
      conn->frame = 0;
      pause_timer(conn);
      sem_post(&conn->lock);
      return;
    }

    const struct cloud_server *cloud_info;
    cloud_info = get_cloud_server(conn->cloud_path, conn->frame);

    printf("Cloud path: %s\n", conn->cloud_path);
    printf("Frame Num: %i\n", conn->frame);

    int fd = 0;
    int i;

    for(i=0; i < conn->cloud_conn_occupancy; i++) {
      if(strcmp(cloud_info->server, conn->cloud_conns[i].server->server) == 0) {
        fd = conn->cloud_conns[i].socket_fd;
      }
    }

    if(fd == 0) {
      printf("Server Name: %s\n", cloud_info->server);
      printf("Port Num: %i\n", cloud_info->port);

      fd = socket(AF_INET , SOCK_STREAM , 0);

      if (fd == -1) {
        puts("Failed to create socket");
      }

      int status;
      struct addrinfo hints, *servinfo;
      memset(&hints, 0, sizeof hints);  // unset mem
      hints.ai_family = AF_INET;     // don't care IPv4 or IPv6
      hints.ai_socktype = SOCK_STREAM;  // TCP stream sockets

      char port[6];
      sprintf(port, "%d", cloud_info->port);
      printf("port: %s\n", port);

      status = getaddrinfo(cloud_info->server, port, &hints, &servinfo);

      if (status != 0) {
        puts("Failed during host lookup");
      }

      if ((connect(fd, servinfo->ai_addr, servinfo->ai_addrlen)) < 0) {
        puts("Failed to connect");
      }

      if(conn->cloud_conn_occupancy < 5) {
        cloud_conn c;
        c.server = cloud_info;
        c.socket_fd = fd;
        conn->cloud_conns[conn->cloud_conn_occupancy] = c;
      }

    }
    /* build request */
    char request[32];

    sprintf(request, "%s:%d\n", conn->cloud_path, conn->frame);
    if (send(fd, request, strlen(request) , 0) < 0) {
      puts("Failed to send");
      return;
    }
    char preamble[5];
    int bytes_read = 0;

    if((bytes_read = read(fd, preamble, 5)) < 0) {
      puts("Fail");
      return;
    } else if(bytes_read == 0) {
      puts("Frame not found");
      //do somehting
    }

    int size = atoi(preamble);
    printf("Preamble %s, %i", preamble, size);

    if(size <= 0) {
      puts("No more frames. (Attempted < 0)");
      pause_timer(conn);
      sem_post(&conn->lock);
      return;
    }

    bytes_read = 0;
    char buffer[size];

    while(bytes_read < size) {
      int n = 0;
      n = read(fd, buffer + bytes_read, size - bytes_read);
      if(n < 0) puts("Error");
      bytes_read += n;
    }

    printf("Bytes read: %i", bytes_read);

    struct timeval  tv;
    gettimeofday(&tv, NULL);
    uint32_t time_in_mill = (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000;

    char *packet = malloc((size + 16) * sizeof(char));
    packet[0] = 0x24;
    packet[1] = 0x00;
    packet[2] = (size + 12) >> 8;
    packet[3] = (size + 12) & 0xff;
    packet[4] = 0x80;
    packet[5] = 0x1a;
    packet[6] = (conn->video_seq_num) >> 8;
    packet[7] = (conn->video_seq_num) & 0xff;
    packet[8] = (time_in_mill) >> 24;
    packet[9] = ((time_in_mill) >> 16) & 0xff;
    packet[10] = ((time_in_mill) >> 8) & 0xff;
    packet[11] = (time_in_mill) & 0xff;
    packet[12] = 0;
    packet[13] = 0;
    packet[14] = 0;
    packet[15] = 0;

    memcpy(&packet[16], buffer, size);
    int result = 0;
    int len = size + 16;

    if((result = send_all(conn->socket, packet, &len)) == -1) {
      printf("Error. Could not send data over socket");
      pause_timer(conn);
    }

  } else {

    IplImage *image;
    CvMat *thumb;
    CvMat *encoded;

    conn->frame += conn->scale;

    if(conn->frame < 0) {
      puts("no more frames. (attempted < 0)");
      conn->frame = 0;
      pause_timer(conn);
      sem_post(&conn->lock);
      return;
    }

    cvSetCaptureProperty(conn->video, CV_CAP_PROP_POS_FRAMES, conn->frame);

    image = cvQueryFrame(conn->video);
    if (!image) {
      puts("No more frames.");
      conn->frame -= conn->scale;
      cvSetCaptureProperty(conn->video, CV_CAP_PROP_POS_FRAMES, conn->frame);
      pause_timer(conn);
      sem_post(&conn->lock);
      return;
    }

    conn->video_seq_num += 1;

    thumb = cvCreateMat(480, 640, CV_8UC3);
    cvResize(image, thumb, CV_INTER_AREA);

    const static int encodeParams[] = { CV_IMWRITE_JPEG_QUALITY, 30 };
    encoded = cvEncodeImage(".jpeg", thumb, encodeParams);

    struct timeval  tv;
    gettimeofday(&tv, NULL);
    uint32_t time_in_mill = (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000;

    char *packet = malloc((encoded->cols + 16) * sizeof(char));
    packet[0] = 0x24;
    packet[1] = 0x00;
    packet[2] = (encoded->cols + 12) >> 8;
    packet[3] = (encoded->cols + 12) & 0xff;
    packet[4] = 0x80;
    packet[5] = 0x1a;
    packet[6] = (conn->video_seq_num) >> 8;
    packet[7] = (conn->video_seq_num) & 0xff;
    packet[8] = (time_in_mill) >> 24;
    packet[9] = ((time_in_mill) >> 16) & 0xff;
    packet[10] = ((time_in_mill) >> 8) & 0xff;
    packet[11] = (time_in_mill) & 0xff;
    packet[12] = 0;
    packet[13] = 0;
    packet[14] = 0;
    packet[15] = 0;
    memcpy(&packet[16], encoded->data.ptr, encoded->cols);

    int result = 0;
    int len = 16 + encoded->cols;

    if((result = send_all(conn->socket, packet, &len)) == -1) {
      printf("Error. Could not send data over socket");
      pause_timer(conn);
    }
  }

  sem_post(&conn->lock);
}

//
// Helpers
//

void resp_to_str(rtsp_resp *resp, char *buff) {

  sprintf(buff, "RTSP/1.0 %s\r\nCSeq: %i\r\nSession: %i\r\n\r\n",
          resp->status, resp->seq, resp->session);

}

int gen_session_num() {
  srand(13);
  return rand();
}

void error_resp(char *code, rtsp_req *req, rtsp_resp *resp) {
  resp->status = code;
  resp->seq = req->seq;
  resp->session = resp->session;
}

int contains_action(char *word) {
  if (strstr(word, "SETUP") != NULL || strstr(word, "PLAY") != NULL ||
     strstr(word, "PAUSE") != NULL || strstr(word, "TEARDOWN") != NULL) {
    return 1;
  } else {
    return 0;
  }
}

