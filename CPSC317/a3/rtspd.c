#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdint.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>

//OpenCV
#include <cv.h>
#include <highgui.h>

//Timer
#include <sys/time.h>
#include <signal.h>
#include <time.h>

#include "rtspd.h"

void *handle_client(void *data) {

  int socket = (int) (intptr_t) data;

  rtsp_conn_data *conn = malloc(sizeof(rtsp_conn_data));
  conn->socket = socket;
  conn->state = INIT;

  //rcv-send loop
  int len = 512;

  char *request = malloc(sizeof(char) * len);
  bzero(request, len);

  char *response = malloc(sizeof(char) * len);
  bzero(response, len);

  while(1) {

    if(read(socket, request, len) < 0) {
      printf("Could not read\n");
      break;
    }

    printf("\nRequest <<<<<<<<<<<<<<<<<<\n%s\n", request);

    process_request(conn, request, response);
    printf("\nResponse >>>>>>>>>>>>>>>>>>\n%s\n", response);

    if (send(socket, response, len, 0) == -1) {
      perror("send");
    }

    bzero(request, len);
    bzero(response, len);
  }

  close(socket);
  free(conn);
  free(request);
  free(response);

  return NULL;
}


void process_request(rtsp_conn_data *conn, char* payload, char *response_str) {
  rtsp_req* request = malloc(sizeof(rtsp_req));
  rtsp_resp* response = malloc(sizeof(rtsp_resp));

  parse(payload, request);
  exec(conn, request, response);
  resp_to_str(response, response_str);

  free(request);
  free(response);
}


int parse(char *payload, rtsp_req *req) {

  char *line_sep = "\r\n";
  char *word_sep = " ";
  char *line, *word, *last_line, *last_word;

  char *action = NULL;
  char *file_path = NULL;
  char *seq = NULL;
  char *session = NULL;

  for(line = strtok_r(payload, line_sep, &last_line);
      line;
      line = strtok_r(NULL, line_sep, &last_line)) {

    if(!line) break;

    for(word = strtok_r(line, word_sep, &last_word);
        word;
        word = strtok_r(NULL, word_sep, &last_word)) {

      if(!word) {
        break;
      } else if(contains_action(word)) {
        action = word;
        file_path = strtok_r(NULL, word_sep, &last_word);
      } else if(strstr(word, "CSeq:")) {
        seq = strtok_r(NULL, word_sep, &last_word);
      } else if(strstr(word, "Session:")) {
        session = strtok_r(NULL, word_sep, &last_word);
      }

    }

  }

  if(!action || !file_path || !seq)
    return 1; //error

  req->action = action;
  req->file_path = file_path;
  req->seq = atoi(seq);
  req->session = (session) ? atoi(session) : -1;

  return 0;
}


void exec(rtsp_conn_data *conn, rtsp_req* req, rtsp_resp* resp) {

  if(req->action == SETUP) {
      setup_player(conn, req, resp);
  } else if(req->action == PLAY) == 0) {
      play_player(conn, req, resp);
  } else if(req->action == PAUSE) == 0) {
      pause_player(conn, req, resp);
  } else if(req->action == TEARDOWN) == 0) {
      teardown_player(conn, req, resp);
  }

}

void setup_player(rtsp_conn_data *conn, rtsp_req* req, rtsp_resp* resp) {

  if(conn->state ==  INIT)  {
    error_resp("455", req, resp);
    return;
  }

  CvCapture *video;
  video = cvCaptureFromFile(req->file_path);

  if (!video) {
    error_resp("404 Not Found", req, resp);
    return;
  }

  conn->state = READY;
  conn->video = video;
  conn->frame = 0;

  resp->status = "200 OK";
  resp->seq = req->seq;
  resp->session = gen_session_num();
}


void play_player(rtsp_conn_data *conn, rtsp_req* req, rtsp_resp* resp) {

  if(strcmp(conn->state, "READY") != 0) {
    error_resp("455", req, resp);
    return;
  }

  resp->status = "200 OK";
  resp->seq = req->seq;
  resp->session = req->session;
 char response[512];

 resp_to_str(resp, response);
 printf("response %s", response);
 send(conn->socket, response, 512, 0);

 send_frame_data *data = malloc(sizeof(send_frame_data));
  data->socket = conn->socket;
  data->video = conn->video;
  data->frame = conn->frame;
  data->seq = 0; //TODO: get this from somewhere

  struct sigevent play_event;
  timer_t play_timer;
  struct itimerspec play_interval;

  memset(&play_event, 0, sizeof(play_event));
  play_event.sigev_notify = SIGEV_THREAD;
  play_event.sigev_value.sival_ptr = data;
  play_event.sigev_notify_function = send_frame;

  play_interval.it_interval.tv_sec = 0;
  play_interval.it_interval.tv_nsec = 40 * 1000000; // 40 ms in ns
  play_interval.it_value.tv_sec = 0;
  play_interval.it_value.tv_nsec = 1; // can't be zero

  timer_create(CLOCK_REALTIME, &play_event, &play_timer);
  timer_settime(play_timer, 0, &play_interval, NULL);

  frame_timer *timer = (frame_timer *) malloc(sizeof(frame_timer));
  timer->play_event = play_event;
  timer->play_timer = play_timer;
  timer->play_interval = play_interval;

  conn->timer = timer;
}

void send_frame(union sigval sv_data) {

  send_frame_data *data = (send_frame_data *) sv_data.sival_ptr;
  printf("socket fd %i", data->socket);

  IplImage *image;
  CvMat *thumb;
  CvMat *encoded;

  cvSetCaptureProperty(data->video, CV_CAP_PROP_POS_FRAMES, data->frame++);

  image = cvQueryFrame(data->video);
  if (!image) {
    // TODO: Next frame doesn't exist or can't be obtained.
  }

  thumb = cvCreateMat(480, 640, CV_8UC3);
  cvResize(image, thumb, CV_INTER_AREA);

  const static int encodeParams[] = { CV_IMWRITE_JPEG_QUALITY, 30 };
  encoded = cvEncodeImage(".jpeg", thumb, encodeParams);

  rtp_hdr_t hdr;

  hdr.dollar = 0x24;
  hdr.channel = 0;
  hdr.length = encoded->cols;
  hdr.version = 1;
  hdr.p = 0;
  hdr.x = 0;
  hdr.cc = 0;
  hdr.m = 0;
  hdr.pt = 26;
  hdr.seq = 6;
  hdr.ts = (unsigned) time(NULL);
  hdr.ssrc = 10298;
  hdr.payload = encoded->data.ptr;

  int bytes_sent;

  if((bytes_sent = send(data->socket, &hdr, sizeof(hdr), 0) < 0)) {
    printf("Could not send on socket");
  }

  printf("%i bytes sent", bytes_sent);

}

void pause_player(rtsp_conn_data *conn, rtsp_req* req, rtsp_resp* resp) {
  printf("PAUSE called\n");
}

void teardown_player(rtsp_conn_data *conn, rtsp_req* req, rtsp_resp* resp) {
  printf("TEARDOWN called\n");
}


//
// Helpers
//

void resp_to_str(rtsp_resp *resp, char *buff) {

  sprintf(buff, "RTSP/1.0 %s\r\nCSeq: %i\r\nSession: %i\r\n\r\n",
          resp->status, resp->seq, resp->session);

}

int gen_session_num() {
  srand(13);
  return rand();
}

void error_resp(char *code, rtsp_req *req, rtsp_resp *resp) {
  resp->status = code;
  resp->seq = req->seq;
  resp->session = resp->session;
}

void print_rtsp_resp(rtsp_resp *resp) {
  printf("===========================\n");
  printf("status %s\n", resp->status);
  printf("seq %i\n", resp->seq);
  printf("session %i\n", resp->session);
  printf("===========================\n");
}

void print_rtsp_req(rtsp_req *req) {
  printf("===========================\n");
  printf("action %s\n", req->action);
  printf("file_path %s\n", req->file_path);
  printf("seq %i\n", req->seq);
  printf("session %i\n", req->session);
  printf("===========================\n");
}

int contains_action(char *word) {
  if (strstr(word, "SETUP") != NULL || strstr(word, "PLAY") != NULL ||
     strstr(word, "PAUSE") != NULL || strstr(word, "TEARDOWN") != NULL) {
    return 1;
  } else {
    return 0;
  }
}


