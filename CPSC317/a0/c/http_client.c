// borrowed code from http://www.binarytides.com/socket-programming-c-linux-tutorial/
// borrowed host lookup code from http://beej.us/guide/bgnet/output/html/singlepage/bgnet.html
// refrenced function info from cplusplus
// borrowed code from fri tut - recv_all

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>

void recv_all(int s, char** out, int* outLen) {
    int bufLen = 1024;
    char* buf = malloc(bufLen);

    int offset = 0;
    while (1) {
        int numRead = recv(s, buf + offset, bufLen - offset, 0);

        if (numRead == -1) {
            printf("Error in recv\n");
            return;
        } else if (numRead == 0) {
            printf("Closed in recv\n");
            return;
        } else {
            offset += numRead;

            if (offset == bufLen) {
                bufLen += 1024;
                buf = realloc(buf, bufLen);
            }

            if (!memcmp(buf + offset - 4, "\r\n\r\n", 4)) {
                break;
            }
        }
}

    *out = realloc(buf, offset - 4);
    *outLen = offset - 4;
}


int main(int argc, char *argv[]) {
    char *get, *url, *http, *host, *close;

    if (argc < 2 || strncmp(argv[1], "http://", 7)) {
        fprintf(stderr, "Usage: %s URL\n", argv[0]);
        return 1;
    }

    url = argv[1];
    host = strndup(url + 7, strchr(url + 7, '/') - url - 7);


    // create socket
    int socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1) {
        puts("Failed to create socket");
        return 1;
    }



    //get server ip
    int status;
    struct addrinfo hints, *servinfo;
    memset(&hints, 0, sizeof hints);  // unset mem
    hints.ai_family = AF_INET;     // don't care IPv4 or IPv6
    hints.ai_socktype = SOCK_STREAM;  // TCP stream sockets

    status = getaddrinfo(host, "80", &hints, &servinfo);
    if (status != 0) {
        puts("Failed during host lookup");
        return 1;
    }


    // struct sockaddr_in *server = (struct sockaddr_in *) servinfo->ai_addr;


    // //format server details
    // struct sockaddr_in server;
    // server.sin_addr.s_addr = inet_addr("74.125.235.20");
    // server.sin_family = AF_INET;
    // server.sin_port = htons( 80 );

    // attempt connection
    // if (connect(socket_desc , (struct sockaddr *)&server , sizeof(server)) < 0){
    if ((connect(socket_desc, servinfo->ai_addr, servinfo->ai_addrlen)) < 0) {
        puts("Failed to connect");
        return 1;
    }


    // build message
    get = "GET ";
    http = " HTTP/1.1\r\nHost: ";
    close = "\r\nConnection: close\r\n\r\n";

    int msg_len = 1 + strlen(get) + strlen(url) + strlen(http) + strlen(host) + strlen(close);

    char* message = (char*) malloc(msg_len * sizeof(char));

    strcpy(message, get);
    strcat(message, url);
    strcat(message, http);
    strcat(message, host);
    strcat(message, close);
    //printf("%s\n", message);


    // send message
    if (send(socket_desc , message, strlen(message) , 0) < 0) {
        puts("Failed to send");
        return 1;
    }

    // Receive a reply from the server
    // char server_reply[1000000];
    // if( recv(socket_desc, server_reply , 2000 , 0) < 0){
    //     puts("recv failed");
    //     return 1;
    // }
    // puts(server_reply);
    char* buf;
    int bufLen, count;
    recv_all(socket_desc, &buf, &bufLen);
    for (count = 0; count < bufLen; count++) {
        printf("%c", buf[count]);
    }

    free(host);

    return 0;
}


