// borrowed code from http://docs.oracle.com/javase/tutorial/networking/sockets/
// borrowed code from fri tut - recv_all
import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;

public class HttpClient {

    public static void main(String[] args) {

        int port = 80;
        String url, host;

        if (args.length < 1 || !args[0].startsWith("http://")) {
            System.err.println("Usage: java " + HttpClient.class.getName() + " URL");
            System.exit(1);
        }

        url = args[0];
        host = url.substring(7).split("/", 2)[0];

        String request = "GET " + url + " HTTP/1.1\r\nHost: " + host + "\r\nConnection: close\r\n\r\n";


        try
        {
        Socket socket = new Socket(host, port);

        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

        //BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        BufferedInputStream in = new BufferedInputStream(socket.getInputStream());

        out.println(request);

        // String data;
        // while ((data = in.readLine()) != null) System.out.println(data);

        System.out.write(recv_all(in));

        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + host);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                host);
            System.exit(1);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static byte[] lineFeed = {'\r', '\n', '\r', '\n'};

    public static byte[] recv_all(BufferedInputStream in) throws Exception{
        byte[] buf = new byte[1024];

        int offset = 0;
        while (true){
            int numRead = in.read(buf, offset, buf.length - offset);

            if (numRead == -1) {
                break;
            }
            else {
                offset += numRead;
                if (offset == buf.length) {
                    buf = realloc(buf, buf.length + 1024);
                }
                if (Arrays.equals(Arrays.copyOfRange(buf, offset - 4, offset), lineFeed)) {
                  break;
                }
            }
        }
        byte[] finished = new byte[buf.length-4];
        System.arraycopy(buf, 0, finished, 0, buf.length-4);
        return finished;
    }

    static byte[] realloc(byte[] buf, int newLen) {
        byte[] newBuf = new byte[newLen];
        System.arraycopy(buf, 0, newBuf, 0, Math.min(buf.length, newBuf.length));
        return newBuf;
    }
}