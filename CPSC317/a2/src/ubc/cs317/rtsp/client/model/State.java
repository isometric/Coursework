package ubc.cs317.rtsp.client.model;

/**
 *
 * Provides valid states for the client and an methods for
 * next and prev state transitions.
 * transition to next and prev
 */
public enum State {
    INIT, READY, PLAYING;

    public State goNext() {
        return values()[(ordinal() + 1) % values().length];
    }

    public State goPrev() {
        return values()[(ordinal() - 1) % values().length];
    }

}

