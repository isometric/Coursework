package ubc.cs317.rtsp.client.net;

import ubc.cs317.rtsp.client.exception.RTSPException;
import ubc.cs317.rtsp.client.util.Logger;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by jtgi on 3/12/14.
 */
public class RTSPConnectionHandler {

    private Socket socket;
    private BufferedReader socketIn;
    private Writer socketOut;
    private boolean connected;

    public void RTSPConnectionHandler(){
        this.connected = false;
    }

    public void connect(String server, int port) throws RTSPException {
        try {
            this.socket = new Socket(server, port);
            this.socketIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.socketOut = new PrintWriter(socket.getOutputStream());
            this.connected = true;
        } catch(UnknownHostException e) {
            throw new RTSPException(e.getMessage(), e);
        } catch(IOException e) {
            throw new RTSPException(e.getMessage(), e);
        }
    }

    public boolean send(String msg) throws RTSPException {
        if(connected) {

            Logger.log("Sending...");
            Logger.log(msg);

            try {
                socketOut.write(msg);
                socketOut.flush();
            } catch(IOException e) {
                throw new RTSPException(e.getMessage(), e);
            }
        }

        return true;
    }

    public RTSPResponse recv() throws RTSPException {
        RTSPResponse response = null;

        if(connected) {
            try {
                response = RTSPResponse.readRTSPResponse(socketIn);
            } catch(IOException e) {
                throw new RTSPException(e.getMessage(), e);
            }

            Logger.log("Received --");
            Logger.log("Resp. Code: " + response.getResponseCode());
            Logger.log("CSeq: " + response.getHeaderValue("CSeq"));
            Logger.log("Session: " + response.getHeaderValue("Session"));
        }

        return response;
    }

    public RTSPResponse sendRecv(String msg) throws RTSPException {
        send(msg);
        return recv();
    }

    public void close() {

        try {
            socket.close();
            socketIn.close();
            socketOut.close();
        } catch(IOException e) {
            /* absorb */
        }

        connected = false;
    }


}
