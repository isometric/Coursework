package ubc.cs317.rtsp.client.net;

/**
 * Encapsulate rtsp response codes to make code more readable
 */
public class RTSPResponseCode {

    public final static int OK = 200;

    private RTSPResponseCode() { /* empty */ }


}
