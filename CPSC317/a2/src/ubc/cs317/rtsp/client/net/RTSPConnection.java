/*
 * University of British Columbia
 * Department of Computer Science
 * CPSC317 - Internet Programming
 * Assignment 2
 *
 * Author: Jonatan Schroeder
 * January 2013
 *
 * This code may not be used without written consent of the authors, except for
 * current and future projects and assignments of the CPSC317 course at UBC.
 */

package ubc.cs317.rtsp.client.net;

import ubc.cs317.rtsp.client.exception.RTSPException;
import ubc.cs317.rtsp.client.model.Frame;
import ubc.cs317.rtsp.client.model.Session;
import ubc.cs317.rtsp.client.model.State;
import ubc.cs317.rtsp.client.util.Logger;
import ubc.cs317.rtsp.client.util.Metrics;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.PriorityQueue;
import java.util.Timer;
import java.util.TimerTask;

/**
 * This class represents a connection with an RTSP server.
 */
public class RTSPConnection {

	private static final int BUFFER_LENGTH = 15000;
	private static final long MINIMUM_DELAY_READ_PACKETS_MS = 20;
    private static final int DEFAULT_RTP_TIMEOUT = 1000; //1s

    //Response Codes
    private static final int STATUS_SUCCESS = 200;

	private Session session;
    private String sessionId;
    private String currentVideo;
	private Timer rtpTimer;
    private int cSeq;
    private State state;
    private RTSPConnectionHandler RTSPConn;
    private DatagramSocket RTPConn;

	/**
	 * Establishes a new connection with an RTSP server. No message is sent at
	 * this point, and no stream is set up.
	 *
	 * @param session
	 *            The Session object to be used for connectivity with the UI.
	 * @param server
	 *            The hostname or IP address of the server.
	 * @param port
	 *            The TCP port number where the server is listening to.
	 * @throws RTSPException
	 *             If the connection couldn't be accepted, such as if the host
	 *             name or port number are invalid or there is no connectivity.
	 */
	public RTSPConnection(Session session, String server, int port)
			throws RTSPException {

		this.session = session;
        this.state = State.INIT;
        this.cSeq = 1;

        Logger.enabled = true;

        this.RTSPConn = new RTSPConnectionHandler();
        this.RTSPConn.connect(server, port);

	}

	/**
	 * Sends a SETUP request to the server. This method is responsible for
	 * sending the SETUP request, receiving the response and retrieving the
	 * session identification to be used in future messages. It is also
	 * responsible for establishing an RTP datagram socket to be used for data
	 * transmission by the server. The datagram socket should be created with a
	 * random UDP port number, and the port number used in that connection has
	 * to be sent to the RTSP server for setup. This datagram socket should also
	 * be defined to timeout after 1 second if no packet is received.
	 *
	 * @param videoName
	 *            The name of the video to be setup.
	 * @throws RTSPException
	 *             If there was an error sending or receiving the RTSP data, or
	 *             if the RTP socket could not be created, or if the server did
	 *             not return a successful response.
	 */
	public synchronized void setup(String videoName) throws RTSPException {

        if(this.state != State.INIT) {
            Logger.log("Error: Must be in INIT state. Current state: " + this.state);
            throw new RTSPException("Error: Must be in INIT to call setup. Current state: " + this.state);
        }

        try {
            this.RTPConn = new DatagramSocket();
            this.RTPConn.setSoTimeout(DEFAULT_RTP_TIMEOUT);
        } catch(SocketException e) {
            throw new RTSPException(e.getMessage(), e);
        }

        int port = this.RTPConn.getLocalPort();

        String msg = String.format("SETUP %s RTSP/1.0\r\n", videoName);
        msg += String.format("CSeq: %s\r\n", cSeq);
        msg += String.format("Transport: RTP/UDP; client_port= %s\r\n", port);
        msg += "\r\n";

        this.RTSPConn.send(msg);
        this.cSeq++;

        RTSPResponse response = this.RTSPConn.recv();

        if(response.getResponseCode() != RTSPResponseCode.OK) {
            throw new RTSPException(String.format("%s response code returned.", response.getResponseCode()));
        }

        this.sessionId = response.getHeaderValue("Session");
        this.currentVideo = videoName;
        this.state = State.READY;
	}

	/**
	 * Sends a PLAY request to the server. This method is responsible for
	 * sending the request, receiving the response and, in case of a successful
	 * response, starting the RTP timer responsible for receiving RTP packets
	 * with frames.
	 *
	 * @throws RTSPException
	 *             If there was an error sending or receiving the RTSP data, or
	 *             if the server did not return a successful response.
	 */
	public synchronized void play() throws RTSPException {

        if(this.state != State.READY) {
            Logger.log("Error: Must be in ready state to play. Current state " + this.state);
            throw new RTSPException("Error: Must be in ready state to play. Current state " + this.state);
        }

        String msg = String.format("PLAY %s RTSP/1.0\r\n", this.currentVideo);
        msg += String.format("CSeq: %s\r\n", this.cSeq);
        msg += String.format("Session: %s\r\n", this.sessionId);
        msg += "\r\n";

        this.RTSPConn.send(msg);
        this.cSeq++;

        RTSPResponse response = this.RTSPConn.recv();

        if(response.getResponseCode() != RTSPResponseCode.OK) {
            throw new RTSPException(String.format("%s response code returned.", response.getResponseCode()));
        }

        this.state = State.PLAYING;

        Metrics.init();
        startRTPTimer();
	}

	/**
	 * Starts a timer that reads RTP packets repeatedly. The timer will wait at
	 * least MINIMUM_DELAY_READ_PACKETS_MS after receiving a packet to read the
	 * next one.
	 */
	private void startRTPTimer() {

		rtpTimer = new Timer();
		rtpTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				receiveRTPPacket();
			}
		}, 0, MINIMUM_DELAY_READ_PACKETS_MS);
	}

	/**
	 * Receives a single RTP packet and processes the corresponding frame. The
	 * data received from the datagram socket is assumed to be no larger than
	 * BUFFER_LENGTH bytes. This data is then parsed into a Frame object (using
	 * the parseRTPPacket method) and the method session.processReceivedFrame is
	 * called with the resulting packet. In case of timeout no exception should
	 * be thrown and no frame should be processed.
	 */
	private void receiveRTPPacket() {

        byte[] recvData = new byte[BUFFER_LENGTH];
        DatagramPacket packet = new DatagramPacket(recvData, recvData.length);

        try {
            RTPConn.receive(packet);
        } catch(IOException e) {
            /* return silently */
            return;
        }

        Frame frame = parseRTPPacket(packet.getData(), packet.getLength());
        this.session.processReceivedFrame(frame);
	}

	/**
	 * Sends a PAUSE request to the server. This method is responsible for
	 * sending the request, receiving the response and, in case of a successful
	 * response, cancelling the RTP timer responsible for receiving RTP packets
	 * with frames.
	 *
	 * @throws RTSPException
	 *             If there was an error sending or receiving the RTSP data, or
	 *             if the server did not return a successful response.
	 */
	public synchronized void pause() throws RTSPException {

        if (this.state != State.PLAYING) {
            Logger.log("invalid pause from init state");
            throw new RTSPException("Must be playing a video to pause it. Current state: " + this.state);
        }

        String msg = String.format("PAUSE %s RTSP/1.0\r\n", this.currentVideo);
        msg += String.format("CSeq: %s\r\n", this.cSeq);
        msg += String.format("Session: %s\r\n", this.sessionId);
        msg += "\r\n";

        this.RTSPConn.send(msg);
        this.cSeq++;

        RTSPResponse response = this.RTSPConn.recv();

        if(response.getResponseCode() != RTSPResponseCode.OK) {
            throw new RTSPException(String.format("%s response code returned.", response.getResponseCode()));
        }

        this.state = State.READY;

	}

	/**
	 * Sends a TEARDOWN request to the server. This method is responsible for
	 * sending the request, receiving the response and, in case of a successful
	 * response, closing the RTP socket. This method does not close the RTSP
	 * connection, and a further SETUP in the same connection should be
	 * accepted. Also this method can be called both for a paused and for a
	 * playing stream, so the timer responsible for receiving RTP packets will
	 * also be cancelled.
	 *
	 * @throws RTSPException
	 *             If there was an error sending or receiving the RTSP data, or
	 *             if the server did not return a successful response.
	 */
	public synchronized void teardown() throws RTSPException {
        Logger.log("Current state " + this.state);

        if (this.state == State.INIT) {
            Logger.log("Error: Cannot call teardown from current state: " + this.state);
            throw new RTSPException("Error: Cannot call teardown from current state: " + this.state);
        }

		String msg = String.format("TEARDOWN %s RTSP/1.0\r\n", this.currentVideo);
        msg += String.format("CSeq: %s\r\n", this.cSeq);
        msg += String.format("Session: %s\r\n", this.sessionId);
        msg += "\r\n";

        this.RTSPConn.send(msg);
        this.cSeq++;

        RTSPResponse response = this.RTSPConn.recv();

        if(response.getResponseCode() != RTSPResponseCode.OK) {
            throw new RTSPException(String.format("%s response code returned.", response.getResponseCode()));
        }

        this.RTPConn.close();

        this.state = State.INIT;
	}

	/**
	 * Closes the connection with the RTSP server. This method should also close
	 * any open resource associated to this connection, such as the RTP
	 * connection, if it is still open.
	 */
	public synchronized void closeConnection() {

        Logger.log("Closing connection..");

        this.RTSPConn.close();

        if(RTPConn != null && !RTPConn.isClosed())
            RTPConn.close();

	}

	/**
	 * Parses an RTP packet into a Frame object.
	 *
	 * @param packet
	 *            the byte representation of a frame, corresponding to the RTP
	 *            packet.
	 * @return A Frame object.
	 */
	private static Frame parseRTPPacket(byte[] packet, int length) {

        Logger.log("===========================");
        ByteBuffer byteBuffer = ByteBuffer.wrap(packet);

        //TODO: Verify this
        boolean marker = (byteBuffer.get(1) >>> 8) == 0x1;
        Logger.log("Marker " + marker);

        //TODO: Verify this
        byte payloadType = (byte)(byteBuffer.get(1) & 0x7f);

        short sequenceNumber = byteBuffer.getShort(2);
        Logger.log("SeqNum " + sequenceNumber);

        int timestamp = byteBuffer.getInt(4);
        Logger.log("Timestamp " + timestamp);

        Frame frame = new Frame(payloadType, marker, sequenceNumber, timestamp, packet, 12, length);
        Metrics.rcvFrame(frame);

        return frame;
	}

}
