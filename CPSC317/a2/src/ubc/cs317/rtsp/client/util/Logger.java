package ubc.cs317.rtsp.client.util;

/**
 * Created by jtgi on 3/12/14.
 */
public class Logger {
    public static boolean enabled = true;

    private Logger() { /* empty */ }

    public static void log(Object o) {
        if(enabled) System.out.println(o.toString());
    }
}
