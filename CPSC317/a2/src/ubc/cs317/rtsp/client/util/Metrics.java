package ubc.cs317.rtsp.client.util;

import ubc.cs317.rtsp.client.model.Frame;

import java.util.Date;

/**
 * Static methods for gathering metrics.
 */
public class Metrics {

    private static int framesReceived = 0;
    private static float frameRate = 0;

    private static int lossCount = 0;
    private static float lossRate = 0;

    private static int outOfOrderCount = 0;
    private static float outOfOrderRate = 0;

    private static float expectedDataRate = 0;

    private static int lastSeqNumReceived = -1;

    private static long start;

    public static void init() {
     framesReceived = 0;
     frameRate = 0;
     lossCount = 0;
     lossRate = 0;
     outOfOrderCount = 0;
     outOfOrderRate = 0;
     expectedDataRate = 0;
     lastSeqNumReceived = -1;
     start = System.currentTimeMillis();
    }

    public static long timeElapsed() {
        return System.currentTimeMillis() - start;
    }

    /**
     * Out of order --
     * The following acknowledges out of order packets as any packet number
     * that is lower than what is expected next. If a packet that is greater
     * than what is expected is received we assume any packet previous to it has
     * been misplaced. If those packets arrive later, they are considered out of order.
     *
     * Lost packets --
     * If a packet is received and it is greater than what is expected, it is assumed
     * all packets in between have been lost. In the case where they arrive later they
     * are still considered lost.
     *
     * EX:
     * [ 1, 3, 2, 4, 5 ], 2 out of order.
     * [ 1, 6, 2, 3, 4, 5 ], (2, 3, 4, 5) are out of order.
     */
    public static void rcvFrame(Frame f) {
        framesReceived++;

        int expectedSeqNum = lastSeqNumReceived + 1;
        int seqNum = f.getSequenceNumber();

        Logger.log("Expected SeqNum " + expectedSeqNum);
        Logger.log("actual SeqNum " + seqNum);

        if(seqNum == expectedSeqNum) {
            lastSeqNumReceived++;
        } else if(seqNum <= lastSeqNumReceived) {
            outOfOrderCount++;
        } else if(seqNum > lastSeqNumReceived) {
            lossCount += (seqNum - lastSeqNumReceived);
            lastSeqNumReceived = seqNum;
        }

        if(seqNum == 0 || f.getTimestamp() == 0) return;

        frameRate = (framesReceived * 1000 / timeElapsed());
        lossRate = (lossCount * 1000 / timeElapsed());
        outOfOrderRate = (outOfOrderCount * 1000 / timeElapsed());
        expectedDataRate = ((expectedSeqNum * 1000) / timeElapsed());

        System.out.println(String.format("Frame rate: %f", frameRate));
        System.out.println(String.format("Loss rate: %f", lossRate));
        System.out.println(String.format("Out of order rate: %f", outOfOrderRate));

    }
}

