import java.lang.System;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import java.io.*;
import java.util.*;
import java.net.*;


public class CSftp{

	static String DEFAULT_PORT = "21";
	static Socket sock;
	static BufferedReader socketInputStream;
	static OutputStream socketOutputStream;
	static PrintWriter out;

    public static void main(String[] csftp_args) {
        //open("ftp.cs.ubc.ca", DEFAULT_PORT);
        //user ("anonymous");
        boolean flag = true;

        while (true){

            String args[]   = prompt().split("\\s+");
            boolean invalid    = false;
            boolean unexpected = false;


            if(flag){
	            switch (args[0]){
	            	case "open":
	            	case "quit":
	            		flag = false;
	            		break;
	            	case "user":
	            	case "get":
	            	case "put":
	            	case "cd":
	            	case "dir":
	            		unexpected = true;
	            		flag = true;
	            		break;
	            	default:
	            		flag = true;
	            		invalid = true;
	            		break;
	            }
            }

	        if(!flag){
	            switch (args[0]){
	                case "open":
	                	if (args.length == 2) open(args[1], DEFAULT_PORT);
	                    if (args.length == 3) open(args[1], args[2]);
	                    if (args.length < 2 || args.length > 3){
	                    	invalid = true;
	                    	flag = true;
	                    }
	                    break;
	                case "user":
	                    if (args.length == 2) user(args[1]);
	                    else invalid = true;
	                    break;
	                case "quit":
	                	if (out != null){
	                		System.out.println("--> QUIT");
	                		out.println("QUIT");
	                		try {
								String responseLine = socketInputStream.readLine();
								System.out.println("<-- " + responseLine);
							} catch (IOException e) {
								e.printStackTrace();
							}
	                	}
	                    System.exit(0);
	                    break;
	                case "get":
	                    if (args.length == 3) get(args[1], args[2]);
	                    else if (args.length == 2) get(args[1], args[1]);
	                    else invalid = true;
	                    break;
	                case "put":
	                    if (args.length == 3) put(args[1], args[2]);
	                    else invalid = true;
	                    break;
	                case "cd":
	                    if (args.length == 2) cd(args[1]);
	                    else invalid = true;
	                    break;
	                case "dir":
	                    if (args.length == 1) dir();
	                    else invalid = true;
	                    break;
	                default:
	                    invalid = true;
	                    break;
	            }
	        }
	        if (invalid) System.out.println("Invalid Command");
	        if (unexpected) System.out.println("Unexpected command. No connection to a server is currently established.");
        }
    }

    private static String prompt(){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String output = null;
        do {
            System.out.print("CSftp> ");
            try {
                output = br.readLine().trim();
            } catch (IOException ioe) {
                continue;
            }
        } while (output.equals("") || output.startsWith("#"));
        return output;
    }

    private static void open(String server, String port){
        //System.out.println("open " + server + " " + port);
        // Convert port from string to int
        int p = Integer.parseInt(port);
        try {
        	// open connection to server
			sock = new Socket(server, p);
			// initialize socket Input and Output streams
			socketOutputStream = sock.getOutputStream();
			out = new PrintWriter(socketOutputStream, true);
		    socketInputStream = new BufferedReader(new InputStreamReader(sock.getInputStream()));

			String responseLine;
			// get response(s) from server
			while(true){
				responseLine = socketInputStream.readLine();
				System.out.println("<-- " + responseLine);
				if (responseLine.startsWith("220 ")){
					//socketInputStream.close();
					break;
				}
			}

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    private static void user(String username){
        System.out.println("--> USER " + username);
        out.println("USER " + username);

        String responseLine;
        try {
        		responseLine = socketInputStream.readLine();
    			System.out.println("<-- " + responseLine);
    			//if server asks for password, prompt user to input it then send it to the server
    			if(responseLine.startsWith("331")){
    				System.out.print("Password: ");
    				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    		        String output = br.readLine();
                    System.out.println("--> PASS " + output);
    				out.println("PASS " + output);
    				// read response (should only be one line I'd hope)
    				System.out.println("<-- " + socketInputStream.readLine());
    			}


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }

    private static void get(String remote, String local){
        //System.out.println("get " + remote + " " + local);
        System.out.println("--> PASV");
        out.println("PASV ");
        try {
            String pasvResp = socketInputStream.readLine();
            System.out.println("<-- "+ pasvResp);

            if (!pasvResp.startsWith("227")){
                return;
            }
            Integer[] pasvData = new Integer[6];

            Integer i = 0;
            for (String s : pasvResp.substring(27,pasvResp.length()-1).split(",")){
                pasvData[i] = Integer.parseInt(s);
                //System.out.println(pasvData[i]);
                i++;
            }

            // open connection to server
            String pasvHost = pasvData[0] + "." + pasvData[1] + "." + pasvData[2] + "." + pasvData[3];

            Integer pasvSoc = pasvData[4] * 256 + pasvData[5];

            // System.out.println(pasvResp);
            // System.out.println(pasvHost + "\t" + pasvSoc);

            Socket pasvSocket = new Socket( pasvHost, pasvSoc);
            OutputStream pasvOutStream = pasvSocket.getOutputStream();
            PrintWriter pasvOut = new PrintWriter(pasvOutStream, true);

            BufferedInputStream fileIn = new BufferedInputStream(pasvSocket.getInputStream());

            System.out.println("--> RETR " + remote);
            out.println("RETR " + remote);

            String responseLine;
            responseLine = socketInputStream.readLine();
            System.out.println("<-- " + responseLine);

            if (responseLine.startsWith("150")){
                responseLine = responseLine.substring(responseLine.indexOf("(") + 1, responseLine.indexOf(" bytes"));
                //System.out.println(responseLine);
                // for buffered input stream so we know exact size of buffer to create
                Integer size = Integer.parseInt(responseLine);


                int x = 0;
                FileOutputStream fileOut = new FileOutputStream(local);
                /*while(true) {
                    x =fileIn.read();
                    if (x==-1) break;
                    fileOut.write(x);
                }*/

                byte[] buf = new byte[1024];

                int offset = 0;
                while (size - offset > 1024) {
                    fileIn.read(buf, 0, 1024);
                    fileOut.write(buf);
                    offset += 1024;
                }

                fileIn.read(buf, 0, size- offset );
                fileOut.write(buf, 0, size - offset);

                System.out.println(size);

                fileOut.close();


                responseLine = socketInputStream.readLine();
                System.out.println("<-- " + responseLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void put(String local, String remote){
        System.out.println("put " + local + " " + remote);


        out.println("PASV ");
        try {
            String pasvResp = socketInputStream.readLine();

            if (!pasvResp.startsWith("227")){
                return;
            }
            Integer[] pasvData = new Integer[6];

            Integer i = 0;
            for (String s : pasvResp.substring(27,pasvResp.length()-1).split(",")){
                pasvData[i] = Integer.parseInt(s);
                //System.out.println(pasvData[i]);
                i++;
            }

            // open connection to server
            String pasvHost = pasvData[0] + "." + pasvData[1] + "." + pasvData[2] + "." + pasvData[3];

            Integer pasvSoc = pasvData[4] * 256 + pasvData[5];

            System.out.println(pasvResp);
            System.out.println(pasvHost + "\t" + pasvSoc);

            Socket pasvSocket = new Socket( pasvHost, pasvSoc);
            OutputStream pasvOutStream = pasvSocket.getOutputStream();
            PrintWriter pasvOut = new PrintWriter(pasvOutStream, true);

            System.out.println("--> STOR " + local + " IN " + remote);
            out.println("STOR " + local + " " + remote);

            String responseLine;
            responseLine = socketInputStream.readLine();
            System.out.println("<-- " + responseLine);


            int y = 0;

            File f = new File(local);
            if (f.isFile()){
            	if(responseLine.startsWith("150")){
    	            FileInputStream fis = new FileInputStream(local);
    	            int x = 0;
    	            while(true){
    	            	x = fis.read();
    	            		if (x == -1) break;
    	            	pasvOut.write(x);
    	            	y++;
    	            }
    	            System.out.println("Sent " + y + " bytes");

    	            pasvOut.flush();
    	            pasvOut.close();

    	            responseLine = socketInputStream.readLine();
    	            // 226 file receive ok
    	            System.out.println("<-- " + responseLine);
                }
            } else
            	System.out.println("Access to local file " + local + " is denied.");


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void cd(String directory){
        System.out.println("--> CWD " + directory);
        out.println("CWD " + directory);

        String responseLine;
        try {
			responseLine = socketInputStream.readLine();
			System.out.println("<-- " + responseLine);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    private static void dir(){
        System.out.println("--> PASV");
        out.println("PASV ");
        try {
            String pasvResp = socketInputStream.readLine();
            System.out.println("<-- " + pasvResp);

            if (!pasvResp.startsWith("227")){
                return;
            }
            Integer[] pasvData = new Integer[6];

            Integer i = 0;
            for (String s : pasvResp.substring(27,pasvResp.length()-1).split(",")){
                pasvData[i] = Integer.parseInt(s);
                //System.out.println(pasvData[i]);
                i++;
            }

            // open connection to server
            String pasvHost = pasvData[0] + "." + pasvData[1] + "." + pasvData[2] + "." + pasvData[3];

            Integer pasvSoc = pasvData[4] * 256 + pasvData[5];

            //System.out.println(pasvResp);
            //System.out.println(pasvHost + "\t" + pasvSoc);

            Socket pasvSocket = new Socket( pasvHost, pasvSoc);
            OutputStream pasvOutStream = pasvSocket.getOutputStream();
            PrintWriter pasvOut = new PrintWriter(pasvOutStream, true);

            BufferedReader pasvIn = new BufferedReader(new InputStreamReader(pasvSocket.getInputStream()));

            System.out.println("--> LIST");
            out.println("LIST");

            String responseLine;

            responseLine = socketInputStream.readLine();
            System.out.println("<-- " + responseLine);

            while(true) {
                responseLine = pasvIn.readLine();
                if (responseLine == null) break;
                System.out.println(responseLine);
            }

            responseLine = socketInputStream.readLine();
            System.out.println("<-- " +responseLine);


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}


